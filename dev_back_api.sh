#!/bin/bash

function max()
{
    echo "$1
$2" | sort -r -n | head -n 1
}

function min()
{
    echo "$1
$2" | sort -n | head -n 1
}

function detect_changes()
{
    detect-changes -M $1 -m $2 -e "*/target/*" -e "*/LICENSE" \
      -e "*/README.md" -e "*/nb*.xml" -e "*/docker_images/*" \
      ${MANAGER_PATH} ${API_PATH} 2>/dev/null
}

function server_running()
{
    echo $(ps -eaf | grep -v grep | grep java | grep -qe " ${SERVER_PID} " ; echo $?)
}

function stop_server()
{
    while [ ! -f ${API_COMPILED} ] ; do
        # echo "Waiting api $(ls ${API_COMPILED} 2>/dev/null ; echo $?)..."
        sleep 0.5
    done
    echo "Stopping server..."
    if [ ${SERVER_RUNNING} ] ; then
        [ ! -z "${SERVER_PID}" ] && kill -15 ${SERVER_PID} 2>/dev/null 1>&2
        if [ -z "${SERVER_PID}" -o $? -ne 0 ] ; then
            SERVER_PID=`echo $(ps -eaf | grep -v grep | grep java) | cut -d" " -f2`
            [ -z "${SERVER_PID}" ] && echo "Server is not running" && return 0
            echo "Change server PID to ${SERVER_PID}"
            kill -15 ${SERVER_PID} 2>/dev/null 1>&2
        fi
    fi
    tries=0
    MAX_TRIES=15
    while [ $(server_running) -eq 0 -a ${tries} -lt ${MAX_TRIES} ] ; do
        echo "Wait for stop ($[tries+1])..."
        tries=$[tries+1]
        sleep 1
    done
    if [ $(server_running) -eq 0 ] ; then
        echo "Force to stop..."
        kill -9 ${SERVER_PID} 2>/dev/null 1>&2
    fi
    rm -f ${STOP_PID}
    echo "Server stopped"
}

function stop_server_bg()
{
    stop_server &
    echo $! > ${STOP_PID}
    echo "Stop server process [$(cat ${STOP_PID})]"
}

function start_server()
{
    while [ ! -f ${API_COMPILED} -o -f ${STOP_PID} ] ; do
        # echo "Waiting for api $(ls ${API_COMPILED} 2>/dev/null ; echo $?)..."
        sleep 0.5
    done
    echo "Starting server..."
    java ${DEBUG_OPTS} -Dmanager.config.path="${CONFIGS_PATH}/manager/manager.properties" \
      -Dlanguages.config.path="${CONFIGS_PATH}/manager/languages.yml" \
      -Dfront.config.path="${CONFIGS_PATH}/api/front-config.yaml" \
      -Dspring.config.location="file:${CONFIGS_PATH}/api/application.yaml" \
      -jar "${JAR_FILE}" &
    SERVER_PID="$!"
    SERVER_RUNNING=true
    rm -f ${START_PID}
    echo "Server started"
}

function start_server_bg()
{
    start_server &
    echo $! > ${START_PID}
    echo "Start server process [$(cat ${START_PID})]"
}

function mvn_compile_manager()
{
    echo "Compile manager..."
    mvn -B clean install -DskipTests >${MVN_LOG} 2>&1
    if [ $? -ne 0 ] ; then
        echo "Manager compilation failed:"
        cat ${MVN_LOG}
        compile_manager
    else
        echo "Manager compiled"
        touch ${MANAGER_COMPILED}
        rm -f ${MANAGER_PID}
    fi
}

function compile_manager()
{
    cd ${MANAGER_PATH} && mvn_compile_manager &
    echo $! > ${MANAGER_PID}
    echo "Manager compilation process [$(cat ${MANAGER_PID})]"
}

function mvn_compile_api()
{
    while [ ! -f ${MANAGER_COMPILED} ] ; do
        # echo "Waiting for manager $(ls ${MANAGER_COMPILED} 2>/dev/null ; echo $?)..."
        sleep 0.5
    done
    echo "Compile API..."
    mvn -B clean package -DskipTests >${MVN_LOG} 2>&1
    # mvn -B clean package -DskipTests 2>&1 | tee -a ${MVN_LOG}
    if [ $? -ne 0 ] ; then
        echo "API compilations failed:"
        cat ${MVN_LOG}
        compile_api
    else
        echo "API compiled"
        touch ${API_COMPILED}
        rm -f ${API_PID}
    fi
}

function compile_api()
{
    cd ${API_PATH} && mvn_compile_api &
    echo $! > ${API_PID}
    echo "API compilation process [$(cat ${API_PID})]"
}

function check_changes()
{
    local need_compile=false
    local need_compile_manager=false
    local timestamp=$(date +"%s")

    echo "Check for sources changes..."

    while true ; do
        ${need_compile} && local max_a=1 || local max_a=$(max 5 ${MTIME})
        ${need_compile} && local min_a=0 || local min_a=$(min 1 ${MTIME})
        local changes="$(detect_changes ${max_a} ${min_a})"
        local now=$(date +"%s")
        [ ! -z "${changes}" ] && changed=true || changed=false
        local manager_changed=$(echo "${changes}" | grep -Pq "^${MANAGER_PATH}.*$" && echo true || echo false)
        # [ $[now-timestamp] -ge ${MTIME} ] && timeOK=true || timeOK=false
        if ${changed} ; then
            if [ -f ${STOP_PID} ] ; then
                echo "Stop process [$(cat ${STOP_PID})]..."
                kill -9 $(cat ${STOP_PID}) 2>/dev/null 1>&2
                rm -f ${STOP_PID}
            fi
            if [ -f ${START_PID} ] ; then
                echo "Stop process [$(cat ${START_PID})]..."
                kill -9 $(cat ${START_PID}) 2>/dev/null 1>&2
                rm -f ${START_PID}
            fi
            if ${need_compile} ; then
                echo "New changes:
${changes}"
                ${manager_changed} && need_compile_manager=true
                timestamp=$(date +"%s")
                echo "Compile in 1 seconds..."
                sleep 1
            else
                echo "Changes detected:
${changes}"
                need_compile=true && rm -f ${API_COMPILED}
                ${manager_changed} && need_compile_manager=true && rm -f ${MANAGER_COMPILED}
                echo "Compile in 1 seconds..."
                timestamp=$(date +"%s")
                sleep 1
            fi
        else
            # echo "No changes"
            # if ${need_compile} && ${timeOK} ; then
            if ${need_compile} ; then
                if ${need_compile_manager} ; then
                    if [ -f ${MANAGER_PID} ] ; then
                        echo "Stop compilation [$(cat ${MANAGER_PID})]..."
                        kill -9 $(cat ${MANAGER_PID}) 2>/dev/null 1>&2
                        rm -f ${MANAGER_PID}
                    fi
                    compile_manager
                    need_compile_manager=false
                fi
                if [ -f ${API_PID} ] ; then
                    echo "Stop compilation [$(cat ${API_PID})]..."
                    kill -9 $(cat ${API_PID}) 2>/dev/null 1>&2
                    rm -f ${API_PID}
                fi
                compile_api
                stop_server_bg
                start_server_bg
                need_compile=false
                sleep ${MTIME}
            else
                sleep 0.5
            fi
        fi
    done


    exit 0

}

API_PID=/var/run/api
MANAGER_PID=/var/run/manager
API_COMPILED=/dev/shm/api
MANAGER_COMPILED=/dev/shm/manager
MVN_LOG=/tmp/mvn.log
START_PID=/var/run/start_server
STOP_PID=/var/run/stop_server
rm -f ${START_PID} ${STOP_PID} ${MANAGER_PID} ${API_PID}

mkdir -p /dev/shm

if [ ! -r ${MANAGER_PATH}/target ] ; then
    compile_manager
    wait $(cat ${MANAGER_PID})
else
    touch ${MANAGER_COMPILED}
fi

FLAG=/root/running
if [ ! -f ${FLAG} ] ; then
    # Install manager in container local maven repository
    cd ${MANAGER_PATH} && mvn -B dependency:resolve && mvn -B install -DskipTests
    cd ${API_PATH} && mvn -B dependency:resolve
    touch ${FLAG}
fi

if [ ! -r ${API_PATH}/target ] ; then
    compile_api
    wait $(cat ${API_PID})
else
    touch ${API_COMPILED}
fi

if [ -f ${CONFIGS_PATH}/manager/manager.properties ] ; then
    cat ${CONFIGS_PATH}/manager/manager.properties | while read -r line ; do
        export ${line}
    done
fi

SERVER_RUNNING=false
MTIME=${MTIME:-10}
[ "$1" = "--debug" ] && DEBUG=true || DEBUG=false
PARAM2="$2"
DEBUG_PORT=${PARAM2:-7777}

cd ${API_PATH}

JAR_FILE="$(ls target/shaded-*.jar 2>/dev/null)"

${DEBUG} && DEBUG_OPTS="-agentlib:jdwp=transport=dt_socket,server=y,address=${DEBUG_PORT},suspend=n" || DEBUG_OPTS=""

if [ -z "${JAR_FILE}" ] ; then
    echo "ERROR: Cannot find jar file"
    exit 1
else
    start_server_bg
fi

echo "API container is running."
check_changes &

pid="$!"
trap "echo 'Stopping API container'; kill -SIGTERM ${pid} 2>/dev/null; kill -SIGTERM ${SERVER_PID} 2>/dev/null" SIGINT SIGTERM

while kill -0 ${pid} > /dev/null 2>&1; do
    wait
done

stop_server 2>/dev/null

exit 0
