# CodePlatform

Refer to Wiki page: [Home](https://gitlab.com/nemolovich/codeplatform/-/wikis/home)

![Yellow Builders Team](https://nemolovich.gitlab.io/codeplatform/schemas/titan-min.png)

By Yellow Builders team

## License

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)



Proudly developed by the Yellow Builders team :heart:


