import { BrowserModule }                    from '@angular/platform-browser';
import { RouterModule, Routes }             from '@angular/router';
import { NgModule }                         from '@angular/core';
import { AceEditorModule }                  from 'ng2-ace-editor';
import { BrowserAnimationsModule }          from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule, MatButtonModule, MatSelectModule, MatDialogModule,
  MatIconModule, MatSidenavModule, MatListModule, MatMenuModule, MatChipsModule,
  MatTabsModule, MatInputModule, MatProgressSpinnerModule, MatExpansionModule,
  MatGridListModule, MatTableModule, MatSortModule, MatPaginatorModule,
  MatStepperModule, MatSlideToggleModule, MatCheckboxModule}   
                                            from '@angular/material';
import { MarkdownModule }                   from 'angular2-markdown';
import { FileUploadModule }                 from 'ng2-file-upload';
import { SplitPaneModule }                  from 'ng2-split-pane/lib/ng2-split-pane';

import { AdminUserComponent }               from './admin/users/user.component';
import { AdminTeamComponent }               from './admin/teams/team.component';
import { AppComponent }                     from './app.component';
import { AccountComponent }                 from './account/account-edit.component';
import { ApplicantEditComponent }           from './applicant/applicant-edit.component';
import { ApplicantData }                    from './applicant/applicant-info.interface';
import { ApplicantInfo }                    from './applicant/applicant-info.component';
import { ApplicantListComponent }           from './applicant/applicants.component';
import { CourseEditorComponent }            from './course-editor/course-editor.component';
import { EditorComponent }                  from './editor/editor.component';
import { LoginComponent }                   from './login/login.component';
import { HomeComponent }                    from './home/home.component';
import { ChallengeListComponent }           from './challenge-list/challenge-list.component';
import { ExerciseListComponent }            from './exercise-list/exercise-list.component';
import { TeamListComponent }                from './team/team-list.component';
import { TeamEditComponent }                from './team/team-edit.component';
import { ScoreboardComponent }              from './scoreboard/scoreboard.component';
import { GdprComponent }                    from './gdpr/gdpr.component';
import { ConfirmDialog }                    from './dialogs/confirm-dialog.component';
import { LoginModal }                       from './login/login-modal.component'
import { ModalDialog }                      from './dialogs/modal-dialog.component';
import { ShowDialog }                       from './dialogs/show-dialog.component';
import { DynamicDialog }                    from './dialogs/dynamic-dialog.component';

import { ApplicantInfoService }             from './applicant/applicant-info.service';
import { CodeblockRendererService }         from './codeblock/codeblock-renderer.service';
import { DialogsService }                   from './dialogs/dialogs.service';
import { TokenService }                     from './token.service';
import { ParticlesModule }                  from 'angular-particle';
import { StopWatchService }                 from './stopwatch.service';

import { BASE_PATH, COLLECTION_FORMATS }    from './swagger-generated-API/variables';
import { APP_BASE_HREF }                    from '@angular/common';

/*swagger stuff*/ 
import { ApiModule }                        from './swagger-generated-API/api.module';
import { HttpModule }                       from '@angular/http';
import { HttpClientModule }                 from '@angular/common/http';

const appRoutes: Routes = [
  { path: 'scoreboard', component: ScoreboardComponent },
  { path: 'scoreboard/:challengeId', component: ScoreboardComponent },
  { path: 'challenges', component: ChallengeListComponent },
  { path: 'courses', component: ChallengeListComponent },
  { path: 'courses/:challengeId/editor', component: CourseEditorComponent },
  { path: 'challenges/:id', component: ExerciseListComponent },
  { path: 'challenges/:challengeId/:exerciceId/editor', component: EditorComponent },
  { path: 'teams', component: TeamListComponent },
  { path: 'teams/:challengeId', component: TeamListComponent },
  { path: 'teams/:challengeId/edit/:teamId', component: TeamEditComponent },
  { path: 'applicants/create', component: ApplicantEditComponent },
  { path: 'applicants/edit/:applicantId', component: ApplicantEditComponent },
  { path: 'applicants/list', component: ApplicantListComponent },
  { path: 'account', component: AccountComponent },
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'admin/users', component: AdminUserComponent },
  { path: 'admin/users/:login', component: AdminUserComponent },
  { path: 'admin/teams/create/:challengeId/:login', component: AdminTeamComponent },
];

@NgModule({
  declarations: [
    AdminUserComponent,
    AdminTeamComponent,
    AppComponent,
    ApplicantEditComponent,
    ApplicantInfo,
    ApplicantListComponent,
    ConfirmDialog,
    DynamicDialog,
    ShowDialog,
    ModalDialog,
    AccountComponent,
    CourseEditorComponent,
    EditorComponent,
    HomeComponent,
    LoginComponent,
    ChallengeListComponent,
    ExerciseListComponent,
    TeamListComponent,
    TeamEditComponent,
    ScoreboardComponent,
    GdprComponent,
    LoginModal
  ],
  exports: [ConfirmDialog, DynamicDialog, ShowDialog, ModalDialog, ApplicantInfo],
  entryComponents: [ConfirmDialog, LoginModal, DynamicDialog, ShowDialog, ModalDialog, ApplicantInfo, GdprComponent],
  imports: [
    RouterModule.forRoot(
     appRoutes,
     { enableTracing: false }
     ),
    BrowserModule,
    AceEditorModule,
    MatCardModule,
    MatButtonModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatIconModule,
    MatSidenavModule,
    MarkdownModule,
    MatListModule,
    MatMenuModule,
    FileUploadModule,
    SplitPaneModule,
    ParticlesModule,
    MatChipsModule,
    MatTabsModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatGridListModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatStepperModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    ApiModule,
    HttpModule,
    HttpClientModule
  ],
  providers: [
    DialogsService,
    TokenService,
    StopWatchService,
    ApplicantInfoService,
    CodeblockRendererService,
    AppComponent,
    {
      provide: BASE_PATH,
      useValue: 'http://codeplatform:8081/YellowBuilders/CodePlatform/0.1.1'
    },
    {
      provide: APP_BASE_HREF,
      useValue: '/titan/'
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
