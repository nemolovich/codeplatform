import { Observable }                               from 'rxjs/Rx';
import { ConfirmDialog }                            from './confirm-dialog.component';
import { ShowDialog }                               from './show-dialog.component';
import { DynamicDialog }                            from './dynamic-dialog.component';
import { ModalDialog }                              from './modal-dialog.component';
import { LoginModal }                               from '../login/login-modal.component';
import { MatDialogRef, MatDialog, MatDialogConfig } from '@angular/material';
import { Injectable }                               from '@angular/core';
import { UserViewModel }                            from '../swagger-generated-API/model/userViewModel';

@Injectable()
export class DialogsService {

  constructor(private dialog: MatDialog) { }

  public confirm(title: string,
                 message: string,
                 selectFirst: boolean = true,
                 firstButton: string = 'YES',
                 secondButton: string = 'NO'): Observable<boolean> {

    let dialogRef: MatDialogRef<ConfirmDialog>;

    dialogRef = this.dialog.open(ConfirmDialog);
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.message = message;
    dialogRef.componentInstance.firstButtonText = firstButton;
    dialogRef.componentInstance.secondButtonText = secondButton;
    dialogRef.componentInstance.selectFirst = selectFirst;

    return dialogRef.afterClosed();
  }

  public show(title: string,
              message: string,
              buttonText: string = 'OK',
              modal: boolean = false): Observable<boolean> {

    let dialogRef: MatDialogRef<ShowDialog>;

    dialogRef = this.dialog.open(ShowDialog, {
      disableClose: modal
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.message = message;
    dialogRef.componentInstance.buttonText = buttonText;

    return dialogRef.afterClosed();
  }

  public dynamic(title: string,
                 componentClass: any,
                 extraData: any = null,
                 buttonText: string = 'OK'): Observable<boolean> {

    let dialogRef: MatDialogRef<DynamicDialog>;

    dialogRef = this.dialog.open(DynamicDialog, {
      data: { component: componentClass } });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.extraData = extraData;
    dialogRef.componentInstance.buttonText = buttonText;

    return dialogRef.afterClosed();
  }

  public modal(title: string,
               message: string,
               closeAfter: number = 10): Observable<boolean> {

    let dialogRef: MatDialogRef<ModalDialog>;

    dialogRef = this.dialog.open(ModalDialog, { disableClose: true });
    dialogRef.componentInstance.ngOnInit = () => { setTimeout(() => { dialogRef.componentInstance.closeFrame(); }, closeAfter * 1000); };
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.message = message;

    return dialogRef.afterClosed();
  }

  public login(user: UserViewModel,
               autocomplete: boolean = true,
               title: string = "You want to login?",
               modal: boolean = false): Observable<boolean> {

    let dialogRef = this.dialog.open(LoginModal, {
      data: { currentUser: user },
      disableClose: modal
    });

    dialogRef.componentInstance.autocomplete = autocomplete;
    dialogRef.componentInstance.title = title;

    return dialogRef.afterClosed();
  }
}
