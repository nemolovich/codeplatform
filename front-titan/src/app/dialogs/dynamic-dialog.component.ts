import { Component, Inject }                from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA }    from '@angular/material';

@Component({
    selector: 'dynamic-dialog',
    template: `
        <h3>{{title}}</h3>
        <div class="dynamic-dialog">
          <ng-container *ngComponentOutlet="data.component"></ng-container>
        </div>
        <button type="button" mat-raised-button class="right"
            (click)="dialogRef.close(true)">{{buttonText}}</button>
    `,
    styleUrls:["./dialog.css"]
})
export class DynamicDialog {
  
  public title: string;
  public buttonText: string;
  public extraData: any;

  constructor(public dialogRef: MatDialogRef<DynamicDialog>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

}
