import { MatDialogRef } from '@angular/material';
import { Component }    from '@angular/core';

@Component({
  selector: 'modal-dialog',
  template: `
      <h3>{{ title }}</h3>
      <p [innerHTML]="message"></p>
  `,
  styleUrls:["./dialog.css"]
})
export class ModalDialog {

  public title: string;
  public message: string;

  constructor(public dialogRef: MatDialogRef<ModalDialog>) {

  }

  ngOnInit() {
  }

  closeFrame(): void {
    this.dialogRef.close();
  }
}
