import { MatDialogRef }         from '@angular/material';
import { Component, ViewChild } from '@angular/core';

@Component({
    selector: 'confirm-dialog',
    template: `
        <h3>{{ title }}</h3>
        <p [innerHTML]="message"></p>
        <button type="button" mat-button class="mat-button {{selectFirst?'mat-raised-button':''}}"
            #firstButton
            (click)="dialogRef.close(true)">{{firstButtonText}}</button>
        <button type="button" mat-button class="mat-button {{selectFirst?'':'mat-raised-button'}}"
            #secondButton
            (click)="dialogRef.close(false)">{{secondButtonText}}</button>
    `,
    styleUrls:["./dialog.css"]
})
export class ConfirmDialog {

  public title: string;
  public message: string;
  public firstButtonText: string = 'YES';
  public secondButtonText: string = 'NO';
  public selectFirst: boolean = true;
  @ViewChild('firstButton') firstButton;
  @ViewChild('secondButton') secondButton;

  constructor(public dialogRef: MatDialogRef<ConfirmDialog>) {

  }

  ngOnInit() {
    if (this.selectFirst) {
      this.firstButton._elementRef.nativeElement.autofocus = true;
    } else {
      this.secondButton._elementRef.nativeElement.autofocus = true;
    }

    if (this.selectFirst) {
      this.firstButton.focus();
    } else {
      this.secondButton.focus();
    }
  }
}
