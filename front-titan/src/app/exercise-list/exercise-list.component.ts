import { Component }                    from '@angular/core';
import { Exercice }                     from '../swagger-generated-API/model/exercice';
import { OnInit }                       from '@angular/core';
import { ActivatedRoute, ParamMap }     from '@angular/router';
import { ChallengeService }             from '../swagger-generated-API/api/challenge.service';
import { UserAccessActivationService }  from '../swagger-generated-API/api/userAccessActivation.service';
import { ChallengeLightViewModel }      from '../swagger-generated-API/model/challengeLightViewModel';
import { UserViewModel }                from '../swagger-generated-API/model/userViewModel';
import { DialogsService }               from '../dialogs/dialogs.service';

@Component({
  templateUrl: './exercise-list.component.html',
  styleUrls: ['./exercise-list.component.css']
})
export class ExerciseListComponent implements OnInit {
  constructor(private dialogsService: DialogsService,
              private challengeService: ChallengeService,
              private route: ActivatedRoute,
              private userAccessActivationService: UserAccessActivationService) {
  }
  exercices:Exercice[];
  currentChallenge : ChallengeLightViewModel;
  myChallenges : ChallengeLightViewModel[];
  errorMessage: string;
  currentUser: UserViewModel;

  isRegistered() {
    return this.currentChallenge && this.myChallenges.some(c => c.id === this.currentChallenge.id);
  }

  leave() {
    this.dialogsService
      .confirm('Confirm action', 'Do you want to unregister from this challenge?', false)
      .subscribe(res => {
        if (res) {
          console.log('Leave challenge', this.currentChallenge.id);
          // this.challengeService.registerTeam(this.currentChallenge.id).subscribe(
            // challenge => {
            // this.challengeService.getAllChallenges("registered").subscribe(
              // myChallenges => {
                // this.myChallenges = myChallenges;
            // });
          // });
        }
    });
  }

  register() {
    this.dialogsService
      .confirm('Confirm action', 'Do you want to register to this challenge?')
      .subscribe(res => {
        if (res) {
          this.challengeService.registerTeam(this.currentChallenge.id).subscribe(
            challenge => {
            this.challengeService.getAllChallenges("registered").subscribe(
              myChallenges => {
                this.myChallenges = myChallenges;
            });
          });
        }
    });
  }

  ngOnInit() {
    this.userAccessActivationService.isAuthenticated().subscribe(
      data => {
        this.currentUser = data;
      });
      this.route.paramMap
        .switchMap((params: ParamMap) => this.challengeService.getAllExercicesByChallengeId(params.get('id')))
        .subscribe(exercices => {
          this.challengeService.getAllChallenges("registered").subscribe(
            myChallenges => {
              this.myChallenges = myChallenges;
              this.exercices = exercices.sort((a,b) => 
                a.coeff === b.coeff ? a.title.localeCompare(b.title) : a.coeff - b.coeff);
          });
      });
      this.route.paramMap
        .switchMap((params: ParamMap) => this.challengeService.getChallengeByID(params.get('id')))
        .subscribe(challenge => {
          this.currentChallenge = challenge;
        });
  }

}
