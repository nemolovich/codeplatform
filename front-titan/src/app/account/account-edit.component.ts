import { Component, ViewChild }         from '@angular/core';
import { FormControl, Validators, AbstractControl,
         ValidatorFn }                  from '@angular/forms';
import { UserAccessActivationService }  from '../swagger-generated-API/api/userAccessActivation.service';
import { AccountService }               from '../swagger-generated-API/api/account.service';
import { UserService }                  from '../swagger-generated-API/api/user.service';
import { UserViewModel }                from '../swagger-generated-API/model/userViewModel';
import { UserRegisterModel }            from '../swagger-generated-API/model/userRegisterModel';
import { HttpClient, HttpHeaders }      from '@angular/common/http';
import { AppComponent }                 from '../app.component';

const LOGIN_REGEX: RegExp = /^[_'.@A-Za-z0-9-]*$/;

@Component({
  selector: 'account-component',
  templateUrl: './account-edit.component.html',
  styleUrls: ['./account-edit.component.scss']
})
export class AccountComponent {
private currentUser: UserViewModel;
private passwordMessage: string = "";
private infoMessage: string = "";
private errorMessage: string = "";
private passwordField: string;
private passwordConfirm: string;
private isValidImgURL: boolean = true;
private hide1: boolean = true;
private hide2: boolean = true;

private emailFormControl = new FormControl('', [
  Validators.required,
  Validators.minLength(1),
  Validators.maxLength(50),
  Validators.email]);
private loginFormControl = new FormControl('', [
  Validators.required,
  Validators.minLength(5),
  Validators.maxLength(100),
  Validators.pattern(LOGIN_REGEX)]);
private passwordFormControl = new FormControl('', [
  Validators.maxLength(100),
  this.matchPassword(() => this.passwordField)]);

  constructor(private userAccessActivationService: UserAccessActivationService,
              private accountService: AccountService,
              protected httpClient: HttpClient,
              private appComponent: AppComponent) {
    this.userAccessActivationService.isAuthenticated().subscribe(
      user => {
        this.currentUser = user;
      },
      error => {
        this.errorMessage = 'You have to be logged';
    });
  }
  @ViewChild('passwordForm') passwordForm
  @ViewChild('accountForm') accountForm
  ngOnInit() {
  }

  private matchPassword(fun: (() => string)): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
       let password = fun();
       let confirmPassword = control.value;
       return password !== confirmPassword ? {'matchPassword': {value: control.value}} : null;
    };
  }

  checkImgURL() {
    this.httpClient.get<any>(this.currentUser.imageUrl, {
            headers: new HttpHeaders()
    }).subscribe(
      rep => {
        this.isValidImgURL = true;
      },
      error => {
        this.isValidImgURL = error.status && error.status === 200;
      });
  }

  updateAccount() {
    if (this.accountForm.nativeElement.checkValidity()
        // && this.loginFormControl.valid
        && this.emailFormControl.valid) {
      this.infoMessage = "";
      this.passwordMessage = "";
      this.errorMessage = "";
      this.accountService.updateAccount(this.currentUser).subscribe(user => {
        this.currentUser.login = user.login;
        this.currentUser.firstName = user.firstName;
        this.currentUser.lastName = user.lastName;
        this.currentUser.email = user.email;
        this.currentUser.imageUrl = user.imageUrl;
        this.infoMessage = "Account updated";
        this.appComponent.setUser(user);
      });
    } else {
      this.infoMessage = '<b class="error-msg">Invalid form</b>';
    }
  }

  updatePassword() {
    this.infoMessage = "";
    this.passwordMessage = "";
    this.errorMessage = "";
    if (!this.passwordField || this.passwordField.length < 4) {
        this.passwordMessage = '<b class="error-msg">Empty password</b>';
    } else if (this.passwordField !== this.passwordConfirm) {
        this.passwordMessage = '<b class="error-msg">Password verification failed</b>';
    } else if (this.passwordForm.nativeElement.checkValidity()
                && this.passwordFormControl.valid) {
      let passModel: UserRegisterModel = {
        id: this.currentUser.id,
        login: this.currentUser.login,
        email: this.currentUser.email,
        firstName: this.currentUser.firstName,
        lastName: this.currentUser.lastName,
        roles: this.currentUser.roles,
        imageUrl: this.currentUser.imageUrl,
        password: this.passwordField
      };
      this.accountService.changePassword(passModel).subscribe(user => {
        this.passwordField = "";
        this.passwordConfirm = "";
        this.passwordMessage = "Password updated";
      });
    } else {
        this.passwordMessage = '<b class="error-msg">Invalid form</b>';
    }
  }
  

}
