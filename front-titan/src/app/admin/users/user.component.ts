import { Component, ViewChild }         from '@angular/core';
import { FormControl, Validators, AbstractControl,
         ValidatorFn }                  from '@angular/forms';
import { ActivatedRoute }               from '@angular/router';
import { HttpClient, HttpHeaders }      from '@angular/common/http';
import { DialogsService }               from '../../dialogs/dialogs.service';
import { UserAccessActivationService }  from '../../swagger-generated-API/api/userAccessActivation.service';
import { UserService }                  from '../../swagger-generated-API/api/user.service';
import { Roles }                        from '../../swagger-generated-API/model/roles';
import { User }                         from '../../swagger-generated-API/model/user';
import { UserViewModel }                from '../../swagger-generated-API/model/userViewModel';
import { AppComponent }                 from '../../app.component';

const LOGIN_REGEX: RegExp = /^[_'.@A-Za-z0-9-]*$/;

@Component({
  selector: 'admin-user-component',
  templateUrl: './user.component.html',
  styleUrls: ['../../account/account-edit.component.scss']
})
export class AdminUserComponent {
private currentUser: UserViewModel;
private editingUser: User;
private isEditing: boolean = false;
private infoMessage: string = "";
private errorMessage: string = "";
private passwordField: string;
private passwordConfirm: string;
private isValidImgURL: boolean = true;
private hide1: boolean = true;
private hide2: boolean = true;

private emailFormControl = new FormControl('', [
  Validators.required,
  Validators.minLength(1),
  Validators.maxLength(50),
  Validators.email]);
private loginFormControl = new FormControl('', [
  Validators.required,
  Validators.minLength(1),
  Validators.maxLength(50),
  Validators.pattern(LOGIN_REGEX)]);
private passwordFormControl = new FormControl('', [
  Validators.minLength(5),
  Validators.maxLength(60),
  this.matchPassword(() => this.passwordField)]);

  constructor(private userAccessActivationService: UserAccessActivationService,
              private userService: UserService,
              protected httpClient: HttpClient,
              private dialogsService: DialogsService,
              private route: ActivatedRoute,
              private appComponent: AppComponent) {
    this.userAccessActivationService.isAuthenticated().subscribe(
      user => {
        if (!user.roles.includes(Roles.ADMIN)) {
          this.errorMessage = 'You must have admin rights';
        } else {
          this.currentUser = user;
        }
      },
      error => {
        this.errorMessage = 'You have to be logged';
    });
  }
  @ViewChild('accountForm') accountForm
  ngOnInit() {
    this.route.params.subscribe(params => {
      let login: string = params['login'];
      if (login) {
        this.userService.getUserByID(login).subscribe(user => {
          console.log(user);
          this.isEditing = true;
          this.editingUser = user;
        });
      } else {
        this.editingUser = AdminUserComponent.createUser();
        this.passwordField = this.editingUser.password;
        this.passwordConfirm = this.editingUser.password;
      }
    });
  }

  private static createUser(): User {
    return {
      login: '',
      email: '',
      firstName: '',
      lastName: '',
      roles: [Roles.USER],
      imageUrl: null,
      password: AdminUserComponent.generatedPassword(),
    };
  }

  private static generatedPassword(size: number = 7): string {
    let possible: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    return Array.from(Array(7),(x,i) => possible.charAt(Math.floor(Math.random() * possible.length))).join('');
  }

  private matchPassword(fun: (() => string)): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
       let password = fun();
       let confirmPassword = control.value;
       return password !== confirmPassword ? {'matchPassword': {value: control.value}} : null;
    };
  }

  private checkImgURL(): void {
    this.httpClient.get<any>(this.currentUser.imageUrl, {
            headers: new HttpHeaders()
    }).subscribe(
      rep => {
        this.isValidImgURL = true;
      },
      error => {
        this.isValidImgURL = error.status && error.status === 200;
      });
  }

  private updateAccount(): void {
    this.infoMessage = "";
    this.errorMessage = "";
    if (!this.passwordField || this.passwordField.length < 4) {
        this.infoMessage = '<b class="error-msg">Empty password</b>';
    } else if (this.passwordField !== this.passwordConfirm) {
        this.infoMessage = '<b class="error-msg">Password verification failed</b>';
    } else if (this.accountForm.nativeElement.checkValidity()
                && this.passwordFormControl.valid
                && this.loginFormControl.valid
                && this.emailFormControl.valid) {
      this.editingUser.password = this.passwordField;
      let serviceAction: any = this.isEditing 
        ? this.userService.updateUser(this.editingUser)
        : this.userService.addUser(this.editingUser);
      serviceAction.subscribe(user => {
        this.editingUser.id = user.id;
        this.editingUser.login = user.login;
        this.editingUser.firstName = user.firstName;
        this.editingUser.lastName = user.lastName;
        this.editingUser.email = user.email;
        this.editingUser.imageUrl = user.imageUrl;
        this.infoMessage = 'User ' + (this.isEditing ? 'updated' : 'created');
        this.dialogsService.show('User created',
          `User has been created with following informations:
          <table class="user-infos">
            <tr>
              <td>
                Login:
              </td>
              <td>
                <b>${this.editingUser.login}</b>
              </td>
            </tr>
            <tr>
              <td>
                Password:
              </td>
              <td>
                <b>${this.editingUser.password}</b>
              </td>
            </tr>
          </table>
            `, undefined, true).subscribe(x => {
          // this.appComponent.navigate(['/admin','teams','create','devfest2018',this.editingUser.login]);
        });
      });
    } else {
      this.infoMessage = '<b class="error-msg">Invalid form</b>';
    }
  }

  

}
