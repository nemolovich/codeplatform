import { Component }                    from '@angular/core';
import { ActivatedRoute }               from '@angular/router';
import { UserAccessActivationService }  from '../../swagger-generated-API/api/userAccessActivation.service';
import { UserService }                  from '../../swagger-generated-API/api/user.service';
import { TeamService }                  from '../../swagger-generated-API/api/team.service';
import { ChallengeService }             from '../../swagger-generated-API/api/challenge.service';
import { UserViewModel }                from '../../swagger-generated-API/model/userViewModel';
import { TeamViewModel }                from '../../swagger-generated-API/model/teamViewModel';
import { Roles }                        from '../../swagger-generated-API/model/roles';
import { AppComponent }                 from '../../app.component';

const LOGIN_REGEX: RegExp = /^[_'.@A-Za-z0-9-]*$/;

@Component({
  selector: 'admin-team-component',
  templateUrl: './team.component.html'
})
export class AdminTeamComponent {
private currentUser: UserViewModel;
private infoMessage: string = "";
private errorMessage: string = "";
private editingTeam: TeamViewModel;

  constructor(private userAccessActivationService: UserAccessActivationService,
              private userService: UserService,
              private teamService: TeamService,
              private challengeService: ChallengeService,
              private route: ActivatedRoute,
              private appComponent: AppComponent) {
    this.userAccessActivationService.isAuthenticated().subscribe(
      user => {
        if (!user.roles.includes(Roles.ADMIN)) {
          this.errorMessage = 'You must have admin rights';
        } else {
          this.currentUser = user;
        }
      },
      error => {
        this.errorMessage = 'You have to be logged';
    });
  }
  ngOnInit() {
    this.route.params.subscribe(params => {
      let challengeId: string = params['challengeId'];
      let login: string = params['login'];
      if (challengeId && login) {
        this.userService.getUserByID(login).subscribe(user => {
            this.editingTeam = AdminTeamComponent.createTeam(user, this.currentUser);
            this.teamService.addTeam(challengeId, this.editingTeam)
              .subscribe(team => {
                this.appComponent.navigate(['/admin','users']);
            });
        });
      } else {
        this.errorMessage = 'Invalid path parameters';
      }
    });
  }

  private static createTeam(user: UserViewModel, creator: UserViewModel): TeamViewModel {
    return {
      name: user.firstName + ' ' + user.lastName,
      creator: creator,
      members: [user],
    };
  }

}
