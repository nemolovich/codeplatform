import { NgModule, ModuleWithProviders }  from '@angular/core';
import { CommonModule }                   from '@angular/common';
import { HttpModule }                     from '@angular/http';
import { Configuration }                  from './configuration';

import { AccountService }                 from './api/account.service';
import { AdminService }                   from './api/admin.service';
import { ChallengeService }               from './api/challenge.service';
import { ExerciceService }                from './api/exercice.service';
import { RecruitmentService }             from './api/recruitment.service';
import { ResultService }                  from './api/result.service';
import { SubmissionService }              from './api/submission.service';
import { TeamService }                    from './api/team.service';
import { UtilsService }                   from './api/utils.service';
import { UserService }                    from './api/user.service';
import { UserAccessActivationService }    from './api/userAccessActivation.service';

@NgModule({
  imports:      [ CommonModule, HttpModule ],
  declarations: [],
  exports:      [],
  providers:    [ AccountService, AdminService, ChallengeService, ExerciceService, RecruitmentService, ResultService, SubmissionService, TeamService, UtilsService, UserService, UserAccessActivationService ]
})
export class ApiModule {
    public static forConfig(configurationFactory: () => Configuration): ModuleWithProviders {
        return {
            ngModule: ApiModule,
            providers: [ {provide: Configuration, useFactory: configurationFactory}]
        }
    }
}
