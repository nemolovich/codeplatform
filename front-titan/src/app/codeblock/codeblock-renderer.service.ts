import { AfterContentInit, Component,
         ElementRef }                   from '@angular/core';
import { Injectable }                   from '@angular/core';
import 'brace'
import 'brace/ext/static_highlight';
import * as Ace                         from 'brace';
const staticHighlight = Ace.acequire('ace/ext/static_highlight');

@Injectable()
export class CodeblockRendererService {

  constructor() {
  }

  private appendLineBreakAtEnd(code: string): string {
    let ret = code;
    if (ret && (ret.length < 1 || ret[ret.length-1] !== '\n')) {
      ret += '\n';
    }
    return ret;
  }

  public getConsole(codes: { code: string, className: string }[],
                    language: string = 'none', theme: string = 'none',
                    appendEOL: boolean = false): string {
    let content: string = codes.map(c => this.codeRenderer(c.code, language, theme, c.className, appendEOL)).join('\n');
    let themeName: string = theme.replace(/_/g, '-');
    return `<div class="console ace-${themeName}">
              ${content}
            </div>`;
  }

  public codeRenderer(code: string, language: string = 'none',
                      theme: string = 'none', className: string = '',
                      appendEOL: boolean = false): string {
    const codeStr = appendEOL ? this.appendLineBreakAtEnd(code) : code;
    const codeLines: string[] = codeStr.split('\n');
    const gutter: string = this.getAceGutter(codeStr);
    const el = document.createElement('div');
    const pre = document.createElement('pre');
    el.innerHTML = codeStr.replace(/    /g,"&nbsp;&nbsp;&nbsp;&nbsp;")
      .replace(/</g,"&lt;").replace(/>/g,"&gt;");
    staticHighlight(el, {mode: 'ace/mode/' + language, theme: 'ace/theme/' + theme});
    const child = el.children[0];
    pre.innerHTML = child.innerHTML;
    pre.className = child.className + ` lang-${language} highlight`;
    pre.style.height = ((codeLines.length + 1) * 14) + 'px';
    pre.setAttribute('highlight', language);
    let copyButton: string = '';
    if (document.queryCommandSupported('copy')) {
      copyButton = `<div class="copy-pre-content" title="Copy content">
          <i class="material-icons"
             onclick="var range = document.createRange();
                      range.selectNode(this.parentElement.parentElement.lastElementChild);
                      window.getSelection().removeAllRanges();
                      window.getSelection().addRange(range);
                      document.execCommand('copy')">content_copy</i>
        </div>`;
    }
    // const formattedCode = pre.outerHTML.replace(/    /g,"&nbsp;&nbsp;&nbsp;&nbsp;")
      // .replace(/</g,"&lt;").replace(/>/g,"&gt;");
    const formattedCode = pre.outerHTML.replace(/(<span class="ace_invisible ace_invisible_space) ace_invalid(">)·(<\/span>)/g, '$1$2 $3');
    return `${copyButton}
                <div class="ace_gutter">
                  ${gutter}
                </div>
                ${formattedCode}
`;
  }

  public getAceGutter(code: string, appendEOL: boolean = false): string {
    if (!code) {
      return '';
    }
    const codeStr = appendEOL ? this.appendLineBreakAtEnd(code) : code;
    let lines: string[] = this.getAceLines(codeStr);
    let codeLines: string[] = codeStr.split('\n');
    return `<div class="ace_layer ace_gutter-layer ace_folding-enabled"
                   style="margin-top: 0px; height: ${(codeLines.length + 1) * 14}px; width: 40px;">` + lines.join('\n') + `
              </div>`;
  }

  public getAceLines(code: string, appendEOL: boolean = false): string[] {
    const codeStr = appendEOL ? this.appendLineBreakAtEnd(code) : code;
    let lines = codeStr.split('\n').map((e,i) => `              <div class="ace_gutter-cell " style="height: 14px;">${i+1}</div>`);
    lines.push(`              <div class="ace_gutter-cell " style="height: 14px;"></div>`);
    return lines;
  }
}
