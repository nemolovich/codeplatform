import 'brace/ext/language_tools';

import 'brace/theme/ambiance';
import 'brace/theme/chaos';
import 'brace/theme/chrome';
import 'brace/theme/clouds';
import 'brace/theme/clouds_midnight';
import 'brace/theme/cobalt';
import 'brace/theme/crimson_editor';
import 'brace/theme/dawn';
import 'brace/theme/dracula';
import 'brace/theme/dreamweaver';
import 'brace/theme/eclipse';
import 'brace/theme/github';
import 'brace/theme/gob';
import 'brace/theme/gruvbox';
import 'brace/theme/idle_fingers';
import 'brace/theme/iplastic';
import 'brace/theme/katzenmilch';
import 'brace/theme/kr_theme';
import 'brace/theme/kuroir';
import 'brace/theme/merbivore';
import 'brace/theme/merbivore_soft';
import 'brace/theme/mono_industrial';
import 'brace/theme/monokai';
import 'brace/theme/pastel_on_dark';
import 'brace/theme/solarized_dark';
import 'brace/theme/solarized_light';
import 'brace/theme/sqlserver';
import 'brace/theme/terminal';
import 'brace/theme/textmate';
import 'brace/theme/tomorrow';
import 'brace/theme/tomorrow_night_blue';
import 'brace/theme/tomorrow_night_bright';
import 'brace/theme/tomorrow_night_eighties';
import 'brace/theme/tomorrow_night';
import 'brace/theme/twilight';
import 'brace/theme/vibrant_ink';
import 'brace/theme/xcode';
import './space_theme';

import 'brace/mode/java';
import 'brace/mode/javascript';
import 'brace/mode/c_cpp';
import 'brace/mode/csharp';
import 'brace/mode/sh';
import 'brace/mode/python';
import 'brace/mode/php';
import 'brace/mode/ruby';
import 'brace/mode/golang';
import 'brace/mode/perl';
import 'brace/mode/lua';
import 'brace/mode/scala';
import 'brace/mode/sql';

import 'brace/snippets/java';
import 'brace/snippets/text';
import 'brace/snippets/javascript';
import 'brace/snippets/csharp';
import 'brace/snippets/c_cpp';
import 'brace/snippets/sh';
import 'brace/snippets/python';
import 'brace/snippets/php';
import 'brace/snippets/ruby';
import 'brace/snippets/golang';
import 'brace/snippets/perl';
import 'brace/snippets/lua';
import 'brace/snippets/scala';
import 'brace/snippets/sql';
