import { Component, OnInit, ViewChild } from '@angular/core';
import { DialogsService}                from '../dialogs/dialogs.service';
import { MarkdownService }              from 'angular2-markdown';
import { Applicant }                    from '../swagger-generated-API/model/applicant';
import { Exercice }                     from '../swagger-generated-API/model/exercice';
import { ExerciceType }                 from '../swagger-generated-API/model/exerciceType';
import { Language }                     from '../swagger-generated-API/model/language';
import { TeamViewModel }                from '../swagger-generated-API/model/teamViewModel';
import { UserViewModel }                from '../swagger-generated-API/model/userViewModel';
import { TestCase }                     from '../swagger-generated-API/model/testCase';
import { ResultViewModel }              from '../swagger-generated-API/model/resultViewModel';
import { SubmissionViewModel }          from '../swagger-generated-API/model/submissionViewModel';
import { RecruitmentService }           from '../swagger-generated-API/api/recruitment.service';
import { ChallengeService }             from '../swagger-generated-API/api/challenge.service';
import { ExerciceService }              from '../swagger-generated-API/api/exercice.service';
import { SubmissionRequest }            from '../swagger-generated-API/model/submissionRequest';
import { SubmissionResponse }           from '../swagger-generated-API/model/submissionResponse';
import { SubmissionType }               from '../swagger-generated-API/model/submissionType';
import { SubmissionService }            from '../swagger-generated-API/api/submission.service';
import { UserAccessActivationService }  from '../swagger-generated-API/api/userAccessActivation.service';
import { CodeblockRendererService }     from '../codeblock/codeblock-renderer.service';
import { AppComponent, ThemeDef, EMOJI_NAMES,
         LanguageDef, RERUITER_START_TEST }
                                        from '../app.component';
import { TokenService }                 from '../token.service';
import { DefaultStarters }              from './default-starters';

import { ActivatedRoute, ParamMap }     from '@angular/router';
import { FileUploader }                 from 'ng2-file-upload';
import { SplitPaneModule }              from 'ng2-split-pane';
import 'rxjs/add/operator/switchMap';
import './brace-imports';
declare let ace: any;

const URL = '/api/';
const CODE_PATTERN_REGEX = new RegExp('<code(?: class=".*")?>((?!</code>)(?:.|\n)*)</code>', 'gim');
const EMOJI_NAMES_REGEX  = new RegExp(':(' + EMOJI_NAMES.join('|') + '):', 'gi');
const START_WAITING_TIME: number = 10;
export const MAX_SUBMISSION_TIME: number = 2147483647;
export const MAX_DATE_TIME: number = Number.MAX_SAFE_INTEGER / 1000000;

@Component({
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {

  private applicant : Applicant = null;
  private currentUser : UserViewModel = null;
  private bestScore : number = null;
  private savedCode: { [id: string] : string; } = {};
  private codeStarters: { [id: string] : string; };
  private text: string;
  private resizeEvent: number;
  private options: any = {
    // maxLines: 1000,
    printMargin: true,
    enableBasicAutocompletion: true,
    enableSnippets: true,
    enableLiveAutocompletion: true,
    useWorker: true,
  };
  private selectedTheme: string;
  private themes: ThemeDef[];
  private langs: LanguageDef[];
  private selectedLang: string;
  private selectedEditorLang: string;
  private errorMessage: string = null;
  private clipboard: string = "";
  private currentExercice: Exercice;
  private exercicesList: Exercice[] = [];
  private navigateExercice: Exercice;
  private originExercice: Exercice;
  private exerciceInstructions: string;
  private testCases: TestCase[];
  private bestCodes: { [id: string] : string; } = {};
  private team: TeamViewModel;
  private exerciceResult: ResultViewModel;
  private countdownMode: boolean = false;
  private results: SubmissionResponse[];
  public uploader: FileUploader = new FileUploader({url: URL});
  public hasBaseDropZoneOver: boolean = false;
  private processing: boolean = true;
  private submitted: boolean = false;
  private currentChallengeId: string;
  private selectedResultsTabIndex: number;
  private expandPanels : { [id: string] : boolean } = {
    "instruction": false,
    "editor": false,
    "results": false,
  };
  private isRecruiterMode: boolean = false;

  constructor (
    private dialogsService: DialogsService,
    private challengeService: ChallengeService,
    private exerciceService: ExerciceService,
    private route: ActivatedRoute,
    private submissionService: SubmissionService,
    private userAccessActivationService: UserAccessActivationService,
    private tokenService: TokenService,
    private markdownService: MarkdownService,
    private recruitmentService: RecruitmentService,
    private codeblockRendererService: CodeblockRendererService,
    private appComponent: AppComponent) {
      this.appComponent.onReady(() => {
        this.appComponent.onUnload(() => {
          this.updateTestTimeAsync();
        });
        this.langs = this.appComponent.getLangs();
        this.themes = this.appComponent.getThemes();
        this.selectedLang = this.appComponent.getCurrentLanguage();
        this.selectedTheme = this.appComponent.getCurrentTheme();
        this.selectedEditorLang = this.appComponent.getSelectedLang(this.selectedLang).realName;
        this.appComponent.onThemeChange((theme) => { 
          this.selectedTheme = theme;
          this.onThemeChange(false);
        });
        this.appComponent.onLanguageChange((language) => {
          this.selectedLang = language;
          this.appComponent.getSelectedLang(this.selectedLang).realName;
          this.clearCode(false);
        });
      });
  }
  @ViewChild('editor') editor;
  ngOnInit() {
    this.appComponent.setLoadingMessage('Loading code starters...');
    this.codeStarters = {};
    for (let cs in DefaultStarters) {
      this.codeStarters[cs] = DefaultStarters[cs];
    };
    this.text = "";
    this.route.params.subscribe(params => {
      this.userAccessActivationService.isAuthenticated().subscribe(
        data => {
          this.bestScore = null;
          this.currentUser = data
          if (!this.currentUser) {
            this.errorMessage = "You must login to access to this page";
            this.processing = false;
            this.appComponent.setLoadingMessage('');
          } else {
            this.currentChallengeId = params['challengeId'];
            let exerciceId = params['exerciceId'];
            this.loadExercice(exerciceId);
          }
        },
        error => {
          if(error.status === 401) {
            this.errorMessage = "You must login to access to editor";
          }
          this.processing = false;
          this.appComponent.setLoadingMessage('');
        });
    });

    this.markdownService.renderer.code = (code: string, language: string) => {
      let lang = this.getSelectedLang(this.selectedLang);
      return this.codeblockRendererService.getConsole([{code:code, className:''}], lang.realName, this.selectedTheme, true);
    };

    this.markdownService.renderer.codespan = (content: string) => {
      let [, lang, code] = content.match(/^(\w+)?\\n((?:\\n|.)*)$/m) || [undefined, undefined, undefined];
      let clazz = lang ? ' class="' + lang +'"' : '';
      let codeStr = code ? code.replace(/\\n/g, '\n') : content;
      return '<code' + clazz + '>' + codeStr + '</code>';
    }

    this.markdownService.renderer.tablecell = (cell: string, flags: any) => {
      let lang = this.getSelectedLang(this.selectedLang);
      let cellContent = cell;
      let clazz = '';
      let codes = cellContent.match(CODE_PATTERN_REGEX) || [];
      if (codes.length > 0) {
        clazz = ' class="code-cell"';
        for (let code of codes) {
          let txtCode = code.replace(CODE_PATTERN_REGEX, '$1').replace(/\\n/g, '\n');
          cellContent = cellContent.replace(code, this.codeblockRendererService.getConsole([{code:txtCode, className:''}], lang.realName, this.selectedTheme, false));
        }
      }

      let type = flags.header ? 'th' : 'td';
      let style = flags.align ? ' style="text-align:' + flags.align + ';"' : '';
      return '<' + type + clazz + style + '>' + cellContent + '</' + type + '>\n';
    };
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    if (this.exerciceResult) {
      this.updateTestTimeAsync();
    }
  }

  private getLoadingMessage(): string {
    return this.appComponent.getLoadingMessage();
  }

  private loadExercice(exerciceId: string): void {
    this.appComponent.setLoadingMessage('Load exercices...');
    this.countdownMode = false;
    // Retrieve all exercices of current challenge
    this.challengeService.getAllExercicesByChallengeId(this.currentChallengeId)
      .subscribe(exercices => {
      // Store exercices list
      this.exercicesList = exercices.sort((a,b) => a.coeff === b.coeff ? a.title.localeCompare(b.title) : a.coeff - b.coeff);
      // Set current select exercice option
      this.navigateExercice = this.exercicesList.find(e => e.id === exerciceId);
      // Set the exercice of this page
      this.originExercice = this.navigateExercice;

      this.appComponent.setLoadingMessage('Load current exercice...');
      // Retrieve complete exercice from ID
      this.challengeService.getExerciceByChallengeId(this.currentChallengeId,
          exerciceId).subscribe(exercice => {
        // Set current exercice
        this.currentExercice = exercice;
        // Set test cases and sort by order and/or name
        this.testCases = this.currentExercice.testCases.sort((a, b) =>
          a.order === b.order ? a.name.localeCompare(b.name) : a.order - b.order);
        this.appComponent.setLoadingMessage('Set current code starters...');
        // Set current code starters for all languages
        for (let cs of this.currentExercice.codeStarters) {
          this.codeStarters[cs.language] = cs.content.replace(/\n+$/, '\n');
        };
        // Load code starter for current exercice if exists
        if (this.codeStarters[this.selectedLang]) {
          this.text = this.codeStarters[this.selectedLang];
        }
        // this.editor._editor.$blockScrolling = Infinity;

        let setEditorConfig: ((lang: string, fun: (() => void)) => void) = (usrLang, fun) => {
          this.appComponent.setLoadingMessage('Set editor config...');
          this.selectedEditorLang = this.appComponent.getSelectedLang(usrLang).realName;
          if (this.codeStarters[usrLang]) {
            this.text = this.codeStarters[usrLang];
          }

          // Load exercice instructions
          this.appComponent.setLoadingMessage('Load exercice instructions...');
          this.exerciceInstructions = this.getExerciceInstructions(this.currentExercice) + "\n";
          setTimeout(() => {
            this.exerciceInstructions = this.getExerciceInstructions(this.currentExercice)
          }, 1000);

          this.appComponent.setLoadingMessage('Load user and team information...');
          // Retrieve current user team for the challenge
          this.challengeService.getTeamsByChallengeId(this.currentChallengeId, "true")
              .subscribe(teams => {
            // Set the current team
            this.team = teams[0];

            // Retrieve results of current team on this challenge
            this.exerciceService.getResultsByExerciceId(this.currentChallengeId,
                this.currentExercice.id, this.team.name).subscribe(results => {

              this.appComponent.setLoadingMessage('Get results for current team...');
              // If there were already submissions then check if it's current user
              // submission to load code
              if (results && results.length > 0) {
                // Get submissions for first result (there is only one by team)
                this.exerciceResult = results[0];

                // Load timer for FASTEST exercice
                let timerFun: (() => void) = () => {
                  this.appComponent.pauseTimer();
                };
                let startDate: number = AppComponent.getTimestampFromEpoch(this.exerciceResult.startDate);
                let endDate: number = this.exerciceResult.endDate ?
                      AppComponent.getTimestampFromEpoch(this.exerciceResult.endDate) : 0;
                let diff: number = endDate > 0 ? endDate - startDate : 0;
                let submissionTime: number = 0;

                // Load submissions
                let submissions: SubmissionViewModel[] = this.exerciceResult.submissions;
                if (submissions && submissions.length > 0) {
                  this.appComponent.setLoadingMessage('Load best score...');
                  // Sort submissions by best score
                  let sortedSubmissions: SubmissionViewModel[] = submissions.sort((a,b) =>
                      b.score === a.score
                        ? b.time - a.time
                        : b.score - a.score);
                  let submission: SubmissionViewModel = sortedSubmissions[0];
                  if (submission.time < MAX_SUBMISSION_TIME) {
                    submissionTime = submission.time;
                  }
                  // Sort submissions by best score
                  this.bestScore = submission.score < 0 ? null : submission.score;
                  // For each language loop over submissions to check if user submitted
                  for (let lang of this.langs) {
                    // Get code for user and current language and sort by score
                    // and submission date to get latest best code
                    let code = sortedSubmissions.filter(s => "" + s.language == lang.language
                      && s.user.login === this.currentUser.login);
                    // If there is code for previous constraints then store the best
                    // code for this language
                    if (code && code[0] && code[0].files && code[0].files[0] && code[0].files[0].content) {
                      this.appComponent.setLoadingMessage('Load best code...');
                      this.bestCodes[lang.language] = atob(code[0].files[0].content);
                    }
                  }
                  // If the language is the current language then load code into editor
                  if (this.bestCodes[usrLang]) {
                    // Reset the undo list
                    this.editor.getEditor().getSession().setUndoManager(new ace.UndoManager());
                    this.text = this.bestCodes[usrLang];
                  }
                }

                diff = Math.max(submissionTime, diff);
                let time: number = MAX_DATE_TIME;
                if (!this.isRecruiterMode && this.currentExercice.maxTime
                      && this.currentExercice.maxTime > 0 && this.currentExercice.type === ExerciceType.Fastest) {
                  this.countdownMode = true;
                  time = (this.currentExercice.maxTime * 1000 - diff) / 1000 + 1;
                  timerFun = () => {
                    this.processing = true;
                    this.dialogsService.show('Time is over :(',
                      `The test is over<br/>
                      Your score: <b>${this.bestScore ? this.bestScore * 100 : '0'} %</b>
                      `).subscribe(x => {
                          this.appComponent.pauseTimer();
                          this.processing = false;
                          this.appComponent.navigate(['/']);
                          // this.appComponent.navigate(['/scoreboard','devfest2018']);
                      });
                  };
                }

                this.appComponent.setTimerDisplay(this.countdownMode);
                this.appComponent.startTimer(Math.max(time, 1), timerFun);
                this.appComponent.pauseTimer();
                if (this.countdownMode) {
                  this.appComponent.setTimerRemainingTime(time);
                } else {
                  this.appComponent.setTimerRemainingTime(MAX_DATE_TIME - (diff / 1000));
                }

              }
              // Reset the undo list
              this.editor.getEditor().getSession().setUndoManager(new ace.UndoManager());
              this.editor.getEditor().getSession().setOption("useWorker", true);

              // Start timer
              this.appComponent.unpauseTimer();

              this.appComponent.setLoadingMessage('');
              this.processing = false;
              if (fun) {
                fun();
              }
            });
          });
        };

        this.isRecruiterMode = this.appComponent.isRecruiterMode();
        if (this.isRecruiterMode) {
          this.appComponent.setLoadingMessage('Get applicant...');
          this.recruitmentService.getApplicantByChallengeID(
            this.currentChallengeId).subscribe(applicant => {
              this.selectedLang = '' + applicant.language;
              this.applicant = {
                ...applicant,
                challenge: {
                  id: applicant.challenge.id,
                  name: applicant.challenge.name,
                  description: applicant.challenge.description,
                  scope: applicant.challenge.scope,
                  startDate: applicant.challenge.startDate,
                  endDate: applicant.challenge.endDate,
                  teams: [],
                  results: [],
                  exercices: []
                }
              };
              setEditorConfig(this.selectedLang, () => {
                this.appComponent.pauseTimer();
                this.appComponent.setTimerRemainingTime(0.0001);
                this.appComponent.resetTimer();
                this.appComponent.setTimerDisplay(true);

                this.appComponent.setLoadingMessage('Load timer...');

                let diff: number = 0;
                if (this.exerciceResult) {
                  let startDate: number = AppComponent.getTimestampFromEpoch(this.exerciceResult.startDate);
                  let endDate: number = this.exerciceResult.endDate ?
                        AppComponent.getTimestampFromEpoch(this.exerciceResult.endDate) : 0;
                  diff = (endDate > 0 ? endDate - startDate : 0) / 1000;
                  console.log(startDate);
                  console.log(endDate);
                }

                let chanllengeTime: number = (AppComponent.getTimestampFromEpoch(this.applicant.challenge.endDate) - 
                    AppComponent.getTimestampFromEpoch(this.applicant.challenge.startDate)) / 1000;
                let time: number = Math.max((chanllengeTime - diff), 15);
                this.appComponent.onStopTimer(() => {
                  this.appComponent.pauseTimer();
                  this.dialogsService.confirm('Leave test?', 'Do you really want to leave the test?', false)
                    .subscribe((res) => {
                      if (res) {
                        this.appComponent.confirmAuthent(
                          (data) => {
                            this.appComponent.resetTimer();
                            this.appComponent.navigate(['/']);
                          },
                          (error) => {
                            console.error(error);
                            this.appComponent.unpauseTimer();
                          }
                        );
                      } else {
                        this.appComponent.unpauseTimer();
                      }
                  });
                });

                let challengeStart: string = localStorage.getItem(RERUITER_START_TEST);
                let createRedirect: boolean = challengeStart === this.applicant.challenge.id;

                let startTestFun: (() => void) = () => {
                  localStorage.removeItem(RERUITER_START_TEST);
                  this.dialogsService.modal('Start test',
                    'Test will start in ' + START_WAITING_TIME + ' seconds ',
                    START_WAITING_TIME).subscribe(nothing => {
                      this.appComponent.startTimer(chanllengeTime, () => {
                        this.processing = true;
                        this.dialogsService.show('Test terminated',
                          `The test is over<br/>
                          Your score: <b>${this.bestScore ? this.bestScore * 100 : '0'} %</b>
                          `).subscribe(x => {
                            this.appComponent.confirmAuthent(
                              (data) => {
                                this.appComponent.resetTimer();
                                this.processing = false;
                                this.appComponent.navigate(['/']);
                              },
                              (error) => {
                                console.error(error);
                                this.processing = false;
                                this.appComponent.logout();
                              }
                            );
                        });
                      });
                      this.appComponent.pauseTimer();
                      this.appComponent.setTimerRemainingTime(time);
                      this.appComponent.unpauseTimer();
                  });
                };

                this.appComponent.setTimerRemainingTime(time);
                if (createRedirect) {
                  startTestFun();
                } else {
                  this.appComponent.confirmAuthent(
                    (data) => {
                      startTestFun();
                    },
                    (error) => {
                      console.error(error);
                    }
                  );
                }
              });
            });
        }

        setEditorConfig(this.selectedLang, () => {});

      });
    });
  }

  private switchExercice(): void {
    this.dialogsService
      .confirm('Confirm action', 'Do you want to move on to another exercise?', false)
      .subscribe(res => {
        if (res) {
          this.updateTestTimeAsync();
          this.processing = true;
          this.appComponent.setLoadingMessage('Load another exercice...');
          this.codeStarters = {};
          for (let cs in DefaultStarters) {
            this.codeStarters[cs] = DefaultStarters[cs];
          };
          this.selectedResultsTabIndex = 0;
          this.results = [];
          this.bestScore = null;
          this.bestCodes = {};
          this.savedCode = {};
          this.appComponent.navigate(['/challenges', this.currentChallengeId, this.navigateExercice.id, 'editor']);
          this.text = "";
          this.loadExercice(this.navigateExercice.id);
        } else {
          this.navigateExercice = this.originExercice;
        }
      });
  }

  private getTimeRemaining(): string {
    return this.appComponent.getTimeRemaining();
  }

  private fileOverBase(e:any): void {
    this.hasBaseDropZoneOver = e;
  }

  private getSelectedLang(key: string) {
    return this.appComponent.getSelectedLang(key);
  }

  private onThemeChange(parentChange: boolean = true) {
    this.exerciceInstructions = this.getExerciceInstructions(this.currentExercice) + "\n";
    setTimeout(() => {
      this.exerciceInstructions = this.getExerciceInstructions(this.currentExercice)
    }, 1000);
    if (parentChange) {
      this.appComponent.changeTheme(this.selectedTheme);
    }
  }

  private getAceSelectedTheme(): string {
    return this.selectedTheme.replace(/_/g, '-');
  }

  private changeIndex(): void {
  }

  private getAceGutter(code: string): string {
    return this.codeblockRendererService.getAceGutter(code);
  }

  private getAceLines(code: string): string[] {
    return this.codeblockRendererService.getAceLines(code);
  }

  private getResult(testCase: TestCase): SubmissionResponse {
    return this.results && this.results.length > 0 
      ? this.results.find(r => r.resultName === testCase.name)
      : null;
  }

  private onChange(code): void {
    let lang = this.getSelectedLang(this.selectedLang);
    this.savedCode[lang.language] = code;
  }

  private isFullScreen(): boolean {
    return Object.keys(this.expandPanels).some(e => this.expandPanels[e]);
  }

  private getFullScreenPanel(): string {
    return Object.keys(this.expandPanels).find(e => this.expandPanels[e]);
  }

  private expand(component): void {
    let collapse : boolean = false;
    if (this.expandPanels[component]) {
      collapse = true;
    }
    Object.keys(this.expandPanels).forEach(e => this.expandPanels[e] = false);
    this.expandPanels[component] = !collapse;
    this.resizeEditor();
  }

  private resizeEditor(n: number = 15): void {
    clearTimeout(this.resizeEvent);
    this.resizeEvent = setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
      if (n > 0) {
        this.resizeEditor(n - 1);
      }
    }, 100);
  }

  private resetCode(): void {
    let lang = this.getSelectedLang(this.selectedLang);
    this.dialogsService
     .confirm('Confirm action', 'Do you want to reset your code?', false)
     .subscribe(res => {
       if (res) {
          if (!(typeof this.codeStarters[lang.language]==='undefined'
            || this.codeStarters[lang.language]==="")) {
            this.text = this.codeStarters[lang.language];
          } else {
            this.text = "";
          }
       }
   });
  }

  private clearCode(parentChange: boolean = true): void {
    let lang = this.getSelectedLang(this.selectedLang);
    this.selectedEditorLang = lang.realName;
    if (parentChange) {
      this.appComponent.changeLanguage(this.selectedLang);
    }
    this.exerciceInstructions = this.getExerciceInstructions(this.currentExercice) + "\n";
    setTimeout(() => {
      this.exerciceInstructions = this.getExerciceInstructions(this.currentExercice)
    }, 1000);
    if(!(typeof this.savedCode[lang.language]==='undefined'
       || this.savedCode[lang.language]==="")
       && this.savedCode[lang.language] !== this.codeStarters[lang.language]
       && this.savedCode[lang.language] !== this.bestCodes[lang.language]) {
      this.dialogsService
           .confirm('Confirm action', 'Do you want to clear and restore your previous code?')
           .subscribe(res => {
             if (res) {
               this.text = this.savedCode[lang.language];
             }
             else {
                if (!(typeof this.bestCodes[lang.language]==='undefined'
                  || this.bestCodes[lang.language]==="")) {
                  this.text = this.bestCodes[lang.language];
                } else if (!(typeof this.codeStarters[lang.language]==='undefined'
                  || this.codeStarters[lang.language]==="")) {
                  this.text = this.codeStarters[lang.language];
                } else {
                  this.text = "";
                }
             }
           });
    } else{
      if (!(typeof this.bestCodes[lang.language]==='undefined'
        || this.bestCodes[lang.language]==="")) {
        this.text = this.bestCodes[lang.language];
      } else if (!(typeof this.codeStarters[lang.language]==='undefined'
        || this.codeStarters[lang.language]==="")) {
        this.text = this.codeStarters[lang.language];
      } else {
        this.text = "";
      }
    }
  }

  private getTotalScore(): string {
    return this.results && this.results.length > 0
        ? ((this.results.map((o) => o.score).reduce((a,b) => a+b) / this.results.length * 100).toFixed(2) + ' %')
        : 'N/A';
  }

  private getBestScore(): string {
    return this.bestScore != null ? (this.bestScore * 100).toFixed(2) + '%' :'N/A';
  }

  private getExerciceInstructions(exercice): string {
    let inst = "";
    if (/\{\{CODE_HELPER_\d+\}\}/.test(exercice.text)) {
      let lang = this.getSelectedLang(this.selectedLang);
      inst = exercice.text.replace(/(\{\{CODE_HELPER_\d+\}\})/g, lang.language + "\n$1");
      let languageHelpers = exercice.codeHelpers
        .filter(h => h.language + '' === lang.language)
        .sort((a,b) => a.order - b.order);
      for (let i=0; i<languageHelpers.length; i++) {
        inst = inst.replace('{{CODE_HELPER_' + (i+1) + '}}', languageHelpers[i].content.replace(/\n\n+$/, '\n'));
      }
    } else {
      inst = exercice.text;
    }
    inst = inst.replace(/</g, "&lt;").replace(/>/g, "&gt;") + "\n ";
    inst = inst.replace(/(\{\{CODE_HELPER_\d+\}\})/g, "<<NOT AVAILABLE FOR THIS LANGUAGE>>");

    inst = inst.replace(EMOJI_NAMES_REGEX, '<span class="emoji $1">');

    return inst;
  }

  private changeLanguageEnabled(): boolean {
    return this.appComponent.canChangeLanguage();
  }

  private updateTestTimeAsync(): void {
    this.appComponent.resetTimer();
    this.appComponent.pauseTimer();
    this.appComponent.setTimerRemainingTime(0.0001);
    this.appComponent.setTimerDisplay(true);
    if (this.exerciceResult) {
      this.submissionService.updateTestTimeAsync(this.exerciceResult.id,
          Language[this.getSelectedLang(this.selectedLang).language]);
    }
  }

  private submitCode(): void{
    this.submitted = true;
    this.processing = true;
    let lang = this.getSelectedLang(this.selectedLang);
    let files = new Map<string, Blob>();
    let blob = new Blob([this.text], {type: 'text/plain'});
    files.set(lang.mainScript, blob);
    let request : SubmissionRequest = {
      "challenge": this.currentChallengeId,
      "exercice" : this.currentExercice.id,
      "team"     : this.team.id,
      "type"     : SubmissionType.Files,
      "language" : Language[lang.language]
    };
    this.submissionService.submitCode(JSON.stringify(request), files).subscribe(
      res => {
        if (res === undefined || res === null) {
          this.userAccessActivationService.isAuthenticated().subscribe(
            data => {
              this.currentUser = data;
              if (!this.currentUser) {
                this.errorMessage = "You must login to access to this page";
              }
              this.dialogsService
                   .show('Submission error', 'The submission failed')
                   .subscribe(x => { this.processing = false; });
            },
            error => {
              if(error.status === 401){
                this.errorMessage = "You must login to access to editor";
              }
              this.dialogsService
                  .show('Submission error', 'The submission failed')
                  .subscribe(x => { this.processing = false; });
            });
        } else {
          let score = res.score ? res.score : 0;
          this.bestScore = !this.bestScore || score > this.bestScore ? score : this.bestScore;
          this.processing = false;
          this.dialogsService.show('Submission score',
              'Your score is: <b>' + (score * 100).toFixed(2) + ' %</b>')
              .subscribe(x => {
            if (this.isRecruiterMode && score === 1) {
              this.appComponent.pauseTimer();
              this.dialogsService.confirm('Test terminated', 'Do you want to leave the test?', false)
                .subscribe((res) => {
                  if (res) {
                    this.appComponent.confirmAuthent(
                      (data) => {
                        this.appComponent.resetTimer();
                        this.appComponent.navigate(['/']);
                      },
                      (error) => {
                        console.error(error);
                        this.appComponent.unpauseTimer();
                      }
                    );
                  } else {
                    this.appComponent.unpauseTimer();
                  }
              });
            } else if (score === 1) {
              // this.appComponent.navigate(['/scoreboard','devfest2018']);
            }
          });
        }
    });
  }

  private testCode():void {
    this.submitted = true;
    this.processing = true;
    let lang = this.getSelectedLang(this.selectedLang);
    let files = new Map<string, Blob>();
    let blob = new Blob([this.text], {type: 'text/plain'});
    files.set(lang.mainScript, blob);
    let request : SubmissionRequest = {
      "challenge": this.currentChallengeId,
      "exercice" : this.currentExercice.id,
      "team"     : this.team.id,
      "type"     : SubmissionType.Files,
      "language" : Language[lang.language]
    };
    this.results = [];
    this.submissionService.testCode(JSON.stringify(request), files).subscribe(
      res => {
        if (!res) {
          this.userAccessActivationService.isAuthenticated().subscribe(
            data => {
              this.currentUser = data;
              if (!this.currentUser) {
                this.errorMessage = "You must login to access to this page";
              }
              this.dialogsService
                  .show('Submission error', 'The submission failed')
                  .subscribe(x => { this.processing = false; });
            },
            error => {
              if(error.status === 401){
                this.errorMessage = "You must login to access to editor";
              }
              this.dialogsService
                  .show('Submission error', 'The submission failed')
                  .subscribe(x => { this.processing = false; });
            });
        } else {
          this.results = res;
          this.selectedResultsTabIndex = 1;
          this.processing = false;
        }
    });
  }
}
