ace.define("ace/theme/space_theme",["require","exports","module","ace/lib/dom"], function(acequire, exports, module) {

exports.isDark = true;
exports.cssClass = "ace-space-theme";
exports.cssText = ".ace-space-theme .ace_gutter {\
background: #0B181840;\
color: #26C6DA;\
border-right: 0.5px solid #26C6DA88;\
}\
.ace-space-theme .ace_print-margin {\
width: 1px;\
background: #26C6DA33\
}\
.ace-space-theme {\
background-color: #101010BB;\
color: #26C6DA;\
}\
.ace-space-theme > *,\
.ace-space-theme .ace-space-theme {\
    background: transparent;\
}\
.ace-space-theme .ace_cursor {\
border-color: rgba(16, 248, 255, 0.90);\
opacity: 0.4;\
}\
.ace-space-theme .ace_marker-layer .ace_selection {\
background: rgba(221, 240, 255, 0.20)\
}\
.ace-space-theme.ace_multiselect .ace_selection.ace_start {\
box-shadow: 0 0 3px 0px #141414;\
}\
.ace-space-theme .ace_marker-layer .ace_step {\
background: rgb(16, 128, 0)\
}\
.ace-space-theme .ace_marker-layer .ace_bracket {\
margin: -1px 0 0 -1px;\
border: 1px solid rgba(64, 255, 255, 0.25)\
}\
.ace-space-theme .ace_marker-layer .ace_active-line {\
background: rgba(255, 255, 255, 0.04)\
}\
.ace-space-theme .ace_gutter-active-line {\
background-color: rgba(255, 255, 255, 0.04)\
}\
.ace-space-theme .ace_marker-layer .ace_selected-word {\
border: 1px solid rgba(192, 240, 255, 0.20)\
}\
.ace-space-theme .ace_invisible {\
color: rgba(255, 255, 255, 0.25)\
}\
.ace-space-theme .ace_keyword,\
.ace-space-theme .ace_meta {\
color: #FFFFFF\
}\
.ace-space-theme .ace_constant,\
.ace-space-theme .ace_constant.ace_character,\
.ace-space-theme .ace_constant.ace_character.ace_escape,\
.ace-space-theme .ace_constant.ace_other,\
.ace-space-theme .ace_heading,\
.ace-space-theme .ace_markup.ace_heading,\
.ace-space-theme .ace_support.ace_constant {\
color: #DDDDFF\
}\
.ace-space-theme .ace_invalid.ace_illegal {\
color: #FF0000;\
background-color: rgba(86, 45, 86, 0.75)\
}\
.ace-space-theme .ace_invalid.ace_deprecated {\
text-decoration: underline;\
font-style: italic;\
color: #53D6FE\
}\
.ace-space-theme .ace_support {\
color: #20B0E8\
}\
.ace-space-theme .ace_fold {\
background-color: #26C6DA;\
border-color: #FFFFFF\
}\
.ace-space-theme .ace_support.ace_function {\
color: #E0F7FA\
}\
.ace-space-theme .ace_list,\
.ace-space-theme .ace_markup.ace_list,\
.ace-space-theme .ace_storage {\
color: #00ACC1\
}\
.ace-space-theme .ace_entity.ace_name.ace_function,\
.ace-space-theme .ace_meta.ace_tag,\
.ace-space-theme .ace_variable {\
color: #8C9EFF\
}\
.ace-space-theme .ace_string {\
color: #00E5FF\
}\
.ace-space-theme .ace_string.ace_regexp {\
color: #1DE9B6;\
}\
.ace-space-theme .ace_comment {\
font-style: italic;\
color: #90A4AE;\
}\
.ace-space-theme .ace_variable {\
color: #80DEEA;\
}\
.ace-space-theme .ace_xml-pe {\
color: #004D40;\
}\
.ace-space-theme .ace_indent-guide {\
background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAACCAYAAACZgbYnAAAAEklEQVQImWMQERFpYLC1tf0PAAgOAnPnhxyiAAAAAElFTkSuQmCC) right repeat-y\
}\
.ace_gutter-cell.ace_error {\
background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4gkZDTIVPyxJhAAAAiRJREFUOMulkz1oU2EUhp/vy71N7jW/1LQhYigG1A5Ca0q4JVV0cJCiVIqbuIgVl3broKuDIiqKFJxExKGLLhlFoXpJtQ0tFOzgT4uU/lBjmhp7m2jvdTCpMa0g9AwfH4dznvc9cA7sMET1o8FR4BTQVkk5NXVOXd8kkLbglVKT7Ok/zunmICrgCLGlCQBFIuZyxO+9BOoAh5uDKAEd9cMiukdl/chBNiry8vkU7tJPXJEguVgYBWgHkDUAFRDv5tDP35/2d18bb0pP4HVAPn2L3ntjPNj/aNY39ZlGUTN+LUC6VZwfG9gAiURCnL2e8Q4+IXDuVsabSCTEwsICYf9mvase4BJANMTq1d7WQjabdQzDkCOz3zXDMKRpmvadi52FJj/5quAWB4oLpKTcEubbpZMd65Zloes6+XyegTNdpb2NrEpJWXFtD1AkYJVQZ5bwDqXfeDRNo1gsEgqFuPvstefTEj6rhCr/MYJUFeTMMt7Bh5lAMpkUpmnax/b5rNHRUSeVSokrj8cCH5fwCYncdgSAooUWi8WEaZr27b6uQnc7yzcvdK5ks1knEomItRKequPNpwrQVDgUIz9wYg/+Xawn4xSlpJyM4wxd7rCLFq79UVbc6h8HtYCR6Xl6WqOUD0T5WrEkhfyt2BJmDcC2Ud4vogJj9YDhBy/w5MsYdftffxNOqIFxYPivY2rb3cDEcrQRcP/HEZbaw/O5yS9ldhy/AFY+rIcRdDOyAAAAAElFTkSuQmCC)\
}\
.ace_dark .ace_gutter-cell.ace_info {\
background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4gkZDTkBxgJEMgAAAIhJREFUOMtjYBgBoHHif4bGif9xSbMQ0s+sa8TAwMDA8JfuLmdcuvM/372f/zmOP/jPoGP8nzxTghMgBhQ04DSACa8BsgoMP+/dZGDYs5mBdAOEpf6zevox/H/6mIHhyllG0g2QlGT49/UrA8OzJ3gdiduAK2cZ/14+x8BoasnAEJzwn2EU4AQAuwMq/e/3ML0AAAAASUVORK5CYII=)\
}\
.ace_tooltip {\
background-color: #101010EE;\
color: #78909C;\
}\
";

var dom = acequire("../lib/dom");
dom.importCssString(exports.cssText, exports.cssClass);
});
