import { Component, Inject }                        from '@angular/core';
import { LoginViewModel }                           from '../swagger-generated-API/model/loginViewModel';
import { UserViewModel }                            from '../swagger-generated-API/model/userViewModel';
import { UserAccessActivationService }              from '../swagger-generated-API/api/userAccessActivation.service';
import { TokenService }                             from '../token.service';
import { AppComponent, LanguageDef }                from '../app.component';

@Component({
  selector: 'login-component',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  private language: string = "";
  private login: string = "";
  private password: string = "";
  private msgError: string;
  private langs: LanguageDef[];
  private selectedLang: string;
  private selectedEditorLang: string;

  constructor (private userAccessActivationService: UserAccessActivationService,
               private appComponent: AppComponent,
               private tokenService: TokenService) {
    this.appComponent.logout(false);
    this.appComponent.onReady(() => {
      this.langs = this.appComponent.getLangs();
      this.selectedLang = this.appComponent.getCurrentLanguage();
      this.selectedEditorLang = this.appComponent.getSelectedLang(this.selectedLang).realName;
    });
  }

  private submitLogin(): void {
    if (this.login==="" || this.password==="") {
      this.msgError = "You must enter your credentials first";
    } else {
      var user : LoginViewModel = {
        password: this.password,
        username: this.login
      };
      this.userAccessActivationService.authenticate(user).subscribe(
        (response) => {
          this.msgError = null;
          this.tokenService.setToken(this.login, response.headers.get('Authorization'));
          let user: UserViewModel = response.body;
          this.appComponent.setUser(user);
          // this.appComponent.navigate(['/challenges', 'devfest2018', 'lightsaber-symphony-devfest', 'editor']);
          this.appComponent.navigate(['/']);
        },
        (err) => {
          this.msgError = "Incorrect credentials"
        }
      );
    }
  }

  private changeLanguage(): void {
    this.appComponent.changeLanguage(this.selectedLang);
  }

}
