import { Component, Inject }                        from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { LoginViewModel }                           from '../swagger-generated-API/model/loginViewModel';
import { UserViewModel }                            from '../swagger-generated-API/model/userViewModel';
import { UserAccessActivationService }              from '../swagger-generated-API/api/userAccessActivation.service';
import { TokenService }                             from '../token.service';

@Component({
  selector: 'login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss']
})
export class LoginModal {
  public title: string = "You want to login?";
  private login: string = "";
  private password: string = "";
  private msgError: string;
  public autocomplete: boolean = true;

  constructor (public dialogRef: MatDialogRef<LoginModal>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private userAccessActivationService: UserAccessActivationService,
              private tokenService : TokenService) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  submitLogin(): void {
    if (this.login==="" || this.password==="") {
      this.msgError = "You must enter your credentials first";
    } else {
      var user : LoginViewModel = {
        password: this.password,
        username: this.login
      };
      this.userAccessActivationService.authenticate(user).subscribe(
        (response) => {
          this.msgError = null;
          this.tokenService.setToken(this.login, response.headers.get('Authorization'));
          let user: UserViewModel = response.body;
          this.dialogRef.close(user);
        },
        (err) => {
          this.msgError = "Incorrect credentials"
        }
      );
    }
  }

}
