import { Component }        from '@angular/core';
import { AppComponent }     from '../app.component';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  // private BG = "../../assets/images/affiche.png";
  constructor(private appComponent: AppComponent) {
  }

  ngOnInit() {
    // this.appComponent.navigate(['/login']);
  }
}
