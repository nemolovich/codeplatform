import { Component, ViewChild, ElementRef } from '@angular/core';
import { DataSource }                       from '@angular/cdk/collections';
import { MatSort, MatPaginator, PageEvent } from '@angular/material';
import { BehaviorSubject }                  from 'rxjs/BehaviorSubject';
import { Observable }                       from 'rxjs/Rx';
import { Router, NavigationEnd }            from '@angular/router';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { AppComponent }                     from '../app.component';
import { DialogsService }                   from '../dialogs/dialogs.service';
import { ApplicantInfoService }             from './applicant-info.service';
import { ApplicantData as Data }            from './applicant-info.interface';
import { RecruitmentService }               from '../swagger-generated-API/api/recruitment.service';
import { ResultService }                    from '../swagger-generated-API/api/result.service';
import { ExerciceService }                  from '../swagger-generated-API/api/exercice.service';
import { Applicant }                        from '../swagger-generated-API/model/applicant';
import { ApplicantResult }                  from '../swagger-generated-API/model/applicantResult';
import { ApplicantViewModel }               from '../swagger-generated-API/model/applicantViewModel';
import { DifficultyLevel }                  from '../swagger-generated-API/model/difficultyLevel';
import { Language }                         from '../swagger-generated-API/model/language';
import { ExerciceViewModel }                from '../swagger-generated-API/model/exerciceViewModel';
import { ResultViewModel }                  from '../swagger-generated-API/model/resultViewModel';

const DISPLAY_ALL_APPLICANT: string = 'display-all-applicants';
const APPLICANTS_ROWS: string = 'applicants-rows';
const DISPLAYED_COLUMNS: string[] = ['name', 'email', 'phone', 'date', 'language', 'score', 'accepted', 'recruiter', 'actions'];
const PAGE_SIZE_OPTIONS: number[] = [10, 25, 50, 100];
const DEFAULT_FORMAT: string = 'DD/MM/YYYY';
const LEVELS: string[] = Object.keys(DifficultyLevel).filter(a => a.match(/^[A-Z]/));
const EMPTY_DATA: Data = {
                          id:             null,
                          recruiter:      null,
                          date:           null,
                          name: {
                            firstName:    null,
                            lastName:     null
                          },
                          email:          null,
                          phone:          null,
                          jobTitle:       null,
                          recruiterNotes: null,
                          language:       null,
                          difficulty:     null,
                          duration:       0,
                          challengeId:    null,
                          exerciceId:     null,
                          cv:             null,
                          result:         null,
                          score:          'N/A',
                          accepted:       -1,
                          actions : {
                            buttons: [
                            ]
                          }
                        };

let filter_level;

@Component({
  templateUrl: './applicants.component.html',
  styleUrls: ['./applicants.component.css']
})
export class ApplicantListComponent {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('filter') filter: ElementRef;
  
  private myApplicants: ApplicantViewModel[];
  private applicants: ApplicantViewModel[];
  private applicantsResults: { [id: string] : any; } = {};
  private resultsRetieve: { [id: string] : boolean; } = {};
  private errorMessage: string = null;
  private processing: boolean = true;
  private dataBase: ResultDatabase;
  private dataSource: ResultDataSource | null;
  private results: Data[];
  private displayedColumns = DISPLAYED_COLUMNS;
  private pageSizeOptions = PAGE_SIZE_OPTIONS;
  private pageSize = localStorage.getItem(APPLICANTS_ROWS) ?
            localStorage.getItem(APPLICANTS_ROWS) : 10;
  private displayAll: boolean = localStorage.getItem(DISPLAY_ALL_APPLICANT) 
            && localStorage.getItem(DISPLAY_ALL_APPLICANT) === 'true';
  private loaded: boolean = false;
  private level: string;
  private levels: string[] = LEVELS;

  constructor(private recruitmentService: RecruitmentService,
              private exerciceService: ExerciceService,
              private resultService: ResultService,
              private dialogsService: DialogsService,
              private applicantInfoService: ApplicantInfoService,
              private appComponent: AppComponent,
              private router: Router) {
    this.appComponent.onSetUser((user) => {
      if (!this.appComponent.getCurrentUser() || !this.appComponent.isRecruiterUser()) {
        this.errorMessage = "You must login as recruiter to access to this page";
        this.processing = false;
      } else {
        this.getApplicants();
      }
    });
  }

  private ngDoCheck() {
    if (this.dataSource && this.filter && this.paginator && this.sort) {
      this.dataSource!._sort = this.sort;
      this.dataSource!._paginator = this.paginator;
      this.updateView();
    }
  }

  private filterLevel(): void {
    filter_level = this.level;
  }

  private switchDisplay(e: any): void {
    this.displayAll = e.source.checked;
    this.applyResults(this.displayAll ? this.applicants : this.myApplicants);
    localStorage.setItem(DISPLAY_ALL_APPLICANT, '' + this.displayAll);
  }

  private getApplicantResult(applicant: ApplicantViewModel): void {
    if (applicant.challengeId) {
      this.resultsRetieve[applicant.id] = false;
      this.recruitmentService.getApplicantResultDetails(applicant.challengeId)
        .subscribe((result: Blob) => {
        this.applicantsResults[applicant.id] = result;
        this.resultsRetieve[applicant.id] = true;
      }, (error) => {
        console.log(error);
      });
    }
  }

  private getApplicants(): void {
    this.recruitmentService.getAllApplicants().subscribe(
      applicants => {
        this.recruitmentService.getAllApplicants("owned").subscribe(
          myApplicants => {
            this.applicants = applicants.sort((a,b) =>
              a.lastName.localeCompare(b.lastName));
            this.myApplicants = myApplicants.sort((a,b) =>
              a.lastName.localeCompare(b.lastName));
            this.applyResults(this.displayAll ? this.applicants : this.myApplicants);
            this.resultsRetieve = {};
            this.applicantsResults = {};
            this.applicants.forEach(applicant => {
              this.getApplicantResult(applicant);
            });
          });
      });
  }

  private applyResults(applicants: ApplicantViewModel[]): void {
    this.processing = true;
    this.results = [EMPTY_DATA];
    let resultsTmp: Data[] = [];
    this.dataSource = null;
    if (applicants.length < 1) {
      this.initEmptyDatabase();
    }
    applicants.forEach(applicant => {
      this.recruitmentService.getApplicantByID(applicant.id).subscribe(
        (a: ApplicantResult) => {
        this.resultService.getResultsByChallengeId(applicant.challengeId || 'recruitment').subscribe(
          (r: ResultViewModel[]) => {
          this.exerciceService.getAllExercicesByChallengeId(applicant.challengeId || 'recruitment').subscribe(
            (exercices: ExerciceViewModel[]) => {
            let score: string = r.length < 1 || r[0].submissions.length < 1 || r[0].submissions[0].score < 0 ?
              'N/A' : (r[0].submissions[0].score * 100) + '%'
            let appl: Data = this.buildApplicant(a);
            appl.exerciceId = exercices[0].id;
            appl.score = score;
            resultsTmp.push(appl);
            if (resultsTmp.length === applicants.length) {
              this.initDatabase(resultsTmp);
            }
          });
        });
      });
    });
  }

  private initEmptyDatabase(): void {
    this.initDatabase([EMPTY_DATA]);
    this.results = [];
  }

  private initDatabase(results: Data[]): void {
    this.dataBase = new ResultDatabase(results);
    this.results = results;
    if (results.length > 0) {
      this.dataSource = new ResultDataSource(this.dataBase,
        this.paginator, this.sort);
      Observable.fromEvent(this.filter.nativeElement, 'keyup')
        .debounceTime(150)
        .distinctUntilChanged()
        .subscribe(this.updateView);
    }
    this.processing = false;
    this.loaded = true;
  }

  private editApplicant(data: Data): boolean {
    this.router.navigate(['applicants', 'edit', data.id]);
    return false;
  }

  private deleteApplicant(data: Data): boolean {
    this.dialogsService
      .confirm('Confirm action', 'Do you really want to delete this applicant?', false)
      .subscribe(res => {
        if (res) {
          this.processing = true;
          this.recruitmentService.deleteApplicant(data.id).subscribe(
             (applicant) => {
              this.getApplicants();
              this.processing = false;
          });
        }
    });
    return false;
  }
  
  private applyFun(fun: ((Applicant) => boolean), data: Data): boolean {
    return fun(data);
  }

  private clearFilters() {
    this.filter.nativeElement.value = ' ';
    this.level = null;
    this.filterLevel();
    this.filter.nativeElement.value = '';
    this.updateView();
  }

  private paginationChange(e: PageEvent) {
    this.updateView();
    localStorage.setItem(APPLICANTS_ROWS, '' + e.pageSize);
  }

  private updateView() {
    if (this.dataSource) {
      this.dataSource.filter = this.filter.nativeElement.value;
    }
  }

  private getDataSource(): ResultDataSource {
    return this.dataSource;
  }

  private getAcceptation(value: number): string {
    return value < 0 ? 'GO' : value > 0 ? '' : 'NO GO';
  }

  private formatTimestamp(timestamp: number, format: string = DEFAULT_FORMAT): string {
    let date: Date = new Date(timestamp);
    let res : string = format;
    res = res.replace('DD', ('00' + date.getDate()).slice(-2));
    res = res.replace('D', '' + date.getDate());
    res = res.replace('MM', ('00' + (date.getMonth() + 1)).slice(-2));
    res = res.replace('M', '' + (date.getMonth() + 1));
    res = res.replace('YYYY', '' + date.getFullYear());
    res = res.replace('YY', '' + date.getFullYear() % 100);
    res = res.replace('Y', '' + date.getFullYear() % 100);
    return res;
  }

  private buildApplicant(a: ApplicantResult): Data {
    let cv: number[] = null;
    if (a.cv) {
      cv = atob(''+a.cv).split('').map(c => c.charCodeAt(0));
    }
    return {
      id: a.id,
      recruiter: {
        login: a.recruiter.login,
        name: {
          firstName: a.recruiter.firstName,
          lastName: a.recruiter.lastName
        }
      },
      date: a.challenge ? AppComponent.getTimestampFromEpoch(a.challenge.startDate) : 0,
      name: {
        firstName: a.firstName,
        lastName: a.lastName
      },
      email: a.email,
      phone: a.phone || '',
      jobTitle: a.jobTitle || '',
      recruiterNotes: a.recruiterNotes || '',
      language: this.appComponent.getLanguageName('' + a.language),
      difficulty: '' + LEVELS.find(l => l.toLowerCase() === '' + a.difficulty),
      duration: a.duration,
      challengeId: a.challenge ? a.challenge.id : null,
      exerciceId: null,
      result: null,
      cv: cv,
      score: 'N/A',
      accepted: a.accepted == null ? 1 : a.accepted ? -1 : 0,
      actions : {
        buttons: [
          {
            title: 'Display Applicant details',
            icon: 'zoom_in',
            fun: (a) => {
              this.processing = true;
              this.applicantInfoService.setMessage('Result is generating...');

              let waitForResult: ((number) => Promise<any>) = 
              (time) => new Promise<any>((resolve, reject) => {
                let count: number = 0;
                let check: (() => void) = () => {
                  if (!(a.id in this.resultsRetieve) ||
                    (a.id in this.resultsRetieve && this.resultsRetieve[a.id])) {
                    a.result = this.applicantsResults[a.id]
                    this.applicantInfoService.resetMessage();
                    this.applicantInfoService.setData(a);
                    clearTimeout(checkTimeout);
                    this.processing = false;
                    resolve();
                  } else if((time -= 100) < 0) {
                    clearTimeout(checkTimeout);
                    this.processing = false;
                    reject('Result retrieve timed out!');
                  } else {
                    if (time % 1000 == 0) {
                      let nbP: number = (count++)%4;
                      this.applicantInfoService.setMessage('Result is generating' + 
                        ('.'.repeat(nbP)) + ('&nbsp;'.repeat(3-nbP)));
                    }
                    checkTimeout = setTimeout(check, 100);
                  }
                }
                let checkTimeout: number = setTimeout(check, 100);
              });

              (async () => { waitForResult(120000); })();

              return this.displayDetails(a);
            }
          },
          {
            title: 'Edit Applicant',
            icon: 'edit',
            color: 'primary',
            className: 'mat-primary',
            fun: (a) => {
              return this.editApplicant(a);
            }
          },
          {
            title: 'Delete Applicant',
            icon: 'delete',
            color: 'warn',
            className: 'mat-warn',
            fun: (a) => {
              return this.deleteApplicant(a);
            }
          }
        ]
      }
    };
  }

  private displayDetails(data: Data): boolean {
    this.applicantInfoService.show(data).subscribe((nil) => {
      this.processing = false;
    });
    return false;
  }

}

export class ResultDatabase {
  dataChange: BehaviorSubject<Data[]> = new BehaviorSubject<Data[]>([]);
  get data(): Data[] {
    return this.dataChange.value;
  }

  constructor(private resultData: Data[]) {
    const copiedData = this.data.slice();
    Array.prototype.push.apply(copiedData, this.resultData);
    this.dataChange.next(copiedData);
  }
}

export class ResultDataSource extends DataSource<any> {
  private _filterChange = new BehaviorSubject('');
  get filter(): string { return this._filterChange.value; }
  set filter(filter: string) { this._filterChange.next(filter); }
  constructor(public _resultDatabase: ResultDatabase,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
  }

  connect(): Observable<Data[]> {
   let displayDataChanges;
    if (this._paginator) {
      displayDataChanges = [
        this._resultDatabase.dataChange,
        this._sort.sortChange,
        this._filterChange,
        this._paginator.page,
      ];
    } else {
      displayDataChanges = [
        this._resultDatabase.dataChange,
        this._sort.sortChange,
        this._filterChange,
      ];
    }

    return Observable.merge(...displayDataChanges).map(() => {
      let data = this.getSortedData().slice().filter((item: Data) => {
        if (item.id === null) {
          return false;
        }
        let re: RegExp;
        let result;
        try {
          re = new RegExp(this.filter, 'gi');
        } catch (e) {
        }
        if (re) {
          result = item.name.firstName.match(re) || item.name.lastName.match(re);
        } else {
          result = (item.name.firstName + ' ' + item.name.lastName)
            .toLowerCase().indexOf(this.filter.toLowerCase()) != -1;
        }
        if (filter_level) {
          return result && item.difficulty === filter_level;
        } else {
          return result;
        }
      });
      if (this._paginator) {
        this._paginator.length = data.length;
        let startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        if (startIndex >= data.length) {
          this._paginator.pageIndex = Math.max(0, Math.ceil(data.length / this._paginator.pageSize) - 1);
          startIndex = Math.max(0, this._paginator.pageIndex * this._paginator.pageSize);
        }
        return data.splice(startIndex, this._paginator.pageSize);
      } else {
        return data;
      }
    });
  }

  disconnect() {}

  getSortedData(): Data[] {
    const data = this._resultDatabase.data.slice();
    if (!this._sort.active || this._sort.direction == '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number|string = '';
      let propertyB: number|string = '';

      switch (this._sort.active) {
        case 'name':      [propertyA, propertyB] =
                          [a.name.lastName, b.name.lastName]; break;
        case 'date':      [propertyA, propertyB] =
                          [b.date, a.date]; break;
        case 'language':  [propertyA, propertyB] =
                          [a.language, b.language]; break;
        case 'accepted':  [propertyA, propertyB] =
                          [a.accepted, b.accepted]; break;
        case 'recruiter': [propertyA, propertyB] =
                          [a.recruiter.name.lastName, b.recruiter.name.lastName]; break;
      }

      let valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      let valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction == 'asc' ? 1 : -1);
    });
  }
}
