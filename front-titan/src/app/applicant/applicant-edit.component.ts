import { Component, ViewChild }         from '@angular/core';
import { FormControl, Validators }      from '@angular/forms';
import { FileUploader, FileItem }       from 'ng2-file-upload';
import { ActivatedRoute, ParamMap }     from '@angular/router';
import { HttpClient, HttpHeaders }      from '@angular/common/http';
import { RecruitmentService }           from '../swagger-generated-API/api/recruitment.service';
import { ChallengeService }             from '../swagger-generated-API/api/challenge.service';
import { DialogsService }               from '../dialogs/dialogs.service';
import { Applicant }                    from '../swagger-generated-API/model/applicant';
import { ApplicantResult }              from '../swagger-generated-API/model/applicantResult';
import { Roles }                        from '../swagger-generated-API/model/roles';
import { DifficultyLevel }              from '../swagger-generated-API/model/difficultyLevel';
import { Language }                     from '../swagger-generated-API/model/language';
import { UserViewModel }                from '../swagger-generated-API/model/userViewModel';
import { AppComponent, LanguageDef,
         RERUITER_START_TEST}           from '../app.component';

const PHONE_REGEX: RegExp = /^\d([\s\.-]?)(\d{2}\1){3}\d{2}$/;
const TEST_DURATIONS = [5, 10, 15, 20, 25, 30];
const UPLOAD_URL = '/upload/';

export interface LanguageDescIT {
  name: string,
  lang: string
}

export class LanguageDesc implements LanguageDescIT {
  name: string;
  lang: string;
  constructor(name: string, language: string) {
    this.name = name;
    this.lang = language;
  }
}

export interface DifficultyDescIT {
  name: string,
  difficulty: DifficultyLevel
}

export class DifficultyDesc implements DifficultyDescIT {
  name: string;
  difficulty: DifficultyLevel;
  constructor(name: string) {
    this.name = name;
    this.difficulty = DifficultyLevel[name];
  }
}

@Component({
  templateUrl: './applicant-edit.component.html',
  styleUrls: ['./applicants.component.css']
})
export class ApplicantEditComponent {
  private processing: boolean = true;
  private applicantId: string;
  private applicant: ApplicantResult;
  private indeterminate: boolean = true;
  private failedMessage: string;
  private errorMessage: string;
  private infoMessage: string;
  private languages: LanguageDescIT[];
  private difficulties: DifficultyDescIT[] = Object.keys(DifficultyLevel)
    .filter(d => d.match(/^[A-Z]/)).map(d => new DifficultyDesc(d));
  private hasBaseDropZoneOver: boolean = false;
  private uploader: FileUploader = new FileUploader({url: UPLOAD_URL});
  private langs: LanguageDef[] = [];

  private emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email]);
  private phoneFormControl = new FormControl('', [
    Validators.pattern(PHONE_REGEX)]);

  constructor(private recruitmentService: RecruitmentService,
              private challengeService: ChallengeService,
              private dialogsService: DialogsService,
              protected httpClient: HttpClient,
              private route: ActivatedRoute,
              private appComponent: AppComponent) {
    this.route.paramMap.switchMap((params: ParamMap) => {
      if (params.get('applicantId')) {
        this.applicantId = params.get('applicantId');
      }
      this.appComponent.onReady(() => {
        this.langs = this.appComponent.getLangs();
        this.initApplicant();
      });
      return params.get('applicantId') || [];
    }).subscribe(applicantId => {
    });
  }

  @ViewChild('applicantForm') applicantForm
  ngOnInit() {
  }

  private initApplicant() {
    this.appComponent.onSetUser((user: UserViewModel) => {
      if (!this.appComponent.getCurrentUser() || !this.appComponent.isRecruiterUser()) {
        this.failedMessage = "You must login as recruiter to access to this page";
        this.processing = false;
      } else {
        if (this.applicantId) {
          this.recruitmentService.getApplicantByID(this.applicantId).subscribe(
            (applicant: ApplicantResult) => {
              this.applicant = applicant;
              if (this.applicant.phone) {
                if (this.applicant.phone.length > 9) {
                  this.applicant.phone = this.applicant.phone.replace(/^0/, '');
                }
              }
              if (this.applicant.cv) {
                let txtCv: string = '' + this.applicant.cv;
                this.addFileToQueue(new File([new Uint8Array(atob(txtCv).split('').map(x => x.charCodeAt(0)))],
                  this.applicant.lastName+'_'+this.applicant.firstName+'_CV.pdf'));
              }
              this.indeterminate = this.applicant.accepted === null;
              this.processing = false;
            },
            (error) => {
              console.log(error);
              this.processing = false;
            }
          );
        } else {
          this.applicant = {
            recruiter: user,
            firstName: "",
            lastName: "",
            email: "",
            phone: null,
            jobTitle: "",
            language: Language.Java,
            difficulty: DifficultyLevel.Beginner,
            duration: 15,
            challenge: null,
            cv: null
          };
          this.processing = false;
        }
      }
    });
    this.languages = this.langs.map((l: LanguageDef) => new LanguageDesc(l.name, l.language));
  }

  private resetAccepted(): boolean {
    this.applicant.accepted = null;
    this.indeterminate = true;
    return false;
  }

  private fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  private removeCV(item: FileItem): void {
    item.remove();
    this.applicant.cv = null;
  }

  private moveFiles(e: any): void {
    this.addFileToQueue(e.target.files[0]);
  }

  private dropFile(e: any): void {
    this.addFileToQueue(e.item(0));
  }

  private addFileToQueue(file: File) {
    this.uploader.clearQueue();
    this.uploader.addToQueue([file], this.uploader.options, this.getFilters());
    let applicant: ApplicantResult = this.applicant;
    let fr = new FileReader();
    fr.onloadend = function() { applicant.cv = Array.from(new Uint8Array(this.result)); };
    fr.readAsArrayBuffer(file.slice());
  }

  private getFilters(): any {
    return {};
  }

  private getDurations(): Array<number> {
    return TEST_DURATIONS;
  }

  isRecruiter(): boolean {
    return !!this.appComponent && !!this.appComponent.getCurrentUser()
      && this.appComponent.getCurrentUser().roles.some(
        r => r === Roles.RECRUITER || r === Roles.ADMIN);
  }

  submitApplicant() {
    this.processing = true;
    this.infoMessage = '';
    this.errorMessage = '';
    if (this.applicantId) {
      this.updateApplicant();
    } else {
      this.createApplicant();
    }
  }

  createApplicant() {
    if (this.applicantForm.nativeElement.checkValidity()
      && this.emailFormControl.valid && this.phoneFormControl.valid) {
      let applicant: Applicant = { ...this.applicant, challenge: null };
      if (applicant.phone) {
        applicant.phone = applicant.phone.replace(/[\s\.-]/g, '');
        if (applicant.phone && applicant.phone.length < 10) {
        applicant.phone = '0' + applicant.phone;
        }
      }
      this.recruitmentService.createApplicant(applicant)
        .subscribe(
          (applicantResult) => {
            this.infoMessage = 'Applicant has been created';
            let challengeId: string = applicantResult.challenge.id;
            this.challengeService.getAllExercicesByChallengeId(challengeId).subscribe(
              (exercices) => {
                let exerciceId: string = exercices[0].id;
                this.processing = false;
                this.dialogsService.show('Start applicant test',
                  'Press the button to start the applicant test', 'Start Test').subscribe((res) => {
                    localStorage.setItem(RERUITER_START_TEST, challengeId);
                    this.appComponent.navigate(['/challenges', challengeId, exerciceId, 'editor']);
                });
            });
          },
          (error) => {
            console.log(error);
            this.errorMessage = 'Cannot create applicant';
            this.processing = false;
          });
    } else {
      this.errorMessage = 'Incomplete form';
      this.processing = false;
    }
  }

  updateApplicant() {
    if (this.applicantForm.nativeElement.checkValidity()
      && this.emailFormControl.valid && this.phoneFormControl.valid) {
      let applicant: Applicant = {
        ...this.applicant,
        challenge: {
          id: this.applicant.challenge.id,
          name: this.applicant.challenge.name
        }
      };
      if (applicant.phone.length < 1) {
        applicant.phone = null;
      }
      if (applicant.phone) {
        applicant.phone = applicant.phone.replace(/[\s\.-]/g, '');
        if (applicant.phone.length < 10) {
          applicant.phone = '0' + applicant.phone;
        }
      }
      this.recruitmentService.updateApplicant(applicant)
        .subscribe(
          (applicantView) => {
            this.infoMessage = 'Applicant has been updated';
            this.processing = false;
          },
          (error) => {
            this.errorMessage = 'Cannot update applicant';
            this.processing = false;
          });
    } else {
      this.errorMessage = 'Incomplete form';
      this.processing = false;
    }
  }

}
