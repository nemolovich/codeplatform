import { Observable }                               from 'rxjs/Rx';
import { ApplicantInfo }                            from './applicant-info.component';
import { MatDialogRef, MatDialog, MatDialogConfig } from '@angular/material';
import { Injectable }                               from '@angular/core';
import { ApplicantData }                            from './applicant-info.interface';

@Injectable()
export class ApplicantInfoService {
  private dialogRef: MatDialogRef<ApplicantInfo>;

  constructor(private dialog: MatDialog) {
  }

  public show(data: ApplicantData): Observable<boolean> {
    this.dialogRef = this.dialog.open(ApplicantInfo);
    this.dialogRef.componentInstance.setData(data);
    return this.dialogRef.afterClosed();
  }

  public setData(data: ApplicantData): void {
    if (this.dialogRef && this.dialogRef.componentInstance) {
      this.dialogRef.componentInstance.setData(data);
    }
  }

  public setMessage(message: string): void {
    if (this.dialogRef && this.dialogRef.componentInstance) {
      this.dialogRef.componentInstance.setMessage(message);
    }
  }

  public resetMessage(): void {
    if (this.dialogRef && this.dialogRef.componentInstance) {
      this.dialogRef.componentInstance.resetMessage();
    }
  }
}
