export interface Button {
  title: string,
  icon?: string,
  color?: string,
  className?: string,
  fun: ((ApplicantData) => boolean)
}

export interface ApplicantData {
  id: string,
  recruiter: {
    login: string,
    name: {
      firstName: string,
      lastName: string
    }
  },
  date: number,
  name: {
    firstName: string,
    lastName: string
  },
  email: string,
  phone: string,
  jobTitle: string,
  recruiterNotes: string,
  language: string,
  difficulty: string,
  duration: number,
  challengeId: string,
  exerciceId: string,
  cv: number[],
  result: Blob,
  accepted: number,
  score: string,
  actions : {
    buttons: Button[]
  }
}
