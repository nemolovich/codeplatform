import { MatDialogRef }               from '@angular/material';
import { Component }                  from '@angular/core';
import { AppComponent }               from '../app.component';
import { ApplicantData }              from './applicant-info.interface';
import { DomSanitizer, SafeStyle }    from '@angular/platform-browser';

@Component({
    styleUrls: ['./applicants.component.css', '../dialogs/dialog.css'],
    templateUrl: './applicant-info.component.html'
})
export class ApplicantInfo {
  private data: ApplicantData;
  private message: string;
  private cvURL: string;
  private testResultURL: string = "javascript:void(0);";

  constructor(public dialogRef: MatDialogRef<ApplicantInfo>,
              private domSanitizer: DomSanitizer) {
  }

  public setData(data: ApplicantData) {
    this.data = data;
    if (data.cv) {
      this.cvURL = 'data:application/octet-stream;base64,' + btoa(String.fromCharCode.apply(String, data.cv));
    }
    if (data.result) {
      var reader = new FileReader();
      reader.onload = () => {
          let content = reader.result;
          this.testResultURL = 'data:application/octet-stream;base64,' + btoa(content);
      }
      reader.readAsBinaryString(data.result);
    }
  }

  public setMessage(message: string) {
    this.message = message;
  }

  public resetMessage() {
    this.message = null;
  }

  private getAcceptation(value: number): string {
    return value < 0 ? 'GO' : value > 0 ? '' : 'NO GO';
  }

  public formatTimestamp(timestamp: number, format: string = AppComponent.DEFAULT_FORMAT): string {
    return AppComponent.formatTimestamp(timestamp, format);
  }

}
