import { Injectable }    from '@angular/core';

export class Waiter {
  private handler: (() => void) = () => {};

  constructor() {
  }

  public handle(): void {
    this.handler();
  }

  public then(fun: (() => void)) {
    this.handler = fun;
  }

}

@Injectable()
export class StopWatchService {
  private startTime: number = 30000;
  private timeRemaining: number = this.startTime;
  private targetDate: Date;
  private speed: number;
  private intervalId: number = null;
  private handler: (() => void) = () => {};

  constructor() {
  }

  private setTimeRemaining(time: number, speed: number): void {
    this.startTime = time;
    this.timeRemaining = time;
    this.targetDate = new Date(new Date().getTime() + this.timeRemaining * 10);
    this.speed = speed;
  }

  public setRemainingTime(time: number): void {
    this.timeRemaining = time * 100;
    this.targetDate = new Date(new Date().getTime() + this.timeRemaining * 10);
  }

  public getTimeRemaining(): number {
    this.timeRemaining = (new Date(this.targetDate).getTime() - new Date().getTime()) / 10;
    return this.timeRemaining / 100;
  }

  public getTotalTime(): number {
    return this.startTime;
  }

  public pauseTimer(): void {
    clearInterval(this.intervalId);
    this.intervalId = null;
  }

  public reStartTimer(): void {
    this.resetTimer();
    this.startTimer();
  }

  public resetTimer(): void {
    this.pauseTimer();
    this.timeRemaining = this.startTime;
  }

  public startTimer(): void {
    this.targetDate = new Date(new Date().getTime() + this.timeRemaining * 10);
    this.intervalId = setInterval(() => {
      this.nextOne(this);
    }, this.speed);
  }

  public stopTimer(): void {
    this.pauseTimer();
    this.timeRemaining = 0;
    this.handler();
  }

  public isRunning(): boolean {
    return this.intervalId !== null;
  }

  private nextOne(elm: StopWatchService): void {
    elm.timeRemaining = (new Date(elm.targetDate).getTime() - new Date().getTime()) / 10;
    // if (elm.timeRemaining > 0) {
      // elm.timeRemaining -= 1;
    // } else {
      // elm.stopTimer();
    // }
    if (elm.timeRemaining <= 0) {
      elm.stopTimer();
    }
  }

  public runStopWatch(time: number, speed: number = 100): Waiter {
    this.handler = () => {};
    this.stopTimer();
    this.setTimeRemaining(time * 100, speed);
    this.startTimer();
    let waiter = new Waiter();
    this.handler = () => {
      waiter.handle();
    };
    return waiter;
  }

  public static getSecondsFrom(time: number): number {
    return Math.floor(time / 100) % 60;
  }

  public getSeconds(remaining: boolean = true): number {
    return StopWatchService.getSecondsFrom(remaining ? 
      this.timeRemaining : this.startTime - this.timeRemaining);
  }

  public static getMinutesFrom(time: number): number {
    return Math.floor((Math.floor(time / 100) % 3600) / 60);
  }

  public getMinutes(remaining: boolean = true): number {
    return StopWatchService.getMinutesFrom(remaining ? 
      this.timeRemaining : this.startTime - this.timeRemaining);
  }

  public static getHoursFrom(time: number): number {
    return Math.floor(Math.floor(time / 100) / 3600);
  }

  public getHours(remaining: boolean = true): number {
    return StopWatchService.getHoursFrom(remaining ? 
      this.timeRemaining : this.startTime - this.timeRemaining);
  }

}
