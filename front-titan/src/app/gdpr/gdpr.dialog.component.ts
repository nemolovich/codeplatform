import { MatDialogRef } from '@angular/material';
import { Component }    from '@angular/core';

@Component({
    selector: 'show-dialog',
    template: `
        <h3>{{ title }}</h3>
        <p [innerHTML]="message"></p>
        <button type="button" mat-raised-button
            (click)="dialogRef.close(true)">{{buttonText}}</button>
    `,
    styleUrls:["./dialog.css"]
})
export class ShowDialog {

  public title: string;
  public message: string;
  public buttonText: string;

  constructor(public dialogRef: MatDialogRef<ShowDialog>) {

  }
}
