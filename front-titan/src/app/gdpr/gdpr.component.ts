import { Component }                    from '@angular/core';
import { HttpClient }                   from '@angular/common/http';
import { DynamicDialog }                from '../dialogs/dynamic-dialog.component';

export interface TemplateDef {
  id: string,
  title: string,
  templateUrl: string,
  content: string
}

export const AVAILABLE_PAGES: TemplateDef[] = [
  {
    id: 'en',
    title: 'GDPR (english)',
    templateUrl: './gdpr_en.component.html',
    content: 'Loading...',
  },
  {
    id: 'fr',
    title: 'RGPD (français)',
    templateUrl: './gdpr_fr.component.html',
    content: 'Chargement...',
  },
];

@Component({
  selector: 'gdpr-component',
  templateUrl: './gdpr.component.html'
})
export class GdprComponent {

  private pages: TemplateDef[] = AVAILABLE_PAGES;
  private loaded: boolean = false;
  private title: string = 'GDPR';

  // @Input() url: string[];
  constructor(private http: HttpClient,
              private dynamicDialog: DynamicDialog) {
    this.pages = AVAILABLE_PAGES.filter((p: TemplateDef) => dynamicDialog.extraData.templates.includes(p.id));
    this.pages.forEach((p) => {
      p['template'] = p['template'].replace(/<companyName>/g, dynamicDialog.extraData.companyName)
        .replace(/<siteUrl>/g, dynamicDialog.extraData.siteUrl)
        .replace(/<currentUrl>/g, dynamicDialog.extraData.currentUrl)
        .replace(/<mails>/g, this.buildMails(dynamicDialog.extraData.mails));
    });
  }

  private buildMails(mails: string[]): string {
    return mails.map((m) => `<li><a href="mailto:${m}">${m}</a></li>`).join('\n    ');
  }

  ngOnInit() {
    this.loaded = true;
  }

}
