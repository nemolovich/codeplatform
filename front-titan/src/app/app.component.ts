import { Component, ViewChild, HostListener }       from '@angular/core';
import { Router, NavigationEnd }                    from '@angular/router';
import { MatDialog, MatCardModule, MatMenuTrigger } from '@angular/material';
import { UserViewModel }                            from './swagger-generated-API/model/userViewModel'
import { Roles }                                    from './swagger-generated-API/model/roles'
import { UserAccessActivationService }              from './swagger-generated-API/api/userAccessActivation.service'
import { ExerciceService }                          from './swagger-generated-API/api/exercice.service'
import { UtilsService }                             from './swagger-generated-API/api/utils.service'
import { TokenService }                             from './token.service'
import { StopWatchService }                         from './stopwatch.service'
import { DialogsService }                           from './dialogs/dialogs.service'
import { GdprComponent }                            from './gdpr/gdpr.component'

export const EMOJI_NAMES: string[] = [
  'wink',
  'smile',
  'smile-smiling-eyes',
  'grinning-face',
  'smile-open-mouth',
  'smile-smiling-eyes-open-mouth',
  'loudly-crying',
  'tears-of-joy',
  'crying',
  'persevering',
  'disapointed',
  'unamused',
  'sleeping',
  'sunglases',
  'medical-mask',
  'savouring',
  'smile-closed-eyes',
  'confounded',
  'wink-stuck-out-tongue',
  'kiss-smiling-eyes',
  'kiss',
  'kiss-closed-eyes',
  'blowing-kiss',
  'heart-eyes',
];

export const MENU_EXPANDED_STORAGE_NAME = 'menu-expanded';
export const USER_LANGUAGE_STORAGE_NAME = 'user-language';
export const USER_THEME_STORAGE_NAME = 'user-theme';
export const RECUITER_MODE_ENABLED = 'recruiter-mode';
export const RERUITER_START_TEST = 'recruiter-start-test';
export const REDIRECT_URL = 'redirect-url';

export interface LanguageDef {
  realName: string,
  name: string,
  language: string,
  mainScript: string
}
export interface ThemeDef {
  realName: string,
  name: string
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  private themes: ThemeDef[];
  private allLangs: LanguageDef[];
  private langs: LanguageDef[] = [];

  private currentUser: UserViewModel;
  private isEditor: boolean = false;
  private isRecruiter: boolean = false;
  private recruiterMode: boolean = false;
  private allowChangeLanguage: boolean = false;
  private displaySWRemaining: boolean = true;
  private redirectURL: string;
  private setUserHandler: ((user: UserViewModel) => void) = (user) => {};
  private stopTimerHandler: (() => void) = () => {};
  private beforeUnload: (() => void) = () => {};
  private themeChangeHandler: ((theme: string) => void) = (theme) => {};
  private languageChangeHandler: ((language: string) => void) = (language) => {};
  private expanded: boolean = !localStorage.getItem(MENU_EXPANDED_STORAGE_NAME) || localStorage.getItem(MENU_EXPANDED_STORAGE_NAME) === 'true';
  private selectedLang: string = localStorage.getItem(USER_LANGUAGE_STORAGE_NAME) ? localStorage.getItem(USER_LANGUAGE_STORAGE_NAME) : "python3";
  private selectedTheme: string = localStorage.getItem(USER_THEME_STORAGE_NAME) ? localStorage.getItem(USER_THEME_STORAGE_NAME) : "cobalt";
  private selectedEditorLang: string;

  private isReady: boolean = false;
  private loadingMessage: string = '';

  private currentPath: string = '';

  private displayMenu: boolean;
  private hiddenMenuPages: string[];

  private globalConfig: object;

  private particlesEnabled: boolean;
  private particlesStyle: object;
  private particlesParams: object[];
  private particlesWidth: number;
  private particlesHeight: number;

  private gdprEnabled: boolean = false;
  private gdprTitle: string = "GDPR";
  private gdprTemplates: string[] = [];
  private gdprDisplayOn: string[] = [];
  private gdprButtonClass: string;
  
  private companyName: string;
  private siteUrl: string;
  private mails: string[];
  private buttonsDisplay: { [id: string]: boolean; } = {
    "account": true,
    "login": true,
    "settings": true,
    "courses": true,
    "challenges": true,
    "scoreboard": true,
    "teams": true,
    "recruiter": true,
  };

  @ViewChild(MatMenuTrigger) menuTrigger: MatMenuTrigger;
  constructor(private dialogsService: DialogsService,
              private userAccessActivationService: UserAccessActivationService,
              private tokenService : TokenService,
              private stopWatchService : StopWatchService,
              private exerciceService : ExerciceService,
              private utilsService : UtilsService,
              private router: Router) {
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        this.loadingMessage = 'Check if page is editor...';
        this.currentPath = e.url+'';
        let match = this.currentPath.match(new RegExp('/editor$'));
        this.isEditor = match && match.length > 0;
        this.utilsService.getFrontConfig().subscribe(config => {
          this.loadingMessage = 'Loading global configuration...';
          this.allLangs = (config||{}).languages || {};
          this.companyName = ((config||{}).app||{}).companyName || 'My Company';
          this.siteUrl = ((config||{}).app||{}).siteUrl || 'http://example.com';
          this.mails = ((config||{}).app||{}).mails || ['mail@example.com'];
          let displayMenu: boolean = ((config||{}).app||{}).displayMenu;
          this.displayMenu = displayMenu !== false;
          this.hiddenMenuPages = ((config||{}).app||{}).hiddenMenuPages || [];
          this.particlesEnabled = ((config||{}).particles||{}).enabled || false;
          this.particlesStyle = ((config||{}).particles||{}).style || {};
          this.particlesParams = ((config||{}).particles||{}).configs || [];
          this.particlesWidth = ((config||{}).particles||{}).width || 100;
          this.particlesHeight = ((config||{}).particles||{}).height || 100;
          this.gdprEnabled = (((config||{}).app||{}).gdpr||{}).enabled || false;
          this.gdprTitle = (((config||{}).app||{}).gdpr||{}).title || this.gdprTitle;
          this.gdprTemplates = (((config||{}).app||{}).gdpr||{}).templates || [];
          this.gdprDisplayOn = (((config||{}).app||{}).gdpr||{}).displayOn || ['.*'];
          this.gdprButtonClass = (((config||{}).app||{}).gdpr||{}).buttonClass || '';
          let buttonsDisplay: { [id: string]: boolean;} = ((config||{}).app||{}).buttonsDisplay || {};
          this.buttonsDisplay = { ...this.buttonsDisplay, ...buttonsDisplay };
          this.themes = (((config||{}).editor||{}).themes || {}).map(x => {
            return {realName:x.toLowerCase().replace(/ /g, '_'), name:x};
          });
          if (!this.themes.map(t => t.realName).includes(this.selectedTheme)) {
            this.changeTheme(this.themes[0].realName);
          }
          this.exerciceService.listLanguages().subscribe(res => {
            this.loadingMessage = 'Retreive available languages...';
            this.langs = this.allLangs.filter((l) => res.includes(l.language));
            if (!this.langs.map(t => t.language).includes(this.selectedLang)) {
              this.selectedLang = this.allLangs[0].language;
              this.storeLanguage();
            }
            this.selectedEditorLang = this.getSelectedLang(this.selectedLang).realName;
            this.userAccessActivationService.isAuthenticated()
              .subscribe(data => {
                this.loadingMessage = 'Load user informations...';
                this.recruiterMode = !!data && (localStorage.getItem(RECUITER_MODE_ENABLED) ?
                  localStorage.getItem(RECUITER_MODE_ENABLED) === 'true' : null)
                this.setUser(data);
              });
            this.isReady = true;
          });
          this.redirectURL = localStorage.getItem(REDIRECT_URL);
          if (this.redirectURL) {
            localStorage.removeItem(REDIRECT_URL);
            this.router.navigate([this.redirectURL]);
          }
        });
      }
    });
    window.addEventListener('click', function (evt) {
      if (evt.detail === 4) {
        var elm = null;
        var staticElm = null;
        var consoleElm = null;
        evt['path'].forEach(p => {
          if (p.className && p.className.indexOf('ace_static_highlight') !== -1) {
            staticElm = p;
          }
          if (p.className && p.className.indexOf('console') !== -1) {
            consoleElm = p;
          }
        });
        if (staticElm) {
          elm = staticElm;
        } else if(consoleElm) {
          elm = consoleElm;
        }
        if (elm) {
          AppComponent.selectContent(elm);
        }
      }
    });
  }

  ngAfterViewInit() {
  }

  public isMenuDisplayed(): boolean {
    return this.displayMenu && !this.hiddenMenuPages.some((p) => {
      let match = this.currentPath.match(new RegExp(p));
      return match && match.length > 0;
    });
  }

  public isGdprDisplayed(): boolean {
    return this.gdprEnabled && this.gdprDisplayOn.some((p) => {
      let match = this.currentPath.match(new RegExp(p))
      return match && match.length > 0;
    });
  }

  public setLoadingMessage(message: string):void {
    this.loadingMessage = message;
  }

  public getLoadingMessage(): string {
    return this.loadingMessage;
  }

  public getTimeRemaining(displayMinus: boolean = false): string {
    let hours: number = this.stopWatchService.getHours(this.displaySWRemaining);
    let minutes: number = this.stopWatchService.getMinutes(this.displaySWRemaining);
    let seconds: number = this.stopWatchService.getSeconds(this.displaySWRemaining);
    return AppComponent.formatHour(seconds, minutes, hours, this.displaySWRemaining && displayMinus);
  }

  public setTimerRemainingTime(time: number): void {
    this.stopWatchService.setRemainingTime(time);
  }

  public getGlobalConfig(): object {
    return {...this.globalConfig};
  }

  public startTimer(time: number, callback: (() => void)): void {
    this.stopWatchService.runStopWatch(time).then(callback);
  }

  public resetTimer(): void {
    this.stopWatchService.resetTimer();
  }

  private switchTimerDisplay(): void {
    this.displaySWRemaining = !this.displaySWRemaining;
  }

  public setTimerDisplay(display: boolean): void {
    this.displaySWRemaining = display;
  }

  private stopTimer(): void {
    this.stopTimerHandler();
  }

  public pauseTimer(): void {
    this.stopWatchService.pauseTimer();
  }

  public unpauseTimer(): void {
    this.stopWatchService.startTimer();
  }

  public onStopTimer(fun: (() => void)): void {
    this.stopTimerHandler = fun;
  }

  private onMenuItemClick(): void {
    this.menuTrigger.closeMenu();
  }

  public onReady(fun: (() => void)): void {
    if (this.isReady) {
      fun();
    } else {
      setTimeout(() => { this.onReady(fun); }, 100);
    }
  }

  public onUnload(fun: (() => void)): void {
    this.beforeUnload = fun;
  }

  @HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler(event) {
    if (this.beforeUnload) {
      this.beforeUnload();
      this.beforeUnload = null;
    }
  }

  public getCurrentLanguage(): string {
      return this.selectedLang;
  }

  public getLangs(): LanguageDef[] {
      return this.langs;
  }

  public getSelectedLang(key: string): LanguageDef {
      return this.langs.find(e => e.language === key);
  }

  public getLanguageName(language: string): string {
    return this.langs.find(t => t.language === language).name;
  }

  public getLanguageMainScript(language: string): string {
    return this.langs.find(t => t.language === language).mainScript;
  }

  public getLanguageRealName(language: string): string {
    return this.langs.find(t => t.language === language).realName;
  }

  public canChangeLanguage(): boolean {
    return this.allowChangeLanguage;
  }

  public changeLanguage(language: string): void {
    if (!this.langs.find(t => t.name === language) && this.allowChangeLanguage) {
      this.selectedLang = language;
      this.storeLanguage();
    }
  }

  private storeLanguage(): void {
      let lang = this.getSelectedLang(this.selectedLang);
      this.selectedEditorLang = lang.realName;
      localStorage.setItem(USER_LANGUAGE_STORAGE_NAME, lang.language);
  }

  public onLanguageChange(fun: (language: string) => void): void {
    this.languageChangeHandler = fun;
  }

  private onLanguageChangeEvent(): void {
    this.storeLanguage();
    this.menuTrigger.closeMenu();
    this.languageChangeHandler(this.selectedLang);
  }

  public getCurrentTheme(): string {
      return this.selectedTheme;
  }

  public getThemes(): ThemeDef[] {
    return this.themes;
  }

  public getThemeName(theme: string) {
    return this.themes.find(t => t.name === theme).name;
  }

  public changeTheme(theme: string) {
    if (!this.themes.find(t => t.name === theme)) {
      this.selectedTheme = theme;
      this.storeTheme();
    }
  }

  public onThemeChange(fun: (theme: string) => void) {
    this.themeChangeHandler = fun;
  }

  private onThemeChangeEvent() {
    this.storeTheme();
    this.menuTrigger.closeMenu();
    this.themeChangeHandler(this.selectedTheme);
  }

  private storeTheme() {
    localStorage.setItem(USER_THEME_STORAGE_NAME, this.selectedTheme);
  }

  public isRecruiterMode(): boolean {
    return this.recruiterMode;
  }

  private setRecruiterMode(enable: boolean) {
    this.recruiterMode = enable;
    this.allowChangeLanguage = !this.recruiterMode;
    localStorage.setItem(RECUITER_MODE_ENABLED, '' + this.recruiterMode);
  }

  public enableRecruiterMode() {
    this.setRecruiterMode(true);
  }

  public disableRecruiterMode() {
    this.setRecruiterMode(false);
  }

  private resizeMenu() {
    this.expanded = !this.expanded;
    localStorage.setItem(MENU_EXPANDED_STORAGE_NAME, '' + this.expanded);
  }

  public navigate(paths: string[]): void {
    this.router.navigate(paths);
  }

  private openLoginModal(): void {
    this.dialogsService.login(this.currentUser).subscribe((result: any) => {
      let user: UserViewModel = result;
      if (user) {
        this.setUser(user);
        if (this.router.url && this.router.url !== '/') {
          this.setRedirectURL(this.router.url);
        }
        this.router.navigate(['/']);
      }
    });
  }

  public setRedirectURL(url: string): void {
    localStorage.setItem(REDIRECT_URL, url);
  }

  public getRedirectURL(): string {
    return localStorage.getItem(REDIRECT_URL);
  }

  private openGdprModal(): void {
    this.dialogsService.dynamic(this.gdprTitle, GdprComponent, {
        templates: this.gdprTemplates,
        companyName: this.companyName,
        siteUrl: this.siteUrl,
        currentUrl: document.location.href.replace(new RegExp(this.currentPath + '$'), ''),
        mails: this.mails,
      }).subscribe(nothing => {});
  }

  public confirmAuthent(onSuccess: ((user: UserViewModel) => void),
                        onError: ((error: Error) => void) = (e) => console.error(e)) {
    this.dialogsService.login(this.currentUser, false, 'Please confirm your identity', true).subscribe((result: any) => {
      let user: UserViewModel = result;
      if (user) {
        onSuccess(user);
      } else {
        onError(new Error('Authentication failed'));
      }
    });
  }

  public onSetUser(fun: (user: UserViewModel) => void) {
    this.setUserHandler = fun;
  }

  public isRecruiterUser(): boolean {
    return (!!this.currentUser && (this.currentUser.roles.some(r => r === Roles.ADMIN) || this.currentUser.roles.some(r => r === Roles.RECRUITER)));
  }

  public setUser(user: UserViewModel) {
    this.currentUser = user;
    let isAdmin: boolean = (!!this.currentUser && this.currentUser.roles.some(r => r === Roles.ADMIN));
    let isRecruiter: boolean = (!!this.currentUser && this.currentUser.roles.some(r => r === Roles.RECRUITER));
    this.isRecruiter = (!!this.currentUser && (isAdmin || isRecruiter));
    this.setRecruiterMode(!!this.currentUser && (this.recruiterMode === null && isRecruiter || this.recruiterMode));
    this.setUserHandler(this.currentUser);
  }

  public logout(redirect: boolean = true) : void {
    this.tokenService.removeToken();
    this.setUser(null);
    localStorage.removeItem(REDIRECT_URL);
    this.recruiterMode = null;
    localStorage.removeItem(RECUITER_MODE_ENABLED);
    if (redirect) {
      this.router.navigate(['/']);
    }
  }

  public getCurrentUser() : UserViewModel {
    return this.currentUser;
  }

  private onComponentChange(value) {
     console.log(value);
  }

  /*
   * Utility methods.
   */
  static DEFAULT_FORMAT: string = 'DD/MM/YYYY';

  public static getTimestampFromEpoch(date): number {
    return +date['epochSecond'] * 1000 + +date['nano'] / 1000000;
  }

  public static formatTimestamp(timestamp: number,
      format: string = AppComponent.DEFAULT_FORMAT): string {
    let date: Date = new Date(timestamp);
    let res : string = format;
    res = res.replace('DD', ('00' + date.getDate()).slice(-2));
    res = res.replace('D', '' + date.getDate());
    res = res.replace('MM', ('00' + (date.getMonth() + 1)).slice(-2));
    res = res.replace('M', '' + (date.getMonth() + 1));
    res = res.replace('YYYY', '' + date.getFullYear());
    res = res.replace('YY', '' + date.getFullYear() % 100);
    res = res.replace('Y', '' + date.getFullYear() % 100);
    return res;
  }

  public static selectContent(container: any): void {
    var range = document.createRange();
    range.selectNode(container);
    window.getSelection().removeAllRanges();
    window.getSelection().addRange(range);
  }

  public static formatDigit(num: number): string {
    return ('00' + num).slice(-2);
  }

  public static formatHour(seconds: number,
                           minutes: number = 0,
                           hours: number = 0,
                           displayMinus: boolean = false): string {
    let h: number = Math.floor(seconds / 3600);
    seconds -= h * 3600;
    h += hours;
    let m: number = Math.floor(seconds / 60);
    seconds -= m * 60;
    m += minutes;
    let s: number = seconds;
    return (displayMinus ? '-' : '') +
            (h < 100 ? AppComponent.formatDigit(h) : h) + ':' +
            AppComponent.formatDigit(m) + ':' +
            AppComponent.formatDigit(s);
  }

}
