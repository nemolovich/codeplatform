#!/bin/bash

target_dir="$1"
exercice="$2"

if [ -z "${target_dir}" -o -r "${BASE_PATH}/${target_dir}" ] ; then
	echo "ERROR: Target directory already exists" 1>&2
	exit 1
fi

if [ $# -lt 2 ] ; then
	echo "ERROR: Unknown exercice" 1>&2
	exit 2
fi

if [ -z "${exercice}" -o ! -r "${BASE_IN}/${exercice}" ] ; then
	echo "ERROR: Cannot find exercice '${exercice}' inputs" 1>&2
	exit 3
fi

CONFIG=true
if [ -z "${exercice}" -o ! -r "${BASE_CONFIG}/${exercice}" ] ; then
	echo "WARNING: Cannot find exercice '${exercice}' config" 1>&2
	CONFIG=false
fi

echo "Creating target path: ${BASE_PATH}/${target_dir}/{out,logs,in,config,code}"
mkdir -p ${BASE_PATH}/${target_dir}/{out,logs,in,config,code}

if [ $? -ne 0 ] ; then
	echo "ERROR: Cannot create target directory" 1>&2
	exit 5
fi

echo "Copying inputs files from: ${BASE_IN}/${exercice}"
cp -rp ${BASE_IN}/${exercice}/* ${BASE_PATH}/${target_dir}/in/

if [ $? -ne 0 ] ; then
	echo "ERROR: Cannot copy inputs files" 1>&2
	exit 6
fi

if ${CONFIG} ; then
  echo "Copying config file from: ${BASE_CONFIG}/${exercice}"
  cp -rp ${BASE_CONFIG}/${exercice}/* ${BASE_PATH}/${target_dir}/config/
fi

if [ $? -ne 0 ] ; then
	echo "ERROR: Cannot copy config file" 1>&2
	exit 7
fi

exit $?

