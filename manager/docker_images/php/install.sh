#!/bin/bash
cd /opt

# Dependencies
apt-get install -y \
    build-essential \
    pkg-config \
    git-core \
    autoconf \
    bison \
    curl \
    libxml2-dev \
    libbz2-dev \
    libmcrypt-dev \
    libicu-dev \
    libssl-dev \
    libcurl4-openssl-dev \
    libltdl-dev \
    libjpeg-dev \
    libpng-dev \
    libpspell-dev \
    libreadline-dev

mkdir /usr/local/php7

git clone https://github.com/php/php-src.git
cd /opt/php-src
git fetch --tags --prune
git checkout tags/php-${PHP_VERSION}
./buildconf --force

./configure --prefix=/usr/local/php7 \
                  --enable-mbstring \
                  --enable-zip \
                  --enable-bcmath \
                  --enable-calendar \
                  --enable-wddx \
                  --enable-intl \
                  --enable-soap \
                  --enable-sockets \
                  --with-openssl

make -j 1
make install

ln -s /usr/local/php7/bin/php /usr/local/bin/php

cd /opt
rm -rf php-src

apt-get remove -y git-core \
    autoconf \
    curl

apt-get clean
apt-get autoclean
apt-get -y autoremove
rm -rf /var/lib/apt/lists/*
rm -rf /usr/share/doc

