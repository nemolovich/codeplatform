#!/bin/bash
cd /opt

# Dependencies
apt-get install -y \
    wget \
    zip \
    unzip \
    bzip2 \
    autoconf \
    autoconf2.13 \
    make \
    build-essential \
    ccache \
    python \
    python-dev \
    zlib1g-dev \
    libncurses5-dev

wget http://ftp.gnu.org/gnu/sed/sed-${SED_VERSION}.tar.gz
tar -xzf sed-${SED_VERSION}.tar.gz
cd /opt/sed-${SED_VERSION}
./configure
make
cd /opt
mv /bin/sed /bin/sed4.4
cp -rp /opt/sed-${SED_VERSION}/sed/sed /bin/sed

rm -rf /opt/sed-${SED_VERSION} /opt/sed-${SED_VERSION}.tar.gz

wget https://ftp.mozilla.org/pub/spidermonkey/releases/${SPIDERMONKEY_VERSION}/mozjs-${SPIDERMONKEY_VERSION}.tar.bz2
tar -xjf mozjs-${SPIDERMONKEY_VERSION}.tar.bz2
cd /opt/mozjs-${SPIDERMONKEY_VERSION}/js/src
export SHELL=/bin/bash
autoconf2.13
mkdir /opt/mozjs-${SPIDERMONKEY_VERSION}/js/src/build_OPT.OBJ
cd /opt/mozjs-${SPIDERMONKEY_VERSION}/js/src/build_OPT.OBJ
/opt/mozjs-${SPIDERMONKEY_VERSION}/js/src/configure
make
make all
make install

cd /opt
rm -rf /opt/mozjs-${SPIDERMONKEY_VERSION} /opt/mozjs-${SPIDERMONKEY_VERSION}.tar.bz2

apt-get remove -y \
    wget \
    zip \
    unzip \
    bzip2 \
    autoconf \
    autoconf2.13 \
    make \
    build-essential \
    ccache \
    python \
    python-dev \
    zlib1g-dev \
    libncurses5-dev

mv /bin/sed /bin/sed${SED_VERSION}
mv /bin/sed4.4 /bin/sed

apt-get clean
apt-get autoclean
apt-get -y autoremove
rm -rf /var/lib/apt/lists/*
rm -rf /usr/share/doc


