package fr.nemolovich.apps.codeplatform.manager.docker;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is used to monitor the manager and retrive information about
 * {@link ThreadPoolExecutor executors}.
 *
 * @author bgohier
 */
class ManagerMonitor implements Runnable {

    /**
     * The time interval in ms between two monitoring tasks executions.
     */
    static final long DEFAULT_CHECK_INTERVAL = 15000L;
    private static final Logger LOGGER = Logger.getLogger(
        ManagerMonitor.class.getName());
    private final long checkInterval;
    private final Map<String, ThreadPoolExecutor> executors;

    /**
     * Construct monitor for an executor.
     *
     * @param executorName {@link String}: The name of the executor to monitor.
     * @param executor {@link ThreadPoolExecutor}: The executor to monitor.
     */
    ManagerMonitor(String executorName, ThreadPoolExecutor executor) {
        this(DEFAULT_CHECK_INTERVAL, executorName, executor);
    }

    /**
     * Construct monitor for a list of executors.
     *
     * @param executors
     * {@link Map}&lt;{@link String},{@link ThreadPoolExecutor}&gt;: The list of
     * executors with their names to monitor.
     *
     * @see #ManagerMonitor(long, java.util.Map)
     */
    ManagerMonitor(Map<String, ThreadPoolExecutor> executors) {
        this(DEFAULT_CHECK_INTERVAL, executors);
    }

    /**
     * Construct monitor for a list of executors.
     *
     * @param checkInterval {@link Long long}: The time interval in ms between
     * two monitoring tasks executions (default value:
     * {@link #DEFAULT_CHECK_INTERVAL}).
     * @param executors
     * {@link Map}&lt;{@link String},{@link ThreadPoolExecutor}&gt;: The list of
     * executors with their names to monitor.
     */
    ManagerMonitor(long checkInterval,
        Map<String, ThreadPoolExecutor> executors) {
        this.checkInterval = checkInterval;
        this.executors = new ConcurrentHashMap<>(executors);
    }

    /**
     * Construct monitor for an executor.
     *
     * @param checkInterval {@link Long long}: The time interval in ms between
     * two monitoring tasks executions (default value:
     * {@link #DEFAULT_CHECK_INTERVAL}).
     * @param executorName {@link String}: The name of the executor to monitor.
     * @param executor {@link ThreadPoolExecutor}: The executor to monitor.
     */
    ManagerMonitor(long checkInterval, String executorName,
        ThreadPoolExecutor executor) {
        this.checkInterval = checkInterval;
        this.executors = new ConcurrentHashMap<>();
        this.executors.put(executorName, executor);
    }

    /**
     * Add an executor to monitor.
     *
     * @param executorName {@link String}: The name of the executor to monitor.
     * @param executor {@link ThreadPoolExecutor}: The executor to monitor.
     *
     * @return {@link ManagerMonitor} - This object.
     */
    ManagerMonitor add(String executorName,
        ThreadPoolExecutor executor) {
        this.executors.put(executorName, executor);
        return this;
    }

    @Override
    public void run() {
        LOGGER.log(Level.FINE, "Monitoring started");
        String details;
        String executorName;
        int executorSize;
        int executorTotalSize;
        long executorTotalTasks;
        long executorTotalTasksTerminated;
        ThreadPoolExecutor executor;
        while (!Thread.currentThread().isInterrupted()) {
            StringBuilder result = new StringBuilder(
                "Monitoring information:");

            for (Entry<String, ThreadPoolExecutor> entry
                : this.executors.entrySet()) {
                executorName = entry.getKey();
                executor = entry.getValue();
                executorSize = executor.getActiveCount();
                executorTotalSize = executor.getMaximumPoolSize();
                executorTotalTasks = executor.getTaskCount();
                executorTotalTasksTerminated = executor.getCompletedTaskCount();
                details = String.format("\n\tExecutor [%s]:"
                    + "\n\t\t%-25s %10d"
                    + "\n\t\t%-25s %10d"
                    + "\n\t\t%-25s %10d"
                    + "\n\t\t%-25s %10d", executorName,
                    "Current size:", executorSize,
                    "Total size:", executorTotalSize,
                    "Total tasks:", executorTotalTasks,
                    "Total terminated tasks:", executorTotalTasksTerminated);
                result.append(details);
            }
            LOGGER.log(Level.INFO, result.toString());
            try {
                Thread.sleep(this.checkInterval);
            } catch (InterruptedException ex) {
                LOGGER.log(Level.INFO, "Monitoring interrupted");
                break;
            }
        }
        LOGGER.log(Level.INFO, "Monitoring terminated");
    }

}
