package fr.nemolovich.apps.codeplatform.manager.docker;

import java.util.concurrent.Callable;

/**
 * This {@link Callable} implementation is used to stop and remove created
 * container and remove all files used for volumes.
 *
 * @author bgohier
 */
class ContainerCleanerTask implements Callable<Boolean> {

    private final ExerciceContainer container;
    private static long nbThread = 0;

    /**
     * Consturct cleaner using {@link ExerciceContainer}.
     *
     * @param container {@link ExerciceContainer}: The container to clean.
     */
    ContainerCleanerTask(ExerciceContainer container) {
        this.container = container;
    }

    /**
     * Get the next thread number to use.
     *
     * @return {@link Long long} - Next thread number.
     */
    public static long getNextThreadNumber() {
        return nbThread++;
    }

    @Override
    public Boolean call() throws Exception {
        boolean cleaned = Manager.stopContainer(container.getContainerID());
        cleaned = cleaned && Manager.removeContainer(container.getContainerID());

        String rootPath = String.format("%s/%s/%s", DockerHelper.TEMP_DIR,
                                        container.getTeam(), container.
                                        getExercice());
        String containerBasePath = String.format("%s/%s",
                                                 DockerHelper.ENV_BASE_PATH,
                                                 "tmp_docker");
        CleanerContainer cleanerContainer = new CleanerContainer(
                         this.container.getName(), containerBasePath, rootPath);
        String cleanerContainerID = Manager.buildCleanerContainer(
               cleanerContainer);
        cleaned = cleaned && Manager.startContainer(cleanerContainerID);
        cleaned = cleaned && Manager.stopContainer(cleanerContainerID);
        cleaned = cleaned && Manager.removeContainer(cleanerContainerID);

//        cleaned = cleaned && Manager.cleanLocalEnv(container);
        return cleaned;
    }

}
