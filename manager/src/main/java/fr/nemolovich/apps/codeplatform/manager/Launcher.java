package fr.nemolovich.apps.codeplatform.manager;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import fr.nemolovich.apps.codeplatform.manager.docker.Manager;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * Command line process.
 *
 * @author bgohier
 */
public class Launcher {

    private final ArgParser parser;
    private JCommander commander;

    private Launcher(ArgParser parser) {
        this.parser = parser;
    }

    /**
     * Main class to run as command line.
     *
     * @param args {@link String}[]: Argument parameters.
     * @throws InterruptedException If the program has been stopped.
     */
    public static void main(String[] args) throws InterruptedException {
        ArgParser parser = new ArgParser();
        Launcher launcher = new Launcher(parser);
        try {
            launcher.commander = new JCommander(parser);
            launcher.commander.setProgramName(Manager.MANAGER_NAME);
            launcher.commander.parse(args);
            launcher.run();
        } catch (ParameterException pe) {
            launcher.commander.usage();
        }
    }

    private void run() throws InterruptedException {

        String result;
        if (this.parser.help) {
            displayHelp();
            result = "";
        } else if (this.parser.version) {
            System.out.printf("%s version %s\n",
                Manager.MANAGER_NAME,
                Manager.MANAGER_VERSION);
            result = "";

        } else if (!this.parser.process && (this.parser.checkImage == null
            || this.parser.checkImage.isEmpty())
            && !this.parser.listImages
            && !this.parser.buildImages
            && !this.parser.listLanguages) {
            String error = String.format(
                "%s: error: at least one of the following arguments are required: %s, %s, %s, %s, %s",
                Launcher.class
                .getSimpleName(), "-p/--process", "--check-image",
                "--list-images", "--build-images", "--list-languages"
            );
            System.err.println(error);
            displayHelp();
            result = "";
        } else if (this.parser.process
            && (this.parser.directory != null
            && !this.parser.directory.isEmpty())
            && (this.parser.exercice != null
            && !this.parser.exercice.isEmpty())
            && (this.parser.team != null
            && !this.parser.team.isEmpty())
            && (this.parser.language != null
            && !this.parser.language.isEmpty())) {
            AtomicReference<String> logRersult = new AtomicReference<>();
//            int processed;

            Manager.process(this.parser.team, this.parser.exercice,
                this.parser.language, this.parser.directory,
                this.parser.keepEnv, (results)
                -> logRersult.set(Arrays.toString(results.toArray())));
            result = logRersult.get();
        } else if (this.parser.checkImage != null
            && !this.parser.checkImage.isEmpty()) {
            result = String.format("%s is %spresent", this.parser.checkImage,
                Manager.dockerImageExists(this.parser.checkImage) ? "" : "not "
            );
        } else if (this.parser.listImages) {
            List<String> images = Manager.dockerGetImages();
            if (images.isEmpty()) {
                result = "There is no image";
            } else {
                result = String.format("Available images: %s",
                    images.stream().collect(
                        Collectors.joining("\n\t - ", "\n\t - ", "")));
            }
        } else if (this.parser.buildImages) {
            List<String> currentImages = Manager.dockerGetImages();
            List<String> expectedImages = Manager.dockerExpectedImages();
            List<String> imagesToBuild = expectedImages.stream().filter(image
                -> !currentImages.contains(image))
                .collect(Collectors.toList());
            System.out.println(String.format("Building images: %s",
                Arrays.toString(imagesToBuild.toArray())));
            result = imagesToBuild.stream().map(image
                -> Manager.dockerBuildImage(image))
                .allMatch((r) -> r) ? "Build success" : "Build error";
        } else if (this.parser.listLanguages) {
            List<String> languages = Manager.availableLanguages();
            if (languages.isEmpty()) {
                result = "There is no available languages";
            } else {
                result = String.format("Available languages: %s",
                    languages.stream().collect(
                        Collectors.joining("\n\t - ", "\n\t - ", "")));
            }
        } else {
            displayHelp();
            result = "";
        }
        System.out.println(result);
        Manager.stopProcesses();

        if (!Manager.DEBUG) {
            System.out.println("Exit called");
            System.exit(0);
        }
    }

    private void displayHelp() {
        this.commander.usage();
    }

    @Override
    public String toString() {
        return "Launcher{" + "parser=" + parser + '}';
    }

}
