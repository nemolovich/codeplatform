package fr.nemolovich.apps.codeplatform.manager.props;

/**
 * Property model for languages.
 *
 * @author bgohier
 */
public final class LanguageProperty {

    private String language;
    private Integer priority;

    /**
     * Return the language name.
     *
     * @return {@link String} - The language name.
     */
    public String getLanguage() {
        return this.language;
    }

    /**
     * Set the language name.
     *
     * @param language {@link String}: The language name.
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * Return the language build priority order.
     *
     * @return {@link Integer} - The language build priority order.
     */
    public Integer getPriority() {
        return this.priority;
    }

    /**
     * Set the language build priority order.
     *
     * @param priority {@link Integer}: The language build priority order.
     */
    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "LanguageProperty{" + "language=" + this.language
            + ", priority=" + this.priority + '}';
    }
}
