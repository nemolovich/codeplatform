package fr.nemolovich.apps.codeplatform.manager.docker;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.nemolovich.apps.codeplatform.manager.props.ExerciceProperty;

/**
 * A model that represents a docker container with required informations about
 * exercice execution information.
 *
 * @author bgohier
 */
class ExerciceContainer {

    private final File baseDir;
    private String containerID;
    private final String exercice;
    private final String team;
    private final String name;
    private final String language;
    private final String image;
    private final Predicate<Path> fileFilter;
    private final Map<String, String> volumes;
    private final Map<String, String> io;
    private final ExerciceProperty config;

    /**
     * Create an exercice container for a specific submission.
     *
     * @param team {@link String}: The team name.
     * @param exercice {@link String}: The exercice name.
     * @param language {@link String}: The language to use.
     * @param baseDir {@link File}: The base directory to take code files.
     *
     * @see #ExerciceContainer(java.lang.String, java.lang.String,
     * java.lang.String, java.io.File, java.util.function.Predicate,
     * java.lang.String)
     */
    ExerciceContainer(String team, String exercice, String language,
        File baseDir) {
        this(team, exercice, language, baseDir, (p) -> true, null);
    }

    /**
     * Create an exercice container for a specific submission.
     *
     * @param team {@link String}: The team name.
     * @param exercice {@link String}: The exercice name.
     * @param language {@link String}: The language to use.
     * @param baseDir {@link File}: The base directory to take code files.
     * @param nameSuffix {@link String}: The suffix of the container name.
     *
     * @see #ExerciceContainer(java.lang.String, java.lang.String,
     * java.lang.String, java.io.File, java.util.function.Predicate,
     * java.lang.String)
     */
    ExerciceContainer(String team, String exercice, String language,
        File baseDir, String nameSuffix) {
        this(team, exercice, language, baseDir, (p) -> true, nameSuffix);
    }

    /**
     * Create an exercice container for a specific submission.
     *
     * @param team {@link String}: The team name.
     * @param exercice {@link String}: The exercice name.
     * @param language {@link String}: The language to use.
     * @param baseDir {@link File}: The base directory to take code files.
     * @param fileFilter {@link Predicate}&lt;{@link Path}&gt;: The filter to
     * use for code files.
     *
     * @see #ExerciceContainer(java.lang.String, java.lang.String,
     * java.lang.String, java.io.File, java.util.function.Predicate,
     * java.lang.String)
     */
    ExerciceContainer(String team, String exercice, String language,
        File baseDir, Predicate<Path> fileFilter) {
        this(team, exercice, language, baseDir, fileFilter, null);
    }

    /**
     * Create an exercice container for a specific submission.
     *
     * @param team {@link String}: The team name.
     * @param exercice {@link String}: The exercice name.
     * @param language {@link String}: The language to use.
     * @param baseDir {@link File}: The base directory to take code files.
     * @param fileFilter {@link Predicate}&lt;{@link Path}&gt;: The filter to
     * use for code files.
     * @param nameSuffix {@link String}: The suffix of the container name.
     */
    ExerciceContainer(String team, String exercice, String language,
        File baseDir, Predicate<Path> fileFilter, String nameSuffix) {

        this.baseDir = baseDir;
        this.team = team;
        this.exercice = exercice;
        this.name = this.contructName(nameSuffix);
        this.language = language;
        this.image = DockerHelper.getImageName(this.language);
        this.volumes = DockerHelper.getVolumes(this.team, this.exercice,
            this.name);
        this.fileFilter = fileFilter;
        this.io = DockerHelper.getIOPath(this.team, this.exercice,
            this.name);
        this.config = DockerHelper.getGlobalConfig().merge(
            DockerHelper.getConfig(this.exercice));
    }

    /**
     * Return all code files.
     *
     * @return {@link Queue}&lt;{@link File}&gt; - The code files of the
     * submission.
     */
    Queue<File> getFiles() {
        Queue<File> filesList = new ConcurrentLinkedQueue<>();
        try {
            filesList = new ConcurrentLinkedQueue(
                Files.walk(baseDir.toPath()).
                filter(fileFilter)
                .map(p -> new File(p.toAbsolutePath().toUri()))
                .collect(Collectors.toList()));
        } catch (IOException ex) {
            Logger.getLogger(ExerciceContainer.class.getName()).log(
                Level.SEVERE, "Cannot browse code files", ex);
        }
        return filesList;
    }

    /**
     * Return the code base directory.
     *
     * @return {@link File} - The code directory.
     */
    File getBaseDir() {
        return this.baseDir;
    }

    /**
     * Return the exercice name.
     *
     * @return {@link String} - The exercice name.
     */
    String getExercice() {
        return this.exercice;
    }

    /**
     * Return the team name.
     *
     * @return {@link String} - The team name.
     */
    String getTeam() {
        return this.team;
    }

    /**
     * Return the container name.
     *
     * @return {@link String} - The container name.
     */
    String getName() {
        return this.name;
    }

    /**
     * Return the language.
     *
     * @return {@link String} - The language.
     */
    String getLanguage() {
        return this.language;
    }

    /**
     * Return the container image name.
     *
     * @return {@link String} - The container image name.
     */
    String getImage() {
        return this.image;
    }

    /**
     * Return the container volumes bindings.
     *
     * @return {@link Map}&lt;{@link String},{@link String}&gt; - The container
     * volumes bindings.
     */
    Map<String, String> getVolumes() {
        return this.volumes;
    }

    /**
     * Return the host directories.
     *
     * @return {@link Map}&lt;{@link String},{@link String}&gt; - The host
     * directories.
     */
    Map<String, String> getIo() {
        return this.io;
    }

    /**
     * Return the exercice properties.
     *
     * @return {@link ExerciceProperty} - The exercice configuration.
     */
    ExerciceProperty getConfig() {
        return this.config;
    }

    /**
     * The docker container ID.
     *
     * @return {@link String} - The docker container ID.
     */
    String getContainerID() {
        return containerID;
    }

    /**
     * Set the docker container ID.
     *
     * @param containerID {@link String}: The docker container ID.
     */
    void setContainerID(String containerID) {
        this.containerID = containerID;
    }

    /**
     * Get the container name based on the team, the exercice and specific
     * suffix.
     *
     * @param suffix {@link String}: The container name suffix. If the suffix is
     * <code>null</code> then create a random suffix.
     *
     * @return {@link String} - The container name.
     */
    private String contructName(String suffix) {
        return Manager.contructName(this.team, this.exercice, suffix);
    }

    @Override
    public String toString() {
        return String.format("%s:" + String.join("", Collections.nCopies(10,
            "\n\t%-20s: %s")),
            ExerciceContainer.class.getSimpleName(),
            "name", this.name,
            "team", this.team,
            "exercice", this.exercice,
            "baseDir", this.baseDir,
            "language", this.language,
            "image", this.image,
            "volumes", this.volumes,
            "ioPath", this.io,
            "config", this.config,
            "containerID", this.containerID
        );
    }

}
