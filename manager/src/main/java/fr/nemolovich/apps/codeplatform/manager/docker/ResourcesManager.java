package fr.nemolovich.apps.codeplatform.manager.docker;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility class that provides methods to manage resources.
 *
 * @author bgohier
 */
public final class ResourcesManager {

    private static final Logger LOGGER = Logger.getLogger(
                                ResourcesManager.class.getName());

    /**
     * Utility class hidden constructor.
     */
    private ResourcesManager() {
        super();
    }

    /**
     * Copy content of an exercice into expected folders. This include the input
     * files, the output files, the resolver Java class and the config file.
     *
     * @param exercice {@link String}: The exercice name.
     * @param inputs {@link List}&lt;{@link File}&gt;: The list of input files
     * to copy.
     * @param outputs {@link List}&lt;{@link File}&gt;: The list of output files
     * to copy.
     * @param resolver {@link File}: The Java resolver file to copy.
     * @param config {@link File}: The exercice config file to copy.
     * @return {@link Boolean boolean} - <code>true</code> if all files has been
     * successfully copied, <code>fase</code> otherwise.
     */
    public static final boolean addExercice(String exercice, List<File> inputs,
                                            List<File> outputs, File resolver,
                                            File config) {
        Map<String, byte[]> inputsContent = new HashMap<>();
        Map<String, byte[]> outputsContent = new HashMap<>();
        byte[] resolverContent = null;
        byte[] configContent = null;

        if (inputs != null) {
            inputs.forEach((input) ->
                {
                    try {
                        inputsContent.put(input.getName(),
                                          Files.readAllBytes(input.toPath()));
                    } catch (IOException ex) {
                        LOGGER.log(Level.SEVERE, String.format(
                                   "Cannot read input file '%s'", input), ex);
                    }
                });
        }
        if (outputs != null) {
            outputs.forEach((output) ->
                {
                    try {
                        outputsContent.put(output.getName(),
                                           Files.readAllBytes(output.toPath()));
                    } catch (IOException ex) {
                        LOGGER.log(Level.SEVERE, String.format(
                                   "Cannot read output file '%s'", output), ex);
                    }
                });
        }
        if (resolver != null && resolver.exists()) {
            try {
                resolverContent = Files.readAllBytes(resolver.toPath());
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, String.format(
                           "Cannot read resolver file '%s'", resolver), ex);
            }
        }
        if (config != null && config.exists()) {
            try {
                configContent = Files.readAllBytes(config.toPath());
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, String.format(
                           "Cannot read config file '%s'", config), ex);
            }
        }

        return addExercice(exercice, inputsContent, outputsContent,
                           resolverContent, configContent);
    }

    /**
     * Copy content of an exercice into expected folders. This include the input
     * files, the output files, the resolver Java class and the config file.
     *
     * @param exercice {@link String}: The exercice name.
     * @param inputsContent {@link List}&lt;{@link File}&gt;: The list of input
     * files to copy.
     * @param outputsContent {@link List}&lt;{@link File}&gt;: The list of
     * output files to copy.
     * @param resolverContent {@link File}: The Java resolver file to copy.
     * @param configContent {@link File}: The exercice config file to copy.
     * @return {@link Boolean boolean} - <code>true</code> if all files has been
     * successfully copied, <code>fase</code> otherwise.
     */
    public static final boolean addExercice(String exercice,
                                            Map<String, byte[]> inputsContent,
                                            Map<String, byte[]> outputsContent,
                                            byte[] resolverContent,
                                            byte[] configContent) {
        boolean created = true;

        if (inputsContent != null && !inputsContent.isEmpty()) {
            File inputFolder = new File(DockerHelper
                 .getInputsFolder(exercice));
            try {
                boolean clean = !inputFolder.exists() ||
                                (inputFolder.listFiles().length == 0 ||
                                 Files.walk(inputFolder.toPath())
                                 .sorted((o1, o2) -> -o1.compareTo(o2))
                                 .map(Path::toFile).map(File::delete)
                                 .allMatch((r) -> r));
                if (!clean) {
                    throw new IOException("Some files could not been deleted");
                }
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, String.format(
                           "Cannot clean directory content in '%s'",
                           inputFolder), ex);
            }
            created &= inputsContent.entrySet().stream().allMatch((entry) ->
            addInput(exercice, entry.getKey(), entry.getValue()));
        }
        if (outputsContent != null && !outputsContent.isEmpty()) {
            File outputFolder = new File(DockerHelper
                 .getOutputsFolder(exercice));
            try {
                boolean clean = !outputFolder.exists() ||
                                (outputFolder.listFiles().length == 0 ||
                                 Files.walk(outputFolder.toPath())
                                 .sorted((o1, o2) -> -o1.compareTo(o2))
                                 .map(Path::toFile).map(File::delete)
                                 .allMatch((r) -> r));
                if (!clean) {
                    throw new IOException("Some files could not been deleted");
                }
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, String.format(
                           "Cannot clean directory content in '%s'",
                           outputFolder), ex);
            }
            created &= outputsContent.entrySet().stream().allMatch((entry) ->
            addOutput(exercice, entry.getKey(), entry.getValue()));
        }
        if (resolverContent != null && resolverContent.length > 0) {
            created &= setConfig(exercice, resolverContent);
        }
        if (configContent != null && configContent.length > 0) {
            created &= setConfig(exercice, configContent);
        }
        return created;
    }

    public static final boolean addInput(String exercice, String inputName,
                                         byte[] inputContent) {
        boolean created = false;

        File inputFolder = new File(DockerHelper.getInputsFolder(exercice));
        File targetFile = new File(inputFolder, inputName);

        try {
            if (!inputFolder.isDirectory() && !inputFolder.mkdirs()) {
                throw new IOException(String.format(
                    "Cannot create input folder '%s'", inputFolder));
            }
            if (!targetFile.createNewFile()) {
                throw new IOException(String.format(
                    "Cannot create input file '%s'", targetFile));
            }
            Files.write(targetFile.toPath(), inputContent,
                        StandardOpenOption.TRUNCATE_EXISTING);
            created = true;
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, String.format(
                       "Cannot create input file to '%s'", targetFile), ex);
        }

        return created;
    }

    public static final boolean addOutput(String exercice, String outputName,
                                          byte[] outputContent) {
        boolean created = false;

        File outputFolder = new File(DockerHelper.getOutputsFolder(exercice));
        File targetFile = new File(outputFolder, outputName);

        try {
            if (!outputFolder.isDirectory() && !outputFolder.mkdirs()) {
                throw new IOException(String.format(
                    "Cannot create output folder '%s'", outputFolder));
            }
            if (!targetFile.createNewFile()) {
                throw new IOException(String.format(
                    "Cannot create output file '%s'", targetFile));
            }
            Files.write(targetFile.toPath(), outputContent,
                        StandardOpenOption.TRUNCATE_EXISTING);
            created = true;
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, String.format(
                       "Cannot create output file to '%s'", targetFile), ex);
        }

        return created;
    }

    public static final boolean setConfig(String exercice, byte[] configContent) {
        boolean created = false;

        File configFolder = new File(DockerHelper.getConfigFolder(exercice));
        File targetFile = new File(configFolder, DockerHelper.CONFIG_FILE_NAME);

        try {
            if (!configFolder.isDirectory() && !configFolder.mkdirs()) {
                throw new IOException(String.format(
                    "Cannot create config folder '%s'", configFolder));
            }
            if (!targetFile.createNewFile()) {
                throw new IOException(String.format(
                    "Cannot create config file '%s'", targetFile));
            }
            Files.write(targetFile.toPath(), configContent,
                        StandardOpenOption.TRUNCATE_EXISTING);
            created = true;
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, String.format(
                       "Cannot create config file to '%s'", targetFile), ex);
        }

        return created;
    }

    public static final boolean setResolver(String exercice,
                                            byte[] resolverContent) {
        boolean created = false;

        File resolverFolder = new File(DockerHelper.getResolverFolder(exercice));
        File targetFile = new File(resolverFolder, "Resolver.java");
        File reloadFlag = new File(resolverFolder, "Resolver.reload");

        try {
            if (!resolverFolder.isDirectory() && !resolverFolder.mkdirs()) {
                throw new IOException(String.format(
                    "Cannot create resolver folder '%s'", resolverFolder));
            }
            if (!targetFile.createNewFile()) {
                throw new IOException(String.format(
                    "Cannot create resolver file '%s'", targetFile));
            }
            Files.write(targetFile.toPath(), resolverContent,
                        StandardOpenOption.TRUNCATE_EXISTING);
            Files.write(reloadFlag.toPath(), new byte[0],
                        StandardOpenOption.TRUNCATE_EXISTING);
            created = true;
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, String.format(
                       "Cannot create resolver file to '%s'", targetFile), ex);
        }

        return created;
    }

}
