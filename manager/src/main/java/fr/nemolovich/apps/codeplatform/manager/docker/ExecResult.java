package fr.nemolovich.apps.codeplatform.manager.docker;

/**
 * Define a docker exec command result.
 *
 * @author bgohier
 */
public class ExecResult {

    private final String logs;
    private final int retCode;

    /**
     * Build an exec result.
     *
     * @param retCode {@link Integer int}: The exec return code.
     * @param logs {@link String}: The exec logs.
     */
    public ExecResult(int retCode, String logs) {
        this.logs = logs;
        this.retCode = retCode;
    }

    /**
     * Return the exec command log.
     *
     * @return {@link String}: The exec logs.
     */
    public String getLogs() {
        return logs;
    }

    /**
     * Return the exec command return code.
     *
     * @return {@link Integer int}: The exec return code.
     */
    public int getRetCode() {
        return retCode;
    }

    @Override
    public String toString() {
        return "ExecResult{" + "logs=" + logs + ", retCode=" + retCode + '}';
    }

}
