package fr.nemolovich.apps.codeplatform.manager.docker;

import com.spotify.docker.client.exceptions.DockerException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A model that represents a docker builder container used to construct all
 * execution environment volumes.
 *
 * @author bgohier
 */
class BuilderContainer {

    private static final BuilderContainer INSTANCE;

    /**
     * The path of the temporary directory into container.
     */
    public static final String BASE_PATH = "/var/tmp_docker";
    /**
     * The path of the inputs directory into container.
     */
    public static final String BASE_IN = "/var/tmp_in";
    /**
     * The path of the configs directory into container.
     */
    public static final String BASE_CONFIG = "/var/tmp_config";
    /**
     * The container script to launch to build environment.
     */
    public static final String BUILDER_SCRIPT = "/root/build.sh";

    static {
        INSTANCE = new BuilderContainer();
    }

    private String containerID;
    private final String cotnainerName;
    private final Map<String, String> volumes;

    /**
     * Private constructor for singleton.
     */
    private BuilderContainer() {
        this.cotnainerName = String.format("%s_manager_builder_%s",
                                           Manager.MANAGER_NAME.toLowerCase(),
                                           Manager.MANAGER_VERSION.toLowerCase());
        this.volumes = new ConcurrentHashMap<>();
        this.volumes.put(BASE_PATH, DockerHelper.TEMP_DIR);
        this.volumes.put(BASE_IN, DockerHelper.INPUTS_DIR);
        this.volumes.put(BASE_CONFIG, String.format(
                         "%s/%s", DockerHelper.EXERCICES_OUTER_VOLUME,
                         DockerHelper.VOLUME_CONFIG));
    }

    /**
     * Return the unique instance of the container.
     *
     * @return {@link BuilderContainer} - The unique instance.
     */
    public static BuilderContainer getInstance() {
        return INSTANCE;
    }

    /**
     * The docker container ID.
     *
     * @return {@link String} - The docker container ID.
     */
    String getContainerID() {
        return this.containerID;
    }

    /**
     * Set the docker container ID.
     *
     * @param containerID {@link String}: The docker container ID.
     */
    void setContainerID(String containerID) {
        this.containerID = containerID;
    }

    /**
     * Return the container name.
     *
     * @return {@link String} - The name of the docker container.
     */
    public String getCotnainerName() {
        return this.cotnainerName;
    }

    /**
     * Return the list of volumes used by the container.
     *
     * @return {@link Map}&lt;${@link String},${@link String}&gt; - An
     * unmodifiable Map container container volumes as keys and hosts volumes as
     * values.
     */
    Map<String, String> getVolumes() {
        return Collections.unmodifiableMap(volumes);
    }

    boolean builEnv(ExerciceContainer ec) {
        boolean result = false;
        try {
            String containerFolder = String.format(
                   "%s/%s/%s", ec.getTeam(),
                   ec.getExercice(), ec.getName());
            ExecResult exec = DockerHelper.execContainer(
                       this.containerID, Arrays.asList(
                           BuilderContainer.BUILDER_SCRIPT,
                           containerFolder, ec.getExercice()));
            if (exec.getRetCode() == 0) {
                String containerPath = String.format(
                       "%s/%s/%s", BASE_PATH, containerFolder,
                       DockerHelper.VOLUME_CODE);
                String rootPath = String.format(
                       "%s/.", ec.getBaseDir().getAbsolutePath());
                DockerHelper.copyContainer(this.containerID, rootPath,
                                           containerPath);
                result = true;
            } else {
                Logger.getLogger(BuilderContainer.class.getName()).log(
                    Level.SEVERE, String.format(
                        "Cannot exec build environment container:\n%s",
                        exec.getLogs()));
            }
        } catch (DockerException | InterruptedException | IOException ex) {
            Logger.getLogger(BuilderContainer.class.getName()).log(Level.SEVERE,
                                                                   "Cannot build environment",
                                                                   ex);
        }
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s:".concat(String.join("", Collections
                                                 .nCopies(4, "\n\t%-20s: %s"))),
                             BuilderContainer.class.getSimpleName(),
                             "containerName", this.cotnainerName,
                             "volumes", this.volumes,
                             "containerID", this.containerID
        );
    }

}
