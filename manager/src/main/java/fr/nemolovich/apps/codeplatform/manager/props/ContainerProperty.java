package fr.nemolovich.apps.codeplatform.manager.props;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The properties model for container.
 *
 * @author bgohier
 */
public class ContainerProperty {

    private String[] env;

    /**
     * Default constructor.
     */
    public ContainerProperty() {
    }

    /**
     * Copy constructor.
     *
     * @param containerProperty {@link ContainerProperty}: The property to copy.
     */
    public ContainerProperty(ContainerProperty containerProperty) {
        this.env = null;
        this.env = new String[containerProperty.env.length];
        System.arraycopy(containerProperty.env, 0, this.env, 0,
            containerProperty.env.length);
    }

    /**
     * The list of environment variables to use for the container.
     *
     * @return {@link String}[] - The list of environment variables.
     */
    public String[] getEnv() {
        return this.env;
    }

    /**
     * Set the configuration environment variables.
     *
     * @param env {@link String}[] - The environment variables.
     */
    public void setEnv(String[] env) {
        this.env = env;
    }

    /**
     * Return a map of environment variable as key -&gt; value.
     *
     * @return {@link Map}&lt;{@link String},{@link String}&gt; - The
     * environment variables name map to its value.
     */
    public Map<String, String> getEnvMap() {
        Map<String, String> result = new ConcurrentHashMap<>();
        for (String variable : this.env) {
            Matcher m = Pattern.compile(
                "^(?<name>\\w+)=(?<value>.*)$").matcher(variable);
            if (m.matches()) {
                result.put(m.group("name"), m.group("value"));
            }
        }
        return result;
    }

    @Override
    public String toString() {
        return ContainerProperty.class.getSimpleName() + "{" + "env="
            + Arrays.toString(this.env) + '}';
    }
}
