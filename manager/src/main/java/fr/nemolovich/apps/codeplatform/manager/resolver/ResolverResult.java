package fr.nemolovich.apps.codeplatform.manager.resolver;

/**
 * A resolver result contains information about an exercice result.
 *
 * @author bgohier
 */
public class ResolverResult {

    private String resultName;
    private String logResult;
    private String stdout;
    private String stderr;
    private float result;

    /**
     * Private constructor.
     */
    private ResolverResult() {
        resultName = null;
        logResult = null;
        stderr = null;
        stderr = null;
        result = -1f;
    }

    /**
     * Construct a resolver result with a name, a log result and a score.
     *
     * @param resultName {@link String}: The result name.
     * @param logResult {@link String}: The result output.
     * @param result {@link Float float}: The result score.
     */
    public ResolverResult(final String resultName, final String logResult,
        final float result) {
        this.resultName = resultName;
        this.logResult = logResult;
        this.result = result;
    }

    /**
     * Construct a resolver result with a log result and a score.
     *
     * @param logResult {@link String}: The result output.
     * @param result {@link Float float}: The result score.
     */
    public ResolverResult(final String logResult, final float result) {
        this.logResult = logResult;
        this.result = result;
    }

    /**
     * Return the result ouput.
     *
     * @return {@link String} - The result output.
     */
    public String getLogResult() {
        return logResult;
    }

    /**
     * Set the result ouput.
     *
     * @param logResult {@link String}: The result output.
     */
    public void setLogResult(final String logResult) {
        this.logResult = logResult;
    }

    /**
     * Return the result score.
     *
     * @return {@link Float float} - The result score.
     */
    public float getResult() {
        return result;
    }

    /**
     * Set the result score.
     *
     * @param result {@link Float float}: The result score.
     */
    public void setResult(final float result) {
        this.result = result;
    }

    /**
     * Add score result to current score.
     *
     * @param result {@link Float float}: The result score to add.
     *
     * @return {@link ResolverResult} - This object.
     */
    public ResolverResult addResult(final float result) {
        this.result += result;
        return this;
    }

    /**
     * Set the result name.
     *
     * @param resultName {@link String}: The result name.
     */
    public void setResultName(String resultName) {
        this.resultName = resultName;
    }

    /**
     * Return the result name.
     *
     * @return {@link String} - The result name.
     */
    public String getResultName() {
        return resultName;
    }

    /**
     * set stdout
     *
     * @param stdout {@link String}: stdout
     */
    public void setStdout(String stdout) {
        this.stdout = stdout;
    }

    /**
     * Return stdout
     *
     * @return {@link String} - stdout
     */
    public String getStdout() {
        return stdout;
    }

    /**
     * set stderr
     *
     * @param stderr {@link String}: stderr
     */
    public void setStderr(String stderr) {
        this.stderr = stderr;
    }

    /**
     * Return stderr
     *
     * @return {@link String} - stderr
     */
    public String getStderr() {
        return stderr;
    }

    @Override
    public String toString() {
        return "ResolverResult{" + "resultName=\"" + resultName
            + "\", logResult=\"" + logResult + "\", result=\"" + result
            + "\", stdout=\"" + stdout + "\", stderr=\"" + stderr + "\"}";
    }

}
