package fr.nemolovich.apps.codeplatform.manager.resolver;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * The exercice resolver will compile and run a resolver implementation on each
 * outputs files genrated by a container execution.
 *
 * @author bgohier
 */
public final class ExerciceResolver {

    private ExerciceResolver() {
        /*
         * Utility class.
         */
    }

    private static final Logger LOGGER = Logger.getLogger(
                                ExerciceResolver.class.getName());

    /**
     * Resolve an exercice using a resolver implementation by comparing
     * exepected output with genrated output.
     *
     * @param resolverPath {@link String}: The resolver path containing
     * <code>Resolver.java</code> java file.
     * @param logFile {@link String}: The file path in witch to log result
     * output.
     * @param targetFilePath {@link String}: The target output file path.
     * @param sourceFilePath {@link String}: The genrated output file path.
     *
     * @return {@link ResolverResult}: The result of the resolver.
     */
    public static ResolverResult resolve(final String resolverPath,
                                         final String logFile,
                                         final String targetFilePath,
                                         final String sourceFilePath) {
        return resolve(resolverPath, logFile, null, targetFilePath,
                       sourceFilePath);
    }

    /**
     * Resolve an exercice using a resolver implementation by comparing
     * exepected output with genrated output.
     *
     * @param resolverPath {@link String}: The resolver path containing
     * <code>Resolver.java</code> java file.
     * @param logFile {@link String}: The file path in witch to log result
     * output.
     * @param inputFilePath {@link String}: The input output file path.
     * @param targetFilePath {@link String}: The target output file path.
     * @param sourceFilePath {@link String}: The genrated output file path.
     *
     * @return {@link ResolverResult}: The result of the resolver.
     */
    public static ResolverResult resolve(final String resolverPath,
                                         final String logFile,
                                         final String inputFilePath,
                                         final String targetFilePath,
                                         final String sourceFilePath) {

        ResolverResult result;

        /**
         * Resolver to use.
         */
        IResolver resolver = ResolverUtils.getResolver(resolverPath);

        if (resolver == null) {
            LOGGER.log(Level.SEVERE, String.format(
                       "ERROR: Resolver cannot not be found from '%s'",
                       resolverPath));
            result = null;
        } else {
            Logger logger = Logger.getAnonymousLogger();
            final Formatter formatter = new Formatter() {
                            @Override
                            public String format(LogRecord record) {
                                return record.getMessage();
                            }
                        };
            FileHandler handler = null;
            List<Logger> loggers = new ArrayList<>();
            loggers.add(LOGGER);
            try {
                handler = new FileHandler(logFile);
                handler.setFormatter(formatter);
                logger.addHandler(handler);
                handler.setLevel(Level.ALL);
                LOGGER.log(Level.INFO, String.format("Log into file '%s'",
                                                     logFile));
                loggers.add(logger);
            } catch (IOException | SecurityException ex) {
                LOGGER.log(Level.SEVERE, String.format(
                           "ERROR: Cannot log into file '%s'", logFile), ex);
            }

            String resultName = new File(targetFilePath).getName()
                   .replaceAll("\\.out$", "");
            /*
             * Runtime error file.
             */
            File runErrorFile = new File(new File(sourceFilePath)
                 .getParentFile(), "run.err");
            /*
             * Result error file.
             */
            File errorFile = new File(sourceFilePath.replaceAll("\\.out$",
                                                                ".err"));

            if (runErrorFile.exists()) {
                /* 
                 * If runtime error then stop.
                 */
                try {
                    String message = Files.readAllLines(runErrorFile.toPath())
                           .stream().collect(Collectors.joining("\n"));
                    result = new ResolverResult(resultName, message, 0f);
                    result.setStderr(message);
                } catch (IOException ex) {
                    LOGGER.log(Level.SEVERE, String.format(
                               "ERROR: Cannot read run error file content from '%s'",
                               runErrorFile.getAbsolutePath()), ex);
                    result = new ResolverResult(resultName,
                                                "Runtime error", 0f);
                }
            } else {
                String stderr = null;
                if (errorFile.exists()) {
                    /* 
                     * If resolver error then stop.
                     */
                    try {
                        String message = Files.readAllLines(errorFile.toPath())
                               .stream().collect(Collectors.joining("\n"));
                        stderr = message;
                    } catch (IOException ex) {
                        LOGGER.log(Level.SEVERE, String.format(
                                   "ERROR: Cannot read error file content from '%s'",
                                   errorFile.getAbsolutePath()), ex);
                    }
                }
                /*
                 * Run resolver.
                 */
                try {
                    result = resolver.resolve(inputFilePath, targetFilePath,
                                              sourceFilePath,
                                              loggers.toArray(new Logger[0]));
                } catch (RuntimeException re) {
                    result = new ResolverResult(resultName, String.format(
                                                "Resolver runtime error (%s)",
                                                re.getMessage()), 0f);
                }
                if (result.getStderr() == null) {
                    result.setStderr(stderr);
                }
                if (result.getResultName() == null) {
                    result.setResultName(resultName);
                }
            }
            if (handler != null) {
                handler.close();
            }
        }
        return result;
    }

    /**
     * Get a mapping between target files and generated files.
     *
     * @param targetPath {@link String}: The target folder path.
     * @param sourcePath {@link String}: The generated folder path.
     *
     * @return {@link Map}&lt;{@link String},{@link String}&gt; The mapping
     * between target files and generated files.
     */
    public static Map<String, String> getOutputsMapping(String targetPath,
                                                        String sourcePath) {

        File target = new File(targetPath);
        File source = new File(sourcePath);

        Map<String, String> result = new ConcurrentHashMap<>();

        try {
            List<File> targetFiles = Files.walk(target.toPath())
                       .filter(p -> p.toString().endsWith(".out"))
                       .map(p -> new File(p.toAbsolutePath().toUri()))
                       .collect(Collectors.toList());
            List<File> sourceFiles = Files.walk(source.toPath())
                       .filter(p -> p.toString().endsWith(".out"))
                       .map(p -> new File(p.toAbsolutePath().toUri()))
                       .collect(Collectors.toList());
            File sourceFile;
            for (File targetFile : targetFiles) {
                sourceFile = sourceFiles.stream()
                .filter(f -> f.getName().equals(targetFile.getName()))
                .findFirst().orElse(null);
                if (sourceFile == null || !sourceFile.exists()) {
                    Logger.getLogger(ExerciceResolver.class.getName()).log(
                        Level.SEVERE, String.format(
                            "ERROR: Cannot find file '%s' into source path '%s'",
                            targetFile.getName(), source.getAbsolutePath()));
                } else {
                    result.put(targetFile.getAbsolutePath(),
                               sourceFile.getAbsolutePath());
                }
            }
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, "ERROR: Cannot browse files to resolve",
                       ex);
        }

        return Collections.unmodifiableMap(result);
    }

    /**
     * Get a mapping between target files and generated log files.
     *
     * @param targetPath {@link String}: The target folder path.
     * @param logsPath {@link String}: The generated logs folder path.
     *
     * @return {@link Map}&lt;{@link String},{@link String}&gt; The mapping
     * between target files and generated log files.
     */
    public static Map<String, String> getLogsMapping(String targetPath,
                                                     String logsPath) {

        File target = new File(targetPath);

        Map<String, String> result = new ConcurrentHashMap<>();

        try {
            List<File> targetFiles = Files.walk(target.toPath())
                       .filter(p -> p.toString().endsWith(".out"))
                       .map(p -> new File(p.toAbsolutePath().toUri()))
                       .collect(Collectors.toList());
            File logFile;
            String fileName;
            for (File targetFile : targetFiles) {
                fileName = targetFile.getName();
                logFile = new File(String.format("%s/%s.log",
                                                 logsPath, fileName
                                                 .substring(0, fileName.
                                                            lastIndexOf('.'))));
                result.put(targetFile.getAbsolutePath(),
                           logFile.getAbsolutePath());
            }
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, "ERROR: Cannot browse files for logs", ex);
        }

        return Collections.unmodifiableMap(result);
    }

    /**
     * Get a mapping between target files and input files.
     *
     * @param targetPath {@link String}: The target folder path.
     * @param inputsPath {@link String}: The inputs folder path.
     *
     * @return {@link Map}&lt;{@link String},{@link String}&gt; The mapping
     * between target files and input files.
     */
    public static Map<String, String> getInputsMapping(String targetPath,
                                                       String inputsPath) {

        File target = new File(targetPath);

        Map<String, String> result = new ConcurrentHashMap<>();

        try {
            List<File> targetFiles = Files.walk(target.toPath())
                       .filter(p -> p.toString().endsWith(".out"))
                       .map(p -> new File(p.toAbsolutePath().toUri()))
                       .collect(Collectors.toList());
            File inputFile;
            String fileName;
            for (File targetFile : targetFiles) {
                fileName = targetFile.getName();
                inputFile = new File(String.format("%s/%s.in",
                                                   inputsPath, fileName.
                                                   substring(0, fileName.
                                                             lastIndexOf('.'))));
                result.put(targetFile.getAbsolutePath(),
                           inputFile.getAbsolutePath());
            }
        } catch (IOException ex) {
            LOGGER.
                log(Level.SEVERE, "ERROR: Cannot browse files for inputs", ex);
        }

        return Collections.unmodifiableMap(result);
    }

}
