package fr.nemolovich.apps.codeplatform.manager;

import com.beust.jcommander.Parameter;

/**
 *
 * @author bgohier
 */
class ArgParser {

    @Parameter(names = {"-h", "--help"}, description = "Display this help",
        help = true)
    boolean help;

    @Parameter(names = {"-v", "--version"}, description
        = "Display manager version")
    boolean version;

    @Parameter(names = {"-p", "--process"}, description = "Run process")
    boolean process;

    @Parameter(names = {"-l", "--language"}, description
        = "Choose user language")
    String language;

    @Parameter(names = {"-t", "--team"}, description = "The submission team")
    String team;

    @Parameter(names = {"-e", "--exercice"}, description
        = "The submission exercice")
    String exercice;

    @Parameter(names = {"-d", "--directory"}, description
        = "The code folder path")
    String directory;

    @Parameter(names = {"-k", "--keep-env"}, description
        = "Define if the environment must be preserved")
    boolean keepEnv;

    @Parameter(names = {"--check-image"}, description
        = "Check if an image exists")
    String checkImage;

    @Parameter(names = {"--list-images"}, description
        = "List the existing docker images")
    boolean listImages;

    @Parameter(names = {"--build-images"}, description
        = "Build all expected docker images")
    boolean buildImages;

    @Parameter(names = {"--list-languages"}, description
        = "List the available languages")
    boolean listLanguages;

    protected ArgParser() {
    }

    @Override
    public String toString() {
        return "ArgParser{" + "help=" + help + ", version=" + version
            + ", process=" + process + ", language=" + language
            + ", team=" + team + ", exercice=" + exercice + ", directory="
            + directory + ", keepEnv=" + keepEnv + ", checkImage=" + checkImage
            + ", listImages=" + listImages + ", buildImages=" + buildImages
            + ", listLanguages=" + listLanguages + '}';
    }

}
