package fr.nemolovich.apps.codeplatform.manager.docker;

import java.util.concurrent.ThreadFactory;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The factory to use to create an new Thread for manager.
 *
 * @author bgohier
 */
class ManagerThreadFactory implements ThreadFactory {

    /**
     * The default thread name pattern to use.
     */
    private static final String DEFAULT_NAME_PATTERN = "mgr-thread-%03d";
    private static long nbThread = 0;
    private final String namePattern;
    private final Supplier<Long> nextIteration;

    /**
     * Default constructor.
     */
    ManagerThreadFactory() {
        this(DEFAULT_NAME_PATTERN);
    }

    /**
     * Constructor with specific thread name pattern.
     *
     * @param namePattern {@link String}: The name pattern to use (default value
     * {@link #DEFAULT_NAME_PATTERN}).
     *
     * @see #ManagerThreadFactory(java.lang.String, java.util.function.Supplier)
     */
    ManagerThreadFactory(String namePattern) {
        this(namePattern, () -> nbThread++);
    }

    /**
     * Constructor with specific thread name pattern.
     *
     * @param namePattern {@link String}: The name pattern to use (default value
     * {@link #DEFAULT_NAME_PATTERN}).
     * @param nextIteration {@link Supplier}&lt;{@link Long}&gt;: The function
     * to use to get the tread number information.
     */
    ManagerThreadFactory(String namePattern, Supplier<Long> nextIteration) {
        this.namePattern = namePattern;
        this.nextIteration = nextIteration;
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread thread = new Thread(r, String.format(this.namePattern,
            this.nextIteration.get()));
        thread.setDaemon(true);
        Logger.getLogger(ManagerThreadFactory.class.getName()).log(Level.INFO,
            String.format("Thread [%s] created", thread.getName()));
        return thread;
    }

}
