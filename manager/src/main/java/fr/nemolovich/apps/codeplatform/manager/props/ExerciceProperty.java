package fr.nemolovich.apps.codeplatform.manager.props;

import com.fasterxml.jackson.databind.annotation.JsonAppend.Attr;

/**
 * Property model for exercices.
 *
 * @author bgohier
 */
public final class ExerciceProperty {

    private String name;
    @Attr(required = false, value = "description")
    private String description = null;
    private ContainerProperty container;

    /**
     * Default constructor.
     */
    public ExerciceProperty() {
        /*
         * Reflexion constructor.
         */
    }

    /**
     * Construct property and set default values from an other
     * {@link ExerciceProperty property}.
     *
     * @param global {@link ExerciceProperty}: The default property to use.
     */
    public ExerciceProperty(ExerciceProperty global) {
        this.name = global.name;
        this.container = global.container;
    }

    /**
     * Merge two properties. Each property present in both side will be taken
     * from given parameter object.
     *
     * @param specific {@link ExerciceProperty}: The property to merge in.
     * @return {@link ExerciceProperty} - The merged property.
     */
    public ExerciceProperty merge(ExerciceProperty specific) {
        if (specific != null) {
            if (specific.container != null
                && specific.container.getEnv().length > 0) {
                this.container = new ContainerProperty(specific.container);
            }
            if (specific.name != null) {
                this.name = specific.name;
            }
            if (specific.description != null) {
                this.description = specific.description;
            }
        }
        return this;
    }

    /**
     * Set the exercice name.
     *
     * @param name {@link String}: The excercice name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Return the exercice name.
     *
     * @return {@link String}: The excercice name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Set the container properties to use for this exercice.
     *
     * @param container {@link ContainerProperty}: The container properties.
     */
    public void setContainer(ContainerProperty container) {
        this.container = container;
    }

    /**
     * Return the container properties to use for this exercice.
     *
     * @return {@link ContainerProperty} - The container properties.
     */
    public ContainerProperty getContainer() {
        return this.container;
    }

    /**
     * Set the exercice description.
     *
     * @param description {@link String}: The excercice description.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Return the exercice description.
     *
     * @return {@link String}: The excercice description.
     */
    public String getDescription() {
        return this.description;
    }

    @Override
    public String toString() {
        return "ExerciceProperty{" + "name=" + name + ", description=" + description + ", container=" + container + '}';
    }

}
