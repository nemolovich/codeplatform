package fr.nemolovich.apps.codeplatform.manager.props;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Jackson reader for YAML properties.
 *
 * @author bgohier
 */
public final class PropertyReader {

    private PropertyReader() {
        /*
         * Utility class.
         */
    }

    /**
     * Read an exercice properties file.
     *
     * @param fileStream {@link InputStream}: The input stream of the property
     * file.
     *
     * @return {@link ExerciceProperty} - The exercice property from file.
     *
     * @throws IOException If the file reading failed.
     */
    public static ExerciceProperty readExerciceFile(InputStream fileStream)
        throws IOException {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        ExerciceProperty result = mapper.readValue(fileStream,
            ExerciceProperty.class);
        return result;
    }

    /**
     * Read an exercice properties file from a path.
     *
     * @param filePath {@link String}: The path of the property file.
     *
     * @return {@link ExerciceProperty} - The exercice property from file.
     *
     * @throws IOException If the file reading failed.
     * 
     * @see #readExerciceFile(java.io.InputStream) 
     */
    public static ExerciceProperty readExerciceFile(String filePath)
        throws IOException {
        return PropertyReader.readExerciceFile(new FileInputStream(filePath));
    }

    /**
     * Read a languages properties file.
     *
     * @param fileStream {@link InputStream}: The input stream of the property
     * file.
     *
     * @return {@link LanguagesProperty} - The languages property from file.
     *
     * @throws IOException If the file reading failed.
     */
    public static LanguagesProperty readLanguagesFile(InputStream fileStream)
        throws IOException {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        LanguagesProperty result = mapper.readValue(fileStream,
            LanguagesProperty.class);
        return result;
    }

    /**
     * Read a languages properties file from a path.
     *
     * @param filePath {@link String}: The path of the property file.
     *
     * @return {@link LanguagesProperty} - The languages property from file.
     *
     * @throws IOException If the file reading failed.
     * 
     * @see #readLanguagesFile(java.io.InputStream) 
     */
    public static LanguagesProperty readLanguagesFile(String filePath)
        throws IOException {
        return readLanguagesFile(new FileInputStream(filePath));
    }

    /**
     * Merge two exercice properties using second one if a property is present
     * in both sides.
     *
     * @param global {@link ExerciceProperty}: The global property.
     * @param specific {@link ExerciceProperty}: The specific property.
     *
     * @return {@link ExerciceProperty} - The merged property.
     */
    public static ExerciceProperty mergeProperties(ExerciceProperty global,
        ExerciceProperty specific) {
        return new ExerciceProperty(global).merge(specific);
    }

}
