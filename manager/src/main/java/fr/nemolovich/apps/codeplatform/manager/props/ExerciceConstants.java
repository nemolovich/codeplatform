package fr.nemolovich.apps.codeplatform.manager.props;

/**
 * Constants about exercices.
 *
 * @author bgohier
 */
public final class ExerciceConstants {

    private ExerciceConstants() {
        /*
         * Utility class.
         */
    }

    /**
     * Runtime timeout before to kill process.
     */
    public static final String EXEC_TIMEOUT = "EXEC_TIMEOUT";

}
