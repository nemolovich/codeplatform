package fr.nemolovich.apps.codeplatform.manager.resolver;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;


/**
 *
 * @author bgohier
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FileResolverTest {

    @Test
    public void testResolver01() {
        String expected = null;
        String current = "";
        ResolverResult result = FileResolver.resolveString(expected, current);
        Assert.assertEquals(0.0f, result.getResult(), 0);
        Assert.assertEquals(FileResolver.NULL_ERROR_MSG, result.getLogResult());
    }

    @Test
    public void testResolver02() {
        String expected = "";
        String current = null;
        ResolverResult result = FileResolver.resolveString(expected, current);
        Assert.assertEquals(0.0f, result.getResult(), 0);
        Assert.assertEquals(FileResolver.NULL_ERROR_MSG, result.getLogResult());
    }

    @Test
    public void testResolver03() {
        String expected = "abc";
        String current = "";
        ResolverResult result = FileResolver.resolveString(expected, current);
        Assert.assertEquals(0.0f, result.getResult(), 0);
        Assert.assertEquals(FileResolver.format(expected, current, 10, 0, 1),
            result.getLogResult());
    }

    @Test
    public void testResolver04() {
        String expected = "";
        String current = "abc";
        ResolverResult result = FileResolver.resolveString(expected, current);
        Assert.assertEquals(0.0f, result.getResult(), 0);
        Assert.assertEquals(FileResolver.format(expected, current, 10, 0, 1),
            result.getLogResult());
    }

    @Test
    public void testResolver05() {
        String expected = "abc";
        String current = "adc";
        ResolverResult result = FileResolver.resolveString(expected, current);
        Assert.assertEquals(0.0f, result.getResult(), 0);
        Assert.assertEquals(FileResolver.format(expected, current, 10, 1, 1),
            result.getLogResult());
    }

    @Test
    public void testResolver06() {
        String expected = "abc";
        String current = "abd";
        ResolverResult result = FileResolver.resolveString(expected, current);
        Assert.assertEquals(0.0f, result.getResult(), 0);
        Assert.assertEquals(FileResolver.format(expected, current, 10, 2, 1),
            result.getLogResult());
    }

    @Test
    public void testResolver07() {
        String expected = "abcdef";
        String current = "abcdeg";
        ResolverResult result = FileResolver.resolveString(expected, current);
        Assert.assertEquals(0.0f, result.getResult(), 0);
        Assert.assertEquals(FileResolver.format(expected, current, 10, 5, 1),
            result.getLogResult());
    }

    @Test
    public void testResolver08() {
        String expected = "abcde";
        String current = "abc";
        ResolverResult result = FileResolver.resolveString(expected, current);
        Assert.assertEquals(0.0f, result.getResult(), 0);
        Assert.assertEquals(FileResolver.format(expected, current, 10, 3, 1),
            result.getLogResult());
    }

    @Test
    public void testResolver09() {
        String expected = "abc";
        String current = "abcde";
        ResolverResult result = FileResolver.resolveString(expected, current);
        Assert.assertEquals(0.0f, result.getResult(), 0);
        Assert.assertEquals(FileResolver.format(expected, current, 10, 3, 1),
            result.getLogResult());
    }

    @Test
    public void testResolver10() {
        String expected = "abcde";
        String current = "adc";
        ResolverResult result = FileResolver.resolveString(expected, current);
        Assert.assertEquals(0.0f, result.getResult(), 0);
        Assert.assertEquals(FileResolver.format(expected, current, 10, 1, 1),
            result.getLogResult());
    }

    @Test
    public void testResolver11() {
        String expected = "adc";
        String current = "abcde";
        ResolverResult result = FileResolver.resolveString(expected, current);
        Assert.assertEquals(0.0f, result.getResult(), 0);
        Assert.assertEquals(FileResolver.format(expected, current, 10, 1, 1),
            result.getLogResult());
    }

    @Test
    public void testResolver12() {
        String expected = "abc";
        String current = "abc";
        ResolverResult result = FileResolver.resolveString(expected, current);
        Assert.assertEquals(1f, result.getResult(), 0);
        Assert.assertEquals(FileResolver.SUCCESS_MSG, result.getLogResult());
    }

    @Test
    public void testResolver13() {
        List<String> expected = Arrays.asList();
        List<String> current = Arrays.asList("abc");
        ResolverResult result = FileResolver.resolveFile(expected, current);
        Assert.assertEquals(0f, result.getResult(), 0);
        Assert.assertEquals(FileResolver.format(null, "abc", 10, 0, 1),
            result.getLogResult());
        Assert.assertEquals(current.stream().collect(Collectors.joining("\n")),
            result.getStdout());
    }

    @Test
    public void testResolver14() {
        List<String> expected = Arrays.asList("abc");
        List<String> current = Arrays.asList();
        ResolverResult result = FileResolver.resolveFile(expected, current);
        Assert.assertEquals(0f, result.getResult(), 0);
        Assert.assertEquals(FileResolver.format("abc", null, 10, 0, 1),
            result.getLogResult());
        Assert.assertEquals(current.stream().collect(Collectors.joining("\n")),
            result.getStdout());
    }

    @Test
    public void testResolver15() {
        List<String> expected = Arrays.asList("abc");
        List<String> current = Arrays.asList("abc", "def");
        ResolverResult result = FileResolver.resolveFile(expected, current);
        Assert.assertEquals(0f, result.getResult(), 0);
        Assert.assertEquals(FileResolver.format(null, "def", 10, 0, 2),
            result.getLogResult());
        Assert.assertEquals(current.stream().collect(Collectors.joining("\n")),
            result.getStdout());
    }

    @Test
    public void testResolver16() {
        List<String> expected = Arrays.asList("abc", "def");
        List<String> current = Arrays.asList("abc");
        ResolverResult result = FileResolver.resolveFile(expected, current);
        Assert.assertEquals(0f, result.getResult(), 0);
        Assert.assertEquals(FileResolver.format("def", null, 10, 0, 2),
            result.getLogResult());
        Assert.assertEquals(current.stream().collect(Collectors.joining("\n")),
            result.getStdout());
    }

    @Test
    public void testResolver17() {
        List<String> expected = Arrays.asList("abc");
        List<String> current = Arrays.asList("adc");
        ResolverResult result = FileResolver.resolveFile(expected, current);
        Assert.assertEquals(0f, result.getResult(), 0);
        Assert.assertEquals(FileResolver.format("abc", "adc", 10, 1, 1),
            result.getLogResult());
        Assert.assertEquals(current.stream().collect(Collectors.joining("\n")),
            result.getStdout());
    }

    @Test
    public void testResolver18() {
        List<String> expected = Arrays.asList("abc", "def");
        List<String> current = Arrays.asList("abc", "ghi");
        ResolverResult result = FileResolver.resolveFile(expected, current);
        Assert.assertEquals(0f, result.getResult(), 0);
        Assert.assertEquals(FileResolver.format("def", "ghi", 10, 0, 2),
            result.getLogResult());
        Assert.assertEquals(current.stream().collect(Collectors.joining("\n")),
            result.getStdout());
    }
}
