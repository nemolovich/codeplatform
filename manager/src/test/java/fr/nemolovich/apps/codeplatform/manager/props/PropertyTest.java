package fr.nemolovich.apps.codeplatform.manager.props;

import fr.nemolovich.apps.codeplatform.manager.docker.DockerHelper;
import fr.nemolovich.apps.codeplatform.manager.docker.Manager;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author bgohier
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PropertyTest {

    @Test
    public void testExerciceRead() throws IOException {
        final ExerciceProperty prop = PropertyReader.readExerciceFile(
            PropertyReader.class.getResourceAsStream("exercice.yml"));
        Assert.assertNotNull(prop);
        System.out.println(prop);
        Assert.assertNotNull(prop.getContainer());
        Assert.assertNotNull(prop.getContainer().getEnv());
        Assert.assertTrue(prop.getContainer().getEnvMap()
            .containsKey(ExerciceConstants.EXEC_TIMEOUT));
        Assert.assertEquals((Integer) 5, Integer.valueOf(prop.getContainer()
            .getEnvMap().get(ExerciceConstants.EXEC_TIMEOUT)));
        Assert.assertEquals("Test Exercice", prop.getName());
    }

    @Test
    public void testLanguagesRead() throws IOException {
        final LanguagesProperty prop = PropertyReader.readLanguagesFile(
            PropertyReader.class.getResourceAsStream("/languages.yml"));
        Assert.assertNotNull(prop);
        System.out.println(prop);
        List<String> ordered = prop.getOrderedLanguages();
        Assert.assertNotNull(ordered);
        System.out.println(ordered);
        Assert.assertTrue(ordered.size() > 0);
        Assert.assertEquals("template", ordered.get(0));
        List<String> images = Manager.availableLanguages();
        Assert.assertFalse(images.contains(DockerHelper.TEMPLATE_IMAGE));
        Assert.assertFalse(images.contains(DockerHelper.CLEANER_IMAGE));
        Assert.assertFalse(images.contains(DockerHelper.BUILDER_IMAGE));
        images.add(DockerHelper.TEMPLATE_IMAGE.substring(
            DockerHelper.IMAGES_PREFIX.length() + 1));
        images.add(DockerHelper.CLEANER_IMAGE.substring(
            DockerHelper.IMAGES_PREFIX.length() + 1));
        images.add(DockerHelper.BUILDER_IMAGE.substring(
            DockerHelper.IMAGES_PREFIX.length() + 1));
        Collections.sort(ordered);
        Collections.sort(images);
        Assert.assertArrayEquals(ordered.toArray(), images.toArray());
    }

    @Test
    public void testConfig() throws IOException {
        final ExerciceProperty prop = DockerHelper.getGlobalConfig();
        Assert.assertNotNull(prop);
        System.out.println(prop);
        Assert.assertNotNull(prop.getContainer());
        Assert.assertNotNull(prop.getContainer().getEnv());
        Assert.assertTrue(prop.getContainer().getEnvMap()
            .containsKey(ExerciceConstants.EXEC_TIMEOUT));
        Assert.assertEquals((Integer) 6, Integer.valueOf(prop.getContainer()
            .getEnvMap().get(ExerciceConstants.EXEC_TIMEOUT)));
        Assert.assertEquals("Global Config", prop.getName());
        Assert.assertNull(prop.getDescription());
    }

    @Test
    public void testConfigOverride() throws IOException {
        final ExerciceProperty global = DockerHelper.getGlobalConfig();
        final ExerciceProperty specific = DockerHelper.getConfig("simple-test");
        ExerciceProperty prop = global.merge(specific);
        Assert.assertNotNull(prop);
        System.out.println(prop);
        Assert.assertNotNull(prop.getContainer());
        Assert.assertNotNull(prop.getContainer().getEnv());
        Assert.assertTrue(prop.getContainer().getEnvMap()
            .containsKey(ExerciceConstants.EXEC_TIMEOUT));
        Assert.assertEquals((Integer) 2, Integer.valueOf(prop.getContainer()
            .getEnvMap().get(ExerciceConstants.EXEC_TIMEOUT)));
        Assert.assertEquals("Simple Test", prop.getName());
        Assert.assertEquals("This is a simple test exercice",
            prop.getDescription());
    }
}
