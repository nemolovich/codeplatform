package fr.nemolovich.apps.codeplatform.manager;

import com.spotify.docker.client.exceptions.DockerException;
import fr.nemolovich.apps.codeplatform.manager.docker.DockerHelper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.stream.Collectors;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author bgohier
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LauncherTest {

    private static ByteArrayOutputStream BAOS;
    private static PrintStream CURR_OUT;
    private static PrintStream PS;
    private static final String TEST_IMAGE = "nginx";

    @BeforeClass
    public static void initOutput() {
        BAOS = new ByteArrayOutputStream();
        PS = new PrintStream(BAOS);
        CURR_OUT = System.out;
    }

    @AfterClass
    public static void closeOutput() throws IOException {
        BAOS.close();
    }

    @Before
    public void prepareOutput() {
        System.setOut(PS);
    }

    @After
    public void flushOutput() throws IOException {
        System.out.flush();
        System.setOut(CURR_OUT);
        BAOS.flush();
        BAOS.reset();
    }

    @Test
//    @Ignore
    public void test01ListImages() throws DockerException, InterruptedException,
        IOException {

        String args[] = {"--list-images"};

        Launcher.main(args);
        String console = BAOS.toString();

        Assert.assertEquals(
            Arrays.asList(console.split("\\n")).stream()
            .filter(l -> l.startsWith("\t - "))
            .map(l -> l.replaceFirst("^\\t - ", "").trim())
            .sorted()
            .collect(Collectors.toList()),
            DockerHelper.getImages().stream()
            .map(i -> DockerHelper.getImageName(i))
            .sorted()
            .collect(Collectors.toList())
        );
    }

    @Test
//    @Ignore
    public void test02CheckImage() throws DockerException, InterruptedException,
        IOException {

        String args[] = {"--check-image", "fake/image"};

        Launcher.main(args);
        String console = BAOS.toString();

        Assert.assertEquals(String.format("%s is not present", "fake/image"),
            console.trim());
    }

    @Test
//    @Ignore
    public void test03CheckImage() throws DockerException, InterruptedException,
        IOException {

        String args[] = {"--check-image", TEST_IMAGE};

        Launcher.main(args);
        String console = BAOS.toString();

        Assert.assertEquals(String.format("%s is present", TEST_IMAGE),
            console.trim());
    }

    @Test
    @Ignore
    public void test04Manual() throws DockerException, InterruptedException,
        IOException {

        String args[] = {"--build-images"};

        Launcher.main(args);
        String console = BAOS.toString();
        flushOutput();
        System.out.println(console);
    }
}
