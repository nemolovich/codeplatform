int sum(int *l, int size) {
    int s = 0;
    for (int i = 0; i < size; i++) {
        s += l[i];
    }
    return s;
}
