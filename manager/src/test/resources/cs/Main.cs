using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

class main
{
    static void Main(string[] args)
    {
        Console.ReadLine();
        Console.WriteLine(Array.ConvertAll(Console.ReadLine().Split(' '),
            s => int.Parse(s)).Sum());
    }
}
