package fr.nemolovich.apps.codeplatform.manager.test;

import java.util.Arrays;

/**
 *
 * @author bgohier
 */
public class Sum {
    public static int getSum(String line) {
        return Arrays.stream(line.split("\\s")).mapToInt(
			Integer::parseInt).sum();
    }
}
