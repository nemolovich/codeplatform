import fr.nemolovich.apps.codeplatform.manager.resolver.FileResolver;
import fr.nemolovich.apps.codeplatform.manager.resolver.IResolver;
import fr.nemolovich.apps.codeplatform.manager.resolver.ResolverResult;
import java.util.logging.Logger;

public class Resolver implements IResolver {

    @Override
    public ResolverResult resolve(String inputFile, String targetFile,
                                  String currentFile, Logger... loggers) {
        System.out.println("Resolve: " + inputFile + ", " + targetFile + ", " +
                           currentFile);
        return new FileResolver().resolve(inputFile, targetFile, currentFile,
                                          loggers);
    }
}
