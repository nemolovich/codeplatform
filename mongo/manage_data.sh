#!/bin/bash

DUMP_BASE_DIR=/logs/dump
LOCAL_BASE_DUMP_DIR=${LOGS_VOLUME}/dump

CONTAINER_ID="`basename $(cat /proc/1/cpuset)`"
HOST_VOLUME_ROOT=`echo $(docker inspect ${CONTAINER_ID} | grep ${VOLUME_ROOT}: | cut -d ":" -f1 | tr '"' ' ' 2>/dev/null)`
HOST_LOGS_VOLUME=""

if [ ! -z "${HOST_VOLUME_ROOT}" ] ; then
    HOST_LOGS_VOLUME="${HOST_VOLUME_ROOT}/$(basename ${LOGS_VOLUME})"
fi

HOST_BASE_DUMP_DIR=${HOST_LOGS_VOLUME:-/volumes/logs}/dump

mkdir -p ${LOCAL_BASE_DUMP_DIR}

REP_NAME=${REPLICA_NAME:-replica}
DB_NAME=${DB_NAME:-database}
START_PORT=${START_PORT:-27040}
USE_HOST=${USE_HOST:-true}

echo -e "\033[0;97mINFO   \033[0;0m: Replica \033[0;36m${REP_NAME}\033[0;0m on DB \033[0;36m${DB_NAME}\033[0;0m - hostname: \033[0;36m${HOSTNAME}\033[0;0m"

compose_file=docker-compose.yml
nb_replicas=$(cat ${compose_file} | grep -Pc "${REPLICA_NAME}_\d+:$")
port=$[START_PORT]

mongo_host="${HOSTNAME}:${port}"
for nb in $(seq $[nb_replicas-1]) ; do
    if ${use_host} ; then
        mongo_host="${mongo_host},${HOSTNAME}:$[port+nb]"
    else
        mongo_host="${mongo_host},${REP_NAME}_${nb}"
    fi
done
echo -e "\033[1;97mINFO   \033[0;0m: Mongo Host: \033[0;36m${mongo_host}\033[0;0m"

DATE=$(date "+%Y%m%d%H%M%S")

DUMP=false
RESTORE=false
EXPORT=false
IMPORT=false
DROP_DB=false
DROP_COLLECTION=false
DUMP_FOLDER=${DB_NAME}_${DATE}
FILE_NAME=""
COLLECTION=""
CONFIRM=false

function usage()
{
    echo -e "\033[0;97mUSAGE  \033[0;0m: \033[0;33mmanage-data\033[0;0m \
        [\033[0;36mdump\033[0;0m,\033[0;36mrestore\033[0;0m,\033[0;36mexport\033[0;0m,\033[0;36mimport\033[0;0m] [...]"
}

function drop_collection()
{
    local container_name="$1"
    local host="$2"
    local database="$3"
    local collection="$4"
    echo -e "\033[1;97mINFO   \033[0;0m: Dropping collection \033[0;36m${collection}\033[0;0m on \033[0;36m${database} - ${host}\033[0;0m"
    docker exec ${container_name} mongo --host "${host}" ${database} --eval "db.${collection}.drop()"
    ret_code=$?
    if [ ${ret_code} -eq 0 ] ; then
        echo -e "\033[0;97mINFO   \033[0;0m: Collection \033[0;36m${collection}\033[0;0m successfully dropped"
    else
        echo -e "\033[0;91mERROR  \033[0;0m: Cannot drop collection \033[0;36m${collection}\033[0;0m"
    fi
}

function drop_database()
{
    local container_name="$1"
    local host="$2"
    local database="$3"
    echo -e "\033[1;97mINFO   \033[0;0m: Dropping database \033[0;36m${database}\033[0;0m on \033[0;36m${host}\033[0;0m"
    docker exec ${container_name} mongo --host "${host}" ${database} --eval "db.dropDatabase()"
    ret_code=$?
    if [ ${ret_code} -eq 0 ] ; then
        echo -e "\033[0;97mINFO   \033[0;0m: Database \033[0;36m${database}\033[0;0m successfully dropped"
    else
        echo -e "\033[0;91mERROR  \033[0;0m: Cannot drop database \033[0;36m${database}\033[0;0m"
    fi
}

function confirm()
{
    if [ ! -t 1 ] ; then
        echo -e "\033[0;33mWARNING\033[0;0m: You are not using terminal. Please add \033[0;36m-y\033[0;0m \
option to force confirmation"
    fi
    local message="$1"
    read -p "${message} ([Y]es/[N]o): " response
    ret=1
    case "${response,,}" in
        y|yes)
            ret=0
            ;;
        *)
            echo -e "\033[0;33mWARNING\033[0;0m: Action canceled"
            ;;
    esac
    return ${ret}
}

function parse_opts()
{
    if [ ! -z "${1}" ] ; then
        case $1 in
            dump)
                DUMP=true
                if [ ! -z "$2" ] ; then
                    DUMP_FOLDER="$2"
                    echo -e "\033[1;97mINFO   \033[0;0m: Using custom dump folder \033[0;36m${DUMP_FOLDER}\033[0;0m"
                fi
                ;;
            restore)
                RESTORE=true
                if [ ! -z "$2" ] ; then
                    DUMP_FOLDER="$2"
                    echo -e "\033[1;97mINFO   \033[0;0m: Using dump folder \033[0;36m${DUMP_FOLDER}\033[0;0m"
                else
                    echo -e "\033[0;91mERROR  \033[0;0m: \033[0;36mrestore\033[0;0m option requires \
\033[0;36mdump folder\033[0;0m parameter"
                    usage
                    exit 2
                fi
                if [ "$3" = "-y" ] ; then
                    CONFIRM=true
                fi
                ;;
            export)
                EXPORT=true
                if [ ! -z "$2" ] ; then
                    COLLECTION="$2"
                    FILE_NAME=${DUMP_FOLDER}/${COLLECTION}.json
                    echo -e "\033[1;97mINFO   \033[0;0m: Using collection \033[0;36m${COLLECTION}\033[0;0m"
                else
                    echo -e "\033[0;91mERROR  \033[0;0m: \033[0;36mexport\033[0;0m option requires \
\033[0;36mcollection name\033[0;0m parameter"
                    usage
                    exit 3
                fi
                if [ ! -z "$3" ] ; then
                    FILE_NAME="$3"
                    echo -e "\033[1;97mINFO   \033[0;0m: Using custom export file \033[0;36m${FILE_NAME}\033[0;0m"
                fi
                ;;
            import)
                IMPORT=true
                if [ ! -z "$2" -a ! -z "$3" ] ; then
                    COLLECTION="$2"
                    FILE_NAME="$3"
                    echo -e "\033[1;97mINFO   \033[0;0m: Using collection \033[0;36m${COLLECTION}\033[0;0m and \
export file \033[0;36m${FILE_NAME}\033[0;0m"
                else
                    echo -e "\033[0;91mERROR  \033[0;0m: \033[0;36mimport\033[0;0m option requires \
\033[0;36mcollection name\033[0;0m and \033[0;36mrelative file path\033[0;0m parameters"
                    usage
                    exit 4
                fi
                if [ "$4" = "-c" ] ; then
                    if [ "$5" = "-y" ] ; then
                        CONFIRM=true
                    else
                        confirm "Are you sure you want to remove current collection content?"
                        [ $? -eq 0 ] && CONFIRM=true || CONFIRM=false
                    fi
                fi
                ;;
            drop-database)
                DROP_DB=true
                if [ "$2" = "-y" ] ; then
                    CONFIRM=true
                fi
                ;;
            drop-collection)
                DROP_COLLECTION=true
                if [ ! -z "$2" ] ; then
                    COLLECTION="$2"
                    echo -e "\033[1;97mINFO   \033[0;0m: Using collection \033[0;36m${COLLECTION}\033[0;0m"
                else
                    echo -e "\033[0;91mERROR  \033[0;0m: \033[0;36mdrop-collection\033[0;0m option requires \
\033[0;36mcollection name\033[0;0m parameter"
                    usage
                    exit 5
                fi
                if [ "$3" = "-y" ] ; then
                    CONFIRM=true
                fi
                ;;
            *)
                echo -e "\033[0;33mWARNING\033[0;0m: Unknown option \033[0;36m$1\033[0;0m"
                ;;
        esac
    fi
}

parse_opts $*

DUMP_DIR=${DUMP_BASE_DIR}/${DUMP_FOLDER}
LOCAL_DUMP_DIR=${LOCAL_BASE_DUMP_DIR}/${DUMP_FOLDER}
HOST_DUMP_DIR=${HOST_BASE_DUMP_DIR}/${DUMP_FOLDER}
EXPORT_FILE=${DUMP_BASE_DIR}/${FILE_NAME}
LOCAL_EXPORT_FILE=${LOCAL_BASE_DUMP_DIR}/${FILE_NAME}
HOST_EXPORT_FILE=${HOST_BASE_DUMP_DIR}/${FILE_NAME}
DOCKER_NODE="mongo_${REPLICA_NAME}_0_1"

if ! ${DUMP} && ! ${RESTORE} && ! ${EXPORT} && ! ${IMPORT}&& ! ${DROP_DB} && ! ${DROP_COLLECTION} ; then
    echo -e "\033[0;91mERROR  \033[0;0m: Require parameters"
    usage
    exit 1
fi

if ${DUMP} ; then
    mkdir -p ${LOCAL_DUMP_DIR} && chmod 777 ${LOCAL_DUMP_DIR}
    echo -e "\033[0;97mINFO   \033[0;0m: Dump database into \033[0;36m${HOST_DUMP_DIR}\033[0;0m"
    docker exec ${DOCKER_NODE} mongodump --db ${DB_NAME} --host "${REPLICA_NAME}/${mongo_host}" --out ${DUMP_DIR} && \
        chmod -R 777 ${LOCAL_DUMP_DIR} && mv ${LOCAL_DUMP_DIR}/${DB_NAME}/* ${LOCAL_DUMP_DIR}/ && rm -r ${LOCAL_DUMP_DIR}/${DB_NAME}
    ret_code=$?
    if [ ${ret_code} -eq 0 ] ; then
        echo -e "\033[0;97mINFO   \033[0;0m: Dump successfully created into \033[0;36m${HOST_DUMP_DIR}\033[0;0m"
    else
        echo -e "\033[0;91mERROR  \033[0;0m: Cannot create Dump into \033[0;36m${HOST_DUMP_DIR}\033[0;0m"
    fi
elif ${RESTORE} ; then
    if ! ${CONFIRM} ; then
        confirm "This action will permanently destroy the current database. Are you sure you want to continue?"
        [ $? -eq 0 ] && CONFIRM=true || CONFIRM=false
    fi
    if ${CONFIRM} ; then
        drop_database ${DOCKER_NODE} ""${REPLICA_NAME}/${mongo_host}"" ${DB_NAME}
        echo -e "\033[0;97mINFO   \033[0;0m: Restore database from \033[0;36m${HOST_DUMP_DIR}\033[0;0m"
        drop_database ${DOCKER_NODE} "${REPLICA_NAME}/${mongo_host}" ${DB_NAME} && \
            docker exec ${DOCKER_NODE} mongorestore --db ${DB_NAME} --host "${REPLICA_NAME}/${mongo_host}" ${DUMP_DIR}
        ret_code=$?
        if [ ${ret_code} -eq 0 ] ; then
            echo -e "\033[0;97mINFO   \033[0;0m: Database successfully restored from \033[0;36m${HOST_DUMP_DIR}\033[0;0m"
        else
            echo -e "\033[0;91mERROR  \033[0;0m: Cannot restore database from \033[0;36m${HOST_DUMP_DIR}\033[0;0m"
        fi
    fi
elif ${EXPORT} ; then
    local_dir=$(dirname ${LOCAL_EXPORT_FILE})
    mkdir -p ${local_dir} && chmod 777 ${local_dir}
    echo -e "\033[0;97mINFO   \033[0;0m: Export collection \033[0;36m${COLLECTION}\033[0;0m to \033[0;36m${HOST_EXPORT_FILE}\033[0;0m"
    docker exec ${DOCKER_NODE} mongoexport -d ${DB_NAME} --host "${REPLICA_NAME}/${mongo_host}" -c ${COLLECTION} \
        --jsonArray --pretty --quiet --out ${EXPORT_FILE} && \
        chmod -R 777 ${local_dir}
    ret_code=$?
    if [ ${ret_code} -eq 0 ] ; then
        echo -e "\033[0;97mINFO   \033[0;0m: Collection successfully exported from \033[0;36m${HOST_EXPORT_FILE}\033[0;0m"
    else
        echo -e "\033[0;91mERROR  \033[0;0m: Cannot export collection from \033[0;36m${HOST_EXPORT_FILE}\033[0;0m"
    fi
elif ${IMPORT} ; then
    if ${CONFIRM} ; then
        drop_collection ${DOCKER_NODE} ""${REPLICA_NAME}/${mongo_host}"" ${DB_NAME} ${COLLECTION}
    fi
    echo -e "\033[0;97mINFO   \033[0;0m: Import collection \033[0;36m${COLLECTION}\033[0;0m from \033[0;36m${HOST_EXPORT_FILE}\033[0;0m"
    docker exec ${DOCKER_NODE} mongoimport -d ${DB_NAME} --host "${REPLICA_NAME}/${mongo_host}" -c ${COLLECTION} \
        --jsonArray --file ${EXPORT_FILE}
    ret_code=$?
    if [ ${ret_code} -eq 0 ] ; then
        echo -e "\033[0;97mINFO   \033[0;0m: Collection successfully imported from \033[0;36m${HOST_EXPORT_FILE}\033[0;0m"
    else
        echo -e "\033[0;91mERROR  \033[0;0m: Cannot import collection from \033[0;36m${HOST_EXPORT_FILE}\033[0;0m"
    fi
elif ${DROP_DB} ; then
    if ! ${CONFIRM} ; then
        confirm "This action will permanently destroy the database. Are you sure you want to continue?"
        [ $? -eq 0 ] && CONFIRM=true || CONFIRM=false
    fi
    if ${CONFIRM} ; then
        drop_database ${DOCKER_NODE} ""${REPLICA_NAME}/${mongo_host}"" ${DB_NAME}
    fi
elif ${DROP_COLLECTION} ; then
    if ! ${CONFIRM} ; then
        confirm "This action will permanently destroy the collection. Are you sure you want to continue?"
        [ $? -eq 0 ] && CONFIRM=true || CONFIRM=false
    fi
    if ${CONFIRM} ; then
        drop_collection ${DOCKER_NODE} ""${REPLICA_NAME}/${mongo_host}"" ${DB_NAME} ${COLLECTION}
    fi
fi
