#!/bin/bash

PORT=${MONGO_PORT:-27040}

config=${CONFIG_DIR}/${CONFIG_FILE}

sed -i 's!<LOG_PATH>!'"${LOG_DIR}/${REPLICA_NAME}/${DB_NAME}_${REPLICA_NUMBER}.log"'!g' ${config}
sed -i 's!<DATA_PATH>!'"${DATA_DIR}"'!g' ${config}
sed -i 's!<REPL_NAME>!'"${REPLICA_NAME}"'!g' ${config}
sed -i 's!<PORT>!'"${PORT}"'!g' ${config}

cat ${config}

mkdir -p ${LOG_DIR}/${REPLICA_NAME}

MAX_TRIES=10
WAITING_TIME=${WAITING_TIME:-5}
INIT_FLAG="${LOG_DIR}/${REPLICA_NAME}/initialized"

echo -e "\033[0;32mINFO\033[0m: Deploying replica ${REPLICA_NUMBER}"
if [ ${REPLICA_NUMBER} -eq 0 -a ! -z "$1" -a ! -f ${INIT_FLAG} ] ; then
	echo -e "\033[0;32mINFO\033[0m: Initialize replica \033[0;36m${REPLICA_NAME}\033[0m"
	mongod -f ${config} &
	PID=$!
	echo -e "\033[0;32mINFO\033[0m: Server PID: \033[0;36m${PID}\033[0m"
	sleep $[5*WAITING_TIME]
	while ! nc -z localhost ${PORT} ; do
		sleep 1
	done
	config="cfg.members[0].priority = 100"
	hostname="$(hostname)"
	if [ ! -z "${hostname}" ] ; then
		hostconfig="cfg.members[0].host = \"${hostname}:${PORT}\""
	else
		hostconfig="cfg.members[0].host = \"${REPLICA_NAME}_0:${PORT}\""
	fi
	config="${config}
${hostconfig}"
	echo "rs.initiate()" | mongo localhost:${PORT}
	echo "rs.config()" | mongo localhost:${PORT}
	echo "cfg = rs.conf()
${config}
rs.reconfig(cfg)" | mongo localhost:${PORT}
	while [ ! -z "$1" ] ; do
		try=0
		server="$(echo "$1" | cut -d":" -f1)"
		port=$(echo "$1" | cut -d":" -f2)
		[ -z "${port}" -o "${port}" = "${server}" ] && port=${PORT}
		while ! nc -z ${server} ${port} 2>/dev/null && [ ${try} -lt ${MAX_TRIES} ] ; do
			echo "Waiting for '${server}:${port}'..."
			sleep ${WAITING_TIME}
			try=$[try+1]
		done
		if [ ${try} -lt ${MAX_TRIES} ] ; then
			echo -e "\033[0;32mINFO\033[0m: Adding replica server '\033[0;36m${server}:${port}\033[0m'"
			echo "rs.add(\"${server}:${port}\")" | mongo localhost:${PORT}
		else
			echo -e "\033[0;31mERROR\033[0m: Cannot access to server '\033[0;36m${server}:${port}\033[0m'"
		fi
		shift
	done
	touch ${INIT_FLAG}
	while nc -z localhost ${PORT} ; do sleep 1 ; done
else
	mongod -f ${config}
fi
