package fr.nemolovich.apps.codeplatform.api.repository;

import fr.nemolovich.apps.codeplatform.api.model.Applicant;
import java.util.List;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Applicant Repository
 */
@Repository
public interface ApplicantRepository extends MongoRepository<Applicant, String> {

    Optional<Applicant> findOneByLastName(String lastName);
    
    Optional<Applicant> findOneById(String id);
    
    Optional<Applicant> findOneByChallengeId(String id);
    
    Optional<Applicant> findOneByEmail(String email);
    
    @Override
    List<Applicant> findAll();
}
