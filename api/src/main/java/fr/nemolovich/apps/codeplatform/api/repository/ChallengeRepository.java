package fr.nemolovich.apps.codeplatform.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import fr.nemolovich.apps.codeplatform.api.model.Challenge;

/**
 * Spring Data MongoDB repository for the Challenge entity.
 */
@Repository
public interface ChallengeRepository extends MongoRepository<Challenge, String> {

}
