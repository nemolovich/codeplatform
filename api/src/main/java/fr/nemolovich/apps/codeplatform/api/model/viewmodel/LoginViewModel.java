package fr.nemolovich.apps.codeplatform.api.model.viewmodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;
import javax.validation.constraints.*;
import org.springframework.validation.annotation.Validated;

/**
 * LoginViewModel
 */
@Validated
@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2017-10-07T19:30:45.309+02:00")
public class LoginViewModel {

    @JsonProperty("password")
    private String password = null;

    @JsonProperty("username")
    private String username = null;

    public LoginViewModel password(String password) {
        this.password = password;
        return this;
    }

    /**
     * Get password
     *
     * @return password
     *
     */
    @ApiModelProperty(required = true, value = "")
    @NotNull
    @Size(min = 4, max = 100)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LoginViewModel username(String username) {
        this.username = username;
        return this;
    }

    /**
     * Get username
     *
     * @return username
     *
     */
    @ApiModelProperty(required = true, value = "")
    @NotNull
    @Pattern(regexp = "^[_'.@A-Za-z0-9-]*$")
    @Size(min = 1, max = 50)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LoginViewModel loginViewModel = (LoginViewModel) o;
        return Objects.equals(this.password, loginViewModel.password)
            && Objects.equals(this.username, loginViewModel.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(password, username);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class LoginViewModel {\n");

        sb.append("    password: ").append(toIndentedString(password))
            .append("\n");
        sb.append("    username: ").append(toIndentedString(username))
            .append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     * 
     * @param o {@link Object}: Object to format.
     * @return {@link String}: String representation of object.
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
