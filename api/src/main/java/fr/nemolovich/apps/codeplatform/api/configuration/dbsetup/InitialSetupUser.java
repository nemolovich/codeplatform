package fr.nemolovich.apps.codeplatform.api.configuration.dbsetup;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;

import fr.nemolovich.apps.codeplatform.api.model.Role;
import fr.nemolovich.apps.codeplatform.api.model.User;
import fr.nemolovich.apps.codeplatform.api.security.Roles;

import java.time.Instant;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * Creates the initial database setup for user
 */
@ChangeLog(order = "001")
@Profile(value = "dev")
public class InitialSetupUser {

    @ChangeSet(order = "01", author = "initiator", id = "01-addRoles")
    public void addRoles(MongoTemplate mongoTemplate) {
        Role adminRole = new Role();
        adminRole.setName(Roles.ADMIN.toString());
        Role userRole = new Role();
        userRole.setName(Roles.USER.toString());
        Role anonymousRole = new Role();
        anonymousRole.setName(Roles.ANONYMOUS.toString());
        mongoTemplate.save(adminRole);
        mongoTemplate.save(userRole);
        mongoTemplate.save(anonymousRole);
    }

    @ChangeSet(order = "02", author = "initiator", id = "02-addUsers")
    public void addUsers(MongoTemplate mongoTemplate) {
        Role adminRole = new Role();
        adminRole.setName(Roles.ADMIN.toString());
        Role userRole = new Role();
        userRole.setName(Roles.USER.toString());
        Role anonymousRole = new Role();
        anonymousRole.setName(Roles.ANONYMOUS.toString());

        User systemUser = new User();
        systemUser.setId("user-id-system");
        systemUser.setLogin("system");
        systemUser.setPassword("$2a$10$NY6L4A3.dyUhp0QMn3VAdOVoyxZLYrpnE.p9V5qOxkw6QvMNaWq9q");
        systemUser.setFirstName("");
        systemUser.setLastName("System");
        systemUser.setEmail("system@localhost");
        systemUser.setActivated(true);
        systemUser.setCreatedBy(systemUser.getLogin());
        systemUser.setCreatedDate(Instant.now());
        systemUser.addRole(adminRole).addRole(userRole);
        mongoTemplate.save(systemUser);

        User anonymousUser = new User();
        anonymousUser.setId("user-id-anonymous");
        anonymousUser.setLogin("anonymoususer");
        anonymousUser.setPassword("$2a$10$Cqn3ZrBGlt.neVEEl.RTDOzmnNlOt3zdoBmRfqMAe0l2asLVMhRbG");
        anonymousUser.setFirstName("Anonymous");
        anonymousUser.setLastName("User");
        anonymousUser.setEmail("anonymous@localhost");
        anonymousUser.setActivated(true);
        anonymousUser.setCreatedBy(systemUser.getLogin());
        anonymousUser.setCreatedDate(Instant.now());
        anonymousUser.addRole(anonymousRole);
        mongoTemplate.save(anonymousUser);

        User adminUser = new User();
        adminUser.setId("user-id-admin");
        adminUser.setLogin("admin");
        adminUser.setPassword("$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC");
        adminUser.setFirstName("admin");
        adminUser.setLastName("Administrator");
        adminUser.setEmail("admin@localhost");
        adminUser.setImageUrl("https://cdn1.iconfinder.com/data/icons/ninja-things-1/1772/ninja-simple-512.png");
        adminUser.setActivated(true);
        adminUser.setCreatedBy(adminUser.getLogin());
        adminUser.setCreatedDate(Instant.now());
        adminUser.addRole(adminRole).addRole(userRole);
        mongoTemplate.save(adminUser);

        User userUser0 = new User();
        userUser0.setId("user-id-0");
        userUser0.setLogin("user0");
        userUser0.setPassword("$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K");
        userUser0.setFirstName("");
        userUser0.setImageUrl("https://maxcdn.icons8.com/Share/icon/Cinema//avatar1600.png");
        userUser0.setLastName("user0");
        userUser0.setEmail("user0@localhost");
        userUser0.setActivated(true);
        userUser0.setCreatedBy(systemUser.getLogin());
        userUser0.setCreatedDate(Instant.now());
        userUser0.getRoles().add(userRole);
        mongoTemplate.save(userUser0);

        User userUser1 = new User();
        userUser1.setId("user-id-1");
        userUser1.setLogin("user1");
        userUser1.setPassword("$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K");
        userUser1.setFirstName("");
        userUser1.setLastName("user1");
        userUser1.setEmail("user1@localhost");
        userUser1.setImageUrl("http://findicons.com/files/icons/1072/face_avatars/300/a02.png");
        userUser1.setActivated(true);
        userUser1.setCreatedBy(systemUser.getLogin());
        userUser1.setCreatedDate(Instant.now());
        userUser1.getRoles().add(userRole);
        mongoTemplate.save(userUser1);

        User userUser2 = new User();
        userUser2.setId("user-id-2");
        userUser2.setLogin("user2");
        userUser2.setPassword("$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K");
        userUser2.setFirstName("");
        userUser2.setLastName("user2");
        userUser2.setImageUrl("plop");
        userUser2.setEmail("user2@localhost");
        userUser2.setActivated(true);
        userUser2.setCreatedBy(systemUser.getLogin());
        userUser2.setCreatedDate(Instant.now());
        userUser2.getRoles().add(userRole);
        mongoTemplate.save(userUser2);

        User userUser3 = new User();
        userUser3.setId("user-id-3");
        userUser3.setLogin("user3");
        userUser3.setPassword("$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K");
        userUser3.setFirstName("");
        userUser3.setLastName("user3");
        userUser3.setImageUrl("https://cdn1.iconfinder.com/data/icons/Superhero_Avatars/512/Iron_Man_Mark_III.png");
        userUser3.setEmail("user3@localhost");
        userUser3.setActivated(false);
        userUser3.setCreatedBy(systemUser.getLogin());
        userUser3.setCreatedDate(Instant.now());
        userUser3.getRoles().add(userRole);
        mongoTemplate.save(userUser3);

        User userUser4 = new User();
        userUser4.setId("user-id-4");
        userUser4.setLogin("user4");
        userUser4.setPassword("$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K");
        userUser4.setFirstName("");
        userUser4.setLastName("user4");
        userUser4.setImageUrl("404 ^^");
        userUser4.setEmail("user4@localhost");
        userUser4.setActivated(false);
        userUser4.setCreatedBy(systemUser.getLogin());
        userUser4.setCreatedDate(Instant.now());
        userUser4.getRoles().add(userRole);
        mongoTemplate.save(userUser4);

        User userUser5 = new User();
        userUser5.setId("user-id-5");
        userUser5.setLogin("user5");
        userUser5.setPassword("$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K");
        userUser5.setFirstName("");
        userUser5.setImageUrl("https://www.google.fr/");
        userUser5.setLastName("user5");
        userUser5.setEmail("user5@localhost");
        userUser5.setActivated(true);
        userUser5.setCreatedBy(systemUser.getLogin());
        userUser5.setCreatedDate(Instant.now());
        userUser5.getRoles().add(userRole);
        mongoTemplate.save(userUser5);
    }

}
