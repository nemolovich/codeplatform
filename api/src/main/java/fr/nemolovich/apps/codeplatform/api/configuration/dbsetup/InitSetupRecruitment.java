package fr.nemolovich.apps.codeplatform.api.configuration.dbsetup;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import fr.nemolovich.apps.codeplatform.api.model.Challenge;
import fr.nemolovich.apps.codeplatform.api.model.Exercice;
import fr.nemolovich.apps.codeplatform.api.model.Team;
import fr.nemolovich.apps.codeplatform.api.model.TestCase;
import fr.nemolovich.apps.codeplatform.api.model.enums.ChallengeScope;
import fr.nemolovich.apps.codeplatform.api.model.enums.ExerciceType;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 *
 * @author fbontemp
 */
@ChangeLog(order = "007")
public class InitSetupRecruitment {

    private static final Set<Team> TEAMS = new HashSet<>();
    private static final Set<Exercice> EXERCICES = new HashSet<>();

    private static Exercice getExercice(String id) {
        return EXERCICES.stream().filter(e -> e.getId().equalsIgnoreCase(id)).
            findFirst().get();
    }

    @ChangeSet(order = "01", author = "initiator", id = "01-addExercices")
    public void addExercices(MongoTemplate mongoTemplate) {

        recruitment1:
            {
                Exercice triangle = new Exercice();
                triangle.setId("rec-ex1");
                triangle.setTitle("Recruitment exercise 1");
                triangle.setSubtitle("Square triangles");
                triangle.setText("# Recruitment exercise 1\n" +
                                 "\n" +
                                 "Your goal is to check if the triangle defined\n" +
                                 "by the 3 input numbers is a square triangle\n" +
                                 "or not\n" +
                                 "\n" +
                                 "### Inputs\n" +
                                 "`a`,`b`,`c` The 3 sides of the triangle\n" +
                                 "\n" +
                                 "### Outputs\n" +
                                 "`R`: The result : 0(=false)or 1(=true)\n" +
                                 "\n" +
                                 "### Example\n" +
                                 "\n" +
                                 "| Inputs    | Outputs    \n" +
                                 "|:----------|:-----------\n" +
                                 "| 3 4 5     | 1          \n" +
                                 "\n");
                triangle.setType(ExerciceType.FASTEST);

                StringBuilder sb;
                StringBuilder sb2;
                for (int i = 1; i <= 3; i++) {
                    sb = new StringBuilder();
                    sb2 = new StringBuilder();
                    String name = "data" + i;
                    try (InputStream is = InitSetupRecruitment.class.
                                     getResourceAsStream(
                                         "/in/rec-ex1/" + name + ".in");
                         BufferedInputStream bis = new BufferedInputStream(is)) {
                        byte[] buffer = new byte[2048];
                        int len;
                        while ((len = bis.read(buffer)) != -1) {
                            sb.append(new String(buffer, 0, len));
                        }
                    } catch (IOException ioe) {
                        Logger.getLogger(InitSetupRecruitment.class.getName()).
                            log(Level.SEVERE, "Cannot read input file", ioe);
                    }
                    try (InputStream is = InitSetupRecruitment.class.
                                     getResourceAsStream(
                                         "/out/rec-ex1/" + name + ".out");
                         BufferedInputStream bis = new BufferedInputStream(is)) {
                        byte[] buffer = new byte[2048];
                        int len;
                        while ((len = bis.read(buffer)) != -1) {
                            sb2.append(new String(buffer, 0, len));
                        }
                    } catch (IOException ioe) {
                        Logger.getLogger(InitSetupRecruitment.class.getName()).
                            log(Level.SEVERE, "Cannot read output file", ioe);
                    }
                    triangle.addTestCase(new TestCase().id("rec-exercise1-" + i)
                        .name(name).hidden(false).input(sb.toString())
                        .output(sb2.toString()).order(i));
                }

                sb = new StringBuilder();
                sb2 = new StringBuilder();
                try (InputStream is = InitSetupRecruitment.class.
                                 getResourceAsStream("/in/rec-ex1/data4.in");
                     BufferedInputStream bis = new BufferedInputStream(is)) {
                    byte[] buffer = new byte[2048];
                    int len;
                    while ((len = bis.read(buffer)) != -1) {
                        sb.append(new String(buffer, 0, len));
                    }
                } catch (IOException ioe) {
                    Logger.getLogger(InitSetupRecruitment.class.getName()).log(
                        Level.SEVERE, "Cannot read input file", ioe);
                }
                try (InputStream is = InitSetupRecruitment.class.
                                 getResourceAsStream("/out/rec-ex1/data4.out");
                     BufferedInputStream bis = new BufferedInputStream(is)) {
                    byte[] buffer = new byte[2048];
                    int len;
                    while ((len = bis.read(buffer)) != -1) {
                        sb2.append(new String(buffer, 0, len));
                    }
                } catch (IOException ioe) {
                    Logger.getLogger(InitSetupRecruitment.class.getName()).log(
                        Level.SEVERE, "Cannot read output file", ioe);
                }
                triangle.addTestCase(new TestCase().id("rec-exercise1-4")
                    .name("data4").hidden(true).input(sb.toString())
                    .output(sb2.toString()).order(4));

                triangle.getTestCases().stream().forEach((testCase) ->
                    mongoTemplate.save(testCase));
                triangle.setCoeff(BigDecimal.valueOf(0.1));

                mongoTemplate.save(triangle);

                EXERCICES.add(triangle);
            }
        recruitment2:
            {
                Exercice leapYears = new Exercice();
                leapYears.setId("rec-ex2");
                leapYears.setTitle("Recruitment exercise 2");
                leapYears.setSubtitle("Leap years");
                leapYears.setText("# Recruitment exercise 2\n" +
                                  "\n" +
                                  "Your goal is to count the number of leap\n" +
                                  "years between two given years (boundaries\n" +
                                  "are included)\n" +
                                  "\n" +
                                  "Reminder : a leap year is exactly divisible\n" +
                                  "by 4, but not by 100 (unless it is also\n" +
                                  "exactly divisible by 400).\n" +
                                  "\n" +
                                  "### Inputs\n" +
                                  "`date1`, `date2` The 2 dates\n" +
                                  "\n" +
                                  "### Outputs\n" +
                                  "`N`: The result = number of leap dates\n" +
                                  "\n" +
                                  "### Examples\n" +
                                  "\n" +
                                  "| Inputs      | Outputs     \n" +
                                  "|:------------|:------------\n" +
                                  "| 2001 2005   | 1           \n" +
                                  "\n" +
                                  "| Inputs      | Outputs     \n" +
                                  "|:------------|:------------\n" +
                                  "| 2001 2009   | 2           \n" +
                                  "\n");
                leapYears.setType(ExerciceType.FASTEST);

                StringBuilder sb;
                StringBuilder sb2;
                for (int i = 1; i <= 5; i++) {
                    sb = new StringBuilder();
                    sb2 = new StringBuilder();
                    String name = "data" + i;
                    try (InputStream is = InitSetupRecruitment.class.
                                     getResourceAsStream(
                                         "/in/rec-ex2/" + name + ".in");
                         BufferedInputStream bis = new BufferedInputStream(is)) {
                        byte[] buffer = new byte[2048];
                        int len;
                        while ((len = bis.read(buffer)) != -1) {
                            sb.append(new String(buffer, 0, len));
                        }
                    } catch (IOException ioe) {
                        Logger.getLogger(InitSetupRecruitment.class.getName()).
                            log(Level.SEVERE, "Cannot read input file", ioe);
                    }
                    try (InputStream is = InitSetupRecruitment.class.
                                     getResourceAsStream(
                                         "/out/rec-ex2/" + name + ".out");
                         BufferedInputStream bis = new BufferedInputStream(is)) {
                        byte[] buffer = new byte[2048];
                        int len;
                        while ((len = bis.read(buffer)) != -1) {
                            sb2.append(new String(buffer, 0, len));
                        }
                    } catch (IOException ioe) {
                        Logger.getLogger(InitSetupRecruitment.class.getName()).
                            log(Level.SEVERE, "Cannot read output file", ioe);
                    }
                    leapYears.addTestCase(new TestCase().
                        id("rec-exercise2-" + i)
                        .name(name).hidden(false).input(sb.toString())
                        .output(sb2.toString()).order(i));
                }

                sb = new StringBuilder();
                sb2 = new StringBuilder();
                try (InputStream is = InitSetupRecruitment.class.
                                 getResourceAsStream("/in/rec-ex2/data6.in");
                     BufferedInputStream bis = new BufferedInputStream(is)) {
                    byte[] buffer = new byte[2048];
                    int len;
                    while ((len = bis.read(buffer)) != -1) {
                        sb.append(new String(buffer, 0, len));
                    }
                } catch (IOException ioe) {
                    Logger.getLogger(InitSetupRecruitment.class.getName()).log(
                        Level.SEVERE, "Cannot read input file", ioe);
                }
                try (InputStream is = InitSetupRecruitment.class.
                                 getResourceAsStream("/out/rec-ex2/data6.out");
                     BufferedInputStream bis = new BufferedInputStream(is)) {
                    byte[] buffer = new byte[2048];
                    int len;
                    while ((len = bis.read(buffer)) != -1) {
                        sb2.append(new String(buffer, 0, len));
                    }
                } catch (IOException ioe) {
                    Logger.getLogger(InitSetupRecruitment.class.getName()).log(
                        Level.SEVERE, "Cannot read output file", ioe);
                }
                leapYears.addTestCase(new TestCase().id("rec-exercise2-6")
                    .name("data6").hidden(true).input(sb.toString())
                    .output(sb2.toString()).order(6));

                leapYears.getTestCases().stream().forEach((testCase) ->
                    mongoTemplate.save(testCase));
                leapYears.setCoeff(BigDecimal.valueOf(0.1));

                mongoTemplate.save(leapYears);

                EXERCICES.add(leapYears);
            }

        recruitment3:
            {
                Exercice scoring = new Exercice();
                scoring.setId("rec-ex3");
                scoring.setTitle("Recruitment exercise 3");
                scoring.setSubtitle("Insurance scoring");
                scoring.setText("# Recruitment exercise 3\n" +
                                "\n" +
                                "Your goal is to calculate an insurance score,\n" +
                                "to predict the risk associated with a client.\n" +
                                "\n" +
                                "The score rules are : \n" +
                                "\n" +
                                "1. Age :\n" +
                                "\n" +
                                " Age >= 60 : score = 1 \n" +
                                "\n" +
                                " Age >= 80 : score = 2 \n" +
                                "\n" +
                                " Other cases : score = 0  \n" +
                                "\n" +
                                "2. Body Mass Index = weight in kg / (size in meter)² \n" +
                                "\n" +
                                " BMC < 16 : score = 2 \n" +
                                "\n" +
                                " BMC < 18 : score = 1 \n" +
                                "\n" +
                                " BMC >= 30 : score = 1 \n" +
                                "\n" +
                                " BMC >= 35 : score = 2 \n" +
                                "\n" +
                                " BMC >= 40 : score = 3 \n" +
                                "\n" +
                                " Other cases : score = 0 \n" +
                                "\n" +
                                "3. Tobacco risk :\n" +
                                "\n" +
                                " Smoker (1) : score = 1 \n" +
                                "\n" +
                                " Non smoker (0) : score = 0\n" +
                                "\n" +
                                "\n" +
                                "The final score is the sum of those 3 subscores \n" +
                                "\n" +
                                "\n" +
                                "The final evaluation goes as follow : \n" +
                                "\n" +
                                " * The risk is acceptable (A) if score < 3 ;\n" +
                                "\n" +
                                " * The risk must be mitigated (M) if 3 <= score <= 4 ;\n" +
                                "\n" +
                                " * The risk is unacceptable (U) for all other cases. \n" +
                                "\n" +
                                "### Inputs\n" +
                                "`birthdate_year`, `weight_in_kg`, `size_in_centimeters`,\n" +
                                "`smoker?` : the client data.\n" +
                                "\n" +
                                "### Outputs\n" +
                                "`R`: The result : type of risk (A/M/U)\n" +
                                "\n" +
                                "### Examples\n" +
                                "\n" +
                                "| Inputs          | Outputs \n" +
                                "|:----------------|:--------\n" +
                                "| 1973 75 180 1   | A       \n" +
                                "\n" +
                                "| Inputs          | Outputs \n" +
                                "|:----------------|:--------\n" +
                                "| 1938 120 160 0  | U       \n" +
                                "\n");
                scoring.setType(ExerciceType.FASTEST);

                StringBuilder sb;
                StringBuilder sb2;
                for (int i = 1; i <= 5; i++) {
                    sb = new StringBuilder();
                    sb2 = new StringBuilder();
                    String name = "data" + i;
                    try (InputStream is = InitSetupRecruitment.class.
                                     getResourceAsStream(
                                         "/in/rec-ex3/" + name + ".in");
                         BufferedInputStream bis = new BufferedInputStream(is)) {
                        byte[] buffer = new byte[2048];
                        int len;
                        while ((len = bis.read(buffer)) != -1) {
                            sb.append(new String(buffer, 0, len));
                        }
                    } catch (IOException ioe) {
                        Logger.getLogger(InitSetupRecruitment.class.getName()).
                            log(Level.SEVERE, "Cannot read input file", ioe);
                    }
                    try (InputStream is = InitSetupRecruitment.class.
                                     getResourceAsStream(
                                         "/out/rec-ex3/" + name + ".out");
                         BufferedInputStream bis = new BufferedInputStream(is)) {
                        byte[] buffer = new byte[2048];
                        int len;
                        while ((len = bis.read(buffer)) != -1) {
                            sb2.append(new String(buffer, 0, len));
                        }
                    } catch (IOException ioe) {
                        Logger.getLogger(InitSetupRecruitment.class.getName()).
                            log(Level.SEVERE, "Cannot read output file", ioe);
                    }
                    scoring.addTestCase(new TestCase().id("rec-exercise3-" + i)
                        .name(name).hidden(false).input(sb.toString())
                        .output(sb2.toString()).order(i));
                }

                sb = new StringBuilder();
                sb2 = new StringBuilder();
                try (InputStream is = InitSetupRecruitment.class.
                                 getResourceAsStream("/in/rec-ex3/data6.in");
                     BufferedInputStream bis = new BufferedInputStream(is)) {
                    byte[] buffer = new byte[2048];
                    int len;
                    while ((len = bis.read(buffer)) != -1) {
                        sb.append(new String(buffer, 0, len));
                    }
                } catch (IOException ioe) {
                    Logger.getLogger(InitSetupRecruitment.class.getName()).log(
                        Level.SEVERE, "Cannot read input file", ioe);
                }
                try (InputStream is = InitSetupRecruitment.class.
                                 getResourceAsStream("/out/rec-ex3/data6.out");
                     BufferedInputStream bis = new BufferedInputStream(is)) {
                    byte[] buffer = new byte[2048];
                    int len;
                    while ((len = bis.read(buffer)) != -1) {
                        sb2.append(new String(buffer, 0, len));
                    }
                } catch (IOException ioe) {
                    Logger.getLogger(InitSetupRecruitment.class.getName()).log(
                        Level.SEVERE, "Cannot read output file", ioe);
                }
                scoring.addTestCase(new TestCase().id("rec-exercise3-6")
                    .name("data6").hidden(true).input(sb.toString())
                    .output(sb2.toString()).order(6));

                scoring.getTestCases().stream().forEach((testCase) ->
                    mongoTemplate.save(testCase));
                scoring.setCoeff(BigDecimal.valueOf(0.5));

                mongoTemplate.save(scoring);

                EXERCICES.add(scoring);
            }

        recruitment4:
            {
                Exercice facto = new Exercice();
                facto.setId("rec-ex4");
                facto.setTitle("Recruitment exercise 4");
                facto.setSubtitle("Factorial Fun");
                facto.setText("# Recruitment exercise 4\n" +
                              "\n" +
                              "Your goal is to calculate the factorial of a\n" +
                              "given number, \n" +
                              "\n" +
                              "then sum the digits of that factorial until this\n" +
                              "sum is reduced to a single digit.\n" +
                              "\n" +
                              " Reminder : factorial(4) = 1x2x3x4 = 24 --> result = 2+4 = 6 " +
                              "\n" +
                              "### Inputs\n" +
                              "`N` : the input number.\n" +
                              "\n" +
                              "### Outputs\n" +
                              "`R`: The computed result \n" +
                              "\n" +
                              "### Examples\n" +
                              "\n" +
                              "| Inputs   | Outputs \n" +
                              "|:---------|:--------\n" +
                              "| 4        | 6       \n" +
                              "\n" +
                              "| Inputs   | Outputs \n" +
                              "|:---------|:--------\n" +
                              "| 7        | 9       \n" +
                              "\n");
                facto.setType(ExerciceType.FASTEST);

                StringBuilder sb;
                StringBuilder sb2;
                for (int i = 1; i <= 5; i++) {
                    sb = new StringBuilder();
                    sb2 = new StringBuilder();
                    String name = "data" + i;
                    try (InputStream is = InitSetupRecruitment.class.
                                     getResourceAsStream(
                                         "/in/rec-ex4/" + name + ".in");
                         BufferedInputStream bis = new BufferedInputStream(is)) {
                        byte[] buffer = new byte[2048];
                        int len;
                        while ((len = bis.read(buffer)) != -1) {
                            sb.append(new String(buffer, 0, len));
                        }
                    } catch (IOException ioe) {
                        Logger.getLogger(InitSetupRecruitment.class.getName()).
                            log(Level.SEVERE, "Cannot read input file", ioe);
                    }
                    try (InputStream is = InitSetupRecruitment.class.
                                     getResourceAsStream(
                                         "/out/rec-ex4/" + name + ".out");
                         BufferedInputStream bis = new BufferedInputStream(is)) {
                        byte[] buffer = new byte[2048];
                        int len;
                        while ((len = bis.read(buffer)) != -1) {
                            sb2.append(new String(buffer, 0, len));
                        }
                    } catch (IOException ioe) {
                        Logger.getLogger(InitSetupRecruitment.class.getName()).
                            log(Level.SEVERE, "Cannot read output file", ioe);
                    }
                    facto.addTestCase(new TestCase().id("rec-exercise4-" + i)
                        .name(name).hidden(false).input(sb.toString())
                        .output(sb2.toString()).order(i));
                }

                sb = new StringBuilder();
                sb2 = new StringBuilder();
                try (InputStream is = InitSetupRecruitment.class.
                                 getResourceAsStream("/in/rec-ex4/data6.in");
                     BufferedInputStream bis = new BufferedInputStream(is)) {
                    byte[] buffer = new byte[2048];
                    int len;
                    while ((len = bis.read(buffer)) != -1) {
                        sb.append(new String(buffer, 0, len));
                    }
                } catch (IOException ioe) {
                    Logger.getLogger(InitSetupRecruitment.class.getName()).log(
                        Level.SEVERE, "Cannot read input file", ioe);
                }
                try (InputStream is = InitSetupRecruitment.class.
                                 getResourceAsStream("/out/rec-ex4/data6.out");
                     BufferedInputStream bis = new BufferedInputStream(is)) {
                    byte[] buffer = new byte[2048];
                    int len;
                    while ((len = bis.read(buffer)) != -1) {
                        sb2.append(new String(buffer, 0, len));
                    }
                } catch (IOException ioe) {
                    Logger.getLogger(InitSetupRecruitment.class.getName()).log(
                        Level.SEVERE, "Cannot read output file", ioe);
                }
                facto.addTestCase(new TestCase().id("rec-exercise4-6")
                    .name("data6").hidden(true).input(sb.toString())
                    .output(sb2.toString()).order(6));

                facto.getTestCases().stream().forEach((testCase) ->
                    mongoTemplate.save(testCase));
                facto.setCoeff(BigDecimal.valueOf(0.35));

                mongoTemplate.save(facto);

                EXERCICES.add(facto);
            }

        recruitment5:
            {
                Exercice pascal = new Exercice();
                pascal.setId("rec-ex5");
                pascal.setTitle("Recruitment exercise 5");
                pascal.setSubtitle("Pascal's triangle");
                pascal.setText("# Recruitment exercise 5\n" +
                               "\n" +
                               "Your goal is to use Pascal's triangle \n" +
                               "\n\n" +
                               "1\n\n" +
                               "1 1\n\n" +
                               "1 2 1\n\n" +
                               "1 3 3 1\n\n" +
                               "1 4 6 4 1\n\n" +
                               "and to calculate the binomial coefficient of a\n" +
                               "given member.\n" +
                               "\n" +
                               "### Inputs\n" +
                               "`N` : the Pascal's triangle row number (starting from 0)\n" +
                               "\n" +
                               "`M` : the rank of the wanted member (starting from 1)\n" +
                               "\n" +
                               "### Outputs\n" +
                               "`R`: The value of the coefficient \n" +
                               "\n" +
                               "### Examples\n" +
                               "\n" +
                               "| Inputs     | Outputs \n" +
                               "|:-----------|:--------\n" +
                               "| 3 2        | 3       \n" +
                               "\n" +
                               "| Inputs     | Outputs \n" +
                               "|:-----------|:--------\n" +
                               "| 3 4        | 1       \n" +
                               "\n" +
                               "| Inputs     | Outputs \n" +
                               "|:-----------|:--------\n" +
                               "| 4 2        | 4       \n" +
                               "\n" +
                               "| Inputs     | Outputs \n" +
                               "|:-----------|:--------\n" +
                               "| 4 3        | 6       \n" +
                               "\n"
                );
                pascal.setType(ExerciceType.FASTEST);

                StringBuilder sb;
                StringBuilder sb2;
                for (int i = 1; i <= 5; i++) {
                    sb = new StringBuilder();
                    sb2 = new StringBuilder();
                    String name = "data" + i;
                    try (InputStream is = InitSetupRecruitment.class.
                                     getResourceAsStream(
                                         "/in/rec-ex5/" + name + ".in");
                         BufferedInputStream bis = new BufferedInputStream(is)) {
                        byte[] buffer = new byte[2048];
                        int len;
                        while ((len = bis.read(buffer)) != -1) {
                            sb.append(new String(buffer, 0, len));
                        }
                    } catch (IOException ioe) {
                        Logger.getLogger(InitSetupRecruitment.class.getName()).
                            log(Level.SEVERE, "Cannot read input file", ioe);
                    }
                    try (InputStream is = InitSetupRecruitment.class.
                                     getResourceAsStream(
                                         "/out/rec-ex5/" + name + ".out");
                         BufferedInputStream bis = new BufferedInputStream(is)) {
                        byte[] buffer = new byte[2048];
                        int len;
                        while ((len = bis.read(buffer)) != -1) {
                            sb2.append(new String(buffer, 0, len));
                        }
                    } catch (IOException ioe) {
                        Logger.getLogger(InitSetupRecruitment.class.getName()).
                            log(Level.SEVERE, "Cannot read output file", ioe);
                    }
                    pascal.addTestCase(new TestCase().id("rec-exercise5-" + i)
                        .name(name).hidden(false).input(sb.toString())
                        .output(sb2.toString()).order(i));
                }

                sb = new StringBuilder();
                sb2 = new StringBuilder();
                try (InputStream is = InitSetupRecruitment.class.
                                 getResourceAsStream("/in/rec-ex5/data6.in");
                     BufferedInputStream bis = new BufferedInputStream(is)) {
                    byte[] buffer = new byte[2048];
                    int len;
                    while ((len = bis.read(buffer)) != -1) {
                        sb.append(new String(buffer, 0, len));
                    }
                } catch (IOException ioe) {
                    Logger.getLogger(InitSetupRecruitment.class.getName()).log(
                        Level.SEVERE, "Cannot read input file", ioe);
                }
                try (InputStream is = InitSetupRecruitment.class.
                                 getResourceAsStream("/out/rec-ex5/data6.out");
                     BufferedInputStream bis = new BufferedInputStream(is)) {
                    byte[] buffer = new byte[2048];
                    int len;
                    while ((len = bis.read(buffer)) != -1) {
                        sb2.append(new String(buffer, 0, len));
                    }
                } catch (IOException ioe) {
                    Logger.getLogger(InitSetupRecruitment.class.getName()).log(
                        Level.SEVERE, "Cannot read output file", ioe);
                }
                pascal.addTestCase(new TestCase().id("rec-exercise5-6")
                    .name("data6").hidden(true).input(sb.toString())
                    .output(sb2.toString()).order(6));

                pascal.getTestCases().stream().forEach((testCase) ->
                    mongoTemplate.save(testCase));
                pascal.setCoeff(BigDecimal.valueOf(0.4));

                mongoTemplate.save(pascal);

                EXERCICES.add(pascal);
            }

        recruitment6:
            {
                Exercice painting = new Exercice();
                painting.setId("rec-ex6");
                painting.setTitle("Recruitment exercise 6");
                painting.setSubtitle("Paint you room !");
                painting.setText("# Recruitment exercise 6\n" +
                                 "\n" +
                                 "Your goal is to calculate the number of paint\n" +
                                 "buckets needed to paint a room \n" +
                                 "\n" +
                                 "Rules : \n" +
                                 " * each bucket can cover 50 m² \n" +
                                 "\n" +
                                 " * you are required to apply 2 layers of paint" +
                                 "\n" +
                                 " * you have to paint the walls and ceiling\n" +
                                 "but not the floor\n" +
                                 "\n" +
                                 " * the room has 4 walls, its ceiling may be\n" +
                                 "leaning ==> bi <= ci, but if it is, the non-rectangular\n" +
                                 "walls are right-angled trapeziums" +
                                 "\n" +
                                 " * you have to exclude from the surface a door\n" +
                                 "(2m²), and a 1m² window per 25 m² of wall." +
                                 "\n" +
                                 "\n" +
                                 "### Inputs\n" +
                                 "`a1 b1 c1 a2 b2 c2 a3 b3 c3 a4 b4 c4` : the\n" +
                                 "dimensions of each wall, counterwise (width,\n" +
                                 "left height, right height) * 4.\n" +
                                 "\n" +
                                 "### Outputs\n" +
                                 "`R`: The number of buckets (integer : every\n" +
                                 "bucket opened is due :) ) \n" +
                                 "\n" +
                                 "### Examples\n" +
                                 "\n" +
                                 "| Inputs                  | Outputs \n" +
                                 "|:------------------------|:--------\n" +
                                 "| 5 3 3 5 3 3 5 3 3 5 3 3 | 5       \n" +
                                 "\n" +
                                 "| Inputs                  | Outputs \n" +
                                 "|:------------------------|:--------\n" +
                                 "| 5 3 4 5 4 4 5 4 3 5 3 3 | 5       \n" +
                                 "\n");
                painting.setType(ExerciceType.FASTEST);

                StringBuilder sb;
                StringBuilder sb2;
                for (int i = 1; i <= 5; i++) {
                    sb = new StringBuilder();
                    sb2 = new StringBuilder();
                    String name = "data" + i;
                    try (InputStream is = InitSetupRecruitment.class.
                                     getResourceAsStream(
                                         "/in/rec-ex6/" + name + ".in");
                         BufferedInputStream bis = new BufferedInputStream(is)) {
                        byte[] buffer = new byte[2048];
                        int len;
                        while ((len = bis.read(buffer)) != -1) {
                            sb.append(new String(buffer, 0, len));
                        }
                    } catch (IOException ioe) {
                        Logger.getLogger(InitSetupRecruitment.class.getName()).
                            log(Level.SEVERE, "Cannot read input file", ioe);
                    }
                    try (InputStream is = InitSetupRecruitment.class.
                                     getResourceAsStream(
                                         "/out/rec-ex6/" + name + ".out");
                         BufferedInputStream bis = new BufferedInputStream(is)) {
                        byte[] buffer = new byte[2048];
                        int len;
                        while ((len = bis.read(buffer)) != -1) {
                            sb2.append(new String(buffer, 0, len));
                        }
                    } catch (IOException ioe) {
                        Logger.getLogger(InitSetupRecruitment.class.getName()).
                            log(Level.SEVERE, "Cannot read output file", ioe);
                    }
                    painting.addTestCase(new TestCase().id("rec-exercise6-" + i)
                        .name(name).hidden(false).input(sb.toString())
                        .output(sb2.toString()).order(i));
                }

                sb = new StringBuilder();
                sb2 = new StringBuilder();
                try (InputStream is = InitSetupRecruitment.class.
                                 getResourceAsStream("/in/rec-ex6/data6.in");
                     BufferedInputStream bis = new BufferedInputStream(is)) {
                    byte[] buffer = new byte[2048];
                    int len;
                    while ((len = bis.read(buffer)) != -1) {
                        sb.append(new String(buffer, 0, len));
                    }
                } catch (IOException ioe) {
                    Logger.getLogger(InitSetupRecruitment.class.getName()).log(
                        Level.SEVERE, "Cannot read input file", ioe);
                }
                try (InputStream is = InitSetupRecruitment.class.
                                 getResourceAsStream("/out/rec-ex6/data6.out");
                     BufferedInputStream bis = new BufferedInputStream(is)) {
                    byte[] buffer = new byte[2048];
                    int len;
                    while ((len = bis.read(buffer)) != -1) {
                        sb2.append(new String(buffer, 0, len));
                    }
                } catch (IOException ioe) {
                    Logger.getLogger(InitSetupRecruitment.class.getName()).log(
                        Level.SEVERE, "Cannot read output file", ioe);
                }
                painting.addTestCase(new TestCase().id("rec-exercise6-6")
                    .name("data6").hidden(true).input(sb.toString())
                    .output(sb2.toString()).order(6));

                painting.getTestCases().stream().forEach((testCase) ->
                    mongoTemplate.save(testCase));
                painting.setCoeff(BigDecimal.valueOf(0.75));

                mongoTemplate.save(painting);

                EXERCICES.add(painting);
            }

        recruitment7:
            {
                Exercice bricks = new Exercice();
                bricks.setId("rec-ex7");
                bricks.setTitle("Recruitment exercise 7");
                bricks.setSubtitle("Building blocks");
                bricks.setText("# Recruitment exercise 7\n" +
                               "\n" +
                               "Your goal is to determine how many Lego bricks\n" +
                               "of each available type you need to build a filled\n" +
                               "pyramid.\n" +
                               "\n" +
                               "You have at your disposal 2 by 4 studs rectangular\n" +
                               "bricks, and 2 by 2 studs square bricks. \n" +
                               "\n" +
                               "You need to minimize the number of square brick\n" +
                               "you use. \n" +
                               "\n" +
                               "### Inputs\n" +
                               "`N` : the size of the base of the pyramid (in\n" +
                               "studs), starting from 2, with a step of 2 (2,\n" +
                               "4, 6, 8, ...).\n" +
                               "\n" +
                               "### Outputs\n" +
                               "`R`: The total number of 2x4 bricks of the pyramid\n" +
                               "\n" +
                               "`S`: The total number of 2x2 bricks of the pyramid\n" +
                               "\n" +
                               "### Examples\n" +
                               "\n" +
                               "| Inputs | Outputs                          \n" +
                               "|:-------|:---------------------------------\n" +
                               "| 2      | 0 1     \n" +
                               "\n" +
                               "| Inputs | Outputs                          \n" +
                               "|:-------|:---------------------------------\n" +
                               "| 4      | 2 1  (--> 2 0 + 0 1)\n" +
                               "\n" +
                               "| Inputs | Outputs                          \n" +
                               "|:-------|:---------------------------------\n" +
                               "| 8      | 14 2 (--> 8 0 + 4 1 + 2 0 + 0 1) \n" +
                               "\n");
                bricks.setType(ExerciceType.FASTEST);

                StringBuilder sb;
                StringBuilder sb2;
                for (int i = 1; i <= 5; i++) {
                    sb = new StringBuilder();
                    sb2 = new StringBuilder();
                    String name = "data" + i;
                    try (InputStream is = InitSetupRecruitment.class.
                                     getResourceAsStream(
                                         "/in/rec-ex7/" + name + ".in");
                         BufferedInputStream bis = new BufferedInputStream(is)) {
                        byte[] buffer = new byte[2048];
                        int len;
                        while ((len = bis.read(buffer)) != -1) {
                            sb.append(new String(buffer, 0, len));
                        }
                    } catch (IOException ioe) {
                        Logger.getLogger(InitSetupRecruitment.class.getName()).
                            log(Level.SEVERE, "Cannot read input file", ioe);
                    }
                    try (InputStream is = InitSetupRecruitment.class.
                                     getResourceAsStream(
                                         "/out/rec-ex7/" + name + ".out");
                         BufferedInputStream bis = new BufferedInputStream(is)) {
                        byte[] buffer = new byte[2048];
                        int len;
                        while ((len = bis.read(buffer)) != -1) {
                            sb2.append(new String(buffer, 0, len));
                        }
                    } catch (IOException ioe) {
                        Logger.getLogger(InitSetupRecruitment.class.getName()).
                            log(Level.SEVERE, "Cannot read output file", ioe);
                    }
                    bricks.addTestCase(new TestCase().id("rec-exercise7-" + i)
                        .name(name).hidden(false).input(sb.toString())
                        .output(sb2.toString()).order(i));
                }

                sb = new StringBuilder();
                sb2 = new StringBuilder();
                try (InputStream is = InitSetupRecruitment.class.
                                 getResourceAsStream("/in/rec-ex7/data6.in");
                     BufferedInputStream bis = new BufferedInputStream(is)) {
                    byte[] buffer = new byte[2048];
                    int len;
                    while ((len = bis.read(buffer)) != -1) {
                        sb.append(new String(buffer, 0, len));
                    }
                } catch (IOException ioe) {
                    Logger.getLogger(InitSetupRecruitment.class.getName()).log(
                        Level.SEVERE, "Cannot read input file", ioe);
                }
                try (InputStream is = InitSetupRecruitment.class.
                                 getResourceAsStream("/out/rec-ex7/data6.out");
                     BufferedInputStream bis = new BufferedInputStream(is)) {
                    byte[] buffer = new byte[2048];
                    int len;
                    while ((len = bis.read(buffer)) != -1) {
                        sb2.append(new String(buffer, 0, len));
                    }
                } catch (IOException ioe) {
                    Logger.getLogger(InitSetupRecruitment.class.getName()).log(
                        Level.SEVERE, "Cannot read output file", ioe);
                }
                bricks.addTestCase(new TestCase().id("rec-exercise7-6")
                    .name("data6").hidden(true).input(sb.toString())
                    .output(sb2.toString()).order(6));

                bricks.getTestCases().stream().forEach((testCase) ->
                    mongoTemplate.save(testCase));
                bricks.setCoeff(BigDecimal.valueOf(0.4));

                mongoTemplate.save(bricks);

                EXERCICES.add(bricks);
            }

    }

    @ChangeSet(order = "02", author = "initiator", id = "02-addChallenge")
    public void addChallenge(MongoTemplate mongoTemplate) {

        Challenge challenge = new Challenge().id("recruitment")
                  .description("Contains all recruitment exercises")
                  .name("Recruitment exercices")
                  .scope(ChallengeScope.RECRUITMENT);
        mongoTemplate.save(challenge);
    }

    @ChangeSet(order = "03", author = "initiator",
               id = "03-addExercicesInChallenges")
    public void addExercicesInChallenges(MongoTemplate mongoTemplate) {

        if (EXERCICES.isEmpty()) {
            EXERCICES.addAll(mongoTemplate.findAll(Exercice.class));
        }

        Challenge recruitment = mongoTemplate.findById("recruitment",
                                                       Challenge.class);
        recruitment.addExerciceItem(getExercice("rec-ex1"))
            .addExerciceItem(getExercice("rec-ex2"))
            .addExerciceItem(getExercice("rec-ex3")).
            addExerciceItem(getExercice("rec-ex4"))
            .addExerciceItem(getExercice("rec-ex5"))
            .addExerciceItem(getExercice("rec-ex6"))
            .addExerciceItem(getExercice("rec-ex7"));
        mongoTemplate.save(recruitment);
    }

    @ChangeSet(order = "04", author = "initiator",
               id = "04-addTeamsToRecruitement")
    @Profile("NTIC")
    public void addTeamsToRecruitement(MongoTemplate mongoTemplate) {
        if (TEAMS.isEmpty()) {
            TEAMS.addAll(mongoTemplate.findAll(Team.class));
        }
        Challenge recruitment = mongoTemplate.findById("recruitment",
                                                       Challenge.class);
        recruitment.setScope(ChallengeScope.RECRUITMENT);
        recruitment.setStartDate(Instant.now().minus(1, ChronoUnit.DAYS));
        recruitment.setEndDate(Instant.now().plus(3650, ChronoUnit.DAYS));
        recruitment.setTeams(TEAMS.stream().collect(Collectors.toSet()));
        mongoTemplate.save(recruitment);
    }
}
