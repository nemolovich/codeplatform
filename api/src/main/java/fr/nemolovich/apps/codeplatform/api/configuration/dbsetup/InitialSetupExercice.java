package fr.nemolovich.apps.codeplatform.api.configuration.dbsetup;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;

import fr.nemolovich.apps.codeplatform.api.model.CodeStarter;
import fr.nemolovich.apps.codeplatform.api.model.Exercice;
import fr.nemolovich.apps.codeplatform.api.model.TestCase;
import fr.nemolovich.apps.codeplatform.api.model.enums.ExerciceType;
import fr.nemolovich.apps.codeplatform.api.model.enums.Language;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * Created by awattez on 10/10/2017.
 */
/**
 * Creates the initial database setup for user
 */
@ChangeLog(order = "003")
@Profile(value = "dev")
public class InitialSetupExercice {

    @ChangeSet(order = "01", author = "initiator", id = "01-addExercices")
    public void addExercices(MongoTemplate mongoTemplate) {

        CodeStarter bashStarter = new CodeStarter().id("ex1-bash-starter")
            .language(Language.BASH)
            .content("# n: the number of integers in list\n"
                + "read n\n"
                + "for (( i=0; i<n; i++ )); do\n"
                + "    # t: an integer\n"
                + "    read t\n"
                + "done\n"
                + "\n"
                + "echo \"sum\"");
        CodeStarter cStarter = new CodeStarter().id("ex1-c-starter")
            .language(Language.C11)
            .content("#include <stdlib.h>\n"
                + "#include <stdio.h>\n"
                + "#include <string.h>\n"
                + "\n"
                + "int main()\n"
                + "{\n"
                + "    int n; // the number of integers in list\n"
                + "    scanf(\"%d\", &n);\n"
                + "    for (int i = 0; i < n; i++) {\n"
                + "        int t; // an integer\n"
                + "        scanf(\"%d\", &t);\n"
                + "    }\n"
                + "\n"
                + "    printf(\"sum\\n\");\n"
                + "\n"
                + "    return 0;\n"
                + "}");
        CodeStarter cppStarter = new CodeStarter().id("ex1-cpp-starter")
            .language(Language.CPP)
            .content("#include <iostream>\n"
                + "#include <string>\n"
                + "#include <vector>\n"
                + "#include <algorithm>\n"
                + "\n"
                + "using namespace std;\n"
                + "\n"
                + "int main()\n"
                + "{\n"
                + "    int n; // the number of integers in list\n"
                + "    cin >> n; cin.ignore();\n"
                + "    for (int i = 0; i < n; i++) {\n"
                + "        int t; // an integer\n"
                + "        cin >> t; cin.ignore();\n"
                + "    }\n"
                + "\n"
                + "    cout << \"sum\" << endl;\n"
                + "}");
        CodeStarter javaStarter = new CodeStarter().id("ex1-java-starter")
            .language(Language.JAVA)
            .content("import java.util.*;\n"
                + "import java.io.*;\n"
                + "import java.math.*;\n"
                + "\n"
                + "class Main {\n"
                + "    public static void main(String args[]) {\n"
                + "        Scanner in = new Scanner(System.in);\n"
                + "        int n = in.nextInt(); // the number of integers in list\n"
                + "        for (int i = 0; i < n; i++) {\n"
                + "            int t = in.nextInt(); // an integer\n"
                + "        }\n"
                + "\n"
                + "        System.out.println(\"sum\");\n"
                + "    }\n"
                + "}");
        CodeStarter jsStarter = new CodeStarter().id("ex1-javascript-starter")
            .language(Language.JAVASCRIPT)
            .content("var n = parseInt(readline()); // the number of integers in list\n"
                + "var inputs = readline().split(' ');\n"
                + "for (var i = 0; i < n; i++) {\n"
                + "    var t = parseInt(inputs[i]); // an integer\n"
                + "}\n"
                + "\n"
                + "print('sum');");
        CodeStarter phpStarter = new CodeStarter().id("ex1-php-starter")
            .language(Language.PHP)
            .content("<?php\n"
                + "\n"
                + "fscanf(STDIN, \"%d\", $n); // the number of integers in list\n"
                + "\n"
                + "$inputs = fgets(STDIN);\n"
                + "$inputs = explode(\" \", $inputs);\n"
                + "for ($i = 0; $i < $n; $i++)\n"
                + "{\n"
                + "    $t = intval($inputs[$i]); // an integer\n"
                + "}\n"
                + "\n"
                + "echo(\"sum\\n\");\n"
                + "?>");
        CodeStarter pythonStarter = new CodeStarter().id("ex1-python-starter")
            .language(Language.PYTHON)
            .content("import sys\n"
                + "import math\n"
                + "\n"
                + "n = input()  # the number of integers in list\n"
                + "for i in raw_input().split():\n"
                + "    # t: an integer\n"
                + "    t = int(i)\n"
                + "\n"
                + "print \"sum\"");
        CodeStarter python3Starter = new CodeStarter().id("ex1-python3-starter")
            .language(Language.PYTHON3)
            .content("import sys\n"
                + "import math\n"
                + "\n"
                + "n = int(input())  # the number of integers in list\n"
                + "for i in input().split():\n"
                + "    # t: an integer\n"
                + "    t = int(i)\n"
                + "\n"
                + "print(\"sum\")");

        Exercice exercice1 = new Exercice();
        exercice1.setId("exercice-1");
        exercice1.setSubtitle("Work with loops");
        exercice1.setText("# Exercice 1\n"
            + "\n"
            + "## Learn to make a sum !\n"
            + "\n"
            + "Your goal is to sum all inputs numbers. You will get a list of numbers separated\n"
            + "by space ('` `'). You have to sum each number and display it.\n"
            + "\n"
            + "### Inputs\n"
            + "`N`: The size of the number list (0 < `N` < 1000000)\n"
            + "`L`: A string containing the list of `N` numbers separated by spaces\n"
            + "\n"
            + "### Outputs\n"
            + "`S`: The sum of the `N` integers in the list\n"
            + "\n"
            + "### Example\n"
            + "\n"
            + "| Inputs                           | Outputs                         \n"
            + "|:---------------------------------|:--------------------------------\n"
            + "| 5                                | 15\n"
            + "| 1 2 3 4 5                        | ");
        exercice1.setTitle("First simple test");
        exercice1.setType(ExerciceType.FASTEST);
        StringBuilder sb = new StringBuilder();
        sb.append("3\n"
            + "1 2 3");
        exercice1.addTestCase(new TestCase().id("test-1")
            .name("t1").hidden(false).input(sb.toString())
            .output("6\n").order(1));
        sb = new StringBuilder();
        sb.append("6\n"
            + "1 1 1 1 1 1");
        exercice1.addTestCase(new TestCase().id("test-2")
            .name("t2").hidden(false).input(sb.toString())
            .output("6\n").order(2));
        sb = new StringBuilder();
        sb.append("10\n"
            + "1 2 1 2 1 2 1 2 1 2");
        exercice1.addTestCase(new TestCase().id("test-3")
            .name("t3").hidden(true).input(sb.toString())
            .output("10\n").order(3));
        sb = new StringBuilder();

        try (InputStream is = InitialSetupExercice.class.getResourceAsStream(
            "/inputs/Many Values.in"); BufferedInputStream bis
            = new BufferedInputStream(is)) {
            byte[] buffer = new byte[2048];
            int len;
            while ((len = bis.read(buffer)) != -1) {
                sb.append(new String(buffer, 0, len));
            }
        } catch (IOException ioe) {
            Logger.getLogger(InitialSetupExercice.class.getName()).log(
                Level.SEVERE, "Cannot read input file", ioe);
        }
        exercice1.addTestCase(new TestCase().id("test-4")
            .name("Many Values").hidden(false).input(sb.toString())
            .output("506255\n").order(4));
        sb = new StringBuilder();

        try (InputStream is = InitialSetupExercice.class.getResourceAsStream(
            "/inputs/More_Values.in"); BufferedInputStream bis
            = new BufferedInputStream(is)) {
            byte[] buffer = new byte[2048];
            int len;
            while ((len = bis.read(buffer)) != -1) {
                sb.append(new String(buffer, 0, len));
            }
        } catch (IOException ioe) {
            Logger.getLogger(InitialSetupExercice.class.getName()).log(
                Level.SEVERE, "Cannot read input file", ioe);
        }
        exercice1.addTestCase(new TestCase().id("test-5")
            .name("More_Values").hidden(true).input(sb.toString())
            .output("50477222\n").order(5));
        exercice1.getTestCases().stream().forEach((testCase)
            -> mongoTemplate.save(testCase));
        exercice1.setCoeff(BigDecimal.valueOf(1.0));
        exercice1.addCodeStarter(bashStarter);
        exercice1.addCodeStarter(cStarter);
        exercice1.addCodeStarter(cppStarter);
        exercice1.addCodeStarter(jsStarter);
        exercice1.addCodeStarter(javaStarter);
        exercice1.addCodeStarter(phpStarter);
        exercice1.addCodeStarter(pythonStarter);
        exercice1.addCodeStarter(python3Starter);
        exercice1.getCodeStarters().stream().forEach((starter)
            -> mongoTemplate.save(starter));

        mongoTemplate.save(exercice1);

        Exercice exercice2 = new Exercice();
        exercice2.setSubtitle("Sous-titre exercice2");
        exercice2.setId("exercice-2");
        exercice2.setText("Text exercice2");
        exercice2.setTitle("Title exercice2");
        exercice2.setType(ExerciceType.SHORTEST);
        exercice2.setCoeff(BigDecimal.valueOf(0.5));
        mongoTemplate.save(exercice2);

        Exercice exercice3 = new Exercice();
        exercice3.setSubtitle("Sous-titre exercice3");
        exercice3.setId("exercice-3");
        exercice3.setText("Text exercice3");
        exercice3.setTitle("Title exercice3");
        exercice3.setType(ExerciceType.REVERSE);
        exercice3.setCoeff(BigDecimal.valueOf(0.5));
        mongoTemplate.save(exercice3);
    }

}
