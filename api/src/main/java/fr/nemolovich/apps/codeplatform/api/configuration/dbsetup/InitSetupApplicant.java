package fr.nemolovich.apps.codeplatform.api.configuration.dbsetup;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import fr.nemolovich.apps.codeplatform.api.model.Applicant;
import fr.nemolovich.apps.codeplatform.api.model.Role;
import fr.nemolovich.apps.codeplatform.api.model.User;
import fr.nemolovich.apps.codeplatform.api.model.enums.DifficultyLevel;
import fr.nemolovich.apps.codeplatform.api.model.enums.Language;
import fr.nemolovich.apps.codeplatform.api.security.Roles;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 *
 * @author bgohier
 */
@ChangeLog(order = "006")
@Profile("dev")
public class InitSetupApplicant {

    private static final Set<User> USERS = new HashSet<>();
    private static final int MAX_APPLICANTS = 18;

    private static User getUser(String id) {
        return USERS.stream().filter(u -> u.getId().equals(id))
            .findFirst().orElse(null);
    }

    @ChangeSet(order = "01", author = "initiator", id = "01-addRole")
    public void addRole(MongoTemplate mongoTemplate) {

        Role role = mongoTemplate.findAll(Role.class).stream()
             .filter(r -> r.getName().equals(Roles.RECRUITER.toString()))
             .findAny().orElse(new Role(Roles.RECRUITER.toString()));
        mongoTemplate.save(role);
    }

    @ChangeSet(order = "02", author = "initiator", id = "02-addApplicants")
    public void addApplicants(MongoTemplate mongoTemplate) {

        if (USERS.isEmpty()) {
            USERS.addAll(mongoTemplate.findAll(User.class));
        }

        User recruiter = getUser("recruiter1");
        List<Integer> range = IntStream.rangeClosed(0, 7).boxed().collect(
                      Collectors.toList());
        String phone;
        for (int i = 1; i <= MAX_APPLICANTS; i++) {
            final int idx = i;
            phone = range.stream().map(x -> String.valueOf((x + idx) % 10)).
            collect(Collectors.joining(""));
            Applicant a =
                      new Applicant().id(String.format("applicant-%02d", i))
                      .recruiter(recruiter)
                      .firstName(String.format("Applicant%02d", i))
                      .lastName(String.format("Name%02d", i))
                      .email(String.format("applicant.%02d@externe.com", i));
            switch (i % 3) {
                case 1:
                    a.setPhone("01".concat(phone));
                    a.setLanguage(Language.JAVASCRIPT);
                    a.setDifficulty(DifficultyLevel.BEGINNER);
                    a.setJobTitle("junior dev");
                    a.setRecruiterNotes("Some notes");
                    break;
                case 2:
                    a.setPhone("02".concat(phone));
                    a.setLanguage(Language.JAVA);
                    a.setDifficulty(DifficultyLevel.INTERMEDIATE);
                    a.setDuration(20);
                    break;
                case 0:
                    a.setLanguage(Language.CPP);
                    a.setDifficulty(DifficultyLevel.EXPERIMENTED);
                    a.setJobTitle("architect");
                    a.setDuration(30);
                    break;
                default:
                    break;
            }
            mongoTemplate.save(a);
        }
    }
}
