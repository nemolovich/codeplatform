package fr.nemolovich.apps.codeplatform.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import fr.nemolovich.apps.codeplatform.api.model.Exercice;
import fr.nemolovich.apps.codeplatform.api.model.Result;
import fr.nemolovich.apps.codeplatform.api.model.Team;

import java.util.Set;

/**
 * Result Repository
 */
@Repository
public interface ResultRepository extends MongoRepository<Result, String>{
    Set<Result> findByExercice(Exercice exercice);
    Set<Result> findByTeam(Team team);
}
