package fr.nemolovich.apps.codeplatform.api.repository;

import fr.nemolovich.apps.codeplatform.api.model.CodeHelper;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the CodeHelper entiry management.
 */
public interface CodeHelperRepository extends
    MongoRepository<CodeHelper, String> {

}
