package fr.nemolovich.apps.codeplatform.api.webapi.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import fr.nemolovich.apps.codeplatform.api.webapi.UtilsApi;
import java.io.File;
import java.net.URISyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * Created by Ionut on 08/10/2017.
 */
@Controller
public class UtilsApiController implements UtilsApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(
                                UtilsApiController.class);

    private static final String FRONT_CONFIG_FILE_PATH = "front.config.path";

    private final ObjectMapper objectMapper;

    public UtilsApiController(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    private final File getFrontConfigFile() {

        File f = null;
        if (System.getProperty(FRONT_CONFIG_FILE_PATH) != null) {
            f = new File(System.getProperty(FRONT_CONFIG_FILE_PATH));
        } else {
            try {
                f = new File(UtilsApiController.class.getResource(
                "/front-config.yaml").toURI());
            } catch (URISyntaxException ex) {
                LOGGER.error("Cannot get front config file", ex);
            }
        }
        return f.exists() && f.isFile() && f.canRead() ? f : null;
    }

    @Override
    public ResponseEntity<Object> getFrontConfig(
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {

        if (accept != null && accept.contains("application/json")) {
            File configFile = getFrontConfigFile();
            if (configFile != null) {
                ObjectMapper yamlMapper = new ObjectMapper(new YAMLFactory());
                Object config =
                       yamlMapper.readValue(configFile, Object.class);
                String json = this.objectMapper.writeValueAsString(config);
                return ResponseEntity.ok(json);
            }
        }
        return ResponseEntity.badRequest().body(null);
    }

}
