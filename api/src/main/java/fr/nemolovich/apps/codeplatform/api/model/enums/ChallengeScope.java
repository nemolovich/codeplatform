package fr.nemolovich.apps.codeplatform.api.model.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets ChallengeScope
 */
public enum ChallengeScope {

    CHALLENGE("challenge"),
    COURSE("course"),
    RECRUITMENT("recruitment");

    private final String value;

    ChallengeScope(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static ChallengeScope fromValue(String text) {
        for (ChallengeScope b : ChallengeScope.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        return null;
    }
}
