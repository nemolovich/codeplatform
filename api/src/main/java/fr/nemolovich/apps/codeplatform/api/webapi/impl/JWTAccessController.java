package fr.nemolovich.apps.codeplatform.api.webapi.impl;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.nemolovich.apps.codeplatform.api.model.User;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.LoginViewModel;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.UserViewModel;
import fr.nemolovich.apps.codeplatform.api.security.jwt.JWTConfigurer;
import fr.nemolovich.apps.codeplatform.api.security.jwt.JWTFilter;
import fr.nemolovich.apps.codeplatform.api.security.jwt.TokenProvider;
import fr.nemolovich.apps.codeplatform.api.service.UserService;
import fr.nemolovich.apps.codeplatform.api.webapi.JWTAccessApi;
import io.swagger.annotations.ApiParam;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2017-10-01T18:58:34.924+02:00")
@Controller
public class JWTAccessController implements JWTAccessApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        JWTAccessController.class);

    private final TokenProvider tokenProvider;
    private final AuthenticationManager authenticationManager;
    private final ObjectMapper objectMapper;
    private final UserService userService;

    public JWTAccessController(ObjectMapper objectMapper,
        TokenProvider tokenProvider,
        AuthenticationManager authenticationManager,
        UserService userService) {
        this.objectMapper = objectMapper;
        this.tokenProvider = tokenProvider;
        this.authenticationManager = authenticationManager;
        this.userService = userService;
    }

    @Override
    public ResponseEntity<UserViewModel> activateAccount(
        @NotNull @ApiParam(value = "Login of user to activate", required = true)
        @Valid @RequestParam(value = "login", required = true) String login,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        if (accept != null && accept.contains("application/json")) {
            Optional<User> user = userService.activateRegistration(login);
            if (user.isPresent() && !user.get().getId().isEmpty()) {
                return ResponseEntity.ok(new UserViewModel(user.get()));
            }
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<UserViewModel> authorize(
        @ApiParam(value = "LoginViewModel", required = true)
        @Valid @RequestBody LoginViewModel LoginVM,
        @RequestHeader(value = "Accept", required = false) String accept,
        HttpServletResponse response) throws Exception {
        UsernamePasswordAuthenticationToken authenticationToken
            = new UsernamePasswordAuthenticationToken(LoginVM.getUsername(),
                LoginVM.getPassword());
        try {
            Authentication authentication = this.authenticationManager
                .authenticate(authenticationToken);
            SecurityContextHolder.getContext().setAuthentication(
                authentication);
            String jwt = tokenProvider.createToken(authentication);
            response.addHeader(JWTConfigurer.AUTHORIZATION_HEADER,
                JWTFilter.TOKEN_PREFIX + jwt);
//            return ResponseEntity.ok(new JWTTokenViewModel(jwt));
            return ResponseEntity.ok(this.userService.getUserViewModelByLogin(
                LoginVM.getUsername()));
        } catch (AuthenticationException ae) {
            LOGGER.trace("Authentication exception trace: {}", ae);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @Override
    public ResponseEntity<UserViewModel> isAuthenticated(
        @RequestHeader(value = "Accept", required = false) String accept,
        HttpServletRequest httpServletRequest) throws Exception {
        if (accept != null && accept.contains("application/json")) {
            Optional<User> user = userService.getCurrentUser();
            if (user.isPresent()) {
                return ResponseEntity.ok(new UserViewModel(user.get()));
            } else {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

}
