package fr.nemolovich.apps.codeplatform.api.model.viewmodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.nemolovich.apps.codeplatform.api.model.Result;
import io.swagger.annotations.ApiModelProperty;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

/**
 * Result
 */
@Validated
@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2017-10-07T19:30:45.309+02:00")
public class ResultViewModel {

    @JsonProperty("id")
    @NotNull
    private String id = null;

    @JsonProperty("exercice")
    private ExerciceViewModel exercice = null;

    @JsonProperty("team")
    private TeamViewModel team = null;

    @JsonProperty("submissions")
    private List<SubmissionViewModel> submissions = new ArrayList<>();

    @JsonProperty("startDate")
    private Instant startDate;

    @JsonProperty("endDate")
    private Instant endDate;

    @JsonProperty("reopenDate")
    private Instant reopenDate;

    public ResultViewModel id(String id) {
        this.id = id;
        return this;
    }

    public ResultViewModel(Result result) {
        this.id = result.getId();
        this.exercice = new ExerciceViewModel(result.getExercice());
        this.team = new TeamViewModel(result.getTeam());
        this.startDate = result.getStartDate();
        this.endDate = result.getEndDate();
        this.reopenDate = result.getReopenDate();
        this.submissions = result.getSubmissions().stream()
        .map(s -> new SubmissionViewModel(s))
        .collect(Collectors.toList());
    }

    /**
     * Get id
     *
     * @return id
     *
     */
    @ApiModelProperty(value = "")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ResultViewModel exercice(ExerciceViewModel exercice) {
        this.exercice = exercice;
        return this;
    }

    /**
     * Get exercice
     *
     * @return exercice
     *
     */
    @ApiModelProperty(value = "")
    public ExerciceViewModel getExercice() {
        return exercice;
    }

    public void setExercice(ExerciceViewModel exercice) {
        this.exercice = exercice;
    }

    public ResultViewModel team(TeamViewModel team) {
        this.team = team;
        return this;
    }

    /**
     * Get team
     *
     * @return team
     *
     */
    @ApiModelProperty(value = "")
    public TeamViewModel getTeam() {
        return team;
    }

    public void setTeam(TeamViewModel team) {
        this.team = team;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Instant getReopenDate() {
        return reopenDate;
    }

    public void setReopenDate(Instant reopenDate) {
        this.reopenDate = reopenDate;
    }

    public ResultViewModel submissions(List<SubmissionViewModel> submissions) {
        this.submissions = submissions;
        return this;
    }

    public ResultViewModel addSubmissionsItem(
        SubmissionViewModel submissionsItem) {
        if (this.submissions == null) {
            this.submissions = new ArrayList<>();
        }
        this.submissions.add(submissionsItem);
        return this;
    }

    /**
     * Get submissions
     *
     * @return submissions
     *
     */
    @ApiModelProperty(value = "")
    @Valid
    public List<SubmissionViewModel> getSubmissions() {
        return submissions;
    }

    public void setSubmissions(List<SubmissionViewModel> submissions) {
        this.submissions = submissions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ResultViewModel result = (ResultViewModel) o;
        return Objects.equals(this.id, result.id) &&
               Objects.equals(this.exercice, result.exercice) &&
               Objects.equals(this.team, result.team) &&
               Objects.equals(this.submissions, result.submissions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, exercice, team, submissions);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Result {\n");
        sb.append("    id: ").append(toIndentedString(id))
            .append("\n");
        sb.append("    exercice: ").append(toIndentedString(exercice))
            .append("\n");
        sb.append("    team: ").append(toIndentedString(team))
            .append("\n");
        sb.append("    startDate: ").append(toIndentedString(startDate))
            .append("\n");
        sb.append("    endDate: ").append(toIndentedString(endDate))
            .append("\n");
        sb.append("    reopenDate: ").append(toIndentedString(reopenDate))
            .append("\n");
        sb.append("    submissions: ").append(toIndentedString(submissions))
            .append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     *
     * @param o {@link Object}: Object to format.
     * @return {@link String}: String representation of object.
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
