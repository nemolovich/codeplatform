package fr.nemolovich.apps.codeplatform.api.model.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import java.math.BigDecimal;

/**
 * Level of exercice difficulty
 */
public enum DifficultyLevel {

    BEGINNER("beginner", BigDecimal.valueOf(0.0), BigDecimal.valueOf(1.0 / 3)),
    INTERMEDIATE("intermediate", BigDecimal.valueOf(1.0 / 3), BigDecimal
                 .valueOf(2.0 / 3)),
    EXPERIMENTED("experimented", BigDecimal.valueOf(2.0 / 3), BigDecimal.
                 valueOf(1.0)
    );

    private final String value;
    private final BigDecimal minCoeff;
    private final BigDecimal maxCoeff;

    DifficultyLevel(String value) {
        this.value = value;
        this.minCoeff = BigDecimal.valueOf(0.0);
        this.maxCoeff = BigDecimal.valueOf(1.0);
    }

    DifficultyLevel(String value, BigDecimal minCoeff, BigDecimal maxCoeff) {
        this.value = value;
        this.minCoeff = minCoeff;
        this.maxCoeff = maxCoeff;
    }

    public BigDecimal getMinCoeff() {
        return this.minCoeff;
    }

    public BigDecimal getMaxCoeff() {
        return this.maxCoeff;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static DifficultyLevel fromValue(String text) {
        for (DifficultyLevel b : DifficultyLevel.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        return null;
    }
}
