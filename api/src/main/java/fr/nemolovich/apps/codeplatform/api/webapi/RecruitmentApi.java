package fr.nemolovich.apps.codeplatform.api.webapi;

import fr.nemolovich.apps.codeplatform.api.model.Applicant;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.ApplicantResult;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.ApplicantViewModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.Set;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Recruitment API
 *
 * @author bgohier
 */
@Api(value = "Recruitment API", description = "The Recruitment API",
     tags = {"recruitment"})
public interface RecruitmentApi {

    @ApiOperation(value = "Create a new Applicant", notes = "",
                  response = ApplicantResult.class,
                  tags = {"recruitment",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The created applicant",
                     response = ApplicantResult.class),
        @ApiResponse(code = 204, message = "No Content",
                     response = Object.class),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/recruitment/createapplicant",
                    produces = {"application/json"},
                    consumes = {"application/json"},
                    method = RequestMethod.POST)
    ResponseEntity<ApplicantResult> createApplicant(
        @ApiParam(value = "Details about an applicant", required = true)
        @Valid @RequestBody Applicant applicant,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Update an Applicant", notes = "",
                  response = ApplicantViewModel.class,
                  tags = {"recruitment",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The updated applicant",
                     response = ApplicantViewModel.class),
        @ApiResponse(code = 204, message = "No Content",
                     response = Object.class),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/recruitment/updateapplicant",
                    produces = {"application/json"},
                    consumes = {"application/json"},
                    method = RequestMethod.PUT)
    ResponseEntity<ApplicantViewModel> updateApplicant(
        @ApiParam(value = "Applicant to update", required = true)
        @Valid @RequestBody Applicant applicant,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Delete an applicant", notes = "",
                  response = ApplicantViewModel.class,
                  tags = {"recruitment",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The updated applicant",
                     response = ApplicantViewModel.class),
        @ApiResponse(code = 204, message = "No Content",
                     response = Object.class),
        @ApiResponse(code = 304, message = "Not Modified",
                     response = Object.class),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/recruitment/applicant/delete/{applicantId}",
                    produces = {"application/json"},
                    method = RequestMethod.DELETE)
    ResponseEntity<ApplicantViewModel> deleteApplicant(
        @ApiParam(value = "ID of the applicant to delete", required = true)
        @PathVariable("applicantId") String applicantId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Start test for an applicant", notes = "",
                  response = ApplicantViewModel.class,
                  tags = {"recruitment",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The updated applicant",
                     response = ApplicantViewModel.class),
        @ApiResponse(code = 204, message = "No Content",
                     response = Object.class),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/recruitment/starttest",
                    produces = {"application/json"},
                    consumes = {"application/json"},
                    method = RequestMethod.POST)
    ResponseEntity<ApplicantViewModel> startTest(
        @ApiParam(value = "Details about the applicant", required = true)
        @Valid @RequestBody ApplicantViewModel applicant,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Get all applicants",
                  response = ApplicantViewModel.class,
                  responseContainer = "Set", tags = {"recruitment",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The list of applicants",
                     response = ApplicantViewModel.class,
                     responseContainer = "Set"),
        @ApiResponse(code = 204, message = "No Content",
                     response = Object.class),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/recruitment/applicant",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    ResponseEntity<Set<ApplicantViewModel>> getAllApplicants(
        @ApiParam(value = "Type of challenge to look for",
                  allowableValues = "owned, accepted")
        @Valid @RequestParam(value = "filter", required = false) String filter,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Get an applicant by ID",
                  response = ApplicantResult.class,
                  tags = {"recruitment",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The corresponding applicant",
                     response = ApplicantResult.class),
        @ApiResponse(code = 204, message = "No Content",
                     response = Object.class),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/recruitment/applicant/{applicantId}",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    ResponseEntity<ApplicantResult> getApplicantByID(
        @ApiParam(value = "ID of the requested applicant", required = true)
        @PathVariable("applicantId") String applicantId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Get an applicant by challenge ID",
                  response = ApplicantResult.class,
                  tags = {"recruitment",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The corresponding applicant",
                     response = ApplicantResult.class),
        @ApiResponse(code = 204, message = "No Content",
                     response = Object.class),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})

    @RequestMapping(value = "/api/recruitment/applicant/challenge/{challengeId}",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    ResponseEntity<ApplicantResult> getApplicantByChallengeID(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Get applicant results as ZIP",
                  response = byte[].class,
                  tags = {"recruitment",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The result of test",
                     response = byte[].class),
        @ApiResponse(code = 204, message = "No Content",
                     response = Object.class),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/recruitment/applicant/{challengeId}/results",
                    produces = {"application/zip"},
                    method = RequestMethod.GET)
    ResponseEntity<byte[]> getApplicantResultDetails(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;
}
