package fr.nemolovich.apps.codeplatform.api.configuration;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.threeten.bp.LocalDate;
import org.threeten.bp.OffsetDateTime;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;

@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2017-10-07T19:30:45.309+02:00")
@Configuration
public class SwaggerDocumentationConfig {

    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("CodePlatform Swagger")
            .description("CodePlatform REST API")
            .license("GNU General Public License v3.0")
            .licenseUrl("https://www.gnu.org/licenses/gpl-3.0")
            .termsOfServiceUrl("")
            .version("0.1.1")
            .contact(new Contact("YellowBuilders Team",
                                 "https://gitlab.com/nemolovich/codeplatform",
                                 "brian.gohier@hotmail.fr;dan.geffroy@gmail.com;adrien.wattez@gmail.com;ionut.iortoman@gmail.com")).
            build();
    }

    @Bean
    public Docket customImplementation() {
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage(
                "fr.nemolovich.apps.codeplatform.api.webapi"))
            .paths(PathSelectors.regex("/api/.*"))
            .build()
            .pathMapping("/")
            .securitySchemes(Arrays.asList(securityScheme()))
            .securityContexts(Arrays.asList(securityContext()))
            .directModelSubstitute(LocalDate.class, java.sql.Date.class)
            .directModelSubstitute(OffsetDateTime.class, Date.class)
            .apiInfo(apiInfo());
    }

    @Bean
    springfox.documentation.swagger.web.SecurityConfiguration security() {
        return SecurityConfigurationBuilder.builder()
            .clientId("id")
            .clientSecret("password")
            .realm("realm")
            .appName("Codeplatform")
            .scopeSeparator(",")
            .additionalQueryStringParams(null)
            .useBasicAuthenticationWithAccessCodeGrant(false)
            .build();
    }

    private ApiKey securityScheme() {
        return new ApiKey(HttpHeaders.AUTHORIZATION, HttpHeaders.AUTHORIZATION,
                          ApiKeyVehicle.HEADER.getValue());
    }

    private List<SecurityReference> defaultAuth() {
        return Arrays.asList(new SecurityReference(HttpHeaders.AUTHORIZATION,
                                                   scopes()));
    }

    private AuthorizationScope[] scopes() {
        AuthorizationScope[] scopes = {
            new AuthorizationScope("user", "Acess to User API"),
            new AuthorizationScope("recruiter", "Access to Recruiter API"),
            new AuthorizationScope("admin", "Access to Admin API")};
        return scopes;
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
            .securityReferences(defaultAuth())
            .forPaths(PathSelectors.regex("/api/.*"))
            .build();
    }
}
