package fr.nemolovich.apps.codeplatform.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.validation.annotation.Validated;

/**
 * Result
 */
@Validated
@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2017-10-07T19:30:45.309+02:00")
@Document(collection = "codeplatform_result")
public class Result {

    @JsonProperty("id")
    @Id
    private String id = null;

    @JsonProperty("exercice")
    @DBRef
    @NotNull
    private Exercice exercice = null;

    @JsonProperty("team")
    @DBRef
    @NotNull
    private Team team = null;

    @JsonProperty("submissions")
    @Valid
    @DBRef
    private List<Submission> submissions = new ArrayList<>();

    @JsonProperty("startDate")
    private Instant startDate;

    @JsonProperty("endDate")
    private Instant endDate;

    @JsonProperty("reopenDate")
    private Instant reopenDate;

    public Result id(String id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     *
     */
    @ApiModelProperty(value = "")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Result exercice(Exercice exercice) {
        this.exercice = exercice;
        return this;
    }

    /**
     * Get exercice
     *
     * @return exercice
     *
     */
    @ApiModelProperty(value = "")
    public Exercice getExercice() {
        return exercice;
    }

    public void setExercice(Exercice exercice) {
        this.exercice = exercice;
    }

    public Result team(Team team) {
        this.team = team;
        return this;
    }

    /**
     * Get team
     *
     * @return team
     *
     */
    @ApiModelProperty(value = "")
    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Result submissions(List<Submission> submissions) {
        this.submissions = submissions;
        return this;
    }

    public Result addSubmissionItem(Submission submission) {
        if (this.submissions == null) {
            this.submissions = new ArrayList<>();
        }
        if (!this.submissions.contains(submission)) {
            this.submissions.add(submission);
        }
        return this;
    }

    public Result removeSubmissionItem(Submission submission) {
        if (this.submissions.contains(submission)) {
            this.submissions.remove(submission);
        }
        return this;
    }

    /**
     * Get submissions
     *
     * @return submissions
     *
     */
    @ApiModelProperty(value = "")
    @Valid
    public List<Submission> getSubmissions() {
        return submissions;
    }

    public void setSubmissions(List<Submission> submissions) {
        this.submissions = submissions;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Instant getReopenDate() {
        return reopenDate;
    }

    public void setReopenDate(Instant reopenDate) {
        this.reopenDate = reopenDate;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Result result = (Result) o;
        return Objects.equals(this.id, result.id) &&
               Objects.equals(this.exercice, result.exercice) &&
               Objects.equals(this.team, result.team);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, exercice, team);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Result {\n");
        sb.append("    id: ").append(toIndentedString(id))
            .append("\n");
        sb.append("    exercice: ").append(toIndentedString(exercice))
            .append("\n");
        sb.append("    team: ").append(toIndentedString(team))
            .append("\n");
        sb.append("    submissions: ").append(toIndentedString(submissions))
            .append("\n");
        sb.append("    startDate: ").append(toIndentedString(startDate))
            .append("\n");
        sb.append("    endDate: ").append(toIndentedString(endDate))
            .append("\n");
        sb.append("    reopenDate: ").append(toIndentedString(reopenDate))
            .append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     *
     * @param o {@link Object}: Object to format.
     * @return {@link String}: String representation of object.
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
