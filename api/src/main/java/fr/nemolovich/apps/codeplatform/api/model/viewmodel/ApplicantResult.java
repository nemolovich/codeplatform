package fr.nemolovich.apps.codeplatform.api.model.viewmodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import fr.nemolovich.apps.codeplatform.api.model.Applicant;
import fr.nemolovich.apps.codeplatform.api.model.enums.DifficultyLevel;
import fr.nemolovich.apps.codeplatform.api.model.enums.Language;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

/**
 *
 * @author bgohier
 */
@Validated
@javax.annotation.Generated(
    value = "bgohier",
    date = "2018-03-04T14:55:00.000+02:00")
public class ApplicantResult {

    @JsonProperty("id")
    @NotNull
    private String id;

    @JsonProperty("recruiter")
    @NotNull
    private UserViewModel recruiter;

    @JsonProperty("firstName")
    @NotNull
    private String firstName;

    @JsonProperty("lastName")
    @NotNull
    private String lastName;

    @JsonProperty("email")
    @NotNull
    private String email;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("jobTitle")
    private String jobTitle;

    @JsonProperty("recruiterNotes")
    private String recruiterNotes;

    @JsonProperty("language")
    @NotNull
    private Language language;

    @JsonProperty("difficulty")
    @NotNull
    private DifficultyLevel difficulty;

    @JsonProperty("duration")
    @NotNull
    private Integer duration;

    @JsonProperty("challenge")
    @NotNull
    private ChallengeLightViewModel challenge;

    @JsonProperty("cv")
    private byte[] cv = null;

    @JsonProperty("accepted")
    private Boolean accepted = null;

    public ApplicantResult() {
    }

    public ApplicantResult(Applicant applicant) {
        this.id = applicant.getId();
        this.recruiter = new UserViewModel(applicant.getRecruiter());
        this.firstName = applicant.getFirstName();
        this.lastName = applicant.getLastName();
        this.email = applicant.getEmail();
        this.phone = applicant.getPhone();
        this.jobTitle = applicant.getJobTitle();
        this.recruiterNotes = applicant.getRecruiterNotes();
        this.language = applicant.getLanguage();
        this.difficulty = applicant.getDifficulty();
        this.duration = applicant.getDuration();
        this.challenge = applicant.getChallenge() != null ?
                         new ChallengeLightViewModel(applicant.getChallenge()) :
                         null;
        this.cv = applicant.getCv();
        this.accepted = applicant.getAccepted();
    }

    /**
     * Get id
     *
     * @return id
     *
     */
    @ApiModelProperty(value = "")
    public String getId() {
        return this.id;
    }

    /**
     * Set id
     *
     * @param id id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Get recruiter
     *
     * @return recruiter
     *
     */
    @ApiModelProperty(value = "")
    public UserViewModel getRecruiter() {
        return this.recruiter;
    }

    /**
     * Set recruiter
     *
     * @param recruiter recruiter to set
     */
    public void setRecruiter(UserViewModel recruiter) {
        this.recruiter = recruiter;
    }

    /**
     * Get firstname
     *
     * @return firstname
     *
     */
    @ApiModelProperty(value = "")
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * Set firstName
     *
     * @param firstName firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Get lastName
     *
     * @return lastName
     *
     */
    @ApiModelProperty(value = "")
    public String getLastName() {
        return this.lastName;
    }

    /**
     * Set lastName
     *
     * @param lastName lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Get email
     *
     * @return email
     *
     */
    @ApiModelProperty(value = "")
    public String getEmail() {
        return this.email;
    }

    /**
     * Set email
     *
     * @param email email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get phone
     *
     * @return phone
     *
     */
    @ApiModelProperty(value = "")
    public String getPhone() {
        return this.phone;
    }

    /**
     * Set phone
     *
     * @param phone phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Get jobTitle
     *
     * @return jobTitle
     *
     */
    @ApiModelProperty(value = "")
    public String getJobTitle() {
        return this.jobTitle;
    }

    /**
     * Set jobTitle
     *
     * @param jobTitle jobTitle to set
     */
    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    /**
     * Get recruiterNotes
     *
     * @return recruiterNotes
     *
     */
    @ApiModelProperty(value = "")
    public String getRecruiterNotes() {
        return this.recruiterNotes;
    }

    /**
     * Set recruiterNotes
     *
     * @param recruiterNotes recruiterNotes to set
     */
    public void setRecruiterNotes(String recruiterNotes) {
        this.recruiterNotes = recruiterNotes;
    }

    /**
     * Get language
     *
     * @return language
     *
     */
    @ApiModelProperty(value = "")
    public Language getLanguage() {
        return this.language;
    }

    /**
     * Set language
     *
     * @param language language to set
     */
    public void setLanguage(Language language) {
        this.language = language;
    }

    /**
     * Get difficulty
     *
     * @return difficulty
     *
     */
    @ApiModelProperty(value = "")
    public DifficultyLevel getDifficulty() {
        return this.difficulty;
    }

    /**
     * Set difficulty
     *
     * @param difficulty difficulty to set
     */
    public void setDifficulty(DifficultyLevel difficulty) {
        this.difficulty = difficulty;
    }

    /**
     * Get duration
     *
     * @return duration
     *
     */
    @ApiModelProperty(value = "")
    public Integer getDuration() {
        return this.duration;
    }

    /**
     * Set duration
     *
     * @param duration duration to set
     */
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    /**
     * Get challenge
     *
     * @return challenge
     *
     */
    @ApiModelProperty(value = "")
    public ChallengeLightViewModel getChallenge() {
        return this.challenge;
    }

    /**
     * Set challenge
     *
     * @param challenge challenge to set
     */
    public void setChallenge(ChallengeLightViewModel challenge) {
        this.challenge = challenge;
    }

    /**
     * Get cv
     *
     * @return cv
     *
     */
    @ApiModelProperty(value = "")
    public byte[] getCv() {
        return this.cv;
    }

    /**
     * Set cv
     *
     * @param cv cv to set
     */
    public void setCv(byte[] cv) {
        this.cv = cv;
    }

    /**
     * Get accepted
     *
     * @return accepted
     *
     */
    @ApiModelProperty(value = "")
    public Boolean getAccepted() {
        return this.accepted;
    }

    /**
     * Set accepted
     *
     * @param accepted accepted to set
     */
    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ApplicantResult applicant = (ApplicantResult) o;
        return Objects.equals(this.recruiter, applicant.recruiter) &&
               Objects.equals(this.firstName, applicant.firstName) &&
               Objects.equals(this.lastName, applicant.lastName) &&
               Objects.equals(this.email, applicant.email) &&
               Objects.equals(this.phone, applicant.phone) &&
               Objects.equals(this.jobTitle, applicant.jobTitle) &&
               Objects.equals(this.recruiterNotes, applicant.recruiterNotes) &&
               Objects.equals(this.language, applicant.language) &&
               Objects.equals(this.difficulty, applicant.difficulty) &&
               Objects.equals(this.duration, applicant.duration) &&
               Objects.equals(this.challenge, applicant.challenge) &&
               Objects.equals(this.accepted, applicant.accepted);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.recruiter, this.firstName, this.lastName,
                            this.email, this.phone, this.jobTitle,
                            this.recruiterNotes, this.language, this.difficulty,
                            this.duration, this.challenge, this.accepted);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Applicant {\n");
        sb.append("    id: ").append(toIndentedString(this.id)).append("\n");
        sb.append("    recruiter: ").append(
            toIndentedString(this.recruiter)).append("\n");
        sb.append("    firstName: ").append(
            toIndentedString(this.firstName)).append("\n");
        sb.append("    lastName: ").append(
            toIndentedString(this.lastName)).append("\n");
        sb.append("    email: ").append(
            toIndentedString(this.email)).append("\n");
        sb.append("    phone: ").append(
            toIndentedString(this.phone)).append("\n");
        sb.append("    jobTitle: ").append(
            toIndentedString(this.jobTitle)).append("\n");
        sb.append("    recruiterNotes: ").append(
            toIndentedString(this.recruiterNotes)).append("\n");
        sb.append("    language: ").append(
            toIndentedString(this.language)).append("\n");
        sb.append("    difficulty: ").append(
            toIndentedString(this.difficulty)).append("\n");
        sb.append("    duration: ").append(
            toIndentedString(this.duration)).append("\n");
        sb.append("    challenge: ").append(
            toIndentedString(this.challenge)).append("\n");
        sb.append("    accepted: ").append(
            toIndentedString(this.accepted)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     *
     * @param o {@link Object}: Object to format.
     * @return {@link String}: String representation of object.
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
