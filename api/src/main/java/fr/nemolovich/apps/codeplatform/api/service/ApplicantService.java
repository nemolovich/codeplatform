package fr.nemolovich.apps.codeplatform.api.service;

import fr.nemolovich.apps.codeplatform.api.model.Applicant;
import fr.nemolovich.apps.codeplatform.api.repository.ApplicantRepository;
import fr.nemolovich.apps.codeplatform.api.repository.ChallengeRepository;
import fr.nemolovich.apps.codeplatform.api.webapi.exception.NotFoundException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Service class for managing applicant.
 */
@Service
public class ApplicantService {

    private static final Logger LOGGER = LoggerFactory.getLogger(
                                ApplicantService.class);

    private final ApplicantRepository applicantRepository;

    public ApplicantService(ChallengeRepository challengeRepository,
                            ApplicantRepository applicantRepository) {
        this.applicantRepository = applicantRepository;
    }

    public Applicant createApplicant(Applicant applicant) {
        LOGGER.debug("Create Applicant:[{}]", applicant.toString());
        return this.applicantRepository.save(applicant);
    }

    public Applicant updateApplicant(Applicant applicant) {
        LOGGER.debug("Update Applicant:[{}]", applicant.toString());
        return this.applicantRepository.save(applicant);
    }

    public Applicant deleteApplicant(String applicantId) throws NotFoundException {
        Optional<Applicant> applicant = this.applicantRepository.findOneById(
                            applicantId);
        if (applicant.isPresent()) {
            this.applicantRepository.delete(applicant.get());
            LOGGER.debug("Delete Applicant:[{}]", applicant.toString());
            return applicant.get();
        } else {
            throw new NotFoundException(202, "Applicant not found");
        }
    }

    public void removeApplicant(Applicant applicant) {
        LOGGER.debug("Remove Applicant:[{}]", applicant.toString());
        this.applicantRepository.delete(applicant);
    }

    public Set<Applicant> getAllApplicants() {
        LOGGER.debug("Find all Applicant");
        return new HashSet<>(applicantRepository.findAll());
    }

    public Applicant getApplicantById(String id) {
        LOGGER.debug("Find Applicant by ID:[{}]", id);
        return applicantRepository.findOne(id);
    }

    public Applicant getApplicantByChallengeId(String id) {
        LOGGER.debug("Find Applicant by challenge ID:[{}]", id);
        return applicantRepository.findOneByChallengeId(id).orElse(null);
    }

    public boolean applicantExists(String id) {
        LOGGER.debug("Applicant Exists ? ID:[{}]", id);
        return applicantRepository.exists(id);
    }

}
