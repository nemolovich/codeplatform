package fr.nemolovich.apps.codeplatform.pdf;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import org.apache.commons.collections.ExtendedProperties;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.resource.Resource;
import org.apache.velocity.runtime.resource.loader.FileResourceLoader;

/**
 *
 * @author bgohier
 */
public class CustomResourceLoader extends FileResourceLoader {

    @Override
    public void init(ExtendedProperties configuration) {
        log.trace("CustomResourceLoader : initialization starting.");
        super.init(configuration);
    }

    @Override
    public InputStream getResourceStream(String source) throws
        ResourceNotFoundException {

        InputStream in = null;

        log.trace(String.format(
            "CustomResourceLoader : Try to load resource %s.", source));
        File sourceFile = new File(source);

        if (sourceFile.exists()) {
            try {
                in = new BufferedInputStream(new FileInputStream(sourceFile));
            } catch (FileNotFoundException ex) {
                log.error(String.format(
                    "CustomResourceLoader : Cannot to load resource %s.",
                    source), ex);
            }
        } else {
            ClassLoader classLoader = Thread.currentThread().
                        getContextClassLoader();
            if (classLoader == null) {
                classLoader = CustomResourceLoader.class.getClassLoader();
            }

            in = classLoader.getResourceAsStream(sourceFile.getPath());
            if (in == null) {
                in = classLoader.getResourceAsStream(
                String.format("/%s", sourceFile.getName()));
            }
        }
        if (in == null) {
            log.error(String.format(
                "CustomResourceLoader : Cannot to load resource %s.", source));
        }
        return in;
    }

    @Override
    public boolean isSourceModified(Resource resource
    ) {

        boolean modified = true;

        File file = new File(resource.getName());
        if (file.exists() && file.canRead()) {
            modified = file.lastModified() != resource.getLastModified();
        }

        return modified;
    }

    @Override
    public long getLastModified(Resource resource
    ) {
        File file = new File(resource.getName());

        if (file.canRead()) {
            return file.lastModified();
        } else {
            return 0;
        }
    }

    @Override
    public boolean resourceExists(String name
    ) {
        File file = new File(name);
        return file.canRead();
    }

}
