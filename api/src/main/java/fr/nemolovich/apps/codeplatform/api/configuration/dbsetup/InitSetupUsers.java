package fr.nemolovich.apps.codeplatform.api.configuration.dbsetup;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;

import fr.nemolovich.apps.codeplatform.api.model.Role;
import fr.nemolovich.apps.codeplatform.api.model.Team;
import fr.nemolovich.apps.codeplatform.api.model.User;
import fr.nemolovich.apps.codeplatform.api.security.Roles;

import java.time.Instant;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 *
 * @author bgohier
 */
@ChangeLog(order = "001")
//@Profile("dev")
public class InitSetupUsers {

    private static final Set<Role> ROLES = new HashSet<>();
    private static final Set<User> USERS = new HashSet<>();

    private static Role getRole(String name) {
        return ROLES.stream().filter(r -> r.getName().equalsIgnoreCase(name))
            .findFirst().get();
    }

    private static User getUser(String login) {
        return USERS.stream().filter(u -> u.getLogin().equalsIgnoreCase(login))
            .findFirst().get();
    }

    @ChangeSet(order = "01", author = "initiator", id = "01-dropDatabase",
        //        runAlways = true)
        runAlways = false)
    public void dropDatabase(MongoTemplate mongoTemplate) {
        mongoTemplate.getDb().dropDatabase();
    }

    @ChangeSet(order = "02", author = "initiator", id = "02-addRoles")
    public void addRoles(MongoTemplate mongoTemplate) {
        Role adminRole = new Role(Roles.ADMIN.toString());
        Role userRole = new Role(Roles.USER.toString());
        Role anonymousRole = new Role(Roles.ANONYMOUS.toString());
        Role recruiterRole = new Role(Roles.RECRUITER.toString());
        mongoTemplate.save(adminRole);
        mongoTemplate.save(userRole);
        mongoTemplate.save(anonymousRole);
        mongoTemplate.save(recruiterRole);
        ROLES.add(adminRole);
        ROLES.add(userRole);
        ROLES.add(anonymousRole);
        ROLES.add(recruiterRole);
    }

    @ChangeSet(order = "03", author = "initiator", id = "03-addUsers")
    public void addUsers(MongoTemplate mongoTemplate) {
        if (ROLES.isEmpty()) {
            ROLES.addAll(mongoTemplate.findAll(Role.class));
        }
        Role adminRole = getRole(Roles.ADMIN.toString());
        Role userRole = getRole(Roles.USER.toString());
        Role recuiterRole = getRole(Roles.RECRUITER.toString());

        Map<String, String> passwords = new HashMap<>();
        passwords.put("admin",
            "$2a$10$Y4ZczSUieceuKXV9uaUpIukNzt/YiULbwrndv5Iv97ynxD9eBN0lO");
        passwords.put("changeit",
            "$2a$10$xD6Dy0UTuKPfeQTrh9rVgeefjIzS05yisdS9Qy2RYgmt7Oy/i.LMG");
        passwords.put("password",
            "$2a$10$sSh2e9XNFObz4jDkLgNFR.keJasEzMRMyMcJXcKWRWpQ.Fk7JEta6");

        User adminUser = new User();
        adminUser.setId("admin");
        adminUser.setLogin("admin");
        adminUser.setPassword(passwords.get("admin"));
        adminUser.setFirstName("Admin");
        adminUser.setLastName("Administrator");
        adminUser.setEmail("admin@capgemini.com");
        adminUser.setImageUrl("https://cdn1.iconfinder.com/data/icons/ninja-things-1/1772/ninja-simple-512.png");
        adminUser.setActivated(true);
        adminUser.setCreatedBy(adminUser.getLogin());
        adminUser.setCreatedDate(Instant.now());
        adminUser.addRole(adminRole).addRole(userRole);
        mongoTemplate.save(adminUser);
        USERS.add(adminUser);

        User user = new User();
        user.setId("recruiter1");
        user.setLogin("recruiter1");
        user.setPassword(passwords.get("changeit"));
        user.setFirstName("Recruiter");
        user.setLastName("Capgemini");
        user.setEmail("recruiter1@capgemini.com");
        user.setActivated(true);
        user.setCreatedBy(adminUser.getLogin());
        user.setCreatedDate(Instant.now());
        user.addRole(recuiterRole);
        mongoTemplate.save(user);
        USERS.add(user);

        user = new User();
        user.setId("user");
        user.setLogin("user");
        user.setPassword(passwords.get("password"));
        user.setFirstName("User");
        user.setLastName("TEMP");
        user.setEmail("user.temp@capgemini.com");
        user.setActivated(true);
        user.setCreatedBy(adminUser.getLogin());
        user.setCreatedDate(Instant.now());
        user.addRole(userRole);
        mongoTemplate.save(user);
        USERS.add(user);

    }

    @ChangeSet(order = "04", author = "initiator", id = "04-addTeams")
    public void addTeams(MongoTemplate mongoTemplate) {
        if (USERS.isEmpty()) {
            USERS.addAll(mongoTemplate.findAll(User.class));
        }

        Team team = new Team();
        team.setId("users-team");
        team.setName("Users Team");
        team.setCreator(getUser("user"));
        team.addMemberItem(getUser("user"));
        mongoTemplate.save(team);

        team = new Team();
        team.setId("super-admins");
        team.setName("Super Admins");
        team.setCreator(getUser("admin"));
        team.addMemberItem(getUser("admin"));
        mongoTemplate.save(team);

        team = new Team();
        team.setId("recuiter-team");
        team.setName("Recuiter Team");
        team.setCreator(getUser("recruiter1"));
        team.addMemberItem(getUser("recruiter1"));
        mongoTemplate.save(team);

    }

}
