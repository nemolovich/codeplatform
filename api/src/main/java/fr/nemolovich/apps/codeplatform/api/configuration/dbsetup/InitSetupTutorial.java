package fr.nemolovich.apps.codeplatform.api.configuration.dbsetup;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;

import fr.nemolovich.apps.codeplatform.api.model.Challenge;
import fr.nemolovich.apps.codeplatform.api.model.CodeHelper;
import fr.nemolovich.apps.codeplatform.api.model.Exercice;
import fr.nemolovich.apps.codeplatform.api.model.Team;
import fr.nemolovich.apps.codeplatform.api.model.TestCase;
import fr.nemolovich.apps.codeplatform.api.model.enums.ChallengeScope;
import fr.nemolovich.apps.codeplatform.api.model.enums.ExerciceType;
import fr.nemolovich.apps.codeplatform.api.model.enums.Language;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 *
 * @author bgohier
 */
@ChangeLog(order = "003")
//@Profile("dev")
public class InitSetupTutorial {

    private static final Set<Team> TEAMS = new HashSet<>();
    private static final Set<Exercice> EXERCICES = new HashSet<>();

    private static Exercice getExercice(String id) {
        return EXERCICES.stream().filter(e -> e.getId().equals(id))
            .findFirst().orElse(null);
    }

    @ChangeSet(order = "01", author = "initiator", id = "01-addTutorialExercice")
    public void addExercices(MongoTemplate mongoTemplate) {

        if (TEAMS.isEmpty()) {
            TEAMS.addAll(mongoTemplate.findAll(Team.class));
        }
        if (EXERCICES.isEmpty()) {
            EXERCICES.addAll(mongoTemplate.findAll(Exercice.class));
        }
        
        tutorial:
        {
            Exercice tutorial = new Exercice();
            tutorial.setId("tutorial");
            tutorial.setSubtitle("Learn to use platform");
            tutorial.setText("# Tutorial\n"
                + "\n"
                + "## Learn to use platform\n"
                + "\n"
                + "In this tutorial you will submit some simple code to learn to use platform.\n"
                + "\n"
                + "First, you can find on you right the editor. You can select your favorite language.\n"
                + "\n"
                + "The goal is to read two integers and return the entire division of them. But warning to zero-division: return `ZERODIV`.\n"
                + "\n"
                + "Then, copy-paste the code below:\n"
                + "```{{CODE_HELPER_1}}\n"
                + "```\n"
                + "\n"
                + "This code read the both integers and store them in variables `a` and `b`.\n"
                + "\n"
                + "Then press **TEST CODE** button.\n"
                + "\n"
                + "Some tests cases don't succeed. Don't forget to use integer division (lower integer bound).\n"
                + "\n"
                + "Copy-paste the code below:\n"
                + "```{{CODE_HELPER_2}}\n"
                + "```\n"
                + "\n"
                + "Press **TEST CODE** button.\n"
                + "\n"
                + "**Almost!** Take care zero-division.\n"
                + "\n"
                + "Copy-paste the code below:\n"
                + "```{{CODE_HELPER_3}}\n"
                + "```\n"
                + "\n"
                + "Press **TEST CODE** button.\n"
                + "\n"
                + "**Congratulations!** You can now submit your code by pressing **SUBMIT** button.\n"
                + "\n"
                + "_INFO_: When you submit your code the test cases are not the same than editor test cases to avoid hardcoding ;)\n"
                + "\n");
            tutorial.setTitle("Tutorial");
            tutorial.setType(ExerciceType.FASTEST);

            tutorial.addTestCase(new TestCase().id("test-tutorial-1")
                .name("Test Case 1").hidden(false).input("6\n"
                + "3")
                .output("2\n").order(1));

            tutorial.addTestCase(new TestCase().id("test-tutorial-2")
                .name("Test Case 2").hidden(false).input("12\n"
                + "4")
                .output("3\n").order(2));

            tutorial.addTestCase(new TestCase().id("test-tutorial-3")
                .name("Test Case 3").hidden(false).input("16\n"
                + "5")
                .output("3\n").order(3));

            tutorial.addTestCase(new TestCase().id("test-tutorial-4")
                .name("Test Case 4").hidden(false).input("14\n"
                + "3")
                .output("4\n").order(4));

            tutorial.addTestCase(new TestCase().id("test-tutorial-5")
                .name("Test Case 5").hidden(false).input("5\n"
                + "0")
                .output("ZERODIV\n").order(5));

            tutorial.addTestCase(new TestCase().id("test-tutorial-6")
                .name("Test Case 6").hidden(true).input("10\n"
                + "5")
                .output("2\n").order(6));

            tutorial.addTestCase(new TestCase().id("test-tutorial-7")
                .name("Test Case 7").hidden(true).input("8\n"
                + "2")
                .output("4\n").order(7));

            tutorial.addTestCase(new TestCase().id("test-tutorial-8")
                .name("Test Case 8").hidden(true).input("10\n"
                + "3")
                .output("3\n").order(8));

            tutorial.addTestCase(new TestCase().id("test-tutorial-9")
                .name("Test Case 9").hidden(true).input("15\n"
                + "4")
                .output("3\n").order(9));

            tutorial.addTestCase(new TestCase().id("test-tutorial-10")
                .name("Test Case 10").hidden(true).input("7\n"
                + "0")
                .output("ZERODIV\n").order(10));

            CodeHelper bashHelper = new CodeHelper().id("help-tutorial-bash-1")
                .content("read a\n"
                    + "read b\n"
                    + "\n"
                    + "printf \"%.0f\" $(bc <<< \"scale=2;$a / $b\")\n"
                    + "\n"
                    + "\n")
                .language(Language.BASH)
                .order(1);
            tutorial.addCodeHelper(bashHelper);
            bashHelper = new CodeHelper().id("help-tutorial-bash-2")
                .content("read a\n"
                    + "read b\n"
                    + "\n"
                    + "printf \"%d\" $(bc <<< \"scale=2;$a / $b\")\n"
                    + "\n"
                    + "\n")
                .language(Language.BASH)
                .order(2);
            tutorial.addCodeHelper(bashHelper);
            bashHelper = new CodeHelper().id("help-tutorial-bash-3")
                .content("read a\n"
                    + "read b\n"
                    + "\n"
                    + "if [ $b -eq 0 ] ; then\n"
                    + "    echo ZERODIV\n"
                    + "else\n"
                    + "    printf \"%d\" $(bc <<< \"scale=2;$a / $b\")\n"
                    + "fi\n"
                    + "\n"
                    + "\n")
                .language(Language.BASH)
                .order(3);
            tutorial.addCodeHelper(bashHelper);

            CodeHelper cHelper = new CodeHelper().id("help-tutorial-c-1")
                .content("#include <stdlib.h>\n"
                    + "#include <stdio.h>\n"
                    + "#include <string.h>\n"
                    + "\n"
                    + "int main()\n"
                    + "{\n"
                    + "    \n"
                    + "    int a, b;\n"
                    + "    scanf(\"%d\", &a);\n"
                    + "    scanf(\"%d\", &b);\n"
                    + "    \n"
                    + "    printf(\"%0.f\\n\", a/(float)b);\n"
                    + "\n"
                    + "    return 0;\n"
                    + "}\n"
                    + "\n"
                    + "\n")
                .language(Language.C11)
                .order(1);
            tutorial.addCodeHelper(cHelper);
            cHelper = new CodeHelper().id("help-tutorial-c-2")
                .content("#include <stdlib.h>\n"
                    + "#include <stdio.h>\n"
                    + "#include <string.h>\n"
                    + "\n"
                    + "int main()\n"
                    + "{\n"
                    + "    \n"
                    + "    int a, b;\n"
                    + "    scanf(\"%d\", &a);\n"
                    + "    scanf(\"%d\", &b);\n"
                    + "    \n"
                    + "    printf(\"%d\\n\", a/b);\n"
                    + "\n"
                    + "    return 0;\n"
                    + "}\n"
                    + "\n"
                    + "\n")
                .language(Language.C11)
                .order(2);
            tutorial.addCodeHelper(cHelper);
            cHelper = new CodeHelper().id("help-tutorial-c-3")
                .content("#include <stdlib.h>\n"
                    + "#include <stdio.h>\n"
                    + "#include <string.h>\n"
                    + "\n"
                    + "int main()\n"
                    + "{\n"
                    + "    \n"
                    + "    int a, b;\n"
                    + "    scanf(\"%d\", &a);\n"
                    + "    scanf(\"%d\", &b);\n"
                    + "    \n"
                    + "    if (b==0)\n"
                    + "    {\n"
                    + "        printf(\"ZERODIV\\n\");\n"
                    + "    }\n"
                    + "    else\n"
                    + "    {\n"
                    + "        printf(\"%d\\n\", a/b);\n"
                    + "    }\n"
                    + "\n"
                    + "    return 0;\n"
                    + "}\n"
                    + "\n"
                    + "\n")
                .language(Language.C11)
                .order(3);
            tutorial.addCodeHelper(cHelper);

            CodeHelper cppHelper = new CodeHelper().id("help-tutorial-cpp-1")
                .content("#include <iostream>\n"
                    + "#include <string>\n"
                    + "#include <vector>\n"
                    + "#include <algorithm>\n"
                    + "\n"
                    + "using namespace std;\n"
                    + "\n"
                    + "int main()\n"
                    + "{\n"
                    + "    int a, b;\n"
                    + "    cin >> a; cin.ignore();\n"
                    + "    cin >> b; cin.ignore();\n"
                    + "    \n"
                    + "    printf(\"%.0f\\n\", (a/(float)b));\n"
                    + "    \n"
                    + "}\n"
                    + "\n"
                    + "\n")
                .language(Language.CPP)
                .order(1);
            tutorial.addCodeHelper(cppHelper);
            cppHelper = new CodeHelper().id("help-tutorial-cpp-2")
                .content("#include <iostream>\n"
                    + "#include <string>\n"
                    + "#include <vector>\n"
                    + "#include <algorithm>\n"
                    + "\n"
                    + "using namespace std;\n"
                    + "\n"
                    + "int main()\n"
                    + "{\n"
                    + "    int a, b;\n"
                    + "    cin >> a; cin.ignore();\n"
                    + "    cin >> b; cin.ignore();\n"
                    + "    \n"
                    + "    printf(\"%d\\n\", a/b);\n"
                    + "    \n"
                    + "}\n"
                    + "\n"
                    + "\n")
                .language(Language.CPP)
                .order(2);
            tutorial.addCodeHelper(cppHelper);
            cppHelper = new CodeHelper().id("help-tutorial-cpp-3")
                .content("#include <iostream>\n"
                    + "#include <string>\n"
                    + "#include <vector>\n"
                    + "#include <algorithm>\n"
                    + "\n"
                    + "using namespace std;\n"
                    + "\n"
                    + "int main()\n"
                    + "{\n"
                    + "    int a, b;\n"
                    + "    cin >> a; cin.ignore();\n"
                    + "    cin >> b; cin.ignore();\n"
                    + "    \n"
                    + "    if (b == 0)\n"
                    + "    {\n"
                    + "        cout << \"ZERODIV\" << endl;\n"
                    + "    }\n"
                    + "    else\n"
                    + "    {\n"
                    + "        printf(\"%d\\n\", a/b);\n"
                    + "    }\n"
                    + "    \n"
                    + "}\n"
                    + "\n"
                    + "\n")
                .language(Language.CPP)
                .order(3);
            tutorial.addCodeHelper(cppHelper);

            CodeHelper csHelper = new CodeHelper().id("help-tutorial-cs-1")
                .content("using System;\n"
                    + "\n"
                    + "class main\n"
                    + "{\n"
                    + "    static void Main(string[] args)\n"
                    + "    {\n"
                    + "        int a = int.Parse(Console.ReadLine());\n"
                    + "        int b = int.Parse(Console.ReadLine());\n"
                    + "        \n"
                    + "        Console.WriteLine(a/(float)b);\n"
                    + "    }\n"
                    + "}\n"
                    + "\n"
                    + "\n")
                .language(Language.CS)
                .order(1);
            tutorial.addCodeHelper(csHelper);
            csHelper = new CodeHelper().id("help-tutorial-cs-2")
                .content("using System;\n"
                    + "\n"
                    + "class main\n"
                    + "{\n"
                    + "    static void Main(string[] args)\n"
                    + "    {\n"
                    + "        int a = int.Parse(Console.ReadLine());\n"
                    + "        int b = int.Parse(Console.ReadLine());\n"
                    + "        \n"
                    + "        Console.WriteLine(a/b);\n"
                    + "    }\n"
                    + "}\n"
                    + "\n"
                    + "\n")
                .language(Language.CS)
                .order(2);
            tutorial.addCodeHelper(csHelper);
            csHelper = new CodeHelper().id("help-tutorial-cs-3")
                .content("using System;\n"
                    + "\n"
                    + "class main\n"
                    + "{\n"
                    + "    static void Main(string[] args)\n"
                    + "    {\n"
                    + "        int a = int.Parse(Console.ReadLine());\n"
                    + "        int b = int.Parse(Console.ReadLine());\n"
                    + "        \n"
                    + "        if (b == 0) {\n"
                    + "            Console.WriteLine(\"ZERODIV\");\n"
                    + "        } else {\n"
                    + "            Console.WriteLine(a/b);\n"
                    + "        }\n"
                    + "    }\n"
                    + "}\n"
                    + "\n"
                    + "\n")
                .language(Language.CS)
                .order(3);
            tutorial.addCodeHelper(csHelper);

            CodeHelper javaHelper = new CodeHelper().id("help-tutorial-java-1")
                .content("import java.util.*;\n"
                    + "import java.io.*;\n"
                    + "import java.math.*;\n"
                    + "\n"
                    + "class Main {\n"
                    + "\n"
                    + "    public static void main(String args[]) {\n"
                    + "        Scanner in = new Scanner(System.in);\n"
                    + "        int a = in.nextInt();\n"
                    + "        int b = in.nextInt();\n"
                    + "\n"
                    + "        System.out.printf(\"%.0f\\n\", a/(float)b);\n"
                    + "    }\n"
                    + "}\n"
                    + "\n"
                    + "\n")
                .language(Language.JAVA)
                .order(1);
            tutorial.addCodeHelper(javaHelper);
            javaHelper = new CodeHelper().id("help-tutorial-java-2")
                .content("import java.util.*;\n"
                    + "import java.io.*;\n"
                    + "import java.math.*;\n"
                    + "\n"
                    + "class Main {\n"
                    + "\n"
                    + "    public static void main(String args[]) {\n"
                    + "        Scanner in = new Scanner(System.in);\n"
                    + "        int a = in.nextInt();\n"
                    + "        int b = in.nextInt();\n"
                    + "\n"
                    + "        System.out.printf(\"%d\\n\", a/b);\n"
                    + "    }\n"
                    + "}\n"
                    + "\n"
                    + "\n")
                .language(Language.JAVA)
                .order(2);
            tutorial.addCodeHelper(javaHelper);
            javaHelper = new CodeHelper().id("help-tutorial-java-3")
                .content("import java.util.*;\n"
                    + "import java.io.*;\n"
                    + "import java.math.*;\n"
                    + "\n"
                    + "class Main {\n"
                    + "\n"
                    + "    public static void main(String args[]) {\n"
                    + "        Scanner in = new Scanner(System.in);\n"
                    + "        int a = in.nextInt();\n"
                    + "        int b = in.nextInt();\n"
                    + "        \n"
                    + "        if (b == 0) {\n"
                    + "            System.out.println(\"ZERODIV\");\n"
                    + "        } else {\n"
                    + "            System.out.printf(\"%d\\n\", a/b);\n"
                    + "        }\n"
                    + "    }\n"
                    + "}\n"
                    + "\n"
                    + "\n")
                .language(Language.JAVA)
                .order(3);
            tutorial.addCodeHelper(javaHelper);

            CodeHelper javascriptHelper = new CodeHelper()
                .id("help-tutorial-javascript-1")
                .content("var a = parseInt(readline());\n"
                    + "var b = parseInt(readline());\n"
                    + "\n"
                    + "print(a/b);\n"
                    + "\n"
                    + "\n")
                .language(Language.JAVASCRIPT)
                .order(1);
            tutorial.addCodeHelper(javascriptHelper);
            javascriptHelper = new CodeHelper().id("help-tutorial-javascript-2")
                .content("var a = parseInt(readline());\n"
                    + "var b = parseInt(readline());\n"
                    + "\n"
                    + "print(parseInt(a/b));\n"
                    + "\n"
                    + "\n")
                .language(Language.JAVASCRIPT)
                .order(2);
            tutorial.addCodeHelper(javascriptHelper);
            javascriptHelper = new CodeHelper().id("help-tutorial-javascript-3")
                .content("var a = parseInt(readline());\n"
                    + "var b = parseInt(readline());\n"
                    + "\n"
                    + "\n"
                    + "if (b === 0) {\n"
                    + "    print('ZERODIV');\n"
                    + "} else {\n"
                    + "    print(parseInt(a/b));\n"
                    + "}\n"
                    + "\n"
                    + "\n")
                .language(Language.JAVASCRIPT)
                .order(3);
            tutorial.addCodeHelper(javascriptHelper);

            CodeHelper perlHelper = new CodeHelper().id("help-tutorial-perl-1")
                .content("chomp(my $a = <STDIN>);\n"
                    + "chomp(my $b = <STDIN>);\n"
                    + "\n"
                    + "print $a/$b;\n"
                    + "\n"
                    + "\n")
                .language(Language.PERL)
                .order(1);
            tutorial.addCodeHelper(perlHelper);
            perlHelper = new CodeHelper().id("help-tutorial-perl-2")
                .content("chomp(my $a = <STDIN>);\n"
                    + "chomp(my $b = <STDIN>);\n"
                    + "\n"
                    + "print int($a/$b);\n"
                    + "\n"
                    + "\n")
                .language(Language.PERL)
                .order(2);
            tutorial.addCodeHelper(perlHelper);
            perlHelper = new CodeHelper().id("help-tutorial-perl-3")
                .content("chomp(my $a = <STDIN>);\n"
                    + "chomp(my $b = <STDIN>);\n"
                    + "\n"
                    + "if ($b == 0) {\n"
                    + "    print 'ZERODIV';\n"
                    + "} else {\n"
                    + "    print int($a/$b);\n"
                    + "}\n"
                    + "\n"
                    + "\n")
                .language(Language.PERL)
                .order(3);
            tutorial.addCodeHelper(perlHelper);

            CodeHelper phpHelper = new CodeHelper().id("help-tutorial-php-1")
                .content("<?php\n"
                    + "\n"
                    + "fscanf(STDIN, \"%d\", $a);\n"
                    + "fscanf(STDIN, \"%d\", $b);\n"
                    + "\n"
                    + "echo($a/$b);\n"
                    + "\n"
                    + "?>\n"
                    + "\n"
                    + "\n")
                .language(Language.PHP)
                .order(1);
            tutorial.addCodeHelper(phpHelper);
            phpHelper = new CodeHelper().id("help-tutorial-php-2")
                .content("<?php\n"
                    + "\n"
                    + "fscanf(STDIN, \"%d\", $a);\n"
                    + "fscanf(STDIN, \"%d\", $b);\n"
                    + "\n"
                    + "echo(floor($a/$b));\n"
                    + "\n"
                    + "?>\n"
                    + "\n"
                    + "\n")
                .language(Language.PHP)
                .order(2);
            tutorial.addCodeHelper(phpHelper);
            phpHelper = new CodeHelper().id("help-tutorial-php-3")
                .content("<?php\n"
                    + "\n"
                    + "fscanf(STDIN, \"%d\", $a);\n"
                    + "fscanf(STDIN, \"%d\", $b);\n"
                    + "\n"
                    + "\n"
                    + "if ($b == 0) {\n"
                    + "    echo('ZERODIV');\n"
                    + "} else {\n"
                    + "    echo((floor($a/$b)));\n"
                    + "}\n"
                    + "\n"
                    + "?>\n"
                    + "\n"
                    + "\n")
                .language(Language.PHP)
                .order(3);
            tutorial.addCodeHelper(phpHelper);

            CodeHelper pythonHelper = new CodeHelper()
                .id("help-tutorial-python-1")
                .content("import sys\n"
                    + "import math\n"
                    + "\n"
                    + "a = input()\n"
                    + "b = input()\n"
                    + "\n"
                    + "print int(round(a/float(b)))\n"
                    + "\n"
                    + "\n")
                .language(Language.PYTHON)
                .order(1);
            tutorial.addCodeHelper(pythonHelper);
            pythonHelper = new CodeHelper().id("help-tutorial-python-2")
                .content("import sys\n"
                    + "import math\n"
                    + "\n"
                    + "a = input()\n"
                    + "b = input()\n"
                    + "\n"
                    + "print int(math.floor(a/float(b)))\n"
                    + "\n"
                    + "\n")
                .language(Language.PYTHON)
                .order(2);
            tutorial.addCodeHelper(pythonHelper);
            pythonHelper = new CodeHelper().id("help-tutorial-python-3")
                .content("import sys\n"
                    + "import math\n"
                    + "\n"
                    + "a = input()\n"
                    + "b = input()\n"
                    + "\n"
                    + "if b == 0:\n"
                    + "    print 'ZERODIV'\n"
                    + "else:\n"
                    + "    print int(math.floor(a/float(b)))\n"
                    + "\n"
                    + "\n")
                .language(Language.PYTHON)
                .order(3);
            tutorial.addCodeHelper(pythonHelper);

            CodeHelper python3Helper = new CodeHelper()
                .id("help-tutorial-python3-1")
                .content("import sys\n"
                    + "import math\n"
                    + "\n"
                    + "a = int(input())\n"
                    + "b = int(input())\n"
                    + "\n"
                    + "print(round(a/b))\n"
                    + "\n"
                    + "\n")
                .language(Language.PYTHON3)
                .order(1);
            tutorial.addCodeHelper(python3Helper);
            python3Helper = new CodeHelper().id("help-tutorial-python3-2")
                .content("import sys\n"
                    + "import math\n"
                    + "\n"
                    + "a = int(input())\n"
                    + "b = int(input())\n"
                    + "\n"
                    + "print(math.floor(a/b))\n"
                    + "\n"
                    + "\n")
                .language(Language.PYTHON3)
                .order(2);
            tutorial.addCodeHelper(python3Helper);
            python3Helper = new CodeHelper().id("help-tutorial-python3-3")
                .content("import sys\n"
                    + "import math\n"
                    + "\n"
                    + "a = int(input())\n"
                    + "b = int(input())\n"
                    + "\n"
                    + "if b == 0:\n"
                    + "    print('ZERODIV')\n"
                    + "else:\n"
                    + "    print(math.floor(a/b))\n"
                    + "\n"
                    + "\n")
                .language(Language.PYTHON3)
                .order(3);
            tutorial.addCodeHelper(python3Helper);

            CodeHelper rubyHelper = new CodeHelper().id("help-tutorial-ruby-1")
                .content("a = gets.to_i\n"
                    + "b = gets.to_i\n"
                    + "\n"
                    + "puts '%.0f' % [a/b.to_f]\n"
                    + "\n"
                    + "\n")
                .language(Language.RUBY)
                .order(1);
            tutorial.addCodeHelper(rubyHelper);
            rubyHelper = new CodeHelper().id("help-tutorial-ruby-2")
                .content("a = gets.to_i\n"
                    + "b = gets.to_i\n"
                    + "\n"
                    + "puts '%d' % [a/b.to_f]\n"
                    + "\n"
                    + "\n")
                .language(Language.RUBY)
                .order(2);
            tutorial.addCodeHelper(rubyHelper);
            rubyHelper = new CodeHelper().id("help-tutorial-ruby-3")
                .content("a = gets.to_i\n"
                    + "b = gets.to_i\n"
                    + "\n"
                    + "if b == 0\n"
                    + "    puts 'ZERODIV'\n"
                    + "else\n"
                    + "    puts '%d' % [a/b.to_f]\n"
                    + "end\n"
                    + "\n"
                    + "\n")
                .language(Language.RUBY)
                .order(3);
            tutorial.addCodeHelper(rubyHelper);

            tutorial.getCodeHelpers().stream().forEach((codeHelper)
                -> mongoTemplate.save(codeHelper));
            tutorial.getTestCases().stream().forEach((testCase)
                -> mongoTemplate.save(testCase));
            tutorial.setCoeff(BigDecimal.valueOf(0.01));

            mongoTemplate.save(tutorial);

            EXERCICES.add(tutorial);
        }
    }

    @ChangeSet(order = "02", author = "initiator", id = "02-addChallengeExercice")
    public void addChallenges(MongoTemplate mongoTemplate) {

        Challenge tutorial = new Challenge();
        tutorial.setId("tutorial");
        tutorial.setDescription("Simple test cases");
        tutorial.setScope(ChallengeScope.CHALLENGE);
        tutorial.setName("Tutorial");
        tutorial.setStartDate(Instant.now().minus(1, ChronoUnit.DAYS));
        tutorial.setEndDate(Instant.now().plus(3650, ChronoUnit.DAYS));
        tutorial.addExerciceItem(getExercice("tutorial"));
        if (getExercice("simple-test") != null) {
            tutorial.addExerciceItem(getExercice("simple-test"));
        }
        tutorial.setTeams(TEAMS.stream().collect(Collectors.toSet()));
        mongoTemplate.save(tutorial);
    }
}
