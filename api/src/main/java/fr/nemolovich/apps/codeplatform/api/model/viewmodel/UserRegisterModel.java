package fr.nemolovich.apps.codeplatform.api.model.viewmodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.validation.annotation.Validated;

/**
 * UserRegisterModel
 */
@Validated
public class UserRegisterModel extends UserViewModel {

    @JsonProperty("password")
    private String password = null;


    public UserRegisterModel password(String password) {
        this.password = password;
        return this;
    }

    /**
     * Get password
     *
     * @return password
     *
     */
    @ApiModelProperty(required = true, value = "")
    @NotNull
    @Size(min = 4, max = 100)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserRegisterModel userRegisterModel = (UserRegisterModel) o;
        return super.equals(o)
            && Objects.equals(this.password, userRegisterModel.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), password);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class UserRegisterModel {\n");
        sb.append("    password: ").append(toIndentedString(password))
                .append("\n");
        sb.append(super.toString()).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     * 
     * @param o {@link Object}: Object to format.
     * @return {@link String}: String representation of object.
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
