package fr.nemolovich.apps.codeplatform.pdf.model;

import java.util.List;

/**
 *
 * @author bgohier
 */
public class CodeFileData {

    private String name;
    private List<CodeLineData> lines;

    public CodeFileData() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CodeFileData name(String name) {
        this.name = name;
        return this;
    }

    public List<CodeLineData> getLines() {
        return lines;
    }

    public void setLines(List<CodeLineData> lines) {
        this.lines = lines;
    }

    public CodeFileData lines(List<CodeLineData> lines) {
        this.lines = lines;
        return this;
    }

    public CodeFileData addLine(CodeLineData line) {
        if (!this.lines.contains(line)) {
            this.lines.add(line);
        }
        return this;
    }

}
