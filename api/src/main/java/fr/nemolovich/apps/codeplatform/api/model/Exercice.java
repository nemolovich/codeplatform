package fr.nemolovich.apps.codeplatform.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.nemolovich.apps.codeplatform.api.model.enums.ExerciceType;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.validation.annotation.Validated;

/**
 * Exercice
 */
@Validated
@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2017-10-07T19:30:45.309+02:00")
@Document(collection = "codeplatform_exercice")
public class Exercice extends AbstractEntity {

    @JsonProperty("id")
    @Id
    private String id = null;

    @JsonProperty("title")
    @Size(min = 5, max = 50)
    @NotNull
    private String title = null;

    @JsonProperty("subtitle")
    @Size(min = 0, max = 100)
    private String subtitle = null;

    @JsonProperty("text")
    private String text = null;

    @JsonProperty("type")
    private ExerciceType type = null;

    @JsonProperty("maxTime")
    private Integer maxTime = null;

    @JsonProperty("testCases")
    @Valid
    @DBRef
    private Set<TestCase> testCases = new HashSet<>();

    @JsonProperty("codeStarters")
    @Valid
    @DBRef
    private Set<CodeStarter> codeStarters = new HashSet<>();

    @JsonProperty("codeHelpers")
    @Valid
    @DBRef
    private Set<CodeHelper> codeHelpers = new HashSet<>();

    @JsonProperty("coeff")
    private BigDecimal coeff = null;

    public Exercice id(String id) {
        this.id = id;
        return this;
    }

    public static Exercice publicExercice(Exercice exercice) {
        Exercice result = new Exercice();
        result.id = exercice.getId();
        result.title = exercice.getTitle();
        result.subtitle = exercice.getSubtitle();
        result.text = exercice.getText();
        result.type = exercice.getType();
        result.maxTime = exercice.getMaxTime();
        result.coeff = exercice.getCoeff();
        result.testCases = exercice.getTestCases().stream()
        .filter(tc -> !tc.getHidden())
        .collect(Collectors.toSet());
        result.codeStarters = exercice.getCodeStarters();
        result.codeHelpers = exercice.getCodeHelpers();
        return result;
    }

    /**
     * Get id
     *
     * @return id
     *
     */
    @ApiModelProperty(value = "")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Exercice title(String title) {
        this.title = title;
        return this;
    }

    /**
     * Get title
     *
     * @return title
     *
     */
    @ApiModelProperty(value = "")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Exercice type(ExerciceType type) {
        this.type = type;
        return this;
    }

    /**
     *
     * Get type
     *
     * @return type
     */
    @ApiModelProperty(value = "")
    public ExerciceType getType() {
        return type;
    }

    public void setType(ExerciceType type) {
        this.type = type;
    }

    public Exercice maxTime(Integer maxTime) {
        this.maxTime = maxTime;
        return this;
    }

    /**
     *
     * Get maxTime
     *
     * @return maxTime
     */
    @ApiModelProperty(value = "")
    public Integer getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(Integer maxTime) {
        this.maxTime = maxTime;
    }

    public Exercice subtitle(String subtitle) {
        this.subtitle = subtitle;
        return this;
    }

    /**
     * Get subtitle
     *
     * @return subtitle
     *
     */
    @ApiModelProperty(value = "")
    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public Exercice text(String text) {
        this.text = text;
        return this;
    }

    /**
     * Get text
     *
     * @return text
     *
     */
    @ApiModelProperty(value = "")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Exercice testCases(Set<TestCase> testcases) {
        this.testCases = testcases;
        return this;
    }

    public Exercice addTestCase(TestCase testcase) {
        this.testCases.add(testcase);
        return this;
    }

    /**
     * Get the testCases
     *
     * @return testCases
     */
    @ApiModelProperty(value = "")
    public Set<TestCase> getTestCases() {
        return this.testCases;
    }

    public void setTestCases(Set<TestCase> testCases) {
        this.testCases = testCases;
    }

    public Exercice addCodeStarter(CodeStarter codeStarter) {
        this.codeStarters.add(codeStarter);
        return this;
    }

    /**
     * Get the codeStarters
     *
     * @return codeStarters
     */
    @ApiModelProperty(value = "")
    public Set<CodeStarter> getCodeStarters() {
        return this.codeStarters;
    }

    public void setCodeStarters(Set<CodeStarter> codeStarters) {
        this.codeStarters = codeStarters;
    }

    public Exercice addCodeHelper(CodeHelper codeHelper) {
        this.codeHelpers.add(codeHelper);
        return this;
    }

    /**
     * Get the codeHelpers
     *
     * @return codeHelpers
     */
    @ApiModelProperty(value = "")
    public Set<CodeHelper> getCodeHelpers() {
        return this.codeHelpers;
    }

    public void setCodeHelpers(Set<CodeHelper> codeHelpers) {
        this.codeHelpers = codeHelpers;
    }

    public Exercice coeff(BigDecimal coeff) {
        this.coeff = coeff;
        return this;
    }

    /**
     * Get coeff
     *
     * @return coeff
     *
     */
    @ApiModelProperty(value = "")
    public BigDecimal getCoeff() {
        return coeff;
    }

    public void setCoeff(BigDecimal coeff) {
        this.coeff = coeff;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Exercice exercice = (Exercice) o;
        return Objects.equals(this.title, exercice.title) &&
             Objects.equals(this.type, exercice.type) &&
             Objects.equals(this.subtitle, exercice.subtitle) &&
             Objects.equals(this.text, exercice.text) &&
             Objects.equals(this.coeff, exercice.coeff);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, type, subtitle, text, coeff);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Exercice {\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    title: ").append(toIndentedString(title)).append("\n");
        sb.append("    type: ").append(toIndentedString(type)).append("\n");
        sb.append("    subtitle: ").append(toIndentedString(subtitle))
            .append("\n");
        sb.append("    text: ").append(toIndentedString(text)).append("\n");
        sb.append("    testCases: ").append(toIndentedString(testCases))
            .append("\n");
        sb.append("    codeStarters: ").append(toIndentedString(codeStarters))
            .append("\n");
        sb.append("    codeHelpers: ").append(toIndentedString(codeHelpers))
            .append("\n");
        sb.append("    coeff: ").append(toIndentedString(coeff)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     *
     * @param o {@link Object}: Object to format.
     * @return {@link String}: String representation of object.
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

}
