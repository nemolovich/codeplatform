package fr.nemolovich.apps.codeplatform.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import fr.nemolovich.apps.codeplatform.api.model.enums.ChallengeScope;
import io.swagger.annotations.ApiModelProperty;

import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.validation.annotation.Validated;

/**
 * Challenge
 */
@Validated
@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2017-10-07T19:30:45.309+02:00")
@Document(collection = "codeplatform_challenge")
public class Challenge extends AbstractEntity {

    @JsonProperty("id")
    @Id
    private String id = null;

    @JsonProperty("name")
    @Size(min = 5, max = 50)
    @NotNull
    @Indexed(unique = true)
    private String name = null;

    @JsonProperty("description")
    private String description = null;

    @JsonProperty("scope")
    @NotNull
    private ChallengeScope scope = null;

    @JsonProperty("teams")
    @Valid
    @DBRef
    private Set<Team> teams = new HashSet<>();

    @JsonProperty("exercices")
    @Valid
    @DBRef
    private Set<Exercice> exercices = new HashSet<>();

    @JsonProperty("results")
    @Valid
    @DBRef
    private Set<Result> results = new HashSet<>();

    @JsonProperty("startDate")
    private Instant startDate = null;

    @JsonProperty("endDate")
    private Instant endDate = null;

    public Challenge id(String id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     *
     */
    @ApiModelProperty(value = "")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Challenge name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     *
     */
    @ApiModelProperty(value = "")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Challenge description(String description) {
        this.description = description;
        return this;
    }

    /**
     * Get description
     *
     * @return description
     *
     */
    @ApiModelProperty(value = "")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Challenge scope(ChallengeScope scope) {
        this.scope = scope;
        return this;
    }

    /**
     * Get scope
     *
     * @return scope
     *
     */
    @ApiModelProperty(value = "")
    public ChallengeScope getScope() {
        return scope;
    }

    public void setScope(ChallengeScope scope) {
        this.scope = scope;
    }

    public Challenge teams(Set<Team> teams) {
        this.teams = teams;
        return this;
    }

    public Challenge addTeamItem(Team team) {
        if (this.teams == null) {
            this.teams = new HashSet<>();
        }
        this.teams.add(team);
        return this;
    }

    /**
     * Get teams
     *
     * @return teams
     *
     */
    @ApiModelProperty(value = "")
    @Valid
    public Set<Team> getTeams() {
        return teams;
    }

    public void setTeams(Set<Team> teams) {
        this.teams = teams;
    }

    public Challenge exercices(Set<Exercice> exercices) {
        this.exercices = exercices;
        return this;
    }

    public Challenge addExerciceItem(Exercice exercice) {
        if (this.exercices == null) {
            this.exercices = new HashSet<>();
        }
        this.exercices.add(exercice);
        return this;
    }

    /**
     * Get exercices
     *
     * @return exercices
     *
     */
    @ApiModelProperty(value = "")
    @Valid
    public Set<Exercice> getExercices() {
        return exercices;
    }

    public void setExercices(Set<Exercice> exercices) {
        this.exercices = exercices;
    }

    public Challenge results(Set<Result> results) {
        this.results = results;
        return this;
    }

    public Challenge addResultItem(Result result) {
        if (this.results == null) {
            this.results = new HashSet<>();
        }
        if (!this.results.contains(result)) {
            this.results.add(result);
        }
        return this;
    }

    /**
     * Get results
     *
     * @return results
     *
     */
    @ApiModelProperty(value = "")
    @Valid
    public Set<Result> getResults() {
        return results;
    }

    public void setResults(Set<Result> results) {
        this.results = results;
    }

    public Challenge startDate(Instant startDate) {
        this.startDate = startDate;
        return this;
    }

    /**
     * Get startDate
     *
     * @return startDate
     *
     */
    @ApiModelProperty(value = "")
    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Challenge endDate(Instant endDate) {
        this.endDate = endDate;
        return this;
    }

    /**
     * Get endDate
     *
     * @return endDate
     *
     */
    @ApiModelProperty(value = "")
    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Challenge challenge = (Challenge) o;
        return Objects.equals(this.name, challenge.name)
            && Objects.equals(this.description, challenge.description)
            && Objects.equals(this.scope, challenge.scope)
            && Objects.equals(this.startDate, challenge.startDate)
            && Objects.equals(this.endDate, challenge.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, scope, startDate, endDate);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Challenge {\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    description: ").append(toIndentedString(description))
            .append("\n");
        sb.append("    scope: ").append(toIndentedString(scope)).append("\n");
        sb.append("    teams: ").append(toIndentedString(teams)).append("\n");
        sb.append("    exercices: ").append(toIndentedString(exercices))
            .append("\n");
        sb.append("    results: ").append(toIndentedString(results))
            .append("\n");
        sb.append("    startDate: ").append(toIndentedString(startDate))
            .append("\n");
        sb.append("    endDate: ").append(toIndentedString(endDate))
            .append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     * 
     * @param o {@link Object}: Object to format.
     * @return {@link String}: String representation of object.
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
