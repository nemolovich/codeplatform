package fr.nemolovich.apps.codeplatform.api.webapi.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.nemolovich.apps.codeplatform.api.model.Challenge;
import fr.nemolovich.apps.codeplatform.api.model.CodeFile;
import fr.nemolovich.apps.codeplatform.api.model.Exercice;
import fr.nemolovich.apps.codeplatform.api.model.Result;
import fr.nemolovich.apps.codeplatform.api.model.Submission;
import fr.nemolovich.apps.codeplatform.api.model.SubmissionRequest;
import fr.nemolovich.apps.codeplatform.api.model.SubmissionResponse;
import fr.nemolovich.apps.codeplatform.api.model.Team;
import fr.nemolovich.apps.codeplatform.api.model.User;
import fr.nemolovich.apps.codeplatform.api.model.enums.Language;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.ScoreViewModel;
import fr.nemolovich.apps.codeplatform.api.service.ChallengeService;
import fr.nemolovich.apps.codeplatform.api.service.ExerciceService;
import fr.nemolovich.apps.codeplatform.api.service.ResultService;
import fr.nemolovich.apps.codeplatform.api.service.SubmissionService;
import fr.nemolovich.apps.codeplatform.api.service.TeamService;
import fr.nemolovich.apps.codeplatform.api.service.UserService;
import fr.nemolovich.apps.codeplatform.api.webapi.SubmissionApi;
import fr.nemolovich.apps.codeplatform.manager.docker.Manager;
import io.swagger.annotations.ApiParam;
import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author bgohier
 */
@Controller
public class SubmissionApiController implements SubmissionApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(
                                SubmissionApiController.class);

    public static final String TMP_CODE_PATH =
                               String.format("%s/codeplatform", System.
                                             getProperty("java.io.tmpdir"));

    private final ObjectMapper objectMapper;
    private final ExerciceService exerciceService;
    private final ChallengeService challengeService;
    private final ResultService resultService;
    private final UserService userService;
    private final TeamService teamService;
    private final SubmissionService submissionService;

    public SubmissionApiController(ObjectMapper objectMapper,
                                   ExerciceService exerciceService,
                                   ChallengeService challengeService,
                                   ResultService resultService,
                                   UserService userService,
                                   TeamService teamService,
                                   SubmissionService submissionService) {
        this.objectMapper = objectMapper;
        this.exerciceService = exerciceService;
        this.challengeService = challengeService;
        this.resultService = resultService;
        this.userService = userService;
        this.teamService = teamService;
        this.submissionService = submissionService;
    }

    @Override
    public ResponseEntity<Object> updateTestTimeAsync(
        @ApiParam(value = "ID of the result", required = true)
        @PathVariable("resultId") String resultId,
        @PathVariable("language") String language,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        if (accept != null && accept.contains("application/json")) {
            User user = this.userService.getCurrentUser().orElse(null);
            Result result = resultService.getResultById(resultId);
            Instant now = Instant.now();
            long diff = 0;
            if (result.getReopenDate() != null) {
                diff = Duration.between(result.getStartDate(),
                                        result.getEndDate()).toMillis() +
                       Duration.between(result.getReopenDate(), now).toMillis();
                result.setReopenDate(null);
            } else {
                diff = Duration.between(result.getStartDate(), now).toMillis();
            }
            result.setEndDate(result.getStartDate().plusMillis(diff));

            Language lang = Language.fromValue(language);
            Submission submission = submissionService
                       .getSubmissionByResultForUser(result, lang, user);
            result.addSubmissionItem(submission);
            resultService.updateResult(result);
            return ResponseEntity.ok(null);
        }
        return new ResponseEntity<>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @Override
    public ResponseEntity<Object> submitCode(
        @ApiParam(value = "Code submission to evaluate", required = true)
        @RequestParam(value = "submission",
                      required = true) String submission,
        @ApiParam(value = "The files to upload", required = true)
        @Valid
        @RequestParam("files") MultipartFile[] files,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {

        if (accept != null && accept.contains("application/json")) {
            SubmissionRequest request = objectMapper.readValue(submission,
                                                               SubmissionRequest.class
                          );

            if (request == null || files.length < 1) {
                return new ResponseEntity<>(Arrays.asList(
                    new SubmissionResponse().score(BigDecimal.ZERO)
                    .resultName("error").returnMessage(
                    "Please provide 'files' and 'submission' in form")),
                                            HttpStatus.NO_CONTENT);
            }

            Optional<User> user = this.userService.getCurrentUser();
            Challenge challenge = this.challengeService.getChallengeById(
                      request.getChallenge());
            Team team = this.teamService.getTeamById(request.getTeam());
            Exercice exercice = this.exerciceService.getExerciceById(
                     request.getExercice());

            if (!user.isPresent() || challenge == null || team == null ||
                exercice == null) {
                return new ResponseEntity<>(Arrays.asList(
                    new SubmissionResponse().score(BigDecimal.ZERO)
                    .resultName("error").returnMessage(
                    "Wrong request information")), HttpStatus.NO_CONTENT);
            }

            Result result = this.resultService.getResultForTeamInChallenge(
                   request.getChallenge(), request.getExercice(),
                   request.getTeam());

            Instant now = Instant.now();

            Submission submissionObject =
                       this.submissionService.getSubmissionByResultForUser(
                           result, request.getLanguage(), user.get());
            Set<CodeFile> codeFiles = new HashSet<>();
            File folder = new File(
                 String.format("%s/%s", TMP_CODE_PATH, Manager.contructName(
                               request.getTeam(), request.getExercice())));
            String fileName;
            File targetFile;
            for (MultipartFile file : files) {
                fileName = file.getOriginalFilename().replaceAll("\\?", "/");
                targetFile = new File(folder, fileName);
                if (!targetFile.getParentFile().exists()) {
                    Files.createDirectories(targetFile.getParentFile()
                        .toPath());
                }
                if (targetFile.createNewFile()) {
//                file.transferTo(targetFile);
                    Files.copy(file.getInputStream(), targetFile.toPath(),
                               StandardCopyOption.REPLACE_EXISTING);
                    codeFiles.add(new CodeFile().name(fileName)
                        .content(file.getBytes()));
                } else {
                    LOGGER.error("Cannot create temporary file '{}'",
                                 targetFile.getAbsolutePath());
                }
            }

            final List<SubmissionResponse> responses = new ArrayList<>();
            Manager.process(request.getTeam(), request.getExercice(),
                            request.getLanguage().toString(), folder.
                            getAbsolutePath(), (results) -> {
                            results.stream().filter(rr -> rr != null).
                                forEach(
                                    (rr) -> {
                                    SubmissionResponse response =
                                                       new SubmissionResponse();
                                    response.setResultName(rr.
                                        getResultName());
                                    response.setReturnMessage(rr.
                                        getLogResult());
                                    response.setScore(BigDecimal.valueOf(rr.
                                        getResult()));
                                    response.setStdout(rr.getStdout());
                                    response.setStderr(rr.getStderr());
                                    responses.add(response);
                                });
                        });

            List<SubmissionResponse> hiddenResponses =
                                     responses.stream().filter(response ->
                                         exercice.getTestCases().stream()
                                         .filter(tc -> tc.getHidden())
                                         .map(tc -> tc.getName())
                                         .collect(Collectors.toSet())
                                         .contains(response.getResultName())).
                                     collect(Collectors.toList());

            BigDecimal score = BigDecimal.valueOf(hiddenResponses
                       .stream().mapToDouble(s -> s.getScore().
                           doubleValue()).
                       sum() / (hiddenResponses.size() > 0 ?
                                hiddenResponses.size() : 1));
            if (score.compareTo(submissionObject.getScore()) >= 0) {
                submissionObject.setScore(score);
                submissionObject.setDate(now);
                long diff;
                if (result.getReopenDate() != null) {
                    diff = Duration.between(result.getStartDate(),
                                            result.getEndDate()).toMillis() +
                           Duration.between(result.getReopenDate(), now)
                           .toMillis();
                } else {
                    diff = Duration.between(result.getStartDate(), now)
                    .toMillis();
                }
                submissionObject.setTime(diff);

                if (!submissionObject.getFiles().isEmpty()) {
                    for (CodeFile cf : submissionObject.getFiles()) {
                        this.submissionService.removeCodeFile(
                            submissionObject,
                            cf);
                    }
                }
                submissionObject.setFiles(new HashSet<>());
                for (CodeFile cf : codeFiles) {
                    submissionObject = this.submissionService.addCodeFile(
                    submissionObject, cf);
                }
            }
            submissionObject = this.submissionService.saveSubmission(
            submissionObject);
            if (result.getReopenDate() != null) {
                long diff = Duration.between(result.getStartDate(),
                                             result.getEndDate()).toMillis() +
                            Duration.between(result.getReopenDate(), now)
                            .toMillis();
                result.setEndDate(result.getStartDate().plusMillis(diff));
            } else {
                result.setEndDate(now);
            }
            result.setReopenDate(now);
            result = this.resultService.addSubmission(result,
                                                      submissionObject);
            challenge = challenge.addResultItem(result);
            this.challengeService.updateChallenge(challenge);
            boolean removed = Files.walk(folder.toPath())
                    .sorted((p1, p2) -> -p1.compareTo(p2))
                    .map(Path::toFile).map(f -> f.delete())
                    .allMatch((r) -> r);
            if (!removed) {
                LOGGER.warn("Cannot remove directory '{}'",
                            folder.getAbsolutePath());
            }
            return new ResponseEntity<>(new ScoreViewModel().score(
                score.floatValue()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @Override
    public ResponseEntity<List<SubmissionResponse>> testCode(
        @ApiParam(value = "Code submission to evaluate", required = true)
        @RequestParam(value = "submission", required = true) String submission,
        @ApiParam(value = "The files to upload", required = true)
        @Valid
        @RequestParam("files") MultipartFile[] files,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {

        if (accept != null && accept.contains("application/json")) {
            SubmissionRequest request = objectMapper.readValue(submission,
                                                               SubmissionRequest.class);

            if (request == null || files.length < 1) {
                return new ResponseEntity<>(Arrays.asList(
                    new SubmissionResponse().score(BigDecimal.ZERO)
                    .resultName("error").returnMessage(
                    "Please provide 'files' and 'submission' in form")),
                                            HttpStatus.NO_CONTENT);
            }

            Optional<User> user = this.userService.getCurrentUser();
            Challenge challenge = this.challengeService.getChallengeById(
                      request.getChallenge());
            Team team = this.teamService.getTeamById(request.getTeam());
            Exercice exercice = this.exerciceService.getExerciceById(
                     request.getExercice());

            if (!user.isPresent() || challenge == null || team == null ||
                exercice == null) {
                return new ResponseEntity<>(Arrays.asList(
                    new SubmissionResponse().score(BigDecimal.ZERO)
                    .resultName("error").returnMessage(
                    "Wrong request information")), HttpStatus.NO_CONTENT);
            }

            File folder = new File(
                 String.format("%s/%s", TMP_CODE_PATH, Manager.contructName(
                               request.getTeam(), request.getExercice())));
            String fileName;
            File targetFile;
            for (MultipartFile file : files) {
                fileName = file.getOriginalFilename().replaceAll("\\?", "/");
                targetFile = new File(folder, fileName);
                if (!targetFile.getParentFile().exists()) {
                    Files.createDirectories(targetFile.getParentFile()
                        .toPath());
                }
                if (targetFile.createNewFile()) {
//                file.transferTo(targetFile);
                    Files.copy(file.getInputStream(), targetFile.toPath(),
                               StandardCopyOption.REPLACE_EXISTING);
                } else {
                    LOGGER.error("Cannot create temporary file '{}'",
                                 targetFile.getAbsolutePath());
                }
            }

            final List<SubmissionResponse> responses = new ArrayList<>();
            Manager.process(request.getTeam(), request.getExercice(),
                            request.getLanguage().toString(), folder.
                            getAbsolutePath(), (results) -> {
                            results.stream().filter(rr -> rr != null).
                                forEach(
                                    (rr) -> {
                                    SubmissionResponse response =
                                                       new SubmissionResponse();
                                    response.setResultName(rr.
                                        getResultName());
                                    response.setReturnMessage(rr.
                                        getLogResult());
                                    response.setScore(BigDecimal.valueOf(rr.
                                        getResult()));
                                    response.setStdout(rr.getStdout());
                                    response.setStderr(rr.getStderr());
                                    responses.add(response);
                                });
                        });

            List<SubmissionResponse> publicResponses =
                                     responses.stream().filter(response ->
                                         exercice.getTestCases().stream()
                                         .filter(tc -> !tc.getHidden())
                                         .map(tc -> tc.getName())
                                         .collect(Collectors.toSet())
                                         .contains(response.getResultName())).
                                     collect(Collectors.toList());

            boolean removed = Files.walk(folder.toPath())
                    .sorted((p1, p2) -> -p1.compareTo(p2))
                    .map(Path::toFile).map(f -> f.delete())
                    .allMatch((r) -> r);
            if (!removed) {
                LOGGER.warn("Cannot remove directory '{}'",
                            folder.getAbsolutePath());
            }
            return new ResponseEntity<>(publicResponses, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }
}
