package fr.nemolovich.apps.codeplatform.api.webapi.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.nemolovich.apps.codeplatform.api.model.Challenge;
import fr.nemolovich.apps.codeplatform.api.model.CodeHelper;
import fr.nemolovich.apps.codeplatform.api.model.CodeStarter;
import fr.nemolovich.apps.codeplatform.api.model.Exercice;
import fr.nemolovich.apps.codeplatform.api.model.Result;
import fr.nemolovich.apps.codeplatform.api.model.Role;
import fr.nemolovich.apps.codeplatform.api.model.Team;
import fr.nemolovich.apps.codeplatform.api.model.TestCase;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.ExerciceViewModel;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.ResultViewModel;
import fr.nemolovich.apps.codeplatform.api.security.Roles;
import fr.nemolovich.apps.codeplatform.api.security.SecurityUtils;
import fr.nemolovich.apps.codeplatform.api.service.ChallengeService;
import fr.nemolovich.apps.codeplatform.api.service.DBFileService;
import fr.nemolovich.apps.codeplatform.api.service.ExerciceService;
import fr.nemolovich.apps.codeplatform.api.service.ResultService;
import fr.nemolovich.apps.codeplatform.api.service.UserService;
import fr.nemolovich.apps.codeplatform.api.webapi.ExerciceApi;
import fr.nemolovich.apps.codeplatform.manager.docker.Manager;
import fr.nemolovich.apps.codeplatform.manager.docker.ResourcesManager;
import io.swagger.annotations.ApiParam;
import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Ionut on 08/10/2017.
 */
@Controller
public class ExerciceApiController implements ExerciceApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(
                                ExerciceApiController.class);

    private final ObjectMapper objectMapper;
    private final UserService userService;
    private final ExerciceService exerciceService;
    private final ChallengeService challengeService;
    private final ResultService resultService;
    private final DBFileService dbFileService;

    public ExerciceApiController(ObjectMapper objectMapper,
                                 UserService userService,
                                 ExerciceService exerciceService,
                                 ChallengeService challengeService,
                                 ResultService resultService,
                                 DBFileService dbFileService) {
        this.objectMapper = objectMapper;
        this.userService = userService;
        this.exerciceService = exerciceService;
        this.challengeService = challengeService;
        this.resultService = resultService;
        this.dbFileService = dbFileService;
    }

    @Override
    public ResponseEntity<Exercice> addExercice(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "The exercice to create", required = true)
        @Valid @RequestBody Exercice exercice,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("addExercice Rest");
        if (accept != null && accept.contains("application/json")) {
            LOGGER.debug("REST request to save addExercice : {}", exercice);
            Exercice createdExercice;
            if (exercice.getId() != null) {
                LOGGER.info(
                    "Exercice id is given, we will use this one and add it to challenge");
                createdExercice = exerciceService.getExerciceById(
                exercice.getId());
            } else {
                LOGGER.info("Exercice will be created");
                Exercice tempExercice = new Exercice()
                         .coeff(exercice.getCoeff())
                         .subtitle(exercice.getSubtitle())
                         .text(exercice.getText())
                         .title(exercice.getTitle())
                         .type(exercice.getType())
                         .maxTime(exercice.getMaxTime());
                createdExercice = exerciceService.createExercice(tempExercice);
                createdExercice.
                    setCreatedBy(SecurityUtils.getCurrentUserLogin());
                createdExercice.setLastModifiedBy(SecurityUtils.
                    getCurrentUserLogin());
                createdExercice.setCodeHelpers(exercice.getCodeHelpers());
                createdExercice.setCodeStarters(exercice.getCodeStarters());
                createdExercice.setTestCases(exercice.getTestCases());
                createExerciceFiles(createdExercice);
                createdExercice = exerciceService.
                updateExercice(createdExercice);
            }
            Challenge challenge = challengeService.addExerciceInChallenge(
                      challengeService.getChallengeById(challengeId),
                      createdExercice);
            LOGGER.debug("Added Exercice to Challenge:[{}]", challenge);
            if (challenge != null) {
                return ResponseEntity.ok(createdExercice);
            }
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Exercice> deleteExercice(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the exercice to delete", required = true)
        @PathVariable("exerciceId") String exerciceId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("deleteChallenge challengeId:{}", challengeId);
        if (accept != null && accept.contains("application/json") &&
            challengeId != null) {
            LOGGER.debug(
                "REST request to delete exercice : {} from challeng : {}",
                exerciceId, challengeId);
            // We just delete the exercice from challenge, don't delete it for real
            Challenge challenge = challengeService.deleteExercicesInChallenge(
                      challengeId, exerciceId);
            if (challenge != null) {
                return ResponseEntity.ok(null);
            }
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Set<ExerciceViewModel>> getAllExercicesByChallengeId(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {

        if (accept != null && accept.contains("application/json")) {
            Challenge challenge = challengeService.getChallengeById(
                      challengeId);
            if (challenge == null) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
            } else {
                return ResponseEntity.ok(challenge.getExercices().stream()
                    .map(ExerciceViewModel::new)
                    .collect(Collectors.toSet()));
            }
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Set<ResultViewModel>> getResultsByExerciceId(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the requested exercice", required = true)
        @PathVariable("exerciceId") String exerciceId,
        @ApiParam(value = "Name of the team to get results")
        @Valid @RequestParam(value = "teamName", required = false) String teamName,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("getResultsByExerciceId exerciceId:{}", exerciceId);
        if (accept != null && accept.contains("application/json")) {
            // Verify if the exercice is inside a challenge
            Exercice exercice = challengeService.getExerciceInChallengeById(
                     challengeId, exerciceId);
            if (exercice != null) {
                Set<ResultViewModel> resultViewModels = resultService
                                     .getResultViewModelsByExerciceId(exerciceId);
                if (teamName != null && !teamName.isEmpty()) {
                    Team team = challengeService.getTeamInChallengeByName(
                         challengeId, teamName);
                    Result result = resultService.getResultForTeamInChallenge(
                           challengeId, exerciceId, team.getId());
                    if (result.getReopenDate() == null &&
                        result.getEndDate() != null) {
                        result.setReopenDate(Instant.now());
                        result = resultService.updateResult(result);
                    }
                    resultViewModels = Collections.singleton(
                    new ResultViewModel(result));
                }
                // return Result of this specific Exercice
                return ResponseEntity.ok(resultViewModels);
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Exercice> updateExercice(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "the exercice to update", required = true)
        @Valid
        @RequestBody Exercice exercice,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("update exercice exerciceId:{}", exercice);
        if (accept != null && accept.contains("application/json")) {
            String previousTitle = null;
            // If creation
            if (exercice.getId() == null) {
                return addExercice(challengeId, exercice, accept);
            } else {
                Exercice exerciceTest = challengeService
                         .getExerciceInChallengeById(challengeId,
                                                     exercice.getId());
                previousTitle = exerciceTest.getTitle();
                Challenge challenge = challengeService.getChallengeById(
                          challengeId);
                // Exercice is an exercice of the challenge
                // And we will not update with the same title
                if (exerciceTest != null &&
                    (previousTitle == null ||
                     previousTitle.equals(exercice.getTitle()) ||
                     !previousTitle.equals(exercice.getTitle()) &&
                     !challengeService.hasExerciceTitleAlreadyInChallenge(
                     challenge, exercice.getTitle()))) {
                    createExerciceFiles(exercice);
                    Exercice updatedExercice = exerciceService.updateExercice(
                             exercice);
                    return ResponseEntity.ok(updatedExercice);
                }
            }
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Exercice> getExerciceById(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the requested exercice", required = true)
        @PathVariable("exerciceId") String exerciceId,
        @RequestParam(value = "fullMode", required = false) Boolean fullMode,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        if (accept != null && accept.contains("application/json")) {
            // Verify if the exercice is inside a challenge
            Exercice exercice = challengeService.getExerciceInChallengeById(
                     challengeId, exerciceId);
            if (exercice != null) {

                Set<Role> roles = userService.getRoles(SecurityUtils
                          .getCurrentUserLogin());
                if (!Boolean.TRUE.equals(fullMode) || !roles.stream().anyMatch((
                    r) -> r.getName().
                    equalsIgnoreCase(Roles.ADMIN.toString()))) {
                    exercice = Exercice.publicExercice(exercice);
                }
                return ResponseEntity.ok(exercice);
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Set> listLanguages(
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        if (accept != null && accept.contains("application/json")) {
            return ResponseEntity.ok(new HashSet(Manager.availableLanguages()));
        }
        return ResponseEntity.badRequest().body(null);
    }

    private void createExerciceFiles(Exercice exercice) {
        Map<String, byte[]> inputs = new HashMap<>();
        Map<String, byte[]> outputs = new HashMap<>();
        exercice.getTestCases().stream().forEach((testCase) ->
            {
                TestCase t;
                if (testCase.getId() == null) {
                    t = new TestCase();
                } else {
                    t = dbFileService.getTestCaseById(testCase.getId());
                }
                t.setName(testCase.getName());
                t.setInput(testCase.getInput());
                t.setOutput(testCase.getOutput());
                t.setOrder(testCase.getOrder());
                t.sethidden(testCase.getHidden());
                t = dbFileService.updateTestCase(t);
                testCase.setId(t.getId());
                inputs.put(t.getName().concat(".in"), t.getInput().
                           getBytes());
                outputs.put(t.getName().concat(".out"), t.getOutput().
                            getBytes());
            });
        exercice.getCodeHelpers().stream().forEach((codeHelper) ->
            {
                CodeHelper c;
                if (codeHelper.getId() == null) {
                    c = new CodeHelper();
                } else {
                    c = dbFileService.getCodeHelperById(
                    codeHelper.getId());
                }
                c.setContent(codeHelper.getContent());
                c.setLanguage(codeHelper.getLanguage());
                c.setOrder(codeHelper.getOrder());
                c = dbFileService.updateCodeHelper(c);
                codeHelper.setId(c.getId());
            });
        exercice.getCodeStarters().stream().forEach((codeStarter) ->
            {
                CodeStarter c;
                if (codeStarter.getId() == null) {
                    c = new CodeStarter();
                } else {
                    c = dbFileService.getCodeStarterById(
                    codeStarter.getId());
                }
                c.setContent(codeStarter.getContent());
                c.setLanguage(codeStarter.getLanguage());
                c = dbFileService.updateCodeStarter(c);
                codeStarter.setId(c.getId());
            });

        boolean created = ResourcesManager
                .addExercice(exercice.getId(), inputs, outputs, null,
                             null);
        if (created) {
            LOGGER.debug("Exercice files created");
        } else {
            LOGGER.error("Exercice files creation failed");
        }
    }
}
