package fr.nemolovich.apps.codeplatform.api.service;

import fr.nemolovich.apps.codeplatform.api.model.Role;
import fr.nemolovich.apps.codeplatform.api.model.User;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.UserRegisterModel;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.UserViewModel;
import fr.nemolovich.apps.codeplatform.api.repository.RoleRepository;
import fr.nemolovich.apps.codeplatform.api.repository.UserRepository;
import fr.nemolovich.apps.codeplatform.api.security.Roles;
import fr.nemolovich.apps.codeplatform.api.security.SecurityUtils;
import fr.nemolovich.apps.codeplatform.api.webapi.exception.ApiException;
import fr.nemolovich.apps.codeplatform.api.webapi.exception.NotFoundException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Service class for managing users.
 */
@Service
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        UserService.class);

    private static final String BCRYPT_PATTERN = "^\\$2a\\$.{56}$";

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;

    public UserService(UserRepository userRepository,
        PasswordEncoder passwordEncoder, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }

    public Optional<User> activateRegistration(String login) {
        LOGGER.debug("Activating user for activation login {}", login);
        return userRepository.findOneByLogin(login)
            .map(user -> {
                // activate given user for the registration key.
                user.setActivated(true);
                userRepository.save(user);
                LOGGER.debug("Activated user: {}", user);
                return user;
            });
    }

    public User createUser(User user) {
        user.setActivated(true);
        if (user.getRoles().isEmpty()) {
            user.getRoles().add(roleRepository.findOne(Roles.USER.toString()));
            LOGGER.debug("No role, USER by default");
        }
        Matcher m = Pattern.compile(BCRYPT_PATTERN).matcher(user.getPassword());
        if (!m.matches()) {
            LOGGER.debug("The field Password is not Crypt, we crypt it");
            user.setPassword(passwordEncoder.encode(
                user.getPassword()));
        }
        user = userRepository.save(user);
        LOGGER.debug("Created Information for User: {}", user);
        LOGGER.debug("Activated by default");
        return user;
    }

    public User createUser(UserRegisterModel userRegisterModel) {
        User user = new User();
        user.setLogin(userRegisterModel.getLogin());
        user.setFirstName(userRegisterModel.getFirstName());
        user.setLastName(userRegisterModel.getLastName());
        user.setEmail(userRegisterModel.getEmail());
        user.setImageUrl(userRegisterModel.getImageUrl());
        String encryptedPassword = passwordEncoder.encode(
            userRegisterModel.getPassword());
        user.setPassword(encryptedPassword);
        user.setActivated(false);
        user.getRoles().add(roleRepository.findOne(Roles.USER.toString()));
        user = userRepository.save(user);
        LOGGER.debug("Created Information for User: {}", user);
        return user;
    }

    /**
     * Update basic information (first name, last name, email, language) for the
     * current user.
     *
     * @param userViewModel userViewModel of user
     */
    public void updateUser(UserViewModel userViewModel) {
        userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin())
            .ifPresent(user -> {
                user.setFirstName(userViewModel.getFirstName());
                user.setLastName(userViewModel.getLastName());
                user.setEmail(userViewModel.getEmail());
                user.setImageUrl(userViewModel.getImageUrl());
                userRepository.save(user);
                LOGGER.debug("Changed Information for User: {}", user);
            });
    }

    /**
     * Update complete user current user.
     *
     * @param user user
     */
    public void updateUser(User user) {
        userRepository.findOneByLogin(user.getLogin())
            .ifPresent(u -> {
                u.setFirstName(user.getFirstName());
                u.setLastName(user.getLastName());
                u.setEmail(user.getEmail());
                u.setImageUrl(user.getImageUrl());
                Matcher m = Pattern.compile(BCRYPT_PATTERN)
                    .matcher(user.getPassword());
                if (!m.matches()) {
                    LOGGER.debug(
                        "The field Password is not Crypt, we crypt it");
                    u.setPassword(passwordEncoder.encode(
                        user.getPassword()));
                }
                if (user.getRoles().isEmpty()) {
                    u.getRoles().add(roleRepository.findOne(
                        Roles.USER.toString()));
                    LOGGER.debug("No role, USER by default");
                } else {
                    u.setRoles(user.getRoles());
                }
                userRepository.save(u);
                LOGGER.debug("Changed Information for User: {}", u);
            });
    }

    public List<User> getAllUsers() {
        return userRepository.findAll().stream().collect(Collectors.toList());
    }

    public List<UserViewModel> getAllUserViewModels() {
        return userRepository.findAll().stream().map(UserViewModel::new)
            .collect(Collectors.toList());
    }

    public void deleteUser(String login) throws ApiException {
        if (login != null && login.equalsIgnoreCase(
            SecurityUtils.getCurrentUserLogin())) {
            throw new ApiException(202, "You try to delete yourself");
        }
        Optional<User> user = userRepository.findOneByLogin(login);
        if (user.isPresent()) {
            userRepository.delete(user.get());
            LOGGER.debug("Deleted User: {}", user);
        } else {
            throw new NotFoundException(202, "User not found");
        }
    }

    public User changePassword(String password) {
        User user = userRepository.findOneByLogin(
            SecurityUtils.getCurrentUserLogin())
            .orElse(null);
        if (user != null) {
            String encryptedPassword = passwordEncoder.encode(password);
            user.setPassword(encryptedPassword);
            LOGGER.debug("Changed password for User: {}", user);
            return userRepository.save(user);
        }
        return null;
    }

    public Optional<User> getCurrentUser() {
        return userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
    }

    public UserViewModel getUserViewModelByLogin(String login) {
        User user = getUserByLogin(login);
        return user == null ? null : new UserViewModel(user);
    }

    public User getUserByLogin(String login) {
        User user = userRepository.findOneByLogin(login).orElse(null);
        return user;
    }

    public User getUserByEmail(String email) {
        User user = userRepository.findOneByEmail(email).orElse(null);
        return user;
    }

    public User getFullUser(String id) {
        return userRepository.findOne(id);
    }

    /**
     * @param userLogin The requester user login
     * @return a list of all the authorities
     */
    public Set<Role> getRoles(String userLogin) {
        User user = userRepository.findOneByLogin(userLogin).orElse(null);
        return user == null ? new HashSet<>() : user.getRoles();
    }

    /**
     * Not activated users should be automatically deleted after numberOfDays
     * days.
     *
     * @param numberOfDays numberOfDays
     */
    public void removeNotActivatedUsers(Integer numberOfDays) {
        List<User> users = userRepository
            .findAllByActivatedIsFalseAndCreatedDateBefore(
                Instant.now().minus(3, ChronoUnit.DAYS));
        users.stream().forEach(user -> {
            LOGGER.debug("User : {} \n will be deleted", user.toString());
            LOGGER.info("Deleting not activated user {}", user.getLogin());
            userRepository.delete(user);
        });
    }
}
