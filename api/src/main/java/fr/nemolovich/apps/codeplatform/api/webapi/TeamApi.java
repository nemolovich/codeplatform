package fr.nemolovich.apps.codeplatform.api.webapi;


import fr.nemolovich.apps.codeplatform.api.model.viewmodel.ResultViewModel;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.TeamViewModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.Set;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Team API
 */
@Api(value = "Team API", description = "The Team API",
     tags = {"team"})
public interface TeamApi {

    @ApiOperation(value = "Delete a team from the challenge", notes = "",
                  response = TeamViewModel.class, tags = {"admin", "challenge",
                                                          "team",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Deletion Succeed",
                     response = TeamViewModel.class),
        @ApiResponse(code = 204, message = "No Content"),
        @ApiResponse(code = 304, message = "Not Modified",
                     response = TeamViewModel.class),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/admin/challenges/{challengeId}/team/{teamId}",
                    produces = {"application/json"},
                    method = RequestMethod.DELETE)
    ResponseEntity<TeamViewModel> deleteTeam(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the team to delete", required = true)
        @PathVariable("teamId") String teamId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Explore the results for the given team", notes = "",
                  response = ResultViewModel.class, responseContainer = "Set",
                  tags = {"challenge", "team", "result",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The requested results",
                     response = ResultViewModel.class, responseContainer = "Set"),
        @ApiResponse(code = 204, message = "No Content"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/challenge/{challengeId}/team/{teamId}/result",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    ResponseEntity<Set<ResultViewModel>> getResultsByTeamId(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the requested team", required = true)
        @PathVariable("teamId") String teamId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Get teams registred to a challenge", notes = "",
                  response = TeamViewModel.class, tags = {"challenge", "team",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The requested team",
                     response = TeamViewModel.class),
        @ApiResponse(code = 204, message = "No Content"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/challenge/{challengeId}/team/{teamId}",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    ResponseEntity<TeamViewModel> getTeamByChallengeId(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the requested team", required = true)
        @PathVariable("teamId") String teamId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "A team join the challenge", notes = "",
                  response = TeamViewModel.class, tags = {"challenge", "team",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The requested team",
                     response = TeamViewModel.class),
        @ApiResponse(code = 204, message = "No Content"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/challenge/{challengeId}/team/{teamId}/join",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    ResponseEntity<TeamViewModel> getTeamByJoiningChallengeId(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the requested team", required = true)
        @PathVariable("teamId") String teamId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Get teams registred to a challenge", notes = "",
                  response = TeamViewModel.class, responseContainer = "Set",
                  tags = {"challenge", "team",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The requested teams",
                     response = TeamViewModel.class, responseContainer = "Set"),
        @ApiResponse(code = 204, message = "No Content"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/challenge/{challengeId}/team",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    ResponseEntity<Set<TeamViewModel>> getTeamsByChallengeId(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @RequestParam(value = "filter", required = false) String filter,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Update a team for the challenge", notes = "",
                  response = TeamViewModel.class, tags = {"admin", "challenge",
                                                          "team",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The updated team",
                     response = TeamViewModel.class),
        @ApiResponse(code = 304, message = "Not Modified",
                     response = TeamViewModel.class),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/admin/challenges/{challengeId}/team",
                    produces = {"application/json"},
                    consumes = {"application/json"},
                    method = RequestMethod.PUT)
    ResponseEntity<TeamViewModel> updateTeam(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "the team to update", required = true)
        @Valid @RequestBody TeamViewModel teamViewModel,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Update my team for the challenge", notes = "",
                  response = TeamViewModel.class, tags = {"challenge", "team",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The updated team",
                     response = TeamViewModel.class),
        @ApiResponse(code = 304, message = "Not Modified",
                     response = TeamViewModel.class),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/challenges/{challengeId}/team",
                    produces = {"application/json"},
                    consumes = {"application/json"},
                    method = RequestMethod.PUT)
    ResponseEntity<TeamViewModel> updateMyTeam(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "the team to update", required = true)
        @Valid @RequestBody TeamViewModel teamViewModel,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(nickname = "addTeam",
                  value = "Create a new team for the challenge",
                  response = TeamViewModel.class,
                  tags = {"admin", "challenge", "team",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Creation Succeed",
                     response = TeamViewModel.class),
        @ApiResponse(code = 201, message = "Creation Succeed",
                     response = TeamViewModel.class),
        @ApiResponse(code = 304, message = "Not Modified",
                     response = TeamViewModel.class),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/admin/challenges/{challengeId}/team",
                    produces = {"application/json"},
                    consumes = {"application/json"},
                    method = RequestMethod.POST)
    ResponseEntity<TeamViewModel> addTeam(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "the team to add", required = true)
        @Valid @RequestBody TeamViewModel TeamViewModel,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Delete a team from the challenge", notes = "",
                  response = TeamViewModel.class, tags = {"admin", "challenge",
                                                          "team",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Deletion Succeed",
                     response = TeamViewModel.class),
        @ApiResponse(code = 204, message = "No Content"),
        @ApiResponse(code = 304, message = "Not Modified"),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(
        value = "/api/admin/challenges/{challengeId}/team/{teamId}/member/{userId}",
        produces = {"application/json"},
        method = RequestMethod.DELETE)
    ResponseEntity<TeamViewModel> removeUserFromTeam(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the team from which delete", required = true)
        @PathVariable("teamId") String teamId,
        @ApiParam(value = "ID of the the user to remove", required = true)
        @PathVariable("userId") String userId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    /* @ApiOperation(value = "A user join a team", response = TeamViewModel.class, tags = { "user", "team",})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Creation Succeed", response = TeamViewModel.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Object.class),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/team",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<TeamViewModel> joinTeam(@ApiParam(value = "Name of the team", required = true) @QueryParam("teamName") String teamName, @ApiParam(value = "", required = true) @Valid @RequestBody Team team, @RequestHeader(value = "Accept", required = false) String accept) throws Exception;
     */
}
