package fr.nemolovich.apps.codeplatform.api.repository;

import fr.nemolovich.apps.codeplatform.api.model.CodeStarter;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the CodeStarter entiry management.
 */
public interface CodeStarterRepository extends
    MongoRepository<CodeStarter, String> {

}
