package fr.nemolovich.apps.codeplatform.pdf.model;

/**
 *
 * @author bgohier
 */
public class CodeLineData {

    private String content;
    private int number;

    public CodeLineData() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public CodeLineData content(String content) {
        this.content = content;
        return this;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public CodeLineData number(int number) {
        this.number = number;
        return this;
    }

}
