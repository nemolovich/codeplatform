package fr.nemolovich.apps.codeplatform.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.nemolovich.apps.codeplatform.api.model.enums.DifficultyLevel;
import fr.nemolovich.apps.codeplatform.api.model.enums.Language;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.validation.annotation.Validated;

/**
 * Information about an applicant.
 *
 * @author bgohier
 */
@Validated
@Document(collection = "codeplatform_applicant")
public class Applicant {

    @JsonProperty("id")
    @Size(min = 0, max = 50)
    @Id
    private String id = null;

    @Field("recruiter")
    @NotNull
    @DBRef
    private User recruiter = null;

    @Field("first_name")
    @Size(max = 50)
    @NotNull
    private String firstName;

    @Field("last_name")
    @Size(max = 50)
    @NotNull
    private String lastName;

    @Email
    @Field("email")
    @NotNull
    @Size(min = 5, max = 100)
    private String email;

    @Field("phone")
    @Size(min = 10, max = 10)
    @Pattern(regexp = "^\\d{10}$")
    private String phone;

    @Field("job_title")
    @Size(max = 100)
    private String jobTitle;

    @Field("recruiter_notes")
    @Size(max = 500)
    private String recruiterNotes;

    @JsonProperty("language")
    @NotNull
    private Language language = null;

    @JsonProperty("difficulty")
    @NotNull
    private DifficultyLevel difficulty = null;

    @JsonProperty("duration")
    @Field("duration")
    @NotNull
    private Integer duration = 15;

    @JsonProperty("challenge")
    @DBRef
    private Challenge challenge = null;

    @JsonProperty("cv")
    private byte[] cv = null;

    @JsonProperty("accepted")
    private Boolean accepted = null;

    public void copy(Applicant applicant) {
        this.id = applicant.getId();
        this.firstName = applicant.getFirstName();
        this.lastName = applicant.getLastName();
        this.email = applicant.getEmail();
        this.phone = applicant.getPhone();
        this.jobTitle = applicant.getJobTitle();
        this.recruiterNotes = applicant.getRecruiterNotes();
        this.language = applicant.getLanguage();
        this.difficulty = applicant.getDifficulty();
        this.duration = applicant.getDuration();
        this.cv = applicant.getCv();
        this.accepted = applicant.getAccepted();
    }

    public Applicant id(String id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     *
     */
    @ApiModelProperty(value = "")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Applicant recruiter(User recruiter) {
        this.recruiter = recruiter;
        return this;
    }

    /**
     * Get recruiter
     *
     * @return recruiter
     *
     */
    @ApiModelProperty(value = "")
    public User getRecruiter() {
        return this.recruiter;
    }

    public void setRecruiter(User recruiter) {
        this.recruiter = recruiter;
    }

    public Applicant firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    /**
     * Get firstname
     *
     * @return firstname
     *
     */
    @ApiModelProperty(value = "")
    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Applicant lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    /**
     * Get lastName
     *
     * @return lastName
     *
     */
    @ApiModelProperty(value = "")
    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Applicant email(String email) {
        this.email = email;
        return this;
    }

    /**
     * Get email
     *
     * @return email
     *
     */
    @ApiModelProperty(value = "")
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Applicant phone(String phone) {
        this.phone = phone;
        return this;
    }

    /**
     * Get phone
     *
     * @return phone
     *
     */
    @ApiModelProperty(value = "")
    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Applicant jobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
        return this;
    }

    /**
     * Get jobTitle
     *
     * @return jobTitle
     *
     */
    @ApiModelProperty(value = "")
    public String getJobTitle() {
        return this.jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public Applicant recruiterNotes(String recruiterNotes) {
        this.recruiterNotes = recruiterNotes;
        return this;
    }

    /**
     * Get recruiterNotes
     *
     * @return recruiterNotes
     *
     */
    @ApiModelProperty(value = "")
    public String getRecruiterNotes() {
        return this.recruiterNotes;
    }

    public void setRecruiterNotes(String recruiterNotes) {
        this.recruiterNotes = recruiterNotes;
    }

    public Applicant language(Language language) {
        this.language = language;
        return this;
    }

    /**
     * Get language
     *
     * @return language
     *
     */
    @ApiModelProperty(value = "")
    public Language getLanguage() {
        return this.language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Applicant difficulty(DifficultyLevel difficulty) {
        this.difficulty = difficulty;
        return this;
    }

    /**
     * Get difficulty
     *
     * @return difficulty
     *
     */
    @ApiModelProperty(value = "")
    public DifficultyLevel getDifficulty() {
        return this.difficulty;
    }

    public void setDifficulty(DifficultyLevel difficulty) {
        this.difficulty = difficulty;
    }

    public Applicant duration(Integer duration) {
        this.duration = duration;
        return this;
    }

    /**
     * Get duration
     *
     * @return duration
     *
     */
    @ApiModelProperty(value = "")
    public Integer getDuration() {
        return this.duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Applicant challenge(Challenge challenge) {
        this.challenge = challenge;
        return this;
    }

    /**
     * Get challenge
     *
     * @return challenge
     *
     */
    @ApiModelProperty(value = "")
    public Challenge getChallenge() {
        return this.challenge;
    }

    public void setChallenge(Challenge challenge) {
        this.challenge = challenge;
    }

    public Applicant cv(byte[] cv) {
        this.cv = cv;
        return this;
    }

    /**
     * Get cv
     *
     * @return cv
     *
     */
    @ApiModelProperty(value = "")
    public byte[] getCv() {
        return this.cv;
    }

    public void setCv(byte[] cv) {
        this.cv = cv;
    }

    public Applicant accepted(Boolean accepted) {
        this.accepted = accepted;
        return this;
    }

    /**
     * Get accepted
     *
     * @return accepted
     *
     */
    @ApiModelProperty(value = "")
    public Boolean getAccepted() {
        return this.accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Applicant applicant = (Applicant) o;
        return Objects.equals(this.recruiter, applicant.recruiter) &&
               Objects.equals(this.firstName, applicant.firstName) &&
               Objects.equals(this.lastName, applicant.lastName) &&
               Objects.equals(this.email, applicant.email) &&
               Objects.equals(this.phone, applicant.phone) &&
               Objects.equals(this.jobTitle, applicant.jobTitle) &&
               Objects.equals(this.recruiterNotes, applicant.recruiterNotes) &&
               Objects.equals(this.language, applicant.language) &&
               Objects.equals(this.difficulty, applicant.difficulty) &&
               Objects.equals(this.duration, applicant.duration) &&
               Objects.equals(this.challenge, applicant.challenge) &&
               Objects.equals(this.accepted, applicant.accepted);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.recruiter, this.firstName, this.lastName,
                            this.email, this.phone, this.jobTitle,
                            this.recruiterNotes, this.language, this.difficulty,
                            this.duration, this.challenge, this.accepted);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Applicant {\n");
        sb.append("    id: ").append(toIndentedString(this.id)).append("\n");
        sb.append("    recruiter: ").append(
            toIndentedString(this.recruiter)).append("\n");
        sb.append("    firstName: ").append(
            toIndentedString(this.firstName)).append("\n");
        sb.append("    lastName: ").append(
            toIndentedString(this.lastName)).append("\n");
        sb.append("    email: ").append(
            toIndentedString(this.email)).append("\n");
        sb.append("    phone: ").append(
            toIndentedString(this.phone)).append("\n");
        sb.append("    jobTitle: ").append(
            toIndentedString(this.jobTitle)).append("\n");
        sb.append("    recruiterNotes: ").append(toIndentedString(
            this.recruiterNotes)).append("\n");
        sb.append("    language: ").append(
            toIndentedString(this.language)).append("\n");
        sb.append("    difficulty: ").append(
            toIndentedString(this.difficulty)).append("\n");
        sb.append("    duration: ").append(
            toIndentedString(this.duration)).append("\n");
        sb.append("    challenge: ").append(
            toIndentedString(this.challenge)).append("\n");
        sb.append("    accepted: ").append(
            toIndentedString(this.accepted)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     *
     * @param o {@link Object}: Object to format.
     * @return {@link String}: String representation of object.
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
