package fr.nemolovich.apps.codeplatform.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.validation.annotation.Validated;

/**
 *
 * @author bgohier
 */
@Validated
@javax.annotation.Generated(
    value = "bgohier",
    date = "2017-10-07T22:21:00.000+02:00")
@Document(collection = "codeplatform_testcase")
public class TestCase {

    @JsonProperty("id")
    @Id
    private String id = null;

    @JsonProperty("name")
    @NotNull
    private String name = null;

    @JsonProperty("hidden")
    @NotNull
    private boolean hidden = true;

    @JsonProperty("input")
    @NotNull
    private String input = null;

    @JsonProperty("output")
    @NotNull
    private String output = null;
    
    @JsonProperty("order")
    private Integer order = 0;

    public TestCase id(String id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     *
     */
    @ApiModelProperty(value = "")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TestCase name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     *
     */
    @ApiModelProperty(value = "")
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TestCase hidden(boolean hidden) {
        this.hidden = hidden;
        return this;
    }

    /**
     * Get hidden
     *
     * @return hidden
     *
     */
    @ApiModelProperty(value = "")
    public boolean getHidden() {
        return hidden;
    }

    public void sethidden(boolean hidden) {
        this.hidden = hidden;
    }

    public TestCase input(String input) {
        this.input = input;
        return this;
    }

    /**
     * Get input
     *
     * @return input
     *
     */
    @ApiModelProperty(value = "")
    public String getInput() {
        return this.input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public TestCase output(String output) {
        this.output = output;
        return this;
    }

    /**
     * Get output
     *
     * @return output
     *
     */
    @ApiModelProperty(value = "")
    public String getOutput() {
        return this.output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public TestCase order(Integer order) {
        this.order = order;
        return this;
    }

    /**
     * Get order
     *
     * @return order
     *
     */
    @ApiModelProperty(value = "")
    public Integer getOrder() {
        return this.order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TestCase testCase = (TestCase) o;
        return Objects.equals(this.name, testCase.name)
            && Objects.equals(this.hidden, testCase.hidden)
            && Objects.equals(this.input, testCase.input)
            && Objects.equals(this.output, testCase.output);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, hidden, input, output);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Result {\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    hidden: ").append(toIndentedString(hidden)).append("\n");
        sb.append("    input: ").append(toIndentedString(input)).append("\n");
        sb.append("    output: ").append(toIndentedString(output)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     * 
     * @param o {@link Object}: Object to format.
     * @return {@link String}: String representation of object.
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

}
