package fr.nemolovich.apps.codeplatform.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import fr.nemolovich.apps.codeplatform.api.model.Submission;

/**
 * Spring Data MongoDB repository for the Challenge entity.
 */
@Repository
public interface SubmissionRepository extends MongoRepository<Submission, String> {

}
