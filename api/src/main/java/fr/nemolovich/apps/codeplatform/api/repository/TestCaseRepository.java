package fr.nemolovich.apps.codeplatform.api.repository;

import fr.nemolovich.apps.codeplatform.api.model.TestCase;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the TestCase entiry management.
 */
public interface TestCaseRepository extends MongoRepository<TestCase, String> {

}
