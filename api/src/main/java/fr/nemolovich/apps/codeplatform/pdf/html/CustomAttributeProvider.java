package fr.nemolovich.apps.codeplatform.pdf.html;

import java.util.Map;
import org.commonmark.ext.gfm.tables.TableBody;
import org.commonmark.ext.gfm.tables.TableHead;
import org.commonmark.node.Code;
import org.commonmark.node.Node;
import org.commonmark.renderer.html.AttributeProvider;
import org.commonmark.renderer.html.AttributeProviderContext;

/**
 *
 * @author bgohier
 */
public class CustomAttributeProvider implements AttributeProvider {

    private static final String STYLE_ATTRIBUTE = "style";
    private static final String CODE_STYLE = "color:#404080;font-weight:bold;";
    private static final String TABLE_HEADER_STYLE = CODE_STYLE.concat(
                                "padding:5px;background-color:#FEFEFE;margin-top:5px;");
    private static final String TABLE_CONTENT_STYLE =
                                "background-color:#101020;color:#DDDDDD;border-right:1px solid #FEFEFE;";

    private final AttributeProviderContext context;

    public CustomAttributeProvider(AttributeProviderContext context) {
        this.context = context;
    }

    @Override
    public void setAttributes(Node node, String tagName,
                              Map<String, String> attributes) {
        if (node instanceof Code) {
            attributes.put(STYLE_ATTRIBUTE, CODE_STYLE);
        } else if (node instanceof TableHead) {
            attributes.put(STYLE_ATTRIBUTE, TABLE_HEADER_STYLE);
        } else if (node instanceof TableBody) {
            attributes.put(STYLE_ATTRIBUTE, TABLE_CONTENT_STYLE);
        }
    }

}
