package fr.nemolovich.apps.codeplatform.api.model.viewmodel;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Object to return as body in JWT Authentication.
 */
/**
 * Created by awattez on 02/10/2017.
 */
public class JWTTokenViewModel {

    private String token;

    public JWTTokenViewModel(String token) {
        this.token = token;
    }

    @JsonProperty("token")
    String getToken() {
        return token;
    }

    void setToken(String Token) {
        this.token = Token;
    }
}
