package fr.nemolovich.apps.codeplatform.api.webapi;

import fr.nemolovich.apps.codeplatform.api.model.Exercice;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.ExerciceViewModel;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.ResultViewModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.Set;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Exercice API
 */
@Api(value = "Exercice API", description = "The Exercice API",
     tags = {"exercice"})
public interface ExerciceApi {

    @ApiOperation(value =
                  "Create a new exercice or add an existing exercice for the challenge",
                  response = Exercice.class, tags = {"admin", "challenge",
                                                     "exercice",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Creation Succeed",
                     response = Exercice.class),
        @ApiResponse(code = 201, message = "Creation Succeed",
                     response = Exercice.class),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 304, message = "Not Modified",
                     response = Exercice.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/admin/challenge/{challengeId}/exercice",
                    produces = {"application/json"},
                    consumes = {"application/json"},
                    method = RequestMethod.POST)
    ResponseEntity<Exercice> addExercice(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "The exercice to create", required = true)
        @Valid @RequestBody Exercice exercice,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "delete an exercice for the challenge",
                  notes = "", response = Exercice.class, tags = {"admin",
                                                                 "challenge",
                                                                 "exercice",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Deletion Succeed",
                     response = Exercice.class),
        @ApiResponse(code = 204, message = "No Content"),
        @ApiResponse(code = 304, message = "Not Modified",
                     response = Exercice.class),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value =
                    "/api/admin/challenge/{challengeId}/exercice/{exerciceId}",
                    produces = {"application/json"},
                    method = RequestMethod.DELETE)
    ResponseEntity<Exercice> deleteExercice(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the exercice to delete", required = true)
        @PathVariable("exerciceId") String exerciceId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Get all the exercices for the given challenge",
                  notes = "", response = Set.class, responseContainer = "Set",
                  tags = {"challenge", "exercice",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The requested exercices",
                     response = Set.class, responseContainer = "Set"),
        @ApiResponse(code = 204, message = "No Content"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/challenge/{challengeId}/exercice",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    ResponseEntity<Set<ExerciceViewModel>> getAllExercicesByChallengeId(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(nickname = "getResultsByChallengeId",
                  value = "Explore the results for the given exercice",
                  notes = "", response = ResultViewModel.class,
                  responseContainer = "Set",
                  tags = {"challenge", "exercice", "result",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The requested results",
                     response = ResultViewModel.class, responseContainer = "Set"),
        @ApiResponse(code = 204, message = "No Content"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value =
                    "/api/challenge/{challengeId}/exercice/{exerciceId}/result",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    ResponseEntity<Set<ResultViewModel>> getResultsByExerciceId(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the requested exercice", required = true)
        @PathVariable("exerciceId") String exerciceId,
        @ApiParam(value = "ID of the team to get results")
        @Valid @RequestParam(value = "teamName", required = false) String teamName,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Update an exercice for the challenge", notes = "",
                  response = Exercice.class, tags = {"admin", "challenge",
                                                     "exercice",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The updated exercice",
                     response = Exercice.class),
        @ApiResponse(code = 304, message = "Not Modified",
                     response = Exercice.class),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/admin/challenge/{challengeId}/exercice",
                    produces = {"application/json"},
                    consumes = {"application/json"},
                    method = RequestMethod.PUT)
    ResponseEntity<Exercice> updateExercice(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "the exercice to update", required = true)
        @Valid @RequestBody Exercice exercice,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(nickname = "getExerciceById",
                  value = "Get an exercice for a challenge by ID", notes = "",
                  response = Exercice.class, tags = {"challenge", "exercice",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The requested exercice",
                     response = Exercice.class),
        @ApiResponse(code = 204, message = "No Content"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/challenge/{challengeId}/exercice/{exerciceId}",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    ResponseEntity<Exercice> getExerciceById(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the requested exercice", required = true)
        @PathVariable("exerciceId") String exerciceId,
        @RequestParam(value = "fullMode", required = false) Boolean fullMode,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "List available languages", notes = "",
                  response = Set.class, tags = {"exercice",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The list of avalable languages",
                     response = Set.class),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/languages",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    ResponseEntity<Set> listLanguages(
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

}
