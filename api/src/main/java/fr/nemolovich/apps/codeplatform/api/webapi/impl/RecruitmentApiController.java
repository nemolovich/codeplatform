package fr.nemolovich.apps.codeplatform.api.webapi.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.nemolovich.apps.codeplatform.api.model.Applicant;
import fr.nemolovich.apps.codeplatform.api.model.Challenge;
import fr.nemolovich.apps.codeplatform.api.model.CodeFile;
import fr.nemolovich.apps.codeplatform.api.model.Exercice;
import fr.nemolovich.apps.codeplatform.api.model.Result;
import fr.nemolovich.apps.codeplatform.api.model.Submission;
import fr.nemolovich.apps.codeplatform.api.model.Team;
import fr.nemolovich.apps.codeplatform.api.model.TestCase;
import fr.nemolovich.apps.codeplatform.api.model.User;
import fr.nemolovich.apps.codeplatform.api.model.enums.ChallengeScope;
import fr.nemolovich.apps.codeplatform.api.model.enums.DifficultyLevel;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.ApplicantResult;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.ApplicantViewModel;
import fr.nemolovich.apps.codeplatform.api.security.Roles;
import fr.nemolovich.apps.codeplatform.api.service.ApplicantService;
import fr.nemolovich.apps.codeplatform.api.service.ChallengeService;
import fr.nemolovich.apps.codeplatform.api.service.ResultService;
import fr.nemolovich.apps.codeplatform.api.service.TeamService;
import fr.nemolovich.apps.codeplatform.api.service.UserService;
import fr.nemolovich.apps.codeplatform.api.webapi.RecruitmentApi;
import fr.nemolovich.apps.codeplatform.manager.docker.DockerHelper;
import fr.nemolovich.apps.codeplatform.manager.docker.Manager;
import fr.nemolovich.apps.codeplatform.pdf.CustomResourceLoader;
import fr.nemolovich.apps.codeplatform.pdf.PDFUtils;
import fr.nemolovich.apps.codeplatform.pdf.html.CustomAttributeProvider;
import fr.nemolovich.apps.codeplatform.pdf.model.ApplicantData;
import fr.nemolovich.apps.codeplatform.pdf.model.CodeFileData;
import fr.nemolovich.apps.codeplatform.pdf.model.CodeLineData;
import fr.nemolovich.apps.codeplatform.pdf.model.ExerciceData;
import io.swagger.annotations.ApiParam;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.validation.Valid;
import javax.xml.transform.TransformerException;
import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FopFactory;
import org.apache.velocity.runtime.RuntimeConstants;
import org.commonmark.Extension;
import org.commonmark.ext.gfm.tables.TablesExtension;
import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.AttributeProviderContext;
import org.commonmark.renderer.html.HtmlRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author bgohier
 */
@Controller
public class RecruitmentApiController implements RecruitmentApi {
    
    private final ObjectMapper objectMapper;
    private final ApplicantService applicantService;
    private final ChallengeService challengeService;
    private final ResultService resultService;
    private final TeamService teamService;
    private final UserService userService;
    private static final ThreadLocalRandom RANDOM = ThreadLocalRandom.current();
    private static final Logger LOGGER = LoggerFactory.getLogger(
                                ChallengeApiController.class);
    private static final DateTimeFormatter FILE_DATE_FORMATTER =
                                           DateTimeFormatter.ofPattern(
                                               "yyyyMMdd")
                                           .withLocale(Locale.getDefault())
                                           .withZone(ZoneId.systemDefault());
    private static final DateTimeFormatter PDF_DATE_FORMATTER =
                                           DateTimeFormatter.ofPattern(
                                               "yyyy-MM-dd")
                                           .withLocale(Locale.getDefault())
                                           .withZone(ZoneId.systemDefault());
    private static final String DEFAULT_TEMPLATE_PATH = "template.html";
    private static final String DEFAULT_XSL_PATH = "convert.xsl";
    private static final String XSL_PATH_PROPERTY = "pdf.xsl.path";
    private static final String TEMPLATE_PATH_PROPERTY = "pdf.template.path";
    private final String templatePath;
    private final String xslPath;
    private static final FopFactory FOP_FACTORY = FopFactory.newInstance(
                                    new File(PDFUtils.PDF_BASE_PATH).toURI());
    private static final List<Extension> MARKDOWN_EXTENSION = Arrays.asList(
                                         TablesExtension.create());
    private static final Parser MARKDOWN_PARSER = Parser.builder()
                                .extensions(MARKDOWN_EXTENSION).build();
    private static final HtmlRenderer MARKDOWN_RENDERER = HtmlRenderer.builder()
                                      .attributeProviderFactory((
                                          AttributeProviderContext context) ->
                                          new CustomAttributeProvider(context))
                                      .extensions(MARKDOWN_EXTENSION).build();
    
    public RecruitmentApiController(ObjectMapper objectMapper,
                                    ApplicantService applicantService,
                                    ChallengeService challengeService,
                                    ResultService resultService,
                                    UserService exerciceService,
                                    TeamService teamService) {
        this.objectMapper = objectMapper;
        this.applicantService = applicantService;
        this.challengeService = challengeService;
        this.resultService = resultService;
        this.userService = exerciceService;
        this.teamService = teamService;
        
        String templateTmpPath = System.getProperty(TEMPLATE_PATH_PROPERTY);
        String xslTmpPath = System.getProperty(XSL_PATH_PROPERTY);
        if (templateTmpPath == null) {
            templateTmpPath = System.getenv(TEMPLATE_PATH_PROPERTY);
        }
        if (xslTmpPath == null) {
            xslTmpPath = System.getenv(XSL_PATH_PROPERTY);
        }
        if (templateTmpPath == null) {
            templateTmpPath = String.format("%s/%s", PDFUtils.PDF_BASE_PATH,
                                            DEFAULT_TEMPLATE_PATH);
        }
        if (xslTmpPath == null) {
            xslTmpPath = String.format("%s/%s", PDFUtils.PDF_BASE_PATH,
                                       DEFAULT_XSL_PATH);
        }
        
        templatePath = templateTmpPath;
        xslPath = xslTmpPath;
        
        File pdfBasePath = new File(PDFUtils.PDF_BASE_PATH);
        File templateFile = new File(templatePath);
        File xslFile = new File(xslPath);
        File xslConvertFile = new File(String.format("%s/%s",
                                                     PDFUtils.PDF_BASE_PATH,
                                                     PDFUtils.XHTML2FOXSL_XSL_FILE));
        File jsFile = new File(String.format("%s/init.js",
                                             PDFUtils.PDF_BASE_PATH));
        File logoFie = new File(String.format("%s/logo.png",
                                              PDFUtils.PDF_BASE_PATH));
        File titanLogoFie = new File(String.
             format("%s/titan.png", PDFUtils.PDF_BASE_PATH));
        
        pdfBasePath.mkdirs();
        
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        
        try {
            if (!templateFile.exists()) {
                Files.copy(classLoader.getResourceAsStream("pdf/template.html"),
                           templateFile.toPath(),
                           StandardCopyOption.REPLACE_EXISTING);
            }
            if (!xslFile.exists()) {
                Files.copy(classLoader.getResourceAsStream("pdf/convert.xsl"),
                           xslFile.toPath(),
                           StandardCopyOption.REPLACE_EXISTING);
            }
            if (!xslConvertFile.exists()) {
                Files.copy(classLoader.getResourceAsStream("pdf/xhtml2fo.xsl"),
                           xslConvertFile.toPath(),
                           StandardCopyOption.REPLACE_EXISTING);
            }
            if (!jsFile.exists()) {
                Files.copy(classLoader.getResourceAsStream("pdf/xhtml2fo.xsl"),
                           jsFile.toPath(),
                           StandardCopyOption.REPLACE_EXISTING);
            }
            if (!logoFie.exists()) {
                Files.copy(classLoader.getResourceAsStream("pdf/logo.png"),
                           logoFie.toPath(),
                           StandardCopyOption.REPLACE_EXISTING);
            }
            Files.copy(classLoader.getResourceAsStream("pdf/titan.png"),
                       titanLogoFie.toPath(),
                       StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(RecruitmentApiController.class.
                getName()).
                log(Level.SEVERE, null, ex);
        }
        
    }
    
    @Override
    public ResponseEntity<ApplicantResult> createApplicant(
        @ApiParam(value = "Details about an applicant", required = true)
        @Valid @RequestBody Applicant applicant,
        @RequestHeader(value = "Accept", required = false) String accept) throws
        Exception {
        LOGGER.debug("createApplicant Rest");
        if (accept != null && accept.contains("application/json")) {
            User user = this.userService.getUserByLogin(
                 applicant.getRecruiter().getLogin());
            if (user == null || user.getRoles().stream()
                .filter(r -> r.getName().equals(Roles.RECRUITER.name()) ||
                               r.getName().equals(Roles.ADMIN.name()))
                .findAny().orElse(null) != null) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
            applicant.setRecruiter(user);
            applicant = this.applicantService.createApplicant(applicant);
            
            DifficultyLevel difficulty = applicant.getDifficulty();
            Challenge c = this.challengeService.getChallengeById("recruitment");
            if (c == null) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            List<Exercice> exercices = c.getExercices().stream().filter(e ->
                           e.getCoeff().compareTo(difficulty.getMinCoeff()) >= 0 &&
                           e.getCoeff().compareTo(difficulty.getMaxCoeff()) < 0).
                           collect(Collectors.toList());
            if (exercices.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            Exercice e = exercices.get(RANDOM.nextInt(exercices.size()));
            
            Instant startDate = Instant.now();
            Instant endDate = Instant.now().plus(applicant.getDuration(),
                                                 ChronoUnit.MINUTES);
            Team team = new Team()
                 .name(String.format("team-%s", applicant.getId()))
                 .creator(user).addMemberItem(user);
            team = this.teamService.saveTeam(team);
            Challenge challenge = new Challenge()
                      .name(String.format("challenge-%s", applicant.getId()))
                      .description(String.format(
                          "Applicant test for applicant %s %s",
                          applicant.getFirstName(),
                          applicant.getLastName()))
                      .startDate(startDate)
                      .endDate(endDate)
                      .scope(ChallengeScope.RECRUITMENT)
                      .addExerciceItem(e)
                      .addTeamItem(team);
            challenge = this.challengeService.createChallenge(challenge);
            if (challenge == null) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            applicant.setChallenge(challenge);
            applicant = this.applicantService.updateApplicant(applicant);
            if (applicant == null) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(new ApplicantResult(applicant),
                                        HttpStatus.OK);
        }
        return ResponseEntity.badRequest().body(null);
    }
    
    @Override
    public ResponseEntity<ApplicantViewModel> updateApplicant(
        @ApiParam(value = "Applicant to update", required = true)
        @Valid @RequestBody Applicant applicant,
        @RequestHeader(value = "Accept", required = false) String accept) throws
        Exception {
        LOGGER.debug("createApplicant Rest");
        if (accept != null && accept.contains("application/json")) {
            if (!this.applicantService.applicantExists(applicant.getId())) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            Applicant newApplicant = this.applicantService.getApplicantById(
                      applicant.getId());
            newApplicant.copy(applicant);
            if (applicant.getChallenge() != null) {
                Challenge challenge = this.challengeService.getChallengeById(
                          applicant.getChallenge().getId());
                newApplicant.setChallenge(challenge);
            }
            User recruiter = this.userService.getUserByLogin(newApplicant.
                 getRecruiter().getLogin());
            newApplicant.setRecruiter(recruiter);
            newApplicant = this.applicantService.updateApplicant(newApplicant);
            if (newApplicant == null) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(new ApplicantViewModel(newApplicant),
                                        HttpStatus.OK);
        }
        return ResponseEntity.badRequest().body(null);
    }
    
    @Override
    public ResponseEntity<ApplicantViewModel> deleteApplicant(
        @ApiParam(value = "ID of the applicant to delete", required = true)
        @PathVariable("applicantId") String applicantId,
        @RequestHeader(value = "Accept", required = false) String accept) throws
        Exception {
        LOGGER.debug("createApplicant Rest");
        if (accept != null && accept.contains("application/json")) {
            Applicant applicant = this.applicantService.deleteApplicant(
                      applicantId);
            if (applicant != null) {
                if (applicant.getChallenge() != null) {
                    Challenge challenge = this.challengeService.deleteChallenge(
                              applicant.getChallenge()
                              .getId());
                    challenge.getTeams().stream()
                        .forEach(t -> this.teamService.deleteTeam(t.getId()));
                    challenge.getResults().stream()
                        .forEach(r -> this.resultService.deleteResult(r, true));
                }
                return new ResponseEntity<>(new ApplicantViewModel(applicant),
                                            HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }
        return ResponseEntity.badRequest().body(null);
    }
    
    @Override
    public ResponseEntity<ApplicantViewModel> startTest(
        @ApiParam(value = "Details about the applicant", required = true)
        @Valid @RequestBody ApplicantViewModel applicant,
        @RequestHeader(value = "Accept", required = false) String accept) throws
        Exception {
        LOGGER.debug("startTest Rest");
        if (accept != null && accept.contains("application/json")) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
        return ResponseEntity.badRequest().body(null);
    }
    
    @Override
    public ResponseEntity<Set<ApplicantViewModel>> getAllApplicants(
        @ApiParam(value = "Filter:\nnull for all applicants\n" +
                          "'owned' => current recruiter applicants\n" +
                          "'accepted' => applicants accepted")
        @Valid @RequestParam(value = "filter", required = false) String filter,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("getAllApplicants Rest");
        if (accept != null && accept.contains("application/json")) {
            Set<ApplicantViewModel> applicants;
            Set<Applicant> allApplicants = applicantService.getAllApplicants();
            final User user = userService.getCurrentUser().orElse(null);
            if ("owned".equalsIgnoreCase(filter)) {
                if (user == null) {
                    applicants = new HashSet<>();
                } else {
                    applicants = allApplicants.stream()
                    .filter(a -> a.getRecruiter().equals(user))
                    .map(a -> new ApplicantViewModel(a))
                    .collect(Collectors.toSet());
                }
            } else if ("accepted".equalsIgnoreCase(filter)) {
                applicants = allApplicants.stream()
                .filter(a -> Boolean.TRUE.equals(a.getAccepted()))
                .map(a -> new ApplicantViewModel(a))
                .collect(Collectors.toSet());
            } else if (filter != null) {
                applicants = allApplicants.stream()
                .filter(a -> a.getLastName().equalsIgnoreCase(filter) ||
                               a.getFirstName().equalsIgnoreCase(filter) ||
                               a.getId().equalsIgnoreCase(filter) ||
                               a.getLanguage().name().equalsIgnoreCase(filter))
                .map(a -> new ApplicantViewModel(a))
                .collect(Collectors.toSet());
            } else {
                applicants = allApplicants.stream()
                .map(a -> new ApplicantViewModel(a))
                .collect(Collectors.toSet());
            }
            return ResponseEntity.ok(applicants);
        }
        return ResponseEntity.badRequest().body(null);
    }
    
    @Override
    public ResponseEntity<ApplicantResult> getApplicantByID(
        @ApiParam(value = "ID of the requested applicant", required = true)
        @PathVariable("applicantId") String applicantId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("getApplicantByID Rest");
        if (accept != null && accept.contains("application/json") &&
            applicantId != null) {
            Applicant applicant = applicantService.getApplicantById(
                      applicantId);
            if (applicant == null) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
            } else {
                return ResponseEntity.ok(new ApplicantResult(applicant));
            }
        }
        return ResponseEntity.badRequest().body(null);
    }
    
    @Override
    public ResponseEntity<ApplicantResult> getApplicantByChallengeID(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("getApplicantByChallengeID Rest");
        if (accept != null && accept.contains("application/json") &&
            challengeId != null) {
            Applicant applicant = applicantService.getApplicantByChallengeId(
                      challengeId);
            if (applicant == null) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
            } else {
                return ResponseEntity.ok(new ApplicantResult(applicant));
            }
        }
        return ResponseEntity.badRequest().body(null);
    }
    
    @Override
    public ResponseEntity<byte[]> getApplicantResultDetails(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("getApplicantResultDetails Rest");
        if (accept != null && accept.contains("application/zip") &&
            challengeId != null) {
            Applicant applicant = applicantService.getApplicantByChallengeId(
                      challengeId);
            if (applicant == null) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
            } else {
                
                Challenge challenge = applicant.getChallenge();
                Result result = challenge.getResults().isEmpty() ?
                                new Result().addSubmissionItem(new Submission()
                                    .score(BigDecimal.ZERO)
                                    .addFileItem(new CodeFile()
                                        .content("".getBytes()))) :
                                challenge.getResults().iterator().next();
                Submission submission = result.getSubmissions().iterator()
                           .next();
                Exercice exercice = challenge.getExercices().iterator().next();
                Map<String, String> ioPaths = Manager.getIOPaths(exercice
                                    .getId());
                File zipFile = new File(
                     String.format("%s/applicant-%s_%s.zip",
                                   SubmissionApiController.TMP_CODE_PATH,
                                   applicant.getId(),
                                   FILE_DATE_FORMATTER.format(
                                       applicant.getChallenge().getStartDate()))
                 );
                
                byte[] fileContent = new byte[0];
                if (zipFile.exists()) {
                    fileContent = Files.readAllBytes(zipFile.toPath());
                }
                if (!zipFile.exists() || fileContent.length < 1) {
                    
                    try (ZipOutputStream zip = new ZipOutputStream(
                                         new BufferedOutputStream(
                                             new FileOutputStream(zipFile)))) {
                        
                        zip.putNextEntry(new ZipEntry("result.pdf"));
                        byte[] pdfContent = getPDFResultsFile(
                               buildApplicantData(applicant, submission,
                                                  exercice),
                               templatePath, xslPath);
                        zip.write(pdfContent);
                        zip.closeEntry();
                        for (CodeFile file : submission.getFiles()) {
                            zip.putNextEntry(new ZipEntry(
                                String.format("code/%s", file.getName())));
                            zip.write(file.getContent());
                            zip.closeEntry();
                        }
                        String testCaseName;
                        File inputTestFile;
                        File outputTestFile;
                        for (TestCase test : exercice.getTestCases()) {
                            testCaseName = test.getName();
                            
                            inputTestFile = new File(
                            String.format("%s/%s.in",
                                          ioPaths.get(DockerHelper.ATTR_INPUTS),
                                          testCaseName));
                            outputTestFile = new File(
                            String.format("%s/%s.out",
                                          ioPaths.get(DockerHelper.ATTR_OUTPUTS),
                                          testCaseName));
                            if (!inputTestFile.exists() || !outputTestFile.
                                exists()) {
                                LOGGER.
                                    error(
                                        "Cannot access to test case '{}' files",
                                        testCaseName);
                                continue;
                            }
                            
                            zip.putNextEntry(new ZipEntry(String.format(
                                "test_cases/in/%s/%s.in",
                                test.getHidden() ? "private" : "public",
                                testCaseName)));
                            zip.
                                write(Files.readAllBytes(inputTestFile.toPath()));
                            zip.closeEntry();
                            
                            zip.putNextEntry(new ZipEntry(String.format(
                                "test_cases/out/%s/%s.out",
                                test.getHidden() ? "private" : "public",
                                testCaseName)));
                            zip.write(Files.
                                readAllBytes(outputTestFile.toPath()));
                            zip.closeEntry();
                        }
                        
                        zip.flush();
                        zip.close();
                    } catch (IOException ioe) {
                        LOGGER.error(String.
                            format("Cannot create zip file '%s'",
                                   zipFile.getAbsolutePath()), ioe);
                        return ResponseEntity.badRequest().body(null);
                    }
                    fileContent = Files.readAllBytes(zipFile.toPath());
                }
                // zipFile.delete();
                return ResponseEntity.ok(fileContent);
            }
        }
        return ResponseEntity.badRequest().body(null);
    }
    
    private static Map<String, Object> buildApplicantData(Applicant applicant,
                                                          Submission submission,
                                                          Exercice exercice) {
        
        Map<String, Object> data =
                            new ConcurrentHashMap<>();
        
        String date = PDF_DATE_FORMATTER.format(applicant.getChallenge().
               getStartDate());
        
        String score = String.format("%.02f%%", submission.getScore().multiply(
                                     BigDecimal.valueOf(100)));
        
        final AtomicInteger number = new AtomicInteger(1);
        Function<String, CodeLineData> buildCodeLineData;
        buildCodeLineData = l ->
        {
            CodeLineData cld = new CodeLineData()
                         .number(number.get())
                         .content(l);
            number.getAndIncrement();
            return cld;
        };
        Function<CodeFile, CodeFileData> buildCodeFileData;
        buildCodeFileData = cf ->
        {
            number.set(1);
            String codeContent = new String(cf.getContent());
            CodeFileData cfd = new CodeFileData()
                         .name(cf.getName())
                         .lines(Arrays.asList(codeContent.split("\\n")).stream()
                             .map(buildCodeLineData)
                             .collect(Collectors.toList()));
            return cfd;
        };
        
        List<CodeFileData> codeFiles = submission.getFiles().stream()
                           .map(buildCodeFileData).collect(Collectors.toList());
        User recruiter = applicant.getRecruiter();
        Node document = MARKDOWN_PARSER.parse(exercice.getText());
        String descHtml = MARKDOWN_RENDERER.render(document);
        ExerciceData exerciceData = new ExerciceData()
                     .name(exercice.getTitle())
                     .description(exercice.getSubtitle())
                     .instructions(descHtml);
        ApplicantData user = new ApplicantData()
                      .name(applicant.getLastName())
                      .firstName(applicant.getFirstName())
                      .mail(applicant.getEmail())
                      .language(applicant.getLanguage().name())
                      .score(score)
                      .recruiterFullName(String.format(
                          "%s %s",
                          recruiter.getFirstName(),
                          recruiter.getLastName()));
        
        data.put("user", user);
        data.put("footer", String.format(
                 "Test results - %s", date));
        data.put("date", date);
        data.put("exercice", exerciceData);
        String company = "Capgemini";
        String location = "Nantes";
        
        data.put(
            "header_message", String.format(
                "This is the individual result of the candidate %s %s during\n" +
                "the interview of %s conducted by recruiter %s at\n" +
                "%s's office in %s.",
                user.getFirstName(), user.getName(), date,
                user.getRecruiterFullName(), company,
                location
            ));
        
        data.put("codeFiles", codeFiles);
        return data;
    }
    
    private static byte[] getPDFResultsFile(Map<String, Object> data,
                                            String templatePath,
                                            String xslPath) throws
        IOException, TransformerException, FOPException, URISyntaxException {
        
        try (ByteArrayOutputStream pdfOutput = new ByteArrayOutputStream()) {
            
            StringWriter htmlWriter = new StringWriter();
            
            Properties props = new Properties();
            props.put(RuntimeConstants.RESOURCE_LOADER, "custom");
            props.put("custom.resource.loader.class",
                      CustomResourceLoader.class.getName());
            
            PDFUtils.applyTemplate(templatePath, data, htmlWriter, props);
            
            StringWriter xhtmlWriter = new StringWriter();
            
            PDFUtils.htmlToXhtml(
                new StringReader(htmlWriter.toString()),
                xhtmlWriter, true);
            
            Writer foWriter = new StringWriter();
            
            InputStream xslIn = null;
            File xslFile = new File(xslPath);
            if (xslFile.exists()) {
                xslIn = new FileInputStream(xslFile);
            }
            if (xslIn == null) {
                xslIn = Thread.currentThread().getContextClassLoader().
                getResourceAsStream(xslPath);
            }
            if (xslIn == null) {
                xslIn = RecruitmentApiController.class.getResourceAsStream(
                xslPath);
            }
            PDFUtils.xhtmlToXslFO(xslIn, new StringReader(
                                  xhtmlWriter.toString()), foWriter);
            
            PDFUtils.
                xslFOtoPDF(FOP_FACTORY, new StringReader(foWriter.toString()),
                           pdfOutput);
            
            return pdfOutput.toByteArray();
        }
    }
}
