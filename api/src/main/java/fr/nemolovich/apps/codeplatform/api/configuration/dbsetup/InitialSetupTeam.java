package fr.nemolovich.apps.codeplatform.api.configuration.dbsetup;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;

import fr.nemolovich.apps.codeplatform.api.model.Team;
import fr.nemolovich.apps.codeplatform.api.model.User;

import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;

/**
 * Creates the initial database setup for user
 */
@ChangeLog(order = "002")
@Profile(value = "dev")
public class InitialSetupTeam {

    @ChangeSet(order = "01", author = "initiator", id = "01-addTeams")
    public void addTeams(MongoTemplate mongoTemplate) {
        
        User userS = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'user-id-system' }"), User.class);
        User userA = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'user-id-admin' }"), User.class);
        User user2 = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'user-id-2' }"), User.class);
        // Not Activated User
        User user3 = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'user-id-3' }"), User.class);

        Team team0 = new Team();
        team0.setId("team-0");
        team0.setName("team-0");
        team0.setCreator(userS);
        team0.addMemberItem(userA);
        team0.addMemberItem(user2);
        mongoTemplate.save(team0);

        Team team1 = new Team();
        team1.setId("team-1");
        team1.setName("team-1");
        team1.setCreator(userA);
        team1.addMemberItem(user2);
        mongoTemplate.save(team1);

        Team team2 = new Team();
        team2.setId("team-2");
        team2.setName("team-2");
        team2.setCreator(user2);
        team2.addMemberItem(userS);
        mongoTemplate.save(team2);

        Team team3 = new Team();
        team3.setId("team-3");
        team3.setName("team-3-ano");
        team3.setCreator(userS);
        team3.addMemberItem(userS);
        team3.addMemberItem(user3);
        mongoTemplate.save(team3);

        Team team4 = new Team();
        team4.setId("team-4");
        team4.setName("team-4-ano");
        // Creator is not activated
        // @TODO what we have to do
        team4.setCreator(user3);
        team4.addMemberItem(user2);
        mongoTemplate.save(team4);

    }

}
