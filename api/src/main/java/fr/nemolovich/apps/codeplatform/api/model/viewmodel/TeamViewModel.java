package fr.nemolovich.apps.codeplatform.api.model.viewmodel;

import com.fasterxml.jackson.annotation.JsonProperty;

import fr.nemolovich.apps.codeplatform.api.model.Team;
import io.swagger.annotations.ApiModelProperty;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

/**
 * Created by awattez on 15/10/2017.
 */
@Validated
public class TeamViewModel {

    @JsonProperty("id")
    private String id = null;

    @JsonProperty("name")
    @NotNull
    private String name = null;

    @JsonProperty("creator")
    @NotNull
    private UserViewModel creator = null;

    @JsonProperty("members")
    @Valid
    private Set<UserViewModel> members = new HashSet<>();

    public TeamViewModel() {
        this.name = "";
        this.creator = new UserViewModel();
        this.members = new HashSet<>();
    }

    public TeamViewModel(Team team) {
        this(team.getId(), team.getName(), new UserViewModel(team.getCreator()),
            team.getMembers().stream().map(UserViewModel::new).collect(
                Collectors.toSet()));
    }

    public TeamViewModel(String id, String name, UserViewModel creator, 
        Set<UserViewModel> members) {
        this.id = id;
        this.name = name;
        this.creator = creator;
        this.members = members;
    }

    public TeamViewModel name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     *
     */
    @ApiModelProperty(value = "")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TeamViewModel creator(UserViewModel creator) {
        this.creator = creator;
        return this;
    }

    public TeamViewModel name(UserViewModel creator) {
        this.creator = creator;
        return this;
    }

    /**
     * Get creator
     *
     * @return creator
     *
     */
    @ApiModelProperty(value = "")
    @NotNull
    @Valid
    public UserViewModel getCreator() {
        return creator;
    }

    public void setCreator(UserViewModel creator) {
        this.creator = creator;
    }

    public TeamViewModel members(Set<UserViewModel> members) {
        this.members = members;
        return this;
    }

    public TeamViewModel addMembersItem(UserViewModel membersItem) {
        if (this.members == null) {
            this.members = new HashSet<>();
        }
        this.members.add(membersItem);
        return this;
    }

    /**
     * Get members
     *
     * @return members
     *
     */
    @ApiModelProperty(value = "")
    @Valid
    public Set<UserViewModel> getMembers() {
        return members;
    }

    public void setMembers(Set<UserViewModel> members) {
        this.members = members;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TeamViewModel teamViewModel = (TeamViewModel) o;
        return Objects.equals(this.name, teamViewModel.name)
            && Objects.equals(this.creator, teamViewModel.creator)
            && Objects.equals(this.members, teamViewModel.members);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, creator, members);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class TeamViewModel {\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    creator: ").append(toIndentedString(creator))
            .append("\n");
        sb.append("    members: ").append(toIndentedString(members))
            .append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     * 
     * @param o {@link Object}: Object to format.
     * @return {@link String}: String representation of object.
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
