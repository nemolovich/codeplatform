/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.nemolovich.apps.codeplatform.api.webapi;

import fr.nemolovich.apps.codeplatform.api.model.SubmissionResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 * Exercice API
 *
 * @author bgohier
 */
@Api(value = "Submission API", description = "The Submission API",
     tags = {"submission"})
public interface SubmissionApi {

    @ApiOperation(nickname = "updateTestTimeAsync",
                  value = "Update the endDate of a result", notes = "",
                  response = Object.class, tags = {"submission",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The response",
                     response = Object.class),
        @ApiResponse(code = 204, message = "No Content"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/updateTestTimeAsync/{resultId}/{language}",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    ResponseEntity<Object> updateTestTimeAsync(
        @ApiParam(value = "ID of the result to update", required = true)
        @PathVariable("resultId") String resultId,
        @PathVariable("language") String language,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Submit code", notes = "",
                  response = Float.class, tags = {"submission",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The submission score",
                     response = Float.class),
        @ApiResponse(code = 204, message = "No Content",
                     response = Object.class),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/submission",
                    produces = {"application/json"},
                    consumes = {"multipart/form-data"},
                    method = RequestMethod.POST)
    ResponseEntity<Object> submitCode(
        @ApiParam(value = "Code submission to evaluate", required = true)
        @RequestParam(value = "submission",
                      required = true) String submission,
        @ApiParam(value = "Files to upload") @Valid
        @RequestParam("files") MultipartFile[] files,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Test code", notes = "",
                  response = List.class, tags = {"submission",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The evaluated submission",
                     response = List.class),
        @ApiResponse(code = 204, message = "No Content",
                     response = Object.class),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/testcode",
                    produces = {"application/json"},
                    consumes = {"multipart/form-data"},
                    method = RequestMethod.POST)
    ResponseEntity<List<SubmissionResponse>> testCode(
        @ApiParam(value = "Code submission to evaluate", required = true)
        @RequestParam(value = "submission",
                      required = true) String submission,
        @ApiParam(value = "Files to upload") @Valid
        @RequestParam("files") MultipartFile[] files,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;
}
