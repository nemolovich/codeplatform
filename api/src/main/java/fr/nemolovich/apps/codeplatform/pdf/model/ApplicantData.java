package fr.nemolovich.apps.codeplatform.pdf.model;

/**
 *
 * @author bgohier
 */
public class ApplicantData {

    private String name;
    private String firstName;
    private String mail;
    private String language;
    private String score;
    private String recruiterFullName;

    public ApplicantData() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ApplicantData name(String name) {
        this.name = name;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public ApplicantData firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public ApplicantData mail(String mail) {
        this.mail = mail;
        return this;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public ApplicantData language(String language) {
        this.language = language;
        return this;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public ApplicantData score(String score) {
        this.score = score;
        return this;
    }

    public String getRecruiterFullName() {
        return recruiterFullName;
    }

    public void setRecruiterFullName(String recruiterFullName) {
        this.recruiterFullName = recruiterFullName;
    }

    public ApplicantData recruiterFullName(String recruiterFullName) {
        this.recruiterFullName = recruiterFullName;
        return this;
    }

}
