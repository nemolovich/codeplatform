package fr.nemolovich.apps.codeplatform.pdf.model;

/**
 *
 * @author bgohier
 */
public class ExerciceData {
    private String name;
    private String description;
    private String instructions;

    public ExerciceData() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ExerciceData name(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ExerciceData description(String description) {
        this.description = description;
        return this;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public ExerciceData instructions(String instructions) {
        this.instructions = instructions;
        return this;
    }
    
    
}
