package fr.nemolovich.apps.codeplatform.api.configuration.dbsetup;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import fr.nemolovich.apps.codeplatform.api.model.Challenge;
import fr.nemolovich.apps.codeplatform.api.model.Exercice;
import fr.nemolovich.apps.codeplatform.api.model.TestCase;
import fr.nemolovich.apps.codeplatform.api.model.enums.ExerciceType;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 *
 * @author fbontemp
 */
@ChangeLog(order = "008")
public class InitSetupRecruitmentPhase2 {

    private static final Set<Exercice> EXERCICES = new HashSet<>();

    private static Exercice getExercice(String id) {
        return EXERCICES.stream().filter(e -> e.getId().equalsIgnoreCase(id)).
            findFirst().get();
    }

    @ChangeSet(order = "01", author = "initiator", id = "01-addExercicesPhase2")
    public void addExercices(MongoTemplate mongoTemplate) {

        recruitment8:
            {
                Exercice files = new Exercice();
                files.setId("rec-ex8");
                files.setTitle("Recruitment exercise 8");
                files.setSubtitle("Extract data from files");
                files.setText("# Recruitment exercise 8\n" +
                                 "\n" +
                                 "A client of yours receives market data files from an external provider. \n\n" + 
                                 "The file format is a bit too much for him, and he'd like you to help him get some results out of them. \n\n" + 
                                 "\n" +
                                 "The file format is as follow :\n" +                                 
                                 "\n" +                                 
                                 "```\n" +
                                 "START-OF-FILE\n" + 
                                 "...\n" + 
                                 "START-OF-FIELDS\n" + 
                                 "Field1Name\n" + 
                                 "Field2Name\n" + 
                                 "Field3Name\n" + 
                                 "Field4Name\n" + 
                                 "END-OF-FIELDS\n" + 
                                 "START-OF-DATA\n" + 
                                 "Field1Value|Field2Value|Field3Value|Field4Value\n" + 
                                 "Field1Value|Field2Value|Field3Value|Field4Value\n" + 
                                 "Field1Value|Field2Value|Field3Value|Field4Value\n" + 
                                 "Field1Value|Field2Value|Field3Value|Field4Value\n" + 
                                 "...\n" + 
                                 "END-OF-DATA\n" + 
                                 "END-OF-FILE\n" + 
                                 "```\n" +
                                 "Field values are pivoted relatively to their names.\n\n" + 
                                 "The TICKER field is **always** available in the files. It's the unique identifier of a quoted stock.\n\n" + 
                                 "There's always a date, as those files are historical market data. The remaining fields are numerical values.\n\n" + 
                                 "\n" +
                                 "He needs you to write a program that takes the following inputs :\n\n" + 
                                 " * The file as described above \n" +
                                 " * **TICKER** : a ticker \n" + 
                                 " * **FIELD** : the name of a field to apply a COMPUTATION to\n" + 
                                 " * **COMPUTATION** : the type of computation to apply to the given FIELD\n" + 
                                 "  * MAX : the maximum value \n" + 
                                 "  * MIN : the minimum value \n" + 
                                 "  * AVG : the average of the values \n" + 
                                 "  * SUM : the sum of the values \n" + 
                                 "\n" + 
                                 "The result of those computation is a single numerical value (the **whole part** of the mathematical result of the computation). \n" + 
                                 "\n" +                                 
                                 "\n" +
                                 "### Example\n" +
                                 "\n" +
                                 "input \n" +                                
                                 "```\n" +
                                 "START-OF-FILE\n" + 
                                 "START-OF-FIELDS\n" + 
                                 "REFERENCE_DATE\n" + 
                                 "TICKER\n" + 
                                 "VOLUME\n" + 
                                 "END-OF-FIELDS\n" + 
                                 "START-OF-DATA\n" + 
                                 "2018-01-01|LVMH|100,00\n" + 
                                 "2018-01-02|LVMH|18,00\n" + 
                                 "2018-01-03|GOOGL|300,00\n" + 
                                 "2018-01-04|LVMH|82,00\n" + 
                                 "END-OF-DATA\n" + 
                                 "END-OF-FILE\n" +
                                 "LMVH VOLUME SUM \n" +
                                 "```\n" +
                                 "\n" +
                                 " output : 200\n\n" +
                                 "same file but last line = LMVH VOLUME AVG --> output : 66\n\n" +
                                 "same file but last line = LMVH VOLUME MIN --> output : 18\n\n" +
                                 "same file but last line = LMVH VOLUME MAX --> output : 100\n" +
                                 "\n");
                files.setType(ExerciceType.FASTEST);

                StringBuilder sb;
                StringBuilder sb2;
                for (int i = 1; i < 6; i++) {
                    sb = new StringBuilder();
                    sb2 = new StringBuilder();
                    String name = "data" + i;
                    try (InputStream is = InitSetupRecruitmentPhase2.class.
                                     getResourceAsStream(
                                         "/in/rec-ex8/" + name + ".in");
                         BufferedInputStream bis = new BufferedInputStream(is)) {
                        byte[] buffer = new byte[2048];
                        int len;
                        while ((len = bis.read(buffer)) != -1) {
                            sb.append(new String(buffer, 0, len));
                        }
                    } catch (IOException ioe) {
                        Logger.getLogger(InitSetupRecruitmentPhase2.class.getName()).
                            log(Level.SEVERE, "Cannot read input file", ioe);
                    }
                    try (InputStream is = InitSetupRecruitmentPhase2.class.
                                     getResourceAsStream(
                                         "/out/rec-ex8/" + name + ".out");
                         BufferedInputStream bis = new BufferedInputStream(is)) {
                        byte[] buffer = new byte[2048];
                        int len;
                        while ((len = bis.read(buffer)) != -1) {
                            sb2.append(new String(buffer, 0, len));
                        }
                    } catch (IOException ioe) {
                        Logger.getLogger(InitSetupRecruitmentPhase2.class.getName()).
                            log(Level.SEVERE, "Cannot read output file", ioe);
                    }
                    files.addTestCase(new TestCase().id("rec-exercise8-" + i)
                        .name(name).hidden(false).input(sb.toString())
                        .output(sb2.toString()).order(i));
                }
                for (int i = 6; i <= 10; i++) {
                    sb = new StringBuilder();
                    sb2 = new StringBuilder();
                    String name = "data" + i;
                    try (InputStream is = InitSetupRecruitmentPhase2.class.
                                     getResourceAsStream(
                                         "/in/rec-ex8/" + name + ".in");
                         BufferedInputStream bis = new BufferedInputStream(is)) {
                        byte[] buffer = new byte[2048];
                        int len;
                        while ((len = bis.read(buffer)) != -1) {
                            sb.append(new String(buffer, 0, len));
                        }
                    } catch (IOException ioe) {
                        Logger.getLogger(InitSetupRecruitmentPhase2.class.getName()).
                            log(Level.SEVERE, "Cannot read input file", ioe);
                    }
                    try (InputStream is = InitSetupRecruitmentPhase2.class.
                                     getResourceAsStream(
                                         "/out/rec-ex8/" + name + ".out");
                         BufferedInputStream bis = new BufferedInputStream(is)) {
                        byte[] buffer = new byte[2048];
                        int len;
                        while ((len = bis.read(buffer)) != -1) {
                            sb2.append(new String(buffer, 0, len));
                        }
                    } catch (IOException ioe) {
                        Logger.getLogger(InitSetupRecruitmentPhase2.class.getName()).
                            log(Level.SEVERE, "Cannot read output file", ioe);
                    }
                    files.addTestCase(new TestCase().id("rec-exercise8-" + i)
                        .name(name).hidden(true).input(sb.toString())
                        .output(sb2.toString()).order(i));
                }

                files.getTestCases().stream().forEach((testCase) ->
                    mongoTemplate.save(testCase));
                files.setCoeff(BigDecimal.valueOf(0.4));

                mongoTemplate.save(files);

                EXERCICES.add(files);
            }
       

    }

    @ChangeSet(order = "02", author = "initiator",
               id = "02-addExercicesInChallengesPhase2")
    public void addExercicesInChallenges(MongoTemplate mongoTemplate) {

        if (EXERCICES.isEmpty()) {
            EXERCICES.addAll(mongoTemplate.findAll(Exercice.class));
        }

        Challenge recruitment = mongoTemplate.findById("recruitment",
                                                       Challenge.class);
        recruitment.addExerciceItem(getExercice("rec-ex8"));
        mongoTemplate.save(recruitment);
    }
   
}
