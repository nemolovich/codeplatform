package fr.nemolovich.apps.codeplatform.pdf;

import fr.nemolovich.apps.codeplatform.manager.docker.DockerHelper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Properties;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.apache.xmlgraphics.util.MimeConstants;
import org.w3c.dom.Document;
import org.w3c.tidy.Tidy;

/**
 * Some utility classes to convert HTML to XHTML, XHTML to XSL-FO, XSL-FO to
 * PDF.
 *
 * @author bgohier
 */
public final class PDFUtils {

    public static final String PDF_BASE_PATH = String.format(
                               "%s/api/pdf", DockerHelper.CONFIGS_PATH);
    public static final String XHTML2FOXSL_XSL_FILE = "xhtml2fo.xsl";
    private static final TransformerFactory TRANSFORMER_FACTORY =
                                            TransformerFactory.newInstance();
    private static final TransformerFactory PDF_TRANSFORMER_FACTORY =
                                            TransformerFactory.newInstance();

    static {
        TRANSFORMER_FACTORY.setURIResolver((String href, String base) -> {
            InputStream is = null;

            File hrefFile =
                 new File(String.format("%s/%s", PDF_BASE_PATH, href));
            if (hrefFile.exists() && hrefFile.canRead()) {
                try {
                    is = new FileInputStream(hrefFile);
                } catch (FileNotFoundException ex) {
                    throw new UnsupportedOperationException(String.format(
                        "Cannot find XSLT file at %s", href));
                }
            }
            if (is == null) {
                is = Thread.currentThread().getContextClassLoader().
                getResourceAsStream(String.format("%s", href));
            }
            if (is == null) {
                is = ClassLoader.getSystemResourceAsStream(href);
            }
            if (is == null) {
                throw new UnsupportedOperationException(String.format(
                    "Cannot find XSLT file at %s", href));
            }
            return new StreamSource(is);
        });
    }

    private PDFUtils() {
        super();
        // Utility class.
    }

    public static final void applyTemplate(String templateURI,
                                           Map<String, Object> data,
                                           Writer writer) {

        Properties properties = new Properties();
        properties.put(RuntimeConstants.RESOURCE_LOADER, "classpath");
        properties.put("classpath.resource.loader.class",
                       ClasspathResourceLoader.class.getName());
        applyTemplate(templateURI, data, writer, properties);
    }

    public static final void applyTemplate(String templateURI,
                                           Map<String, Object> data,
                                           Writer writer,
                                           Properties engineProperties) {
        final VelocityEngine engine = new VelocityEngine();

        engine.init(engineProperties);

        Template template = engine.getTemplate(templateURI);
        VelocityContext context = new VelocityContext(data);

        template.merge(context, writer);
    }

    public static final Document htmlToXhtml(Reader reader, Writer writer) {
        return htmlToXhtml(reader, writer, StandardCharsets.UTF_8.name(),
                           true);
    }

    public static final Document htmlToXhtml(Reader reader, Writer writer,
                                             boolean quiet) {
        return htmlToXhtml(reader, writer, StandardCharsets.UTF_8.name(),
                           quiet);
    }

    public static final Document htmlToXhtml(Reader reader, Writer writer,
                                             String encoding) {
        return htmlToXhtml(reader, writer, encoding, false);
    }

    public static final Document htmlToXhtml(Reader reader, Writer writer,
                                             String encoding, boolean quiet) {
        Tidy tidy = new Tidy();
        tidy.setQuiet(quiet);
        tidy.setInputEncoding(encoding);
        tidy.setOutputEncoding(encoding);
        tidy.setXHTML(true);
        tidy.setMakeClean(true);
        return tidy.parseDOM(reader, writer);
    }

    public static final void xhtmlToXslFO(Reader reader, Writer writer) throws
        TransformerException {
        xhtmlToXslFO(Thread.currentThread().getContextClassLoader().
            getResourceAsStream(XHTML2FOXSL_XSL_FILE), reader, writer);
    }

    public static final void xhtmlToXslFO(InputStream xslStream, Reader reader,
                                          Writer writer) throws
        TransformerConfigurationException, TransformerException {

        Transformer transformer = TRANSFORMER_FACTORY.newTransformer(
                    new StreamSource(xslStream));

        transformer.
            transform(new StreamSource(reader), new StreamResult(writer));
    }

    public static final void xslFOtoPDF(FopFactory fopFactory, Reader reader,
                                        OutputStream output)
        throws FOPException, TransformerException, URISyntaxException {

        FOUserAgent userAgent = fopFactory.newFOUserAgent();

        Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, userAgent, output);

        Transformer transformer = PDF_TRANSFORMER_FACTORY.newTransformer();

        Source src = new StreamSource(reader);

        Result res = new SAXResult(fop.getDefaultHandler());

        transformer.transform(src, res);
    }
}
