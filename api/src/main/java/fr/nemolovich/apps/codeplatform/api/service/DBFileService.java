package fr.nemolovich.apps.codeplatform.api.service;

import fr.nemolovich.apps.codeplatform.api.model.CodeHelper;
import fr.nemolovich.apps.codeplatform.api.model.CodeStarter;
import fr.nemolovich.apps.codeplatform.api.model.TestCase;
import fr.nemolovich.apps.codeplatform.api.repository.CodeHelperRepository;
import fr.nemolovich.apps.codeplatform.api.repository.CodeStarterRepository;
import fr.nemolovich.apps.codeplatform.api.repository.TestCaseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Service class for managing {@link TestCase}, {@link CodeStarter} and
 * {@link CodeHelper}.
 */
@Service
public class DBFileService {

    private static final Logger LOGGER = LoggerFactory.getLogger(
                                DBFileService.class);

    private final TestCaseRepository testCaseRepository;
    private final CodeHelperRepository codeHelperRepository;
    private final CodeStarterRepository codeStarterRepository;

    public DBFileService(TestCaseRepository testCaseRepository,
                         CodeHelperRepository codeHelperRepository,
                         CodeStarterRepository codeStarterRepository) {
        this.testCaseRepository = testCaseRepository;
        this.codeHelperRepository = codeHelperRepository;
        this.codeStarterRepository = codeStarterRepository;
    }

    public TestCase getTestCaseById(String testCaseId) {
        LOGGER.debug("GetTestCase By Id: {}", testCaseId);
        return testCaseRepository.findOne(testCaseId);
    }

    public TestCase createTestCase(TestCase testCase) {
        LOGGER.debug("Create TestCase:[{}]", testCase.toString());
        TestCase testCaseSaved = testCaseRepository.save(testCase);
        LOGGER.debug("Created testCase: {}", testCaseSaved);
        return testCaseSaved;
    }

    public TestCase updateTestCase(TestCase testCase) {
        LOGGER.debug("Update TestCase:[{}]", testCase.toString());
        TestCase testCaseUpdated = testCaseRepository.save(testCase);
        LOGGER.debug("Updated TestCase: {}", testCaseUpdated);
        return testCaseUpdated;
    }

    public CodeHelper getCodeHelperById(String codeHelperId) {
        LOGGER.debug("GetCodeHelper By Id: {}", codeHelperId);
        return codeHelperRepository.findOne(codeHelperId);
    }

    public CodeHelper createCodeHelper(CodeHelper codeHelper) {
        LOGGER.debug("Create CodeHelper:[{}]", codeHelper.toString());
        CodeHelper codeHelperSaved = codeHelperRepository.save(codeHelper);
        LOGGER.debug("Created CodeHelper: {}", codeHelperSaved);
        return codeHelperSaved;
    }

    public CodeHelper updateCodeHelper(CodeHelper codeHelper) {
        LOGGER.debug("Update CodeHelper:[{}]", codeHelper.toString());
        CodeHelper codeHelperUpdated = codeHelperRepository.save(codeHelper);
        LOGGER.debug("Updated CodeHelper: {}", codeHelperUpdated);
        return codeHelperUpdated;
    }

    public CodeStarter getCodeStarterById(String codeStarterId) {
        LOGGER.debug("GetCodeStarter By Id: {}", codeStarterId);
        return codeStarterRepository.findOne(codeStarterId);
    }

    public CodeStarter createCodeStarter(CodeStarter codeStarter) {
        LOGGER.debug("Create CodeStarter:[{}]", codeStarter.toString());
        CodeStarter codeStarterSaved = codeStarterRepository.save(codeStarter);
        LOGGER.debug("Created CodeStarter: {}", codeStarterSaved);
        return codeStarterSaved;
    }

    public CodeStarter updateCodeStarter(CodeStarter codeStarter) {
        LOGGER.debug("Update CodeStarter:[{}]", codeStarter.toString());
        CodeStarter codeStarterUpdated = codeStarterRepository.save(codeStarter);
        LOGGER.debug("Updated CodeStarter: {}", codeStarterUpdated);
        return codeStarterUpdated;
    }

}
