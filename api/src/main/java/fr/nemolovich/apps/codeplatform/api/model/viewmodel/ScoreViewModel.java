package fr.nemolovich.apps.codeplatform.api.model.viewmodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

@Validated
public class ScoreViewModel {

    @JsonProperty("score")
    @NotNull
    private Float score;

    public ScoreViewModel() {
    }

    public ScoreViewModel score(Float score) {
        this.score = score;
        return this;
    }

    public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }

}
