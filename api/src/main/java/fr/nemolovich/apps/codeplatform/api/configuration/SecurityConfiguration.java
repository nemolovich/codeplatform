package fr.nemolovich.apps.codeplatform.api.configuration;

import fr.nemolovich.apps.codeplatform.api.security.Roles;
import fr.nemolovich.apps.codeplatform.api.security.jwt.JWTConfigurer;
import fr.nemolovich.apps.codeplatform.api.security.jwt.TokenProvider;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.data.repository.query.SecurityEvaluationContextExtension;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    private final UserDetailsService userDetailsService;

    private final TokenProvider tokenProvider;

    private final Environment environment;

    public SecurityConfiguration(
        AuthenticationManagerBuilder authenticationManagerBuilder,
        UserDetailsService userDetailsService,
        TokenProvider tokenProvider, Environment environment) {

        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.userDetailsService = userDetailsService;
        this.tokenProvider = tokenProvider;
        this.environment = environment;
    }

    @PostConstruct
    public void init() {
        try {
            this.authenticationManagerBuilder.userDetailsService(
                this.userDetailsService).passwordEncoder(passwordEncoder());
        } catch (Exception e) {
            throw new BeanInitializationException(
                "Security configuration failed", e);
        }
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
            .antMatchers(HttpMethod.OPTIONS, "/**")
            //            .antMatchers("/app/**/*.{js,html}")//add front
            .antMatchers("/webjars/**")
            .antMatchers("/configuration/**")
            .antMatchers("/swagger-resources/**")
            .antMatchers("/api-docs")
            .antMatchers("/swagger-ui.html**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        if (this.environment.getProperty("server.ssl.enabled", Boolean.class)) {
            http
                .requiresChannel()
                .anyRequest()
                .requiresSecure()
                .and()
                .portMapper()
                .http(this.environment.getProperty("server.http.port",
                                                   Integer.class))
                .mapsTo(this.environment.getProperty("server.port",
                                                     Integer.class));
        }
        http
            .exceptionHandling()
            .authenticationEntryPoint(new BasicAuthenticationEntryPoint())
            .and()
            .csrf()
            .disable()
            .headers()
            .frameOptions()
            .disable()
            .and()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .authorizeRequests()
            .antMatchers("/api/admin/**").hasAuthority(Roles.ADMIN.toString())
            .antMatchers("/api/recruitment/**").hasAnyAuthority(
            Roles.ADMIN.toString(), Roles.RECRUITER.toString())
            .antMatchers("/api/account/**").authenticated()
            .antMatchers("/api/submission").authenticated()
            .antMatchers("/api/testcode").authenticated()
            .antMatchers("/api/updateTestTimeAsync/**").authenticated()
            .antMatchers("/api/**").permitAll()
            .and()
            .apply(securityConfigurerAdapter());
    }

    private JWTConfigurer securityConfigurerAdapter() throws Exception {
        return new JWTConfigurer(this.tokenProvider);
    }

    @Bean
    public SecurityEvaluationContextExtension
        securityEvaluationContextExtension() {
        return new SecurityEvaluationContextExtension();
    }
}
