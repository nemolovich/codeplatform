package fr.nemolovich.apps.codeplatform.api.webapi;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.Set;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Exercice API
 */
@Api(value = "Utils API", description =
                          "The API to access some utility Web Services",
     tags = {"utils"})
public interface UtilsApi {

    @ApiOperation(value =
                  "Return the configuration for the front side as JSON",
                  response = Object.class, tags = {"utils",},
                  nickname = "getFrontConfig")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The configuration",
                     response = Set.class, responseContainer = "Set"),
        @ApiResponse(code = 204, message = "No Content"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/front-config",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    ResponseEntity<Object> getFrontConfig(
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

}
