package fr.nemolovich.apps.codeplatform.api.service;

import fr.nemolovich.apps.codeplatform.api.model.Challenge;
import fr.nemolovich.apps.codeplatform.api.model.Exercice;
import fr.nemolovich.apps.codeplatform.api.model.Result;
import fr.nemolovich.apps.codeplatform.api.model.Team;
import fr.nemolovich.apps.codeplatform.api.model.User;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.ResultViewModel;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.TeamViewModel;
import fr.nemolovich.apps.codeplatform.api.repository.ChallengeRepository;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Service class for managing users.
 */
@Service
public class ChallengeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(
                                ChallengeService.class);

    // my repository
    private final ChallengeRepository challengeRepository;

    // other services
    private final UserService userService;
    private final TeamService teamService;
    private final ExerciceService exerciceService;

    public ChallengeService(UserService userRepository,
                            ChallengeRepository challengeRepository,
                            TeamService teamService,
                            ExerciceService exerciceService) {
        this.userService = userRepository;
        this.challengeRepository = challengeRepository;
        this.teamService = teamService;
        this.exerciceService = exerciceService;
    }

    public Challenge getChallengeById(String id) {
        return challengeRepository.findOne(id);
    }

    public Challenge deleteExercicesInChallenge(String challengeId,
                                                String exerciceId) {
        Challenge challenge = challengeRepository.findOne(challengeId);
        Exercice exercice = exerciceService.getExerciceById(exerciceId);
        if (challenge != null && exercice != null && challenge.getExercices().
            remove(exercice)) {
            return challengeRepository.save(challenge);
        } else {
            LOGGER.error(
                "challenge is null, verify challengeId Or if the team is a team enrolled in the challenge");
            return null;
        }
    }

    public Challenge deleteTeamInChallenge(String challengeId, String teamId) {
        Challenge challenge = challengeRepository.findOne(challengeId);
        Team team = teamService.getTeamById(teamId);
        if (challenge != null && team != null && challenge.getTeams().remove(
            team)) {
            return challengeRepository.save(challenge);
        } else {
            LOGGER.error(
                "challenge is null, verify challengeId Or if the team is a team enrolled in the challenge");
            return null;
        }
    }

    public Set<TeamViewModel> getTeamsRegisteredInChallenge(
        String challengeId) {
        Challenge challenge = challengeRepository.findOne(challengeId);
        if (challenge != null) {
            return challenge.getTeams().stream()
                .filter(t -> t != null)
                .map(TeamViewModel::new)
                .collect(Collectors.toSet());
        } else {
            LOGGER.error("challenge is null, verify challengeId");
            return new HashSet<>();
        }
    }

    public Team getTeamInChallengeByName(String challengeId, String teamName) {
        Challenge challenge = challengeRepository.findOne(challengeId);
        Team team = teamService.getTeamByName(teamName);
        if (challenge != null && team != null && challenge.getTeams()
            .contains(team)) {
            return team;
        } else {
            LOGGER.error("challenge or team is null, verify parameters");
            return null;
        }
    }

    public Team getTeamInChallengeById(String challengeId, String teamId) {
        Challenge challenge = challengeRepository.findOne(challengeId);
        return getTeamInChallenge(challenge, teamId);
    }

    public Team getTeamInChallenge(Challenge challenge, String teamId) {
        Team team = teamService.getTeamById(teamId);
        if (challenge != null && team != null && challenge.getTeams()
            .contains(team)) {
            return team;
        } else {
            LOGGER.error("challenge is null, verify challengeId");
            return null;
        }
    }

    public Exercice getExerciceInChallengeById(String challengeId,
                                               String exerciceId) {
        Challenge challenge = challengeRepository.findOne(challengeId);
        Exercice exercice = exerciceService.getExerciceById(exerciceId);
        if (challenge != null && exercice != null && challenge.getExercices()
            .contains(exercice)) {
            return exercice;
        } else {
            LOGGER.error("challenge is null, verify challengeId");
            return null;
        }
    }

    public Challenge joinTeamInChallenge(String challengeId, String teamId,
                                         String username) {
        Team team = teamService.getTeamById(teamId);
        User user = userService.getUserByLogin(username);
        Challenge challenge = challengeRepository.findOne(challengeId);
        // Only a user of a team can ask to join the challenge
        if (teamService.isMemberOfTeam(team, user) &&
             !isTeamOfChallenge(challenge, team) &&
             challenge.getTeams().add(team)) {
            challenge = challengeRepository.save(challenge);
            return challenge;
        } else {
            LOGGER.error(
                "The team is already a team a this channel or verify if the user username:[{}] is member of the team id:[{}]",
                username, teamId);
            return null;
        }
    }

    public Challenge addExerciceInChallenge(Challenge challenge,
                                            Exercice exercice) {
        // Here it's different, it's from creation of an exercice
        // We need to verify if the exercice is the exercic of a challenge manually
        if (challenge != null && exercice != null &&
             !isExerciceOfChallenge(challenge, exercice)) {
            return challenge.getExercices().add(exercice) ?
                 challengeRepository.save(challenge) : null;
        } else {
            LOGGER.error(
                "challenge is null, verify challengeId or the exercice is null and it can't be add");
            return null;
        }
    }

    public Challenge addTeamInChallenge(Challenge challenge, Team team) {
        // Here it's different, it's from creation of a team
        // We need to verify if the team is the team of a challenge manually
        if (challenge != null && team != null) {
            if (!isTeamOfChallenge(challenge, team)) {
                return (challenge.getTeams().add(team) ?
                         challengeRepository.save(challenge) : null);
            } else {
                return challenge;
            }
        } else {
            LOGGER.error("challenge is null, verify challengeId");
            return null;
        }
    }

    public Challenge createChallenge(Challenge challenge) {
        LOGGER.debug("Create Challenge:[{}]", challenge.toString());
        Challenge challengeSaved = challengeRepository.save(challenge);
        LOGGER.debug("Created challenge: {}", challengeSaved);
        return challengeSaved;
    }

    public Challenge updateChallenge(Challenge challenge) {
        LOGGER.debug("Update Challenge:[{}]", challenge.toString());
        Challenge challengeUpdated = challengeRepository.save(challenge);
        LOGGER.debug("Updated challenge: {}", challengeUpdated);
        return challengeUpdated;
    }

    public Set<Challenge> getAllChallenges() {
        return new HashSet<>(challengeRepository.findAll());
    }

    public Set<Result> getAllResultsByChallengeId(String challengeId) {
        Challenge challenge = challengeRepository.findOne(challengeId);
        return challenge != null ? challenge.getResults() : null;
    }

    public Set<ResultViewModel> getAllResultViewModelsByChallengeId(
        String challengeId) {
        Set<Result> results = getAllResultsByChallengeId(challengeId);
        return results != null ? results.stream().map(ResultViewModel::new)
               .collect(Collectors.toSet()) :
             new HashSet<>();
    }

    public Set<Result> getAllResultsByChallengeIdAndTeamId(String challengeId,
                                                           String teamId) {
        Challenge challenge = challengeRepository.findOne(challengeId);
        return challenge != null ? challenge.getResults().stream().filter(
               r -> r.getTeam().getId().equals(teamId)).collect(Collectors.
                   toSet()) :
             null;
    }

    public Set<ResultViewModel> getAllResultViewModelsByChallengeIdAndTeamId(
        String challengeId, String teamId) {
        Set<Result> results = getAllResultsByChallengeIdAndTeamId(challengeId,
                                                                  teamId);
        return results != null ? results.stream().map(ResultViewModel::new)
               .collect(Collectors.toSet()) :
             new HashSet<>();
    }

    public Challenge deleteChallenge(String id) {
        Challenge challenge = challengeRepository.findOne(id);
        challengeRepository.delete(challenge);
        LOGGER.debug("Deleted challenge: {}", challenge);
        return challenge;
    }

    public Boolean isTeamOfChallenge(Challenge challenge, Team team) {
        return team != null && challenge != null &&
             challenge.getTeams().stream().filter(
                   team1 ->
                   team1.getName().equalsIgnoreCase(team.getName()) ||
                    team1.getId().equalsIgnoreCase(team.getId()))
               .count() > 0;
    }

    public Boolean hasTeamNameAlreadyInChallenge(Challenge challenge,
                                                 String name) {
        return name != null && challenge != null &&
             challenge.getTeams().stream().filter(
                   team1 -> team1.getName().equalsIgnoreCase(name))
               .count() > 0;
    }

    public Boolean isExerciceOfChallenge(Challenge challenge,
                                         Exercice exercice) {
        return exercice != null && challenge != null &&
             challenge.getExercices().stream().filter(
                   exercice1 -> exercice1.getId().equalsIgnoreCase(
                       exercice.getId()) || exercice1.getId().equalsIgnoreCase(
                                      exercice.getId())).count() > 0;
    }

    public Boolean hasExerciceTitleAlreadyInChallenge(Challenge challenge,
                                                      String title) {
        return title != null && challenge != null &&
             challenge.getExercices().stream().filter(
                   exercice1 -> exercice1.getTitle().equalsIgnoreCase(title))
               .count() > 0;
    }

}
