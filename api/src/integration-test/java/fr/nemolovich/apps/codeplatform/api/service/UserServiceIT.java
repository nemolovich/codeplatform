package fr.nemolovich.apps.codeplatform.api.service;

import fr.nemolovich.apps.codeplatform.api.Swagger2SpringBoot;
import fr.nemolovich.apps.codeplatform.api.model.Role;
import fr.nemolovich.apps.codeplatform.api.model.User;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.UserViewModel;
import fr.nemolovich.apps.codeplatform.api.repository.UserRepository;
import fr.nemolovich.apps.codeplatform.api.security.Roles;
import fr.nemolovich.apps.codeplatform.api.webapi.exception.ApiException;
import fr.nemolovich.apps.codeplatform.api.webapi.exception.NotFoundException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for the UserResource REST controller.
 *
 * @see UserService
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Swagger2SpringBoot.class)
public class UserServiceIT {
    private static final Logger LOGGER = LoggerFactory.getLogger(
            UserServiceIT.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;


    private User myTestUser;

    @Before
    public void createMyTestUser(){
        myTestUser = new User();
        myTestUser.setPassword("password");
        myTestUser.setEmail("john-doe@john.com");
        myTestUser.setLogin("john_doe_dalton_1234567890");
        myTestUser.setActivated(false);
        myTestUser.setCreatedDate(Instant.now().minus(40, ChronoUnit.SECONDS));
        myTestUser.setFirstName("John");
        myTestUser.setLastName("Doe");
        LOGGER.info("Create an user object myTestUser -> {}", myTestUser.toString());
    }

    @After
    public void cleanDatabase() throws ApiException {
        SecurityContextHolder.clearContext();
        try{
            userService.deleteUser(myTestUser.getLogin());
        }catch(NotFoundException ne){
            LOGGER.info("User not present in database -> continue");
        }
    }

    @Test
    public void testGlobalFindNotActivatedUsersByCreationDateBefore() {
        userService.removeNotActivatedUsers(3);
        Instant now = Instant.now();
        List<User> users = userRepository.findAllByActivatedIsFalseAndCreatedDateBefore(now.minus(3, ChronoUnit.DAYS));
        assertThat(users).isEmpty();
    }


    @Test
    public void testSpecificFindNotActivatedUsersByCreationDateBefore() throws ApiException {
        User user = userService.createUser(myTestUser);
        user.setActivated(false);
        user.setCreatedDate(Instant.now().minus(3, ChronoUnit.DAYS));
        userRepository.save(user);
        List<User> users = userRepository.findAllByActivatedIsFalseAndCreatedDateBefore(Instant.now().minus(2, ChronoUnit.DAYS));
        assertThat(users).isNotEmpty();
        assertThat(users.get(0)).isEqualTo(user);
    }

    @Test
    public void assertThatAllUsersAreGet() {
        final List<UserViewModel> allManagedUsers = userService.getAllUserViewModels();
        assertThat(allManagedUsers.size()).isGreaterThan(0);
    }

    @Test
    public void testRemoveNotActivatedUsers() {
        User user = userService.createUser(myTestUser);
        user.setCreatedDate(Instant.now().minus(30, ChronoUnit.DAYS));
        userRepository.save(user);
        assertThat(userRepository.findOneByLogin("johndoe").isPresent());
        userService.removeNotActivatedUsers(29);
        assertThat(!userRepository.findOneByLogin("johndoe").isPresent());
    }

    @Test
    public void activateRegistration() throws Exception {
        User user = userService.createUser(myTestUser);
        user.setActivated(false);
        userRepository.save(user);
        assertThat(userService.getUserByLogin(myTestUser.getLogin()).getActivated()).isFalse();
        userService.activateRegistration(myTestUser.getLogin());
        assertThat(userService.getUserByLogin(myTestUser.getLogin()).getActivated()).isTrue();

    }

    @Test
    public void createUser() throws Exception {
        User user = userService.createUser(myTestUser);
        assertThat(userService.getFullUser(user.getId())).isNotNull();
    }

    @Test
    public void updateUser() throws Exception {
        User user = userService.createUser(myTestUser);
        String keepLastName = user.getLastName();
        assertThat(userService.getUserViewModelByLogin(user.getLogin())).isNotNull();
        user.setLastName("NewLastName");
        userService.updateUser(user);
        assertThat(userService.getUserViewModelByLogin(user.getLogin()).getLastName()).isNotEqualTo(keepLastName);
        assertThat(userService.getUserViewModelByLogin(user.getLogin()).getLastName()).isEqualTo("NewLastName");
        // When we used userViewModel it's depends on Security Context
        // We have to set the security context with the current user
        SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(new UsernamePasswordAuthenticationToken(myTestUser.getLogin(), myTestUser.getPassword()));
        SecurityContextHolder.setContext(securityContext);
        // TEST
        UserViewModel userViewModel=new UserViewModel(user);
        String keepFirstName = userViewModel.getFirstName();
        userViewModel.setFirstName("NewFirstName");
        userService.updateUser(userViewModel);
        assertThat(userService.getUserViewModelByLogin(user.getLogin()).getFirstName()).isNotEqualTo(keepFirstName);
        assertThat(userService.getUserViewModelByLogin(user.getLogin()).getFirstName()).isEqualTo("NewFirstName");
        String keepMail = userViewModel.getEmail();
        userViewModel.setEmail("test@hottest.com");
        userService.updateUser(userViewModel);
        assertThat(userService.getUserViewModelByLogin(user.getLogin()).getEmail()).isNotEqualTo(keepMail);
        assertThat(userService.getUserViewModelByLogin(user.getLogin()).getEmail()).isEqualTo("test@hottest.com");


    }

    @Test
    public void deleteUser() throws Exception {
        createUser();
        userService.deleteUser(myTestUser.getLogin());
        assertThat(userService.getUserByLogin(myTestUser.getLogin())).isNull();
    }
    // @TODO verify if delete yourself works
    // @Test ...

    @Test
    public void changePassword() throws Exception {
        createUser();
        SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(new UsernamePasswordAuthenticationToken(myTestUser.getLogin(), myTestUser.getPassword()));
        SecurityContextHolder.setContext(securityContext);
        userService.changePassword("TestNewPassword");
        User user = userService.getUserByLogin(myTestUser.getLogin());
        assertThat(passwordEncoder.matches("TestNewPassword",user.getPassword())).isTrue();
    }

    @Test
    public void getCurrentUser() throws Exception {
        User user = userService.createUser(myTestUser);
        SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(new UsernamePasswordAuthenticationToken(user.getLogin(), user.getPassword()));
        SecurityContextHolder.setContext(securityContext);
        assertThat(userService.getCurrentUser().isPresent()).isTrue();
        assertThat(userService.getCurrentUser().get()).isEqualTo(user);
    }

    @Test
    public void getUserViewModelByLogin() throws Exception {
        createUser();
        UserViewModel userViewModel = new UserViewModel(userService.getUserByLogin(myTestUser.getLogin()));
        assertThat(userService.getUserViewModelByLogin(myTestUser.getLogin())).isEqualTo(userViewModel);
    }

    @Test
    public void getUserByLogin() throws Exception {
        User user = userService.createUser(myTestUser);
        assertThat(userService.getUserByLogin(myTestUser.getLogin())).isEqualTo(user);
    }

    @Test
    public void getFullUser() throws Exception {
        User user = userService.createUser(myTestUser);
        assertThat(userService.getFullUser(user.getId())).isEqualTo(user);
    }

    @Test
    public void getRoles() throws Exception {
        createUser();
        // Verify that role USER is created by default
        assertThat(userService.getUserByLogin(myTestUser.getLogin()).getRoles()).isNotEmpty();
        assertThat(userService.getUserByLogin(myTestUser.getLogin()).getRoles()).contains(new Role(Roles.USER.toString()));
        // Verify the correct function
        assertThat(userService.getRoles(myTestUser.getLogin())).contains(new Role(Roles.USER.toString()));
    }
}
