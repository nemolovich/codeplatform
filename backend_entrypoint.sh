#!/bin/bash

export DOCKER_HOST="${DOCKER_HOST:-tcp://$(hostname):2376}"
export DOCKER_TLS_VERIFY=1
export DOCKER_CERT_PATH="${BASE_PATH}/certs"

if [ -z "$(ls ${BASE_PATH}/manager/docker_images 2>/dev/null)" ] ; then
  cp -rp /src/docker_images/* ${BASE_PATH}/manager/docker_images/
fi

cd api

JAR_FILE="$(ls target/shaded-*.jar 2>/dev/null)"

if [ -z "${JAR_FILE}" ] ; then
  echo "ERROR: Cannot find jar file"
  exit 1
else
java -Dmanager.config.path="${BASE_PATH}/configs/manager/manager.properties" \
  -Dlanguages.config.path="${BASE_PATH}/configs/manager/languages.yml" \
  -Dfront.config.path="${BASE_PATH}/configs/api/front-config.yaml" \
  -Dspring.config.location="file:${BASE_PATH}/configs/api/application.yaml" \
  -jar "${JAR_FILE}"
fi

exit 0
